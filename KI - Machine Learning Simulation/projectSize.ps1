$count = 0
$files = Get-ChildItem -Path ".\KI - Machine Learning Simulation" -Include @("*.cpp") -Recurse
for ($i=0; $i -lt $files.Count; $i++) {
	$count = $count + (Get-Content $files[$i]).Length
}
$files = Get-ChildItem -Path ".\KI - Machine Learning Simulation" -Include @("*.h") -Recurse
for ($i=0; $i -lt $files.Count; $i++) {
	$count = $count + (Get-Content $files[$i]).Length
}
$files = Get-ChildItem -Path ".\KI - Machine Learning Simulation" -Include @("*.hlsl") -Recurse
for ($i=0; $i -lt $files.Count; $i++) {
	$count = $count + (Get-Content $files[$i]).Length
}
echo $count
struct VS_IN {
	int2 pos : POSITION0;
	uint2 size : POSITION1;
	float3 color : COLOR0;
};

struct VS_OUT {
	float4 pos : SV_POSITION;
	uint2 size : POSITION0;
	float3 color : COLOR0;
};

VS_OUT main(VS_IN vsIn) {
	VS_OUT vsOut = (VS_OUT)0;

	vsOut.pos = float4(vsIn.pos, 0.0f, 1.0f);
	vsOut.size = vsIn.size;
	vsOut.color = vsIn.color;

	return vsOut;
}
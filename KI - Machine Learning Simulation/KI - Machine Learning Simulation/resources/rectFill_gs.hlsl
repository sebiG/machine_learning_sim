struct GS_IN {
	float4 pos : SV_POSITION;
	uint2 size : POSITION0;
	float3 color : COLOR0;
};

struct GS_OUT {
	float4 pos : SV_POSITION;
	float3 color : COLOR0;
};

cbuffer Transform : register(b0) {
	float4x4 pixelTransform; // Abbildungsmatrix, die pro Pixel eine Lšngeneinheit zuweist
}

[maxvertexcount(6)]
void main(point GS_IN input[1], inout TriangleStream< GS_OUT > output) {
	// Erstelle Rechteck

	GS_OUT gsOut;
	gsOut.color = input[0].color;

	float rectWidth = (float)input[0].size.x;
	float rectHeight = (float)input[0].size.y;

	gsOut.pos = mul(pixelTransform, input[0].pos + float4(0.0f, 0.0f, 0.0f, 0.0f));
	output.Append(gsOut);
	gsOut.pos = mul(pixelTransform, input[0].pos + float4(0.0f, rectHeight, 0.0f, 0.0f));
	output.Append(gsOut);
	gsOut.pos = mul(pixelTransform, input[0].pos + float4(rectWidth, rectHeight, 0.0f, 0.0f));
	output.Append(gsOut);
	output.RestartStrip();

	gsOut.pos = mul(pixelTransform, input[0].pos + float4(0.0f, 0.0f, 0.0f, 0.0f));
	output.Append(gsOut);
	gsOut.pos = mul(pixelTransform, input[0].pos + float4(rectWidth, rectHeight, 0.0f, 0.0f));
	output.Append(gsOut);
	gsOut.pos = mul(pixelTransform, input[0].pos + float4(rectWidth, 0.0f, 0.0f, 0.0f));
	output.Append(gsOut);
	output.RestartStrip();
}
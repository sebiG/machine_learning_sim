struct PS_IN {
	float4 pos : SV_POSITION;
	float2 texCoord : TEXCOORD0;
};

Texture2D tex : register(t0);

SamplerState pixelSampler : register(s0);

float4 main(PS_IN psIn) : SV_TARGET {
	return tex.Sample(pixelSampler, psIn.texCoord);
}
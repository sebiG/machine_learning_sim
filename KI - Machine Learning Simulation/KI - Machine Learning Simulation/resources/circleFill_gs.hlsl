struct GS_IN {
	float4 pos : SV_POSITION;
	uint radius : COLOR0;
	uint2 sectorAngles : COLOR1;
	float3 color : COLOR2;
};

struct GS_OUT {
	float4 pos : SV_POSITION;
	float3 color : COLOR0;
};

cbuffer Transform : register(b0) {
	float4x4 pixelTransform; // Abbildungsmatrix, die pro Pixel eine Lšngeneinheit zuweist
}

static const uint verticesPerCircle = 40;
static const uint degreesPerVertex = 360 / verticesPerCircle;

[maxvertexcount((verticesPerCircle + 6) * 3)]
void main(point GS_IN input[1], inout TriangleStream< GS_OUT > output) {
	if(input[0].sectorAngles.x > 360) {
		input[0].sectorAngles.x -= 360;
	}
	if(input[0].sectorAngles.y > 360) {
		input[0].sectorAngles.y -= 360;
	}

	uint firstRegularVertex = ceil((float)input[0].sectorAngles.x / (float)degreesPerVertex);
	uint lastRegularVertex = floor((float)(input[0].sectorAngles.x + input[0].sectorAngles.y) / (float)degreesPerVertex);
	int regularVertexCount = lastRegularVertex - firstRegularVertex;

	GS_OUT gsOut;
	gsOut.color = input[0].color;

	if(input[0].sectorAngles.y != 0) {
		// Winkel nicht null

		// Beginne Kreissektor
		if(regularVertexCount <= 0) {
			// Verbinde direkt mit Endpunkt
			gsOut.pos = mul(pixelTransform, float4(input[0].pos.x + (float)input[0].radius * cos(radians(input[0].sectorAngles.x + input[0].sectorAngles.y)), input[0].pos.y + (float)input[0].radius * sin(radians(input[0].sectorAngles.x + input[0].sectorAngles.y)), 0.0f, 1.0f));
		} else {
			// Verbinde mit erstem vorgegeben Punkt
			gsOut.pos = mul(pixelTransform, float4(input[0].pos.x + (float)input[0].radius * cos(radians(firstRegularVertex * degreesPerVertex)), input[0].pos.y + (float)input[0].radius * sin(radians(firstRegularVertex * degreesPerVertex)), 0.0f, 1.0f));
		}
		output.Append(gsOut);
		gsOut.pos = mul(pixelTransform, float4(input[0].pos.x + (float)input[0].radius * cos(radians(input[0].sectorAngles.x)), input[0].pos.y + (float)input[0].radius * sin(radians(input[0].sectorAngles.x)), 0.0f, 1.0f));
		output.Append(gsOut);
		gsOut.pos = mul(pixelTransform, input[0].pos);
		output.Append(gsOut);
		output.RestartStrip();

		for(uint i = firstRegularVertex; i < lastRegularVertex; i++) {
			uint angle = i * degreesPerVertex;
			uint nextAngle = (i + 1) * degreesPerVertex;
			if(angle >= 360) {
				angle -= 360;
			}
			if(nextAngle >= 360) {
				nextAngle -= 360;
			}
			gsOut.pos = mul(pixelTransform, float4(input[0].pos.x + (float)input[0].radius * cos(radians(nextAngle)), input[0].pos.y + (float)input[0].radius * sin(radians(nextAngle)), 0.0f, 1.0f));
			output.Append(gsOut);
			gsOut.pos = mul(pixelTransform, float4(input[0].pos.x + (float)input[0].radius * cos(radians(angle)), input[0].pos.y + (float)input[0].radius * sin(radians(angle)), 0.0f, 1.0f));
			output.Append(gsOut);
			// Schliesse Dreieck in der Mitte
			gsOut.pos = mul(pixelTransform, input[0].pos);
			output.Append(gsOut);
			output.RestartStrip();
		}

		if(regularVertexCount > 0) {
			// Beende Kreissektor
			uint angle = lastRegularVertex * degreesPerVertex;
			uint lastAngle = input[0].sectorAngles.x + input[0].sectorAngles.y;
			if(angle >= 360) {
				angle -= 360;
			}
			if(lastAngle >= 360) {
				lastAngle -= 360;
			}
			gsOut.pos = mul(pixelTransform, float4(input[0].pos.x + (float)input[0].radius * cos(radians(lastAngle)), input[0].pos.y + (float)input[0].radius * sin(radians(lastAngle)), 0.0f, 1.0f));
			output.Append(gsOut);
			gsOut.pos = mul(pixelTransform, float4(input[0].pos.x + (float)input[0].radius * cos(radians(angle)), input[0].pos.y + (float)input[0].radius * sin(radians(angle)), 0.0f, 1.0f));
			output.Append(gsOut);
			gsOut.pos = mul(pixelTransform, input[0].pos);
			output.Append(gsOut);
			output.RestartStrip();
		}

	} else {
		// Ansonsten zeichne einen normalen Kreis

		for(uint i = 0; i < verticesPerCircle; i++) {
			uint angle = i * degreesPerVertex;
			uint nextAngle = (i + 1) * degreesPerVertex;
			if(angle >= 360) {
				angle -= 360;
			}
			if(nextAngle >= 360) {
				nextAngle -= 360;
			}
			gsOut.pos = mul(pixelTransform, float4(input[0].pos.x + (float)input[0].radius * cos(radians(nextAngle)), input[0].pos.y + (float)input[0].radius * sin(radians(nextAngle)), 0.0f, 1.0f));
			output.Append(gsOut);
			gsOut.pos = mul(pixelTransform, float4(input[0].pos.x + (float)input[0].radius * cos(radians(angle)), input[0].pos.y + (float)input[0].radius * sin(radians(angle)), 0.0f, 1.0f));
			output.Append(gsOut);
			// Schliesse Dreieck in der Mitte
			gsOut.pos = mul(pixelTransform, input[0].pos);
			output.Append(gsOut);
			output.RestartStrip();
		}
	}
}
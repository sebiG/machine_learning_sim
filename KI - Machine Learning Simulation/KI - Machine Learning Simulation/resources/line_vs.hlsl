struct VS_IN {
	int2 pos1 : POSITION0;
	int2 pos2 : POSITION1;
	float3 color : COLOR0;
};

struct VS_OUT {
	float4 pos : SV_POSITION; // kombiniert pos1 und pos2
	float3 color : COLOR0;
};

VS_OUT main(VS_IN vsIn) {
	VS_OUT vsOut = (VS_OUT)0;
	vsOut.pos = float4((float)vsIn.pos1.x, (float)vsIn.pos1.y, (float)vsIn.pos2.x, (float)vsIn.pos2.y);
	vsOut.color = vsIn.color;

	return vsOut;
}
struct PS_IN {
	float4 pos : SV_POSITION;
	float3 color : COLOR0;
};

float4 main(PS_IN psIn) : SV_TARGET {
	return float4(psIn.color, 1.0f);
}
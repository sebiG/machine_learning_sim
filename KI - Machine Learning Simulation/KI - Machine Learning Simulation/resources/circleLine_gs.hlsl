struct GS_IN {
	float4 pos : SV_POSITION;
	uint radius : COLOR0;
	uint2 sectorAngles : COLOR1;
	float3 color : COLOR2;
};

struct GS_OUT {
	float4 pos : SV_POSITION;
	float3 color : COLOR0;
};

cbuffer Transform : register(b0) {
	float4x4 pixelTransform; // Abbildungsmatrix, die pro Pixel eine Lšngeneinheit zuweist
}

static const uint verticesPerCircle = 40;
static const uint degreesPerVertex = 360 / verticesPerCircle;

[maxvertexcount(verticesPerCircle + 6)]
void main(point GS_IN input[1], inout LineStream< GS_OUT > output) {
	if(input[0].sectorAngles.x > 360) {
		input[0].sectorAngles.x -= 360;
	}
	if(input[0].sectorAngles.y > 360) {
		input[0].sectorAngles.y -= 360;
	}

	uint firstRegularVertex = ceil((float)input[0].sectorAngles.x / (float)degreesPerVertex);
	uint lastRegularVertex = floor((float)(input[0].sectorAngles.x + input[0].sectorAngles.y) / (float)degreesPerVertex);
	uint regularVertexCount = lastRegularVertex - firstRegularVertex;

	GS_OUT gsOut;
	gsOut.color = input[0].color;

	if(input[0].sectorAngles.y != 0) {
		// Wenn winkel nicht sind, starte in der Mitte
		gsOut.pos = mul(pixelTransform, input[0].pos);
		output.Append(gsOut);

		// Beginne Kreisbogen
		gsOut.pos = mul(pixelTransform, float4(input[0].pos.x + (float)input[0].radius * cos(radians(input[0].sectorAngles.x)), input[0].pos.y + (float)input[0].radius * sin(radians(input[0].sectorAngles.x)), 0.0f, 1.0f));
		output.Append(gsOut);

		// mache Kreis mit vorgegebenen Punkten
		for(uint i = firstRegularVertex; i < lastRegularVertex + 1; i++) {
			uint angle = i * degreesPerVertex;
			if(angle >= 360) {
				angle -= 360;
			}
			gsOut.pos = mul(pixelTransform, float4(input[0].pos.x + (float)input[0].radius * cos(radians(angle)), input[0].pos.y + (float)input[0].radius * sin(radians(angle)), 0.0f, 1.0f));
			output.Append(gsOut);
		}

		// Beende Kreisbogen
		uint angle = input[0].sectorAngles.x + input[0].sectorAngles.y;
		if(angle >= 360) {
			angle -= 360;
		}
		gsOut.pos = mul(pixelTransform, float4(input[0].pos.x + (float)input[0].radius * cos(radians(angle)), input[0].pos.y + (float)input[0].radius * sin(radians(angle)), 0.0f, 1.0f));
		output.Append(gsOut);

		// Beende mit Punkt in der Mitte
		gsOut.pos = mul(pixelTransform, input[0].pos);
		output.Append(gsOut);
	} else {
		// Ansonsten zeichne einen normalen Kreis
		for(uint i = 0; i < verticesPerCircle; i++) {
			uint angle = i * degreesPerVertex;
			if(angle >= 360) {
				angle -= 360;
			}
			gsOut.pos = mul(pixelTransform, float4(input[0].pos.x + (float)input[0].radius * cos(radians(angle)), input[0].pos.y + (float)input[0].radius * sin(radians(angle)), 0.0f, 1.0f));
			output.Append(gsOut);
		}

		// Kreis schliessen
		gsOut.pos = mul(pixelTransform, float4(input[0].pos.x + (float)input[0].radius, input[0].pos.y, 0.0f, 1.0f));
		output.Append(gsOut);
	}

	output.RestartStrip();
}
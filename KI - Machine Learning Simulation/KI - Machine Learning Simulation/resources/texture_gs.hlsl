#pragma pack_matrix(column_major)

struct GS_IN {
	float4 pos : SV_POSITION;
	uint squareSize : POSITION0;
	uint textureIndex : TEXCOORD0;
};

struct GS_OUT {
	float4 pos : SV_POSITION;
	float2 texCoord : TEXCOORD0;
};

cbuffer Transform : register(b0) {
	float4x4 pixelTransform; // Abbildungsmatrix, die pro Pixel eine Lšngeneinheit zuweist
}

[maxvertexcount(6)]
void main(point GS_IN input[1], inout TriangleStream< GS_OUT > output) {	
	GS_OUT vertices[4];

	float squareSize = (float)input[0].squareSize;
	float2 texCoordBase;
	float texOffsetPerTile = 0.0625f; // 1 Tile = 16 x 16 pixel
	texCoordBase.x = texOffsetPerTile * (float)(input[0].textureIndex % 16);
	texCoordBase.y = texOffsetPerTile * (float)(input[0].textureIndex / 16);

	vertices[0].pos = mul(pixelTransform, input[0].pos + float4(0.0f, 0.0f, 0.0f, 0.0f));
	vertices[0].texCoord = texCoordBase + float2(0.0f, texOffsetPerTile);

	vertices[1].pos = mul(pixelTransform, input[0].pos + float4(0.0f, squareSize, 0.0f, 0.0f));
	vertices[1].texCoord = texCoordBase + float2(0.0f, 0.0f);

	vertices[2].pos = mul(pixelTransform, input[0].pos + float4(squareSize, squareSize, 0.0f, 0.0f));
	vertices[2].texCoord = texCoordBase + float2(texOffsetPerTile, 0.0f);

	vertices[3].pos = mul(pixelTransform, input[0].pos + float4(squareSize, 0.0f, 0.0f, 0.0f));
	vertices[3].texCoord = texCoordBase + float2(texOffsetPerTile, texOffsetPerTile);

	output.Append(vertices[0]);
	output.Append(vertices[1]);
	output.Append(vertices[2]);

	output.RestartStrip();

	output.Append(vertices[0]);
	output.Append(vertices[2]);
	output.Append(vertices[3]);

	output.RestartStrip();
}
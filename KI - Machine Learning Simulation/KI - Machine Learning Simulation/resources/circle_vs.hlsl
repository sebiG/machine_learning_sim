struct VS_IN {
	int2 pos : POSITION0;
	uint radius : COLOR0;
	uint2 sectorAngles : COLOR1;
	float3 color : COLOR2;
};

struct VS_OUT {
	float4 pos : SV_POSITION;
	uint radius : COLOR0;
	uint2 sectorAngles : COLOR1;
	float3 color : COLOR2;
};

VS_OUT main(VS_IN vsIn) {
	VS_OUT vsOut = (VS_OUT)0;
	vsOut.pos = float4(vsIn.pos, 0.0f, 1.0f);
	vsOut.radius = vsIn.radius;
	vsOut.sectorAngles = vsIn.sectorAngles;
	vsOut.color = vsIn.color;

	return vsOut;
}
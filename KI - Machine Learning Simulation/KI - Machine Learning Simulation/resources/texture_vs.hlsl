struct VS_IN {
	int2 squareOffset : POSITION0;
	uint squareSize : POSITION1;
	uint textureIndex : TEXCOORD0;
};

struct VS_OUT {
	float4 pos : SV_POSITION;
	uint squareSize : POSITION0;
	uint textureIndex : TEXCOORD0;
};

VS_OUT main(VS_IN vsIn) {
	VS_OUT vsOut = (VS_OUT)0;
	
	vsOut.pos = float4(vsIn.squareOffset, 0.0f, 1.0f);
	vsOut.squareSize = vsIn.squareSize;
	vsOut.textureIndex = vsIn.textureIndex;

	return vsOut;
}
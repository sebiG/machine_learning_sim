struct GS_IN {
	float4 pos : SV_POSITION; // kombiniert pos1 und pos2
	float3 color : COLOR0;
};

struct GS_OUT {
	float4 pos : SV_POSITION;
	float3 color : COLOR0;
};

cbuffer Transform : register(b0) {
	float4x4 pixelTransform; // Abbildungsmatrix, die pro Pixel eine Lšngeneinheit zuweist
}

[maxvertexcount(2)]
void main(point GS_IN input[1] : SV_POSITION, inout LineStream< GS_OUT > output) {
	GS_OUT o1;
	GS_OUT o2;

	o1.pos = mul(pixelTransform, float4(input[0].pos.xy, 0.0f, 1.0f));
	o1.color = input[0].color;
	output.Append(o1);

	o2.pos = mul(pixelTransform, float4(input[0].pos.zw, 0.0f, 1.0f));
	o2.color = input[0].color;
	output.Append(o2);

	output.RestartStrip();
}
#pragma once

// Verhindere CRT Sicherheitswarnungen
#define _CRT_SECURE_NO_WARNINGS

// Deklariere D2D-Version
#define DIRECTINPUT_VERSION 0x0800

// Verwende mathematische Konstanten
#define _USE_MATH_DEFINES

// Verwende max() und min() nicht
#define NOMINMAX

// Windows-Platform Headerdateien
#include <Windows.h>
#include <d3d11.h>
#include <d2d1.h>
#include <dwrite_3.h>
#include <dinput.h>
#include <wincodec.h>

// C++ standard Headerdateien
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include <codecvt>
#include <cstdio>
#include <ctime>
#include <cmath>
#include <exception>
#include <algorithm>
#include <map>
#include <vector>
#include <list>
#include <queue>

// C++ standard namespace
using namespace std;

// Typendefinition f�r Stringumwandlung
typedef wstring_convert<codecvt_utf8_utf16<wchar_t>> StringConverter;
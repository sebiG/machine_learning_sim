#include <menu/mainMenu.h>

#include <system/log.h>
#include <system/window.h>
#include <system/graphics.h>
#include <system/program.h>
#include <ui/ui.h>
#include <ui/button.h>

using namespace System;
using namespace UI;

namespace Menu {

void MainMenu::Update(double deltaTime) {
	this -> mainMenuUi -> Update(deltaTime);
	this -> mainMenuUi -> Draw();

	Graphics::FlushFilledRects();
	Graphics::FlushRects();
	Graphics::FlushLines();
	Graphics::DrawTextQueue();
}

void MainMenu::Init() {
	Log::WriteImportant("MainMenu: begin Init");
	
	// Erstelle UI-Elemente
	this -> mainMenuUi = new Ui();
	this -> mainMenuUi -> x = Window::GetWidth() / 2 - 200;
	this -> mainMenuUi -> y = Window::GetHeight() / 2 - 100;
	this -> mainMenuUi -> width = 400;
	this -> mainMenuUi -> height = 200;
	this -> mainMenuUi -> text = L"Machine Learning Simulation\nby Sébastien Garmier 2015 Kantonsschule Wohlen";

	this -> simulateButton = new Button();
	this -> simulateButton -> x = 10;
	this -> simulateButton -> y = 70;
	this -> simulateButton -> width = 380;
	this -> simulateButton -> height = 20;
	this -> simulateButton -> text = L"Starte Simulation";
	this -> mainMenuUi -> AddUiElement(this -> simulateButton);

	this -> editButton = new Button();
	this -> editButton -> x = 10;
	this -> editButton -> y = 40;
	this -> editButton -> width = 380;
	this -> editButton -> height = 20;
	this -> editButton -> text = L"Welteditor";
	this -> mainMenuUi -> AddUiElement(this -> editButton);

	this -> quitButton = new Button();
	this -> quitButton -> x = 10;
	this -> quitButton -> y = 10;
	this -> quitButton -> width = 380;
	this -> quitButton -> height = 20;
	this -> quitButton -> text = L"Beenden";
	this -> mainMenuUi -> AddUiElement(this -> quitButton);

	// Abonniere Ereignisse
	Window::Register(this, Window::WINDOW_SIZE);
	this -> simulateButton -> Register(this, Button::BUTTON_ACTIVATE);
	this -> editButton -> Register(this, Button::BUTTON_ACTIVATE);
	this -> quitButton -> Register(this, Button::BUTTON_ACTIVATE);

	Log::WriteImportant("MainMenu: Init terminated");
}

void MainMenu::DeInit() {
	Log::WriteImportant("MainMenu: begin DeInit");

	// Zerstöre UI
	delete this -> quitButton;
	delete this -> editButton;
	delete this -> simulateButton;
	delete this -> mainMenuUi;

	// Beende Ereignisabonnement
	Window::Unregister(this, Window::WINDOW_SIZE);
	// Da Ui-Elemente bereits zerstört wurden, wurde Abonnement bereits beendet

	Log::WriteImportant("MainMenu:: Deinit terminated");
}

void MainMenu::OnEvent(Event const& e) {
	if(e.eventID == Window::WINDOW_SIZE) {
		uint32_t windowWidth = e.param1.Decode<uint32_t>();
		uint32_t windowHeight = e.param2.Decode<uint32_t>();
		this -> mainMenuUi -> x = windowWidth / 2 - 200;
		this -> mainMenuUi -> y = windowHeight / 2 - 100;
	} else if(e.eventID == Button::BUTTON_ACTIVATE) {
		if(e.param1.Decode<EventSender*>() == this -> simulateButton) {
			Program::SwitchProgramMode(Program::ProgramModeEnum::PROGRAM_MODE_SIMULATION);
		} else if(e.param1.Decode<EventSender*>() == this -> editButton) {
			Program::SwitchProgramMode(Program::ProgramModeEnum::PROGRAM_MODE_EDITOR);
		} else if(e.param1.Decode<EventSender*>() == this -> quitButton) {
			Program::Stop();
		}
	}
}

}
#pragma once

#include <stdafx.h>

#include <system/programMode.h>
#include <system/event.h>

namespace UI {
class Ui;
class Button;
}

namespace Menu {

// Klasse f�r Hauptmen�-Programmmodus
class MainMenu : public System::ProgramMode, public System::EventReciever {
public:
	MainMenu() = default;
	virtual ~MainMenu() = default;

	virtual void Update(double deltaTime);
	virtual void Init();
	virtual void DeInit();

	virtual void OnEvent(System::Event const& e);

private:
	UI::Ui* mainMenuUi;
	UI::Button* simulateButton;
	UI::Button* editButton;
	UI::Button* quitButton;
};

}
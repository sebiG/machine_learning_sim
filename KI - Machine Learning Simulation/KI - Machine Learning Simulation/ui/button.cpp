#include <ui/button.h>

#include <system/graphics.h>
#include <system/input.h>
#include <system/log.h>

using namespace System;

namespace UI {

void Button::Update(double deltaTime) {
	// Nicht n�tig
}

void Button::Draw(int32_t offsetX, int32_t offsetY) {
	// Taster besteht nur aus Schriftzug
	if(this -> visible) {
		Graphics::AddTextToQueue(this -> text, offsetX + this -> x + 5, offsetY + this -> y + this -> height - 2, 0.1f, 0.1f, 0.1f);
	}
}

void Button::Activate(bool* done) {
	// Gib Kontrolle sofort zur�ck
	this -> active = false;
	*done = true;

	// Sende eigene Adresse mit Ereignis mit, so dass der Empf�nger verschiedene Absender unterscheiden kann
	this -> SendEvent(Event(BUTTON_ACTIVATE, EventParam::Encode<EventSender*>(this), EventParam()));
}

}
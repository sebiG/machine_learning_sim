#pragma once

#include <stdafx.h>

#include <system/event.h>

namespace UI {

// Klasse für Benutzeroberflächenelemente
class UiElement : public System::EventReciever, public System::EventSender {
public:
	friend class Ui;

	UiElement() : x(0), y(0), width(0), height(0), visible(true), active(false) {};
	virtual ~UiElement() = default; // Standarddestruktor

	// Soll jede Frame aufgerufen werden
	// Ist das Element teil von Ui, übernimmt Ui diese Aufgabe
	virtual void Update(double deltaTime) = 0;
	virtual void Draw(int32_t offsetX, int32_t offsetY) = 0;

	// Wird aufgerufen, wenn Ui dem Element die Kontrolle übergibt, nachdem active auf true gesetzt wurde
	// done wird vom Element auf true gesezt, sobald das Element mit seiner Arbeit fertig ist (entspricht Ui::active)
	virtual void Activate(bool* done) = 0;

	// Soll Name des Elements zurückgeben, damit korrekte Protokolleinträge gemacht werden können
	virtual string GetName() = 0;

	// Eigenschaften
	wstring text;
	int32_t x;
	int32_t y;
	uint32_t width;
	uint32_t height;
	bool visible;
	bool active;
};

// Klasse für Benutzeroberfläche
// Gruppiert Benutzeroberflächenelemente
class Ui {
public:
	Ui() : x(0), y(0), width(0), height(0), visible(true), active(true), selectedItem(0) {}
	~Ui() = default; // Standarddestruktor

	// Soll jede Frame aufgerufen werden
	void Update(double deltaTime);
	void Draw();

	// Fügt Element hinzu
	// (Elemente können später nicht entfernt werden)
	void AddUiElement(UiElement* element);

	// Eigenschaften
	wstring text;
	int32_t x;
	int32_t y;
	uint32_t width;
	uint32_t height;
	bool visible;
	bool active;

private:
	uint32_t selectedItem;
	vector<UiElement*> uiElements;
};

}
#include <ui/ui.h>

#include <system/errors.h>
#include <system/log.h>
#include <system/graphics.h>
#include <system/input.h>

using namespace System;

namespace UI {

// Ui

void Ui::Update(double deltaTime) {
	if(this -> uiElements.size() == 0) {
		return;
	}

	if(this -> active) {
		// ENTER wurde gedr�ckt
		if(Input::GetKeyboardState(DIK_RETURN) == Input::KeyState::DOWN) {
			UiElement* selectedElement = this -> uiElements[this -> selectedItem];
			selectedElement -> active = true;
			this -> active = false;
			selectedElement -> Activate(&this -> active);
		}

		// Anderes Element wurde angew�hlt
		if(Input::GetKeyboardState(DIK_UP) == Input::KeyState::DOWN || Input::GetKeyboardState(DIK_TAB) == Input::KeyState::DOWN) {
			if(this -> selectedItem > 0) {
				this -> selectedItem--;
			} else {
				this -> selectedItem = (uint32_t)this -> uiElements.size() - 1;
			}
		}
		if(Input::GetKeyboardState(DIK_DOWN) == Input::KeyState::DOWN) {
			if(this -> selectedItem < this -> uiElements.size() - 1) {
				this -> selectedItem++;
			} else {
				this -> selectedItem = 0;
			}
		}
	}

	for(uint32_t i = 0; i < this -> uiElements.size(); i++) {
		this -> uiElements[i] -> Update(deltaTime);
	}
}

void Ui::Draw() {
	if(this -> visible) {
		Graphics::Rect rect = { this -> x, this -> y, this -> width, this -> height, 0.9f, 0.9f, 0.9f };
		Graphics::FillRects(&rect, 1);
		rect.color.red = 0.0f;
		rect.color.green = 0.0f;
		rect.color.blue = 0.0f;
		Graphics::DrawRects(&rect, 1);
		if(this -> uiElements.size() > 0 && this -> active) {
			UiElement* selectedElement = this -> uiElements[this -> selectedItem];
			Graphics::Rect selection = { this -> x + selectedElement -> x - 2, this -> y + selectedElement -> y - 2, selectedElement -> width + 4, selectedElement -> height + 4, 0.2f, 0.5f, 0.2f };
			Graphics::DrawRects(&selection, 1);
		}

		Graphics::AddTextToQueue(this -> text, this -> x + 10, this -> y + this -> height - 7, 0.1f, 0.1f, 0.1f);

		for(uint32_t i = 0; i < this -> uiElements.size(); i++) {
			this -> uiElements[i] -> Draw(this -> x, this -> y);
		}
	}
}

void Ui::AddUiElement(UiElement* element) {
	stringstream ss;

	if(element == nullptr) {
		ss.str("");
		ss << "0x" << this << flush;
		throw Error("Error occured in Ui::AddUiElement, element mustn't be null\nobject: " + ss.str());
	}

	this -> uiElements.push_back(element);

	ss.str("");
	ss << "0x" << this << " added " << element -> GetName() << " 0x" << element << flush;
	Log::Write("Ui construction: " + ss.str());
}

}
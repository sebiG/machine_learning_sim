#include <ui/textbox.h>

#include <system/window.h>
#include <system/graphics.h>
#include <system/input.h>

using namespace System;

namespace UI {

Textbox::Textbox() : maxCharacters(0), enteredText(L"") {
	Window::Register(this, Window::WINDOW_CHAR);
}

Textbox::~Textbox() {
	Window::Unregister(this, Window::WINDOW_CHAR);
}

void Textbox::Update(double deltaTime) {
	if(this -> active) {
		// Verhindere, dass Element in der gleichen Frame nach dem Aktivieren sofort wieder deaktiviert wird
		if(this -> firstFrame) {
			this -> firstFrame = false;
			return;
		}
		// Escape wurde gedr�ckt
		if(Input::GetKeyboardState(DIK_ESCAPE) == Input::KeyState::DOWN) {
			this -> active = false;
			*(this -> done) = true;
			this -> enteredText = this -> oldText;
			this -> SendEvent(Event(TEXTBOX_ABORT, EventParam::Encode<EventSender*>(this), EventParam()));
		}

		// Enter wurde gedr�ckt
		if(Input::GetKeyboardState(DIK_RETURN) == Input::KeyState::DOWN) {
			this -> active = false;
			if(this -> done != nullptr) {
				*(this -> done) = true;
			}
			this -> SendEvent(Event(TEXTBOX_CHANGE, EventParam::Encode<EventSender*>(this), EventParam()));
		}
	}
}

void Textbox::Draw(int32_t offsetX, int32_t offsetY) {
	if(this -> visible) {
		Graphics::AddTextToQueue(this -> text, offsetX + this -> x + 5, offsetY + this -> y + this -> height - 2, 0.1f, 0.1f, 0.1f);
		Graphics::AddTextToQueue(this -> enteredText, offsetX + this -> x + 5, offsetY + this -> y + this -> height - 26, 0.1f, 0.1f, 0.1f);
		Graphics::Rect rect = { this -> x + offsetX, this -> y + (int32_t)this -> height - 44 + offsetY, this -> width, 20, 0.6f, 0.6f, 0.6f };
		if(this -> active) {
			rect.color.red = 0.2f;
			rect.color.green = 0.5f;
			rect.color.blue = 0.2f;
		}
		Graphics::FillRects(&rect, 1);
	}
}

void Textbox::Activate(bool* done) {
	this -> oldText = this -> enteredText;
	this -> firstFrame = true;
	this -> done = done;
}

void Textbox::OnEvent(Event const& e) {
	if(this -> active) {
		if(e.eventID == Window::WINDOW_CHAR) {
			char c = e.param1.Decode<char>();
			if(c == '\b') {
				// Letztes Zeichen l�schen
				if(this -> enteredText.size() > 0) {
					this -> enteredText.pop_back();
				}
				return;
			}
			if(c != '\t' && c != '\n' && c != '\f' && c != '\r' && c != '\v') {
				if(this -> enteredText.size() < this -> maxCharacters) {
					this -> enteredText.push_back(c);
				}
				return;
			}
		}
	}
}

}
#include <ui/scrollbar.h>

#include <system/graphics.h>
#include <system/input.h>

using namespace System;

namespace UI {

void Scrollbar::Update(double deltaTime) {
	if(this -> active) {
		// Verhindere, dass Element in der gleichen Frame nach dem Aktivieren sofort wieder deaktiviert wird
		if(this -> firstFrame) {
			this -> firstFrame = false;
			return;
		}
		// Escape wurde gedr�ckt
		if(Input::GetKeyboardState(DIK_ESCAPE) == Input::KeyState::DOWN) {
			this -> active = false;
			*(this -> done) = true;
			this -> scrollPosition = this -> oldScrollPosition;
			this -> SendEvent(Event(SCROLLBAR_ABORT, EventParam::Encode<EventSender*>(this), EventParam()));
		}

		// Enter wurde gedr�ckt
		if(Input::GetKeyboardState(DIK_RETURN) == Input::KeyState::DOWN) {
			this -> active = false;
			if(this -> done != nullptr) {
				*(this -> done) = true;
			}
			this -> SendEvent(Event(SCROLLBAR_CHANGE, EventParam::Encode<EventSender*>(this), EventParam::Encode<double>(this -> scrollPosition)));
		}

		// Pfeil nach rechts wurde gedr�ckt
		if(Input::GetKeyboardState(DIK_RIGHT) == Input::KeyState::DOWN) {
			double stepSize = (this -> steps > 0) ? 1.0 / (double)this -> steps : 1.0;
			this -> scrollPosition += stepSize;
		}

		// Pfeil nach links wurde gedr�ckt
		if(Input::GetKeyboardState(DIK_LEFT) == Input::KeyState::DOWN) {
			double stepSize = (this -> steps > 0) ? 1.0 / (double)this -> steps : 1.0;
			this -> scrollPosition -= stepSize;
		}
	}

	// Korrigiere �berlauf bzw. Underlauf
	if(this -> scrollPosition > 1.0) {
		this -> scrollPosition = 1.0;
	}
	if(this -> scrollPosition < 0.0) {
		this -> scrollPosition = 0.0;
	}
	
	// Aktualisiere scrollValue
	this -> scrollValue = (this -> maxValue > 0) ? (uint32_t)(round((double)this -> maxValue * this -> scrollPosition)) : (uint32_t)round(this -> scrollPosition);
}

void Scrollbar::Draw(int32_t offsetX, int32_t offsetY) {
	if(this -> visible) {
		Graphics::AddTextToQueue(this -> text, offsetX + this -> x + 5, offsetY + this -> y + this -> height - 2, 0.1f, 0.1f, 0.1f);
		wstringstream ss;
		ss.str(L"");
		if(this -> maxValue > 0) {
			ss << this -> scrollValue << flush;
		} else {
			ss << ((this -> scrollPosition > 0.0) ? L"true" : L"false") << flush;
		}
		Graphics::AddTextToQueue(ss.str(), offsetX + this -> x + 5, offsetY + this -> y + this -> height - 26, 0.1f, 0.1f, 0.1f);

		Graphics::Rect rect = { this -> x + offsetX, this -> y + (int32_t)this -> height - 44 + offsetY, this -> width, 20, 0.6f, 0.6f, 0.6f };
		if(this -> active) {
			rect.color.red = 0.2f;
			rect.color.green = 0.5f;
			rect.color.blue = 0.2f;
		}
		Graphics::DrawRects(&rect, 1);

		Graphics::Rect caret = { this -> x + offsetX + 2 + (int32_t)((double)(this -> width - 7) * this -> scrollPosition), this -> y + (int32_t)this -> height - 44 + offsetY, 3, 20, 0.6f, 0.6f, 0.6f };
		if(this -> active) {
			caret.color.red = 0.2f;
			caret.color.green = 0.5f;
			caret.color.blue = 0.2f;
		}
		Graphics::FillRects(&caret, 1);
	}
}

void Scrollbar::Activate(bool* done) {
	this -> oldScrollPosition = this -> scrollPosition;
	this -> firstFrame = true;
	this -> done = done;
}

}
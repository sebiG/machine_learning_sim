#include <ui/statusBar.h>

#include <system/graphics.h>

using namespace System;

namespace UI {

void StatusBar::Update(double deltaTime) {
	if(this -> timer > 0.0) {
		this -> timer -= deltaTime;
		if(this -> timer <= 0.0) {
			this -> timer = 0.0;
			this -> statusText = L"Bereit";
		}
	}
}

void StatusBar::Draw() {
	Graphics::AddTextToQueue(L"Status: " + this -> statusText, 1, Window::GetHeight() - 1, 1.0f, 1.0f, 1.0f);
}

const double StatusBar::STATUS_DISPLAY_TIME = 10.0;

}
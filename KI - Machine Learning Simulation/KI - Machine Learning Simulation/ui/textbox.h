#pragma once

#include <stdafx.h>

#include <ui/ui.h>

namespace UI {

// Klasse f�r Texteingabefelder
class Textbox : public UiElement {
public:
	Textbox();
	virtual ~Textbox();

	// Eindeutige Konstanten f�r Ereignistypen
	const static uint32_t TEXTBOX_ABORT = 0x0200;
	const static uint32_t TEXTBOX_CHANGE = 0x0201;

	// Implementiere abstrakte Methoden
	virtual void Update(double deltaTime);
	virtual void Draw(int32_t offsetX, int32_t offsetY);

	virtual void Activate(bool* done);

	virtual void OnEvent(System::Event const& e);

	uint32_t maxCharacters;
	wstring enteredText;

	inline virtual string GetName() {
		return "Textbox";
	}

private:
	bool* done;
	bool firstFrame;
	wstring oldText;
};

}
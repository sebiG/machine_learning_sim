#pragma once

#include <stdafx.h>

#include <ui/ui.h>

namespace UI {

// Klasse f�r hotizontaler Scrollbalken
class Scrollbar : public UiElement {
public:
	Scrollbar() : scrollPosition(0.0), steps(1), maxValue(1), scrollValue(0) {}
	~Scrollbar() = default;

	// Eindeutige Konstanten f�r Ereignistypen
	const static uint32_t SCROLLBAR_ABORT = 0x0300;
	const static uint32_t SCROLLBAR_CHANGE = 0x0301;

	// Implementiere abstrakte Methoden
	virtual void Update(double deltaTime);
	virtual void Draw(int32_t offsetX, int32_t offsetY);

	virtual void Activate(bool* done);

	virtual void OnEvent(System::Event const& e) {}

	double scrollPosition; // 0 <= scrollPosition <= 1
	uint32_t steps; // != 0
	uint32_t maxValue;
	uint32_t scrollValue;

	inline virtual string GetName() {
		return "Scrollbar";
	}

private:
	bool* done;
	bool firstFrame;
	double oldScrollPosition;
};

}
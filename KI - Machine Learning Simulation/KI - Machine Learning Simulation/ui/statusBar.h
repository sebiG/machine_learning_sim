#pragma once

#include <stdafx.h>

namespace UI {

// Klasse f�r Statusleiste, welche oben links im Fenster angezeigt wird
class StatusBar {
public:
	StatusBar() : statusText(L"Bereit") {};
	~StatusBar() = default; // Standarddestruktor

	// Gibt an, wie lange der Status angezeigt wird bevor er mit dem Standardstatus ersetzt wird
	const static double STATUS_DISPLAY_TIME;

	// Aktualisiere Statusleiste
	void Update(double deltaTime);
	// Zichne Statusleiste
	void Draw();

	// Setzt Status
	inline void SetStatus(wstring status) {
		this -> statusText = status;
		this -> timer = StatusBar::STATUS_DISPLAY_TIME;
	}

private:
	wstring statusText;
	double timer;
};

}
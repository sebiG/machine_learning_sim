#pragma once

#include <stdafx.h>

#include <ui/ui.h>

namespace UI {

// Klasse f�r Taster
class Button : public UiElement {
public:
	// Eindeutige Konstante f�r Ereignistyp
	const static uint32_t BUTTON_ACTIVATE = 0x0100;

	// Standardfunktion
	virtual void OnEvent(System::Event const& e) {};

	// implementiere abstrakte Methoden
	virtual void Update(double deltaTime);
	virtual void Draw(int32_t offsetX, int32_t offsetY);

	// Sendet BUTTON_ACTIVATE-Ereignis
	virtual void Activate(bool* done);

	inline virtual string GetName() {
		return "Button";
	}
};

}
#include <ai/pathController.h>

#include <system/graphics.h>
#include <simulation/character.h>
#include <simulation/render.h>
#include <ai/ai.h>
#include <ai/view.h>
#include <ai/physicsMemory.h>

using namespace System;
using namespace Sim;

namespace AI {

PathController::PathController(Character* debugCharacter, View* view, PhysicsMemory* physicsMemory, WorldMemory* worldMemory) : debugCharacter(debugCharacter), view(view), physicsMemory(physicsMemory), worldMemory(worldMemory) {

}

bool PathController::IsStationary() {
	return this -> currentState == State::STATIONARY;
}

bool PathController::Update(double deltaTime) {
	if(this -> path.size() != 0) {
		this -> currentTrajectory = *this -> pathIterator;
		this -> currentPlatform = this -> currentTrajectory -> start;
	}

	// Setze Statusvariabeln
	this -> x = this -> view -> GetData(PhysicsBehaviour::Variable(PhysicsBehaviour::Variable::Name::S_POSITION_X, 0));
	this -> y = this -> view -> GetData(PhysicsBehaviour::Variable(PhysicsBehaviour::Variable::Name::S_POSITION_Y, 0));

	this -> vX = this -> view -> GetData(PhysicsBehaviour::Variable(PhysicsBehaviour::Variable::Name::S_VELOCITY_X, 0));
	this -> vY = this -> view -> GetData(PhysicsBehaviour::Variable(PhysicsBehaviour::Variable::Name::S_VELOCITY_Y, 0));

	this -> collisionUp = this -> view -> GetData(PhysicsBehaviour::Variable(PhysicsBehaviour::Variable::Name::B_COLLISION_U, 0)) > 0.0;
	this -> collisionDown = this -> view -> GetData(PhysicsBehaviour::Variable(PhysicsBehaviour::Variable::Name::B_COLLISION_D, 0)) > 0.0;
	this -> collisionRight = this -> view -> GetData(PhysicsBehaviour::Variable(PhysicsBehaviour::Variable::Name::B_COLLISION_R, 0)) > 0.0;
	this -> collisionLeft = this -> view -> GetData(PhysicsBehaviour::Variable(PhysicsBehaviour::Variable::Name::B_COLLISION_L, 0)) > 0.0;

	this -> standingPlatform1 = this -> worldMemory -> GetPlatformAt((int32_t)floor(this -> x), (int32_t)round(this -> y - 1));
	this -> standingPlatform2 = this -> worldMemory -> GetPlatformAt((int32_t)ceil(this -> x), (int32_t)round(this -> y - 1));

	this -> gravitationalAcceleration = abs(this -> physicsMemory -> GetConstantKnowledge(PhysicsBehaviour::Constant::S_GRAVITY) -> value);
	this -> horizontalForceGround = this -> physicsMemory -> GetConstantKnowledge(PhysicsBehaviour::Constant::S_HORIZONTAL_FORCE_GROUND) -> value;
	this -> horizontalForceAir = this -> physicsMemory -> GetConstantKnowledge(PhysicsBehaviour::Constant::S_HORIZONTAL_FORCE_AIR) -> value;
	this -> jumpImpulse = this -> physicsMemory -> GetConstantKnowledge(PhysicsBehaviour::Constant::S_JUMP_IMPULSE) -> value;

	if(this -> walk) {
		// Gehe automatisch auf Zielposition zu
		this -> Walk(deltaTime);
	}

	// Setze aktuelle Flugbahn auf unm�glich, wenn die KI aus der Welt gefallen ist
	if(this -> y < 0.0) {
		if(this -> currentTrajectory != nullptr) {
			this -> currentTrajectory -> possible = false;
		}
		this -> EndPath();
		return true;
	}

	// EXPLORING_LEFT, EXPLORING_RIGHT, TOUCHED_DOWN und STATIONNARY werden unabh�ngig des Flugbahntyps gleich ausgef�hrt
	switch(this -> currentState) {
	case State::EXPLORING_LEFT:
	{
		// Solange die KI immer noch geht mache nichts
		if(!this -> walk) {
			// Gehe weiter, falls die Plattform noch weiter geht
			if(this -> x > (double)this -> currentPlatform -> x) {
				this -> StartWalking(this -> currentPlatform -> x - 1.0 + Ai::CHARACTER_SIZE_BIAS);
			} else {
				this -> StartWalking(this -> currentPlatform -> x + this -> currentPlatform -> width - Ai::CHARACTER_SIZE_BIAS);
				this -> currentState = State::EXPLORING_RIGHT;
			}
		}
		break;
	}
	case State::EXPLORING_RIGHT:
	{
		if(!this -> walk) {
			// Gehe weiter, falls die Plattform noch weiter geht
			if(this -> x < this -> currentPlatform -> x + this -> currentPlatform -> width - 1.0) {
				this -> StartWalking(this -> currentPlatform -> x + this -> currentPlatform -> width - Ai::CHARACTER_SIZE_BIAS);
			} else {
				this -> currentPlatform -> Visit();
				this -> currentState = State::STATIONARY;
			}
		}
		break;
	}
	case State::TOUCHED_DOWN:
	{
		bool standing = false;
		if(this -> collisionDown && this -> view -> GetData(PhysicsBehaviour::Variable(PhysicsBehaviour::Variable::Name::B_IMPULSE_CHANGE_Y, 0)) < 1.0 && abs(this -> vY) < PathController::EPSILON) {
			standing = true;
		}

		// Solange die KI nicht steht
		if(!standing || abs(this -> vX) > PathController::EPSILON) {
			// Bremse solange die Geschwindigkeit nicht null ist
			double referenceVelocity;
			if(this -> currentTrajectory -> xDirection > 0.0) {
				referenceVelocity = this -> vX - PathController::AIR_SPEED_BIAS;
			} else {
				referenceVelocity = this -> vX + PathController::AIR_SPEED_BIAS;
			}
			if(abs(referenceVelocity) > PathController::AIR_SPEED_BIAS) {
				if(referenceVelocity > 0.0) {
					this -> view -> DoInput(Character::Input::WALK_LEFT);
				} else {
					this -> view -> DoInput(Character::Input::WALK_RIGHT);
				}
			}
		} else {
			// Teste, ob auf der richtigen Plattform gelandet wurde
			if(!this -> CheckLanding()) {
				this -> currentTrajectory -> possible = false;
				this -> EndPath();
				return true;
			}

			if(this -> NextTrajectory()) {
				this -> EndPath();
				return true;
			}
		}
		break;
	}
	case State::STATIONARY:
	{
		if(!this -> currentPlatform -> HasBeenVisited()) {
			this -> StartWalking(this -> currentPlatform -> x - 1.0 + Ai::CHARACTER_SIZE_BIAS);
			this -> currentState = State::EXPLORING_LEFT;
		} else {
			if(this -> path.size() == 0) {
				// Plattform erkundet
				return true;
			} else {
				this -> StartWalking(this -> currentTrajectory -> startPosition);
				this -> currentState = State::GETTING_TO_START_POSITION;
			}
		}
		break;
	}
	default:
	{
		// Alle anderen Modi werden je nach Typ anders ausgef�hrt
		if(this -> currentTrajectory -> type == WorldMemory::Trajectory::Type::ADJACENT) {
			return this -> FollowAdjacentTrajectory(deltaTime);
			break;
		} else if(this -> currentTrajectory -> type == WorldMemory::Trajectory::Type::STAIR) {
			return this -> FollowStairTrajectory(deltaTime);
			break;
		} else if(this -> currentTrajectory -> type == WorldMemory::Trajectory::Type::FALL || this -> currentTrajectory -> type == WorldMemory::Trajectory::Type::JUMP)	{
			return this -> FollowFallAndJumpTrajectory(deltaTime);
			break;
		}
	}
	}

	return false;
}

void PathController::DrawDebugInformation() {
	Vector2 offset = this -> debugCharacter -> GetPosition() * (double)Render::BLOCK_SIZE;
	int32_t offsetX = (int32_t)offset.GetX() - Window::GetWidth() / 2;
	int32_t offsetY = (int32_t)offset.GetY() - Window::GetHeight() / 2;

	double gravitationalAcceleration = abs(this -> physicsMemory -> GetConstantKnowledge(Sim::PhysicsBehaviour::Constant::S_GRAVITY) -> value);
	double airAcceleration = abs(this -> physicsMemory -> GetConstantKnowledge(Sim::PhysicsBehaviour::Constant::S_HORIZONTAL_FORCE_AIR) -> value);
	double jumpImpulse = this -> physicsMemory -> GetConstantKnowledge(Sim::PhysicsBehaviour::Constant::S_JUMP_IMPULSE) -> value;

	// Zeichne alle Flugbahnen ein
	uint32_t colorIndex = 0;
	for(list<WorldMemory::Trajectory*>::iterator i = this -> path.begin(); i != this -> path.end(); i++) {
		switch((*i) -> type) {
		case WorldMemory::Trajectory::Type::ADJACENT: 
		{
			// Zeichne Zeilpunkt ein
			Graphics::Line targetPoint[2];

			targetPoint[0] = { (int32_t)((*i) -> startPosition * (double)Render::BLOCK_SIZE) - offsetX - 5, (int32_t)(((*i) -> start -> y + 1) * (double)Render::BLOCK_SIZE) - offsetY - 5, (int32_t)((*i) -> startPosition * (double)Render::BLOCK_SIZE) - offsetX + 5, (int32_t)(((*i) -> start -> y + 1) * (double)Render::BLOCK_SIZE) - offsetY + 5, PathController::DEBUG_COLOR_MAP[colorIndex][0], PathController::DEBUG_COLOR_MAP[colorIndex][1], PathController::DEBUG_COLOR_MAP[colorIndex][2]
			};
			targetPoint[1] = { (int32_t)((*i) -> startPosition * (double)Render::BLOCK_SIZE) - offsetX - 5, (int32_t)(((*i) -> start -> y + 1) * (double)Render::BLOCK_SIZE) - offsetY + 5, (int32_t)((*i) -> startPosition * (double)Render::BLOCK_SIZE) - offsetX + 5, (int32_t)(((*i) -> start -> y + 1) * (double)Render::BLOCK_SIZE) - offsetY - 5, PathController::DEBUG_COLOR_MAP[colorIndex][0], PathController::DEBUG_COLOR_MAP[colorIndex][1], PathController::DEBUG_COLOR_MAP[colorIndex][2] 
			};

			Graphics::DrawLines(targetPoint, 2);

			Graphics::FlushLines();

			break;
		}
		case WorldMemory::Trajectory::Type::FALL:
		{
			// Zeichne wichtige Punkte der Flugbahn ein
			// Punkte werden mit Kreuzen gekennzeichnet
			Graphics::Line importantPoints[8];

			// Startpunkt
			importantPoints[0] = { (int32_t)((*i) -> startPosition * (double)Render::BLOCK_SIZE) - offsetX - 5, (int32_t)(((*i) -> start -> y + 1) * (double)Render::BLOCK_SIZE) - offsetY - 5, (int32_t)((*i) -> startPosition * (double)Render::BLOCK_SIZE) - offsetX + 5, (int32_t)(((*i) -> start -> y + 1) * (double)Render::BLOCK_SIZE) - offsetY + 5, PathController::DEBUG_COLOR_MAP[colorIndex][0], PathController::DEBUG_COLOR_MAP[colorIndex][1], PathController::DEBUG_COLOR_MAP[colorIndex][2] };
			importantPoints[1] = { (int32_t)((*i) -> startPosition * (double)Render::BLOCK_SIZE) - offsetX - 5, (int32_t)(((*i) -> start -> y + 1) * (double)Render::BLOCK_SIZE) - offsetY + 5, (int32_t)((*i) -> startPosition * (double)Render::BLOCK_SIZE) - offsetX + 5, (int32_t)(((*i) -> start -> y + 1) * (double)Render::BLOCK_SIZE) - offsetY - 5, PathController::DEBUG_COLOR_MAP[colorIndex][0], PathController::DEBUG_COLOR_MAP[colorIndex][1], PathController::DEBUG_COLOR_MAP[colorIndex][2] };

			// Sprungpunkt
			importantPoints[2] = { (int32_t)((*i) -> liftoffPosition * (double)Render::BLOCK_SIZE) - offsetX - 5, (int32_t)(((*i) -> start -> y + 1) * (double)Render::BLOCK_SIZE) - offsetY - 5, (int32_t)((*i) -> liftoffPosition * (double)Render::BLOCK_SIZE) - offsetX + 5, (int32_t)(((*i) -> start -> y + 1) * (double)Render::BLOCK_SIZE) - offsetY + 5, PathController::DEBUG_COLOR_MAP[colorIndex][0], PathController::DEBUG_COLOR_MAP[colorIndex][1], PathController::DEBUG_COLOR_MAP[colorIndex][2] };
			importantPoints[3] = { (int32_t)((*i) -> liftoffPosition * (double)Render::BLOCK_SIZE) - offsetX - 5, (int32_t)(((*i) -> start -> y + 1) * (double)Render::BLOCK_SIZE) - offsetY + 5, (int32_t)((*i) -> liftoffPosition * (double)Render::BLOCK_SIZE) - offsetX + 5, (int32_t)(((*i) -> start -> y + 1) * (double)Render::BLOCK_SIZE) - offsetY - 5, PathController::DEBUG_COLOR_MAP[colorIndex][0], PathController::DEBUG_COLOR_MAP[colorIndex][1], PathController::DEBUG_COLOR_MAP[colorIndex][2] };

			// Bresmpunkt in der Luft
			double brakePositionX = (*i) -> liftoffPosition + ((*i) -> t - (*i) -> tA) * (*i) -> v0 * (*i) -> xDirection;
			double brakePositionY = (*i) -> start -> y + 1 - 0.5 * gravitationalAcceleration * pow((*i) -> t - (*i) -> tA, 2);
			importantPoints[4] = { (int32_t)(brakePositionX * (double)Render::BLOCK_SIZE) - offsetX - 5, (int32_t)(brakePositionY * (double)Render::BLOCK_SIZE) - offsetY - 5, (int32_t)(brakePositionX * (double)Render::BLOCK_SIZE) - offsetX + 5, (int32_t)(brakePositionY * (double)Render::BLOCK_SIZE) - offsetY + 5, PathController::DEBUG_COLOR_MAP[colorIndex][0], PathController::DEBUG_COLOR_MAP[colorIndex][1], PathController::DEBUG_COLOR_MAP[colorIndex][2] };
			importantPoints[5] = { (int32_t)(brakePositionX * (double)Render::BLOCK_SIZE) - offsetX - 5, (int32_t)(brakePositionY * (double)Render::BLOCK_SIZE) - offsetY + 5, (int32_t)(brakePositionX * (double)Render::BLOCK_SIZE) - offsetX + 5, (int32_t)(brakePositionY * (double)Render::BLOCK_SIZE) - offsetY - 5, PathController::DEBUG_COLOR_MAP[colorIndex][0], PathController::DEBUG_COLOR_MAP[colorIndex][1], PathController::DEBUG_COLOR_MAP[colorIndex][2] };

			// Landepunkt
			importantPoints[6] = { (int32_t)((*i) -> touchdownPosition * (double)Render::BLOCK_SIZE) - offsetX - 5, (int32_t)(((*i) -> end -> y + 1) * (double)Render::BLOCK_SIZE) - offsetY - 5, (int32_t)((*i) -> touchdownPosition * (double)Render::BLOCK_SIZE) - offsetX + 5, (int32_t)(((*i) -> end -> y + 1) * (double)Render::BLOCK_SIZE) - offsetY + 5, PathController::DEBUG_COLOR_MAP[colorIndex][0], PathController::DEBUG_COLOR_MAP[colorIndex][1], PathController::DEBUG_COLOR_MAP[colorIndex][2] };
			importantPoints[7] = { (int32_t)((*i) -> touchdownPosition * (double)Render::BLOCK_SIZE) - offsetX - 5, (int32_t)(((*i) -> end -> y + 1) * (double)Render::BLOCK_SIZE) - offsetY + 5, (int32_t)((*i) -> touchdownPosition * (double)Render::BLOCK_SIZE) - offsetX + 5, (int32_t)(((*i) -> end -> y + 1) * (double)Render::BLOCK_SIZE) - offsetY - 5, PathController::DEBUG_COLOR_MAP[colorIndex][0], PathController::DEBUG_COLOR_MAP[colorIndex][1], PathController::DEBUG_COLOR_MAP[colorIndex][2] };

			Graphics::DrawLines(importantPoints, 8);

			// Zeichne Beschleunigungsweg
			Graphics::Line accelerationPath = { (int32_t)((*i) -> startPosition * (double)Render::BLOCK_SIZE) - offsetX, (int32_t)(((*i) -> start -> y + 1) * (double)Render::BLOCK_SIZE) - offsetY, (int32_t)((*i) -> liftoffPosition * (double)Render::BLOCK_SIZE) - offsetX, (int32_t)(((*i) -> start -> y + 1) * (double)Render::BLOCK_SIZE) - offsetY, PathController::DEBUG_COLOR_MAP[colorIndex][0], PathController::DEBUG_COLOR_MAP[colorIndex][1], PathController::DEBUG_COLOR_MAP[colorIndex][2] };

			Graphics::DrawLines(&accelerationPath, 1);

			Graphics::FlushLines();

			// Zeichne Flugbahn ein
			{
				uint32_t lineCounter = 0;

				// Laufe Pfad in Zehntelsekundenschritten ab

				// Zeichen unbeschleunigter Fall
				double time = 0.1;

				while(time < (*i) -> t - (*i) -> tA) {
					double t1 = time - 0.1;
					double t2 = time;

					double x1 = (*i) -> liftoffPosition + t1 * (*i) -> v0 * (*i) -> xDirection;
					double y1 = (*i) -> start -> y + 1 - 0.5 * gravitationalAcceleration * t1 * t1;
					double x2 = (*i) -> liftoffPosition + t2 * (*i) -> v0 * (*i) -> xDirection;
					double y2 = (*i) -> start -> y + 1 - 0.5 * gravitationalAcceleration * t2 * t2;

					Graphics::Line line = { (int32_t)(x1 * (double)Render::BLOCK_SIZE) - offsetX, (int32_t)(y1 * (double)Render::BLOCK_SIZE) - offsetY, (int32_t)(x2 * (double)Render::BLOCK_SIZE) - offsetX, (int32_t)(y2 * (double)Render::BLOCK_SIZE) - offsetY, PathController::DEBUG_COLOR_MAP[colorIndex][0], PathController::DEBUG_COLOR_MAP[colorIndex][1], PathController::DEBUG_COLOR_MAP[colorIndex][2] };
					Graphics::DrawLines(&line, 1);

					lineCounter++;

					if(lineCounter >= Graphics::MAX_LINE_LIST_SIZE - 2) {
						Graphics::FlushLines();
						lineCounter = 0;
					}

					time += 0.1;
				}

				// Zeichne letztes Liniensegment
				{
					double t1 = time - 0.1;
					double t2 = (*i) -> t - (*i) -> tA;

					double x1 = (*i) -> liftoffPosition + t1 * (*i) -> v0 * (*i) -> xDirection;
					double y1 = (*i) -> start -> y + 1 - 0.5 * gravitationalAcceleration * t1 * t1;
					double x2 = (*i) -> liftoffPosition + t2 * (*i) -> v0 * (*i) -> xDirection;
					double y2 = (*i) -> start -> y + 1 - 0.5 * gravitationalAcceleration * t2 * t2;

					Graphics::Line line = { (int32_t)(x1 * (double)Render::BLOCK_SIZE) - offsetX, (int32_t)(y1 * (double)Render::BLOCK_SIZE) - offsetY, (int32_t)(x2 * (double)Render::BLOCK_SIZE) - offsetX, (int32_t)(y2 * (double)Render::BLOCK_SIZE) - offsetY, PathController::DEBUG_COLOR_MAP[colorIndex][0], PathController::DEBUG_COLOR_MAP[colorIndex][1], PathController::DEBUG_COLOR_MAP[colorIndex][2] };
					Graphics::DrawLines(&line, 1);
				}

				Graphics::FlushLines();

				lineCounter = 0;
				time = 0.1;

				// Zeichne gebremster Fall

				while(time < (*i) -> tA) {
					double t1 = time - 0.1;
					double t2 = time;

					double x1 = brakePositionX + (*i) -> v0 * t1 * (*i) -> xDirection - 0.5 * airAcceleration * t1 * t1 * (*i) -> xDirection;
					double y1 = (*i) -> start -> y + 1 - 0.5 * gravitationalAcceleration * pow((*i) -> t - (*i) -> tA + t1, 2);
					double x2 = brakePositionX + (*i) -> v0 * t2 * (*i) -> xDirection - 0.5 * airAcceleration * t2 * t2 * (*i) -> xDirection;
					double y2 = (*i) -> start -> y + 1 - 0.5 * gravitationalAcceleration * pow((*i) -> t - (*i) -> tA + t2, 2);

					Graphics::Line line = { (int32_t)(x1 * (double)Render::BLOCK_SIZE) - offsetX, (int32_t)(y1 * (double)Render::BLOCK_SIZE) - offsetY, (int32_t)(x2 * (double)Render::BLOCK_SIZE) - offsetX, (int32_t)(y2 * (double)Render::BLOCK_SIZE) - offsetY, PathController::DEBUG_COLOR_MAP[colorIndex][0], PathController::DEBUG_COLOR_MAP[colorIndex][1], PathController::DEBUG_COLOR_MAP[colorIndex][2] };
					Graphics::DrawLines(&line, 1);

					lineCounter++;

					if(lineCounter >= Graphics::MAX_LINE_LIST_SIZE - 2) {
						Graphics::FlushLines();
						lineCounter = 0;
					}

					time += 0.1;
				}

				// Zeichne letztes Liniensegment
				{
					double t1 = time - 0.1;
					double t2 = (*i) -> tA;

					double x1 = brakePositionX + (*i) -> v0 * t1 * (*i) -> xDirection - 0.5 * airAcceleration * t1 * t1 * (*i) -> xDirection;
					double y1 = (*i) -> start -> y + 1 - 0.5 * gravitationalAcceleration * pow((*i) -> t - (*i) -> tA + t1, 2);
					double x2 = brakePositionX + (*i) -> v0 * t2 * (*i) -> xDirection - 0.5 * airAcceleration * t2 * t2 * (*i) -> xDirection;
					double y2 = (*i) -> start -> y + 1 - 0.5 * gravitationalAcceleration * pow((*i) -> t - (*i) -> tA + t2, 2);

					Graphics::Line line = { (int32_t)(x1 * (double)Render::BLOCK_SIZE) - offsetX, (int32_t)(y1 * (double)Render::BLOCK_SIZE) - offsetY, (int32_t)(x2 * (double)Render::BLOCK_SIZE) - offsetX, (int32_t)(y2 * (double)Render::BLOCK_SIZE) - offsetY, PathController::DEBUG_COLOR_MAP[colorIndex][0], PathController::DEBUG_COLOR_MAP[colorIndex][1], PathController::DEBUG_COLOR_MAP[colorIndex][2] };
					Graphics::DrawLines(&line, 1);
				}

				Graphics::FlushLines();
			}

			break;
		}
		case WorldMemory::Trajectory::Type::STAIR:
		{
			// Zeichne wichtige Punkte der Flugbahn ein
			// Punkte werden mit Kreuzen gekennzeichnet
			Graphics::Line importantPoints[6];

			// Absprungpunkt
			importantPoints[0] = { (int32_t)((*i) -> liftoffPosition * (double)Render::BLOCK_SIZE) - offsetX - 5, (int32_t)(((*i) -> start -> y + 1) * (double)Render::BLOCK_SIZE) - offsetY - 5, (int32_t)((*i) -> liftoffPosition * (double)Render::BLOCK_SIZE) - offsetX + 5, (int32_t)(((*i) -> start -> y + 1) * (double)Render::BLOCK_SIZE) - offsetY + 5, PathController::DEBUG_COLOR_MAP[colorIndex][0], PathController::DEBUG_COLOR_MAP[colorIndex][1], PathController::DEBUG_COLOR_MAP[colorIndex][2] };
			importantPoints[1] = { (int32_t)((*i) -> liftoffPosition * (double)Render::BLOCK_SIZE) - offsetX - 5, (int32_t)(((*i) -> start -> y + 1) * (double)Render::BLOCK_SIZE) - offsetY + 5, (int32_t)((*i) -> liftoffPosition * (double)Render::BLOCK_SIZE) - offsetX + 5, (int32_t)(((*i) -> start -> y + 1) * (double)Render::BLOCK_SIZE) - offsetY - 5, PathController::DEBUG_COLOR_MAP[colorIndex][0], PathController::DEBUG_COLOR_MAP[colorIndex][1], PathController::DEBUG_COLOR_MAP[colorIndex][2] };

			// Beschleunigungspunkt
			double apexT = (*i) -> t - (*i) -> tA;
			double brakeY = jumpImpulse * apexT - 0.5 * gravitationalAcceleration * apexT * apexT;
			importantPoints[2] = { (int32_t)((*i) -> liftoffPosition * (double)Render::BLOCK_SIZE) - offsetX - 5, (int32_t)((brakeY + (*i) -> start -> y + 1.0) * (double)Render::BLOCK_SIZE) - offsetY - 5, (int32_t)((*i) -> liftoffPosition * (double)Render::BLOCK_SIZE) - offsetX + 5, (int32_t)((brakeY + (*i) -> start -> y + 1.0) * (double)Render::BLOCK_SIZE) - offsetY + 5, PathController::DEBUG_COLOR_MAP[colorIndex][0], PathController::DEBUG_COLOR_MAP[colorIndex][1], PathController::DEBUG_COLOR_MAP[colorIndex][2] };
			importantPoints[3] = { (int32_t)((*i) -> liftoffPosition * (double)Render::BLOCK_SIZE) - offsetX - 5, (int32_t)((brakeY + (*i) -> start -> y + 1.0) * (double)Render::BLOCK_SIZE) - offsetY + 5, (int32_t)((*i) -> liftoffPosition * (double)Render::BLOCK_SIZE) - offsetX + 5, (int32_t)((brakeY + (*i) -> start -> y + 1.0) * (double)Render::BLOCK_SIZE) - offsetY - 5, PathController::DEBUG_COLOR_MAP[colorIndex][0], PathController::DEBUG_COLOR_MAP[colorIndex][1], PathController::DEBUG_COLOR_MAP[colorIndex][2] };

			// Landepunkt
			importantPoints[4] = { (int32_t)((*i) -> touchdownPosition * (double)Render::BLOCK_SIZE) - offsetX - 5, (int32_t)(((*i) -> end -> y + 1) * (double)Render::BLOCK_SIZE) - offsetY - 5, (int32_t)((*i) -> touchdownPosition * (double)Render::BLOCK_SIZE) - offsetX + 5, (int32_t)(((*i) -> end -> y + 1) * (double)Render::BLOCK_SIZE) - offsetY + 5, PathController::DEBUG_COLOR_MAP[colorIndex][0], PathController::DEBUG_COLOR_MAP[colorIndex][1], PathController::DEBUG_COLOR_MAP[colorIndex][2] };
			importantPoints[5] = { (int32_t)((*i) -> touchdownPosition * (double)Render::BLOCK_SIZE) - offsetX - 5, (int32_t)(((*i) -> end -> y + 1) * (double)Render::BLOCK_SIZE) - offsetY + 5, (int32_t)((*i) -> touchdownPosition * (double)Render::BLOCK_SIZE) - offsetX + 5, (int32_t)(((*i) -> end -> y + 1) * (double)Render::BLOCK_SIZE) - offsetY - 5, PathController::DEBUG_COLOR_MAP[colorIndex][0], PathController::DEBUG_COLOR_MAP[colorIndex][1], PathController::DEBUG_COLOR_MAP[colorIndex][2] };

			Graphics::DrawLines(importantPoints, 6);

			// Zeichne Beschleunigungsweg
			Graphics::Line accelerationPath = { (int32_t)((*i) -> liftoffPosition * (double)Render::BLOCK_SIZE) - offsetX, (int32_t)(((*i) -> start -> y + 1) * (double)Render::BLOCK_SIZE) - offsetY, (int32_t)((*i) -> liftoffPosition * (double)Render::BLOCK_SIZE) - offsetX, (int32_t)((brakeY + (*i) -> start -> y + 1.0) * (double)Render::BLOCK_SIZE) - offsetY, PathController::DEBUG_COLOR_MAP[colorIndex][0], PathController::DEBUG_COLOR_MAP[colorIndex][1], PathController::DEBUG_COLOR_MAP[colorIndex][2] };

			Graphics::DrawLines(&accelerationPath, 1);

			Graphics::FlushLines();

			break;
		}
		case WorldMemory::Trajectory::Type::JUMP:
		{
			// Zeichne wichtige Punkte der Flugbahn ein
			// Punkte werden mit Kreuzen gekennzeichnet
			Graphics::Line importantPoints[8];

			// Startpunkt
			importantPoints[0] = { (int32_t)((*i) -> startPosition * (double)Render::BLOCK_SIZE) - offsetX - 5, (int32_t)(((*i) -> start -> y + 1) * (double)Render::BLOCK_SIZE) - offsetY - 5, (int32_t)((*i) -> startPosition * (double)Render::BLOCK_SIZE) - offsetX + 5, (int32_t)(((*i) -> start -> y + 1) * (double)Render::BLOCK_SIZE) - offsetY + 5, PathController::DEBUG_COLOR_MAP[colorIndex][0], PathController::DEBUG_COLOR_MAP[colorIndex][1], PathController::DEBUG_COLOR_MAP[colorIndex][2] };
			importantPoints[1] = { (int32_t)((*i) -> startPosition * (double)Render::BLOCK_SIZE) - offsetX - 5, (int32_t)(((*i) -> start -> y + 1) * (double)Render::BLOCK_SIZE) - offsetY + 5, (int32_t)((*i) -> startPosition * (double)Render::BLOCK_SIZE) - offsetX + 5, (int32_t)(((*i) -> start -> y + 1) * (double)Render::BLOCK_SIZE) - offsetY - 5, PathController::DEBUG_COLOR_MAP[colorIndex][0], PathController::DEBUG_COLOR_MAP[colorIndex][1], PathController::DEBUG_COLOR_MAP[colorIndex][2] };

			// Sprungpunkt
			importantPoints[2] = { (int32_t)((*i) -> liftoffPosition * (double)Render::BLOCK_SIZE) - offsetX - 5, (int32_t)(((*i) -> start -> y + 1) * (double)Render::BLOCK_SIZE) - offsetY - 5, (int32_t)((*i) -> liftoffPosition * (double)Render::BLOCK_SIZE) - offsetX + 5, (int32_t)(((*i) -> start -> y + 1) * (double)Render::BLOCK_SIZE) - offsetY + 5, PathController::DEBUG_COLOR_MAP[colorIndex][0], PathController::DEBUG_COLOR_MAP[colorIndex][1], PathController::DEBUG_COLOR_MAP[colorIndex][2] };
			importantPoints[3] = { (int32_t)((*i) -> liftoffPosition * (double)Render::BLOCK_SIZE) - offsetX - 5, (int32_t)(((*i) -> start -> y + 1) * (double)Render::BLOCK_SIZE) - offsetY + 5, (int32_t)((*i) -> liftoffPosition * (double)Render::BLOCK_SIZE) - offsetX + 5, (int32_t)(((*i) -> start -> y + 1) * (double)Render::BLOCK_SIZE) - offsetY - 5, PathController::DEBUG_COLOR_MAP[colorIndex][0], PathController::DEBUG_COLOR_MAP[colorIndex][1], PathController::DEBUG_COLOR_MAP[colorIndex][2] };

			// Bresmpunkt in der Luft
			double brakePositionX = (*i) -> liftoffPosition + ((*i) -> t - (*i) -> tA) * (*i) -> v0 * (*i) -> xDirection;
			double brakePositionY = (*i) -> start -> y + 1 + jumpImpulse * ((*i) -> t - (*i) -> tA) - 0.5 * gravitationalAcceleration * pow((*i) -> t - (*i) -> tA, 2);
			importantPoints[4] = { (int32_t)(brakePositionX * (double)Render::BLOCK_SIZE) - offsetX - 5, (int32_t)(brakePositionY * (double)Render::BLOCK_SIZE) - offsetY - 5, (int32_t)(brakePositionX * (double)Render::BLOCK_SIZE) - offsetX + 5, (int32_t)(brakePositionY * (double)Render::BLOCK_SIZE) - offsetY + 5, PathController::DEBUG_COLOR_MAP[colorIndex][0], PathController::DEBUG_COLOR_MAP[colorIndex][1], PathController::DEBUG_COLOR_MAP[colorIndex][2] };
			importantPoints[5] = { (int32_t)(brakePositionX * (double)Render::BLOCK_SIZE) - offsetX - 5, (int32_t)(brakePositionY * (double)Render::BLOCK_SIZE) - offsetY + 5, (int32_t)(brakePositionX * (double)Render::BLOCK_SIZE) - offsetX + 5, (int32_t)(brakePositionY * (double)Render::BLOCK_SIZE) - offsetY - 5, PathController::DEBUG_COLOR_MAP[colorIndex][0], PathController::DEBUG_COLOR_MAP[colorIndex][1], PathController::DEBUG_COLOR_MAP[colorIndex][2] };

			// Landepunkt
			importantPoints[6] = { (int32_t)((*i) -> touchdownPosition * (double)Render::BLOCK_SIZE) - offsetX - 5, (int32_t)(((*i) -> end -> y + 1) * (double)Render::BLOCK_SIZE) - offsetY - 5, (int32_t)((*i) -> touchdownPosition * (double)Render::BLOCK_SIZE) - offsetX + 5, (int32_t)(((*i) -> end -> y + 1) * (double)Render::BLOCK_SIZE) - offsetY + 5, PathController::DEBUG_COLOR_MAP[colorIndex][0], PathController::DEBUG_COLOR_MAP[colorIndex][1], PathController::DEBUG_COLOR_MAP[colorIndex][2] };
			importantPoints[7] = { (int32_t)((*i) -> touchdownPosition * (double)Render::BLOCK_SIZE) - offsetX - 5, (int32_t)(((*i) -> end -> y + 1) * (double)Render::BLOCK_SIZE) - offsetY + 5, (int32_t)((*i) -> touchdownPosition * (double)Render::BLOCK_SIZE) - offsetX + 5, (int32_t)(((*i) -> end -> y + 1) * (double)Render::BLOCK_SIZE) - offsetY - 5, PathController::DEBUG_COLOR_MAP[colorIndex][0], PathController::DEBUG_COLOR_MAP[colorIndex][1], PathController::DEBUG_COLOR_MAP[colorIndex][2] };

			Graphics::DrawLines(importantPoints, 8);

			// Zeichne Beschleunigungsweg
			Graphics::Line accelerationPath = { (int32_t)((*i) -> startPosition * (double)Render::BLOCK_SIZE) - offsetX, (int32_t)(((*i) -> start -> y + 1) * (double)Render::BLOCK_SIZE) - offsetY, (int32_t)((*i) -> liftoffPosition * (double)Render::BLOCK_SIZE) - offsetX, (int32_t)(((*i) -> start -> y + 1) * (double)Render::BLOCK_SIZE) - offsetY, PathController::DEBUG_COLOR_MAP[colorIndex][0], PathController::DEBUG_COLOR_MAP[colorIndex][1], PathController::DEBUG_COLOR_MAP[colorIndex][2] };

			Graphics::DrawLines(&accelerationPath, 1);

			Graphics::FlushLines();

			// Zeichne Flugbahn ein
			{
				uint32_t lineCounter = 0;

				// Laufe Pfad in Zehntelsekundenschritten ab

				// Zeichen unbeschleunigter Flug
				double time = 0.1;

				while(time < (*i) -> t - (*i) -> tA) {
					double t1 = time - 0.1;
					double t2 = time;

					double x1 = (*i) -> liftoffPosition + t1 * (*i) -> v0 * (*i) -> xDirection;
					double y1 = (*i) -> start -> y + 1 + jumpImpulse * t1 - 0.5 * gravitationalAcceleration * t1 * t1;
					double x2 = (*i) -> liftoffPosition + t2 * (*i) -> v0 * (*i) -> xDirection;
					double y2 = (*i) -> start -> y + 1 + jumpImpulse * t2 - 0.5 * gravitationalAcceleration * t2 * t2;

					Graphics::Line line = { (int32_t)(x1 * (double)Render::BLOCK_SIZE) - offsetX, (int32_t)(y1 * (double)Render::BLOCK_SIZE) - offsetY, (int32_t)(x2 * (double)Render::BLOCK_SIZE) - offsetX, (int32_t)(y2 * (double)Render::BLOCK_SIZE) - offsetY, PathController::DEBUG_COLOR_MAP[colorIndex][0], PathController::DEBUG_COLOR_MAP[colorIndex][1], PathController::DEBUG_COLOR_MAP[colorIndex][2] };
					Graphics::DrawLines(&line, 1);

					lineCounter++;

					if(lineCounter >= Graphics::MAX_LINE_LIST_SIZE - 2) {
						Graphics::FlushLines();
						lineCounter = 0;
					}

					time += 0.1;
				}

				// Zeichne letztes Liniensegment
				{
					double t1 = time - 0.1;
					double t2 = (*i) -> t - (*i) -> tA;

					double x1 = (*i) -> liftoffPosition + t1 * (*i) -> v0 * (*i) -> xDirection;
					double y1 = (*i) -> start -> y + 1 + jumpImpulse * t1 - 0.5 * gravitationalAcceleration * t1 * t1;
					double x2 = (*i) -> liftoffPosition + t2 * (*i) -> v0 * (*i) -> xDirection;
					double y2 = (*i) -> start -> y + 1 + jumpImpulse * t2 - 0.5 * gravitationalAcceleration * t2 * t2;

					Graphics::Line line = { (int32_t)(x1 * (double)Render::BLOCK_SIZE) - offsetX, (int32_t)(y1 * (double)Render::BLOCK_SIZE) - offsetY, (int32_t)(x2 * (double)Render::BLOCK_SIZE) - offsetX, (int32_t)(y2 * (double)Render::BLOCK_SIZE) - offsetY, PathController::DEBUG_COLOR_MAP[colorIndex][0], PathController::DEBUG_COLOR_MAP[colorIndex][1], PathController::DEBUG_COLOR_MAP[colorIndex][2] };
					Graphics::DrawLines(&line, 1);
				}

				Graphics::FlushLines();

				lineCounter = 0;
				time = 0.1;

				// Zeichne gebremster Fall

				while(time < (*i) -> tA) {
					double t1 = time - 0.1;
					double t2 = time;

					double x1 = brakePositionX + (*i) -> v0 * t1 * (*i) -> xDirection - 0.5 * airAcceleration * t1 * t1 * (*i) -> xDirection;
					double y1 = (*i) -> start -> y + 1 + jumpImpulse * ((*i) -> t - (*i) -> tA + t1) - 0.5 * gravitationalAcceleration * pow((*i) -> t - (*i) -> tA + t1, 2);
					double x2 = brakePositionX + (*i) -> v0 * t2 * (*i) -> xDirection - 0.5 * airAcceleration * t2 * t2 * (*i) -> xDirection;
					double y2 = (*i) -> start -> y + 1 + jumpImpulse * ((*i) -> t - (*i) -> tA + t2) - 0.5 * gravitationalAcceleration * pow((*i) -> t - (*i) -> tA + t2, 2);

					Graphics::Line line = { (int32_t)(x1 * (double)Render::BLOCK_SIZE) - offsetX, (int32_t)(y1 * (double)Render::BLOCK_SIZE) - offsetY, (int32_t)(x2 * (double)Render::BLOCK_SIZE) - offsetX, (int32_t)(y2 * (double)Render::BLOCK_SIZE) - offsetY, PathController::DEBUG_COLOR_MAP[colorIndex][0], PathController::DEBUG_COLOR_MAP[colorIndex][1], PathController::DEBUG_COLOR_MAP[colorIndex][2] };
					Graphics::DrawLines(&line, 1);

					lineCounter++;

					if(lineCounter >= Graphics::MAX_LINE_LIST_SIZE - 2) {
						Graphics::FlushLines();
						lineCounter = 0;
					}

					time += 0.1;
				}

				// Zeichne letztes Liniensegment
				{
					double t1 = time - 0.1;
					double t2 = (*i) -> tA;

					double x1 = brakePositionX + (*i) -> v0 * t1 * (*i) -> xDirection - 0.5 * airAcceleration * t1 * t1 * (*i) -> xDirection;
					double y1 = (*i) -> start -> y + 1 + jumpImpulse * ((*i) -> t - (*i) -> tA + t1) - 0.5 * gravitationalAcceleration * pow((*i) -> t - (*i) -> tA + t1, 2);
					double x2 = brakePositionX + (*i) -> v0 * t2 * (*i) -> xDirection - 0.5 * airAcceleration * t2 * t2 * (*i) -> xDirection;
					double y2 = (*i) -> start -> y + 1 + jumpImpulse * ((*i) -> t - (*i) -> tA + t2) - 0.5 * gravitationalAcceleration * pow((*i) -> t - (*i) -> tA + t2, 2);

					Graphics::Line line = { (int32_t)(x1 * (double)Render::BLOCK_SIZE) - offsetX, (int32_t)(y1 * (double)Render::BLOCK_SIZE) - offsetY, (int32_t)(x2 * (double)Render::BLOCK_SIZE) - offsetX, (int32_t)(y2 * (double)Render::BLOCK_SIZE) - offsetY, PathController::DEBUG_COLOR_MAP[colorIndex][0], PathController::DEBUG_COLOR_MAP[colorIndex][1], PathController::DEBUG_COLOR_MAP[colorIndex][2] };
					Graphics::DrawLines(&line, 1);
				}

				Graphics::FlushLines();
			}

			break;
		}
		}

		colorIndex++;
		if(colorIndex >= PathController::DEBUG_COLORS_COUNT) {
			colorIndex = 0;
		}
	}
}

void PathController::StartWalking(double xWalkPosition) {
	this -> walk = true;
	this -> walkTargetX = xWalkPosition;

	double targetFrictionCoefficient = numeric_limits<double>::infinity();

	if(this -> standingPlatform1 != nullptr) {
		uint32_t block = standingPlatform1 -> block;
		targetFrictionCoefficient = this -> physicsMemory -> GetConstantKnowledge(PhysicsBehaviour::Constant::S_PER_MATERIAL_FRICTION_COEFFICIENT, block) -> value;
	}
	if(this -> standingPlatform2 != nullptr) {
		uint32_t block = standingPlatform2 -> block;
		targetFrictionCoefficient = min(targetFrictionCoefficient, this -> physicsMemory -> GetConstantKnowledge(PhysicsBehaviour::Constant::S_PER_MATERIAL_FRICTION_COEFFICIENT, block) -> value);
	}

	// Berechne maximale Geschwindigkeit, so dass noch gebremst werden kann

	this -> walkVelocity = sqrt(2.0 * targetFrictionCoefficient * (gravitationalAcceleration + horizontalForceGround) * PathController::APPROACH_BRAKE_DISTANCE * 0.5);
	this -> walkApproachVelocity = sqrt(2.0 * (gravitationalAcceleration * targetFrictionCoefficient) * 0.05);
}

void PathController::Walk(double deltaTime) {
	if(this -> walkTargetX < x) {
		this -> walkDirection = -1.0;
	} else {
		this -> walkDirection = 1.0;
	}

	this -> walkDistance = this -> walkTargetX - this -> x;

	if(this -> walkDirection < 0.0 && this -> collisionLeft && this -> vX < PathController::EPSILON) {
		this -> StopWalking();
		return;
	}
	if(this -> walkDirection > 0.0 && this -> collisionRight && this -> vX < PathController::EPSILON) {
		this -> StopWalking();
		return;
	}
	if(!this -> collisionDown) {
		this -> StopWalking();
		return;
	}

	// Wenn die KI noch weit genug vom Ziel entfernt ist, beschleunige oder halte Geschwindigkeit
	if(abs(this -> walkDistance) > PathController::APPROACH_BRAKE_DISTANCE) {
		if(abs(this -> vX) < this -> walkVelocity) {
			if(this -> walkDirection > 0.0) {
				this -> view -> DoInput(Character::Input::WALK_RIGHT);
			} else {
				this -> view -> DoInput(Character::Input::WALK_LEFT);
			}
		}
	} else {
		if(abs(this -> vX) > PathController::SPEED_BIAS) {
			if(this -> walkDirection > 0.0) {
				this -> view -> DoInput(Character::Input::WALK_LEFT);
			} else {
				this -> view -> DoInput(Character::Input::WALK_RIGHT);
			}
		} else {
			if(abs(this -> vX) < this -> walkApproachVelocity) {
				if(abs(this -> walkDistance) < PathController::DISTANCE_BIAS) {
					if(abs(vX) < PathController::EPSILON) {
						// Ziel erreicht
						this -> StopWalking();
					}
				} else {
					// Bewegung Richtung Ziel
					if(this -> walkDirection > 0.0) {
						this -> view -> DoInput(Character::Input::WALK_RIGHT);
					} else {
						this -> view -> DoInput(Character::Input::WALK_LEFT);
					}
				}
			}
		}
	}
}

bool PathController::NextTrajectory() {
	if(this -> pathIterator != prev(this -> path.end())) {
		this -> pathIterator++;
	} else {
		return true;
	}
	this -> currentTrajectory = *this -> pathIterator;
	this -> currentPlatform = this -> currentTrajectory -> start;
	this -> currentState = State::STATIONARY;

	return false;
}

bool PathController::CheckLanding() {
	if((int32_t)round(this -> y) - 1 != this -> currentTrajectory -> end -> y) {
		return false;
	}
	if(this -> standingPlatform1 != this -> currentTrajectory -> end && this -> standingPlatform2 != this -> currentTrajectory -> end) {
		WorldMemory::Platform* currentPlatformChecked = this -> standingPlatform1;
		if(currentPlatformChecked == nullptr) {
			currentPlatformChecked = this -> standingPlatform2;
		}
		while(true) {
			if(currentPlatformChecked == this -> currentTrajectory -> end) {
				break;
			}

			int32_t nextBlockX;
			if(this -> currentTrajectory -> xDirection > 0.0) {
				nextBlockX = currentPlatformChecked -> x - 1;
			} else {
				nextBlockX = currentPlatformChecked -> x + currentPlatformChecked -> width;
			}

			// Ein Block ist im Weg, es ist unm�glich von der Landeposition zur Zielplattform zu gelangen
			if(this -> worldMemory -> GetBlock(nextBlockX, currentPlatformChecked -> y + 1).block != 0) {
				return false;
			}

			WorldMemory::Platform* nextPlatform = this -> worldMemory -> GetPlatformAt(nextBlockX, currentPlatformChecked -> y);
			if(nextPlatform == nullptr) {
				nextPlatform = this -> worldMemory -> GetPlatformAt(nextBlockX - (int32_t)round(this -> currentTrajectory -> xDirection), currentPlatformChecked -> y);
			}

			if(nextPlatform == nullptr) {
				return false;
			}

			currentPlatformChecked = nextPlatform;
		}
	}

	return true;
}

bool PathController::FollowAdjacentTrajectory(double deltaTime) {
	if(this -> currentState == State::GETTING_TO_START_POSITION) {
		if(!this -> walk) {
			// Pr�fe, ob die Position richtig ist
			if(abs(this -> x - this -> currentTrajectory -> startPosition) > PathController::DISTANCE_BIAS) {
				this -> currentTrajectory -> possible = false;
				this -> EndPath();
				return true;
			}

			// Fertig mit Flugbahn
			if(this -> NextTrajectory()) {
				this -> EndPath();
				return true;
			}
		}
	}

	return false;
}

bool PathController::FollowStairTrajectory(double deltaTime) {
	switch(this -> currentState) {
	case State::GETTING_TO_START_POSITION:
	{
		if(!this -> walk) {
			// Pr�fe, ob die Position richtig ist
			if(abs(this -> x - this -> currentTrajectory -> liftoffPosition) > PathController::DISTANCE_BIAS && abs(this -> x - this -> currentTrajectory -> startPosition) > PathController::DISTANCE_BIAS) {
				this -> currentTrajectory -> possible = false;
				this -> EndPath();
				return true;
			}

			this -> view -> DoInput(Character::Input::JUMP);
			this -> t = 0.0;
			this -> currentState = State::FREE_FLIGHT;
		}
		break;
	}
	case State::FREE_FLIGHT:
	{
		this -> t += deltaTime;
		if(this -> t >= (this -> currentTrajectory -> t - this -> currentTrajectory -> tA)) {
			this -> t = 0.0;
			this -> currentState = State::ACCELERATED_FLIGHT;
		}
		break;
	}
	case State::ACCELERATED_FLIGHT:
	{
		// Solange die Bresmzeit noch nicht vor�ber ist
		this -> t += deltaTime;
		if(this -> t < this -> currentTrajectory -> tA) {
			if(abs(this -> vX) < this -> currentTrajectory -> v0) {
				if(this -> currentTrajectory -> xDirection > 0.0) {
					this -> view -> DoInput(Character::Input::WALK_RIGHT);
				} else {
					this -> view -> DoInput(Character::Input::WALK_LEFT);
				}
			}
		} else {
			// Sollte gelandet sein
			this -> currentState = State::TOUCHED_DOWN;
		}
		break;
	}
	}

	return false;
}

bool PathController::FollowFallAndJumpTrajectory(double deltaTime) {
	switch(this -> currentState) {
	case State::GETTING_TO_START_POSITION:
	{
		if(!this -> walk) {
			// Pr�fe, ob die Position richtig ist
			if(abs(this -> x - this -> currentTrajectory -> startPosition) > PathController::DISTANCE_BIAS) {
				this -> currentTrajectory -> possible = false;
				this -> EndPath();
				return true;
			}

			this -> currentState = State::ACCELERATING;
		}
		break;
	}
	case State::ACCELERATING:
	{
		double distance;
		if(this -> currentTrajectory -> xDirection > 0.0) {
			distance = this -> currentTrajectory -> liftoffPosition - this -> x;
		} else {
			distance = this -> x - this -> currentTrajectory -> liftoffPosition;
		}

		if(distance > PathController::EPSILON && this -> collisionDown) {
			if((this -> currentTrajectory -> xDirection > 0.0 && this -> collisionRight) || (this -> currentTrajectory -> xDirection < 0.0 && this -> collisionLeft)) {
				this -> currentTrajectory -> possible = false;
				this -> EndPath();
				return true;
			}

			// Abflugpunkt noch nicht erreicht
			if(abs(this -> vX) < this -> currentTrajectory -> v0) {
				// Zu langsam
				if(this -> currentTrajectory -> xDirection > 0.0) {
					this -> view -> DoInput(Character::Input::WALK_RIGHT);
				} else {
					this -> view -> DoInput(Character::Input::WALK_LEFT);
				}
			}
		} else {
			// Abflugpunkt erreicht
			if(this -> currentTrajectory -> type == WorldMemory::Trajectory::Type::JUMP) {
				this -> view -> DoInput(Character::Input::JUMP);
			}
			this -> t = 0.0;
			this -> currentState = State::FREE_FLIGHT;
		}
		break;
	}
	case State::FREE_FLIGHT:
	{
		this -> t += deltaTime;
		if(this -> t < (this -> currentTrajectory -> t - this -> currentTrajectory -> tA)) {
			// Bremspunkt noch nicht erreicht

			// Nimm Geschwindigkeitskorrekturen vor
			double targetVelocity = this -> currentTrajectory -> xDirection * this -> currentTrajectory -> v0;
			if(this -> vX - targetVelocity > PathController::AIR_SPEED_BIAS) {
				this -> view -> DoInput(Character::Input::WALK_LEFT);
			}
			if(targetVelocity - this -> vX > PathController::AIR_SPEED_BIAS) {
				this -> view -> DoInput(Character::Input::WALK_RIGHT);
			}
		} else {
			this -> t = 0.0;
			this -> currentState = State::ACCELERATED_FLIGHT;

			if(this -> currentTrajectory -> type == WorldMemory::Trajectory::Type::STAIR) {
				if(this -> currentTrajectory -> xDirection > 0.0) {
					this -> view -> DoInput(Character::Input::WALK_RIGHT);
				} else {
					this -> view -> DoInput(Character::Input::WALK_LEFT);
				}
			}
		}
		break;
	}
	case State::ACCELERATED_FLIGHT:
	{
		// Solange die Bresmzeit noch nicht vor�ber ist
		this -> t += deltaTime;

		double neededVelocity = this -> currentTrajectory -> v0 - this -> t * this -> horizontalForceAir;

		if(this -> t < this -> currentTrajectory -> tA) {
			if(this -> currentTrajectory -> xDirection > 0.0) {
				if(this -> vX < neededVelocity) {
					this -> view -> DoInput(Character::Input::WALK_RIGHT);
				} else {
					this -> view -> DoInput(Character::Input::WALK_LEFT);
				}
			} else {
				if(this -> vX < -neededVelocity) {
					this -> view -> DoInput(Character::Input::WALK_RIGHT);
				} else {
					this -> view -> DoInput(Character::Input::WALK_LEFT);
				}
			}
		} else {
			// Sollte gelandet sein
			this -> currentState = State::TOUCHED_DOWN;
		}
		break;
	}
	}

	return false;
}

const float PathController::DEBUG_COLOR_MAP[DEBUG_COLORS_COUNT][3] = {
	{0.8f, 0.0f, 0.0f},
	{0.8f, 0.8f, 0.0f},
	{0.0f, 0.8f, 0.0f},
	{0.0f, 0.8f, 0.8f},
	{0.4f, 0.4f, 0.8f},
	{0.8f, 0.0f, 0.8f}
};

const double PathController::APPROACH_BRAKE_DISTANCE = 1.0;
const double PathController::SPEED_BIAS = 0.5;
const double PathController::AIR_SPEED_BIAS = 0.01;
const double PathController::DISTANCE_BIAS = 0.05;
const double PathController::EPSILON = 0.000000001;

}
#pragma once

#include <stdafx.h>

#include <system/log.h>
#include <system/event.h>
#include <simulation/world.h>
#include <simulation/physicsBehaviour.h>
#include <simulation/character.h>
#include <ai/worldMemory.h>

namespace Sim {

class Character;

}

namespace AI {

class PhysicsMemory;
class View;
class PathFinding;
class PathController;

// Klasse f�r k�nstliche Intelligenz
// Enth�lt alle wichtigen Komponenten
class Ai : public System::EventSender {
public:
	Ai(Sim::World* world, Sim::Character* character);
	~Ai();

	// Zust�nde, in denen sich die KI befinden kann
	enum class State {
		SEARCHING_PATH, // Suche nach bestem Weg
		FOLLOWING_PATH, // Folgen von Weg
		RECOVERING, // Auf irgendeiner Plattform anhalten (funktioniert auch, wenn KI bereits auf einer Plattform steht)
		MEASURING // Aktiv Experimente durchf�hren (Standard-Zustand)
	};

	const static uint32_t EVENT_REQUEST_RESET = 0x0400;

	const static uint32_t VISION_DISTANCE = 8; // Distanz, wie weit die KI sieht (Sichtfeld ist ein Quadrat und kein Kreis)

	const static double CHARACTER_SIZE_BIAS;
	const static double EPSILON; // Unsch�rfe bei vergleichen von double-Werten sowie beim erkennen von Bl�cken

	// Gibt Status der KI zur�ck
	inline bool IsActivated() {
		return this -> activated;
	}
	// Aktiviere KI
	inline void Activate() {
		this -> activated = true;
		System::Log::Write("AI activated");
	}
	// Deaktiviert AI
	inline void Deactivate() {
		this -> activated = false;
		System::Log::Write("AI deactivated");
	}
	// Signalisiert der KI, dass die Position zur�ckgesetzt wurde
	inline void Reset() {
		this -> reset = true;
		System::Log::Write("AI recieved signal for position discontinuity (reset)");
	}

	// L�scht Weltged�chtnis der KI
	void ClearWorldMemory();
	// L�dt Weltged�chtnis von Datei
	void LoadWorldMemory(string filename);
	// Speichert Weltged�chtnis in Datei
	void SaveWorldMemory(string filename);

	// L�scht Physikged�chtnis der KI
	void ClearPhysicsMemory();
	// L�dt Physikged�chtnis von Datei
	void LoadPhysicsMemory(string filename);
	// Speichert Physikged�chtnis in Datei
	void SavePhysicsMemory(string filename);

	// Wird jede Frame aufgerufen
	void Update(double deltaTime);

	// Zeichne Debuginformationen
	void DrawDebugInformation();

private:
#pragma region Path Finding
	// Sehfunktion der KI
	// R�ckgabewert bestimmt, ob einer neuer Weg gefunden werden muss
	bool RegisterEnvironment();

	// Registriert Plattform(en), auf der die KI steht
	void RegisterStandingPlatforms();

	// Berechne Flugbahnen zwischen Plattformen
	void CalculateTrajectories(WorldMemory::PlatformRelation* relation);
	void CalculateTrajectory(WorldMemory::Trajectory* trajectory);

	// Funktionen, die die Flugbahnen mit zwei M�glichkeiten pr�fen
	// Unterfunktionen von CalculateTrajectory
	void CalculateTrajectoryType2(WorldMemory::Trajectory* trajectory);
	void CalculateTrajectoryType3(WorldMemory::Trajectory* trajectory);

	// Berechnet Startposition
	bool CalculateTrajectoryStartPosition(WorldMemory::Trajectory* trajectory, WorldMemory::Platform* start, double liftoffPosition, double v0, double* startPosition);

	// Berechnet, wie gross die minimale Stargeschwindigkeit auf einer Plattform bei gegebener Endgeschwindigkeit ist
	// Falls die Stargeschwindigkeit null ist, wird in length die minimale Distanz angegeben
	double StartVelocityOnPlatform(double frictionCoefficient, double* length, double vE);
#pragma endregion

#pragma region Physics Measurement
	// Testet Reaktionen auf Benutzereingben
	// Im Parameter wird ein Array zur�ckgegeben, das angibt ob die Reaktionen im Moment erfolgen k�nnen
	bool* TestInputReactions();

	// Misst alle m�glichen Konstanten
	// Gibt n�chste Aktion zur�ck
	bool* MeasureData(bool* inputReactionsPossible, bool* recalculateTrajectories);

	// F�hrt Aktion aus und gibt false zur�ck, falls die KI gerade ein Experiment macht, ansonsten true
	bool DoExperiments(bool* actions);

	// Teste Bedingung und sammle allf�llige Infomrationen
	struct ConditionCheckReturn {
		bool fulfilled;
		bool needsInputNextFrame;
		bool allConstantsKnown;
		bool givesCollisionInformation;
		Sim::Character::Side collisionSide;
		bool givesImpulseChangeInformation;
		Sim::Character::Axis impulseChangeAxis;
	};
	ConditionCheckReturn* CheckCondition(Sim::PhysicsBehaviour::Condition const& condition);

	// Berechnet Konstante mit Formel
	// R�ckgabewert gibt an, ob die Flugbahnen neu berechnet werden m�ssen
	bool CalculateConstantFromFormula(Sim::PhysicsBehaviour::ConstantFormula const* formula, double* inputValues, uint32_t frictionBlock1, uint32_t frictionBlock2, uint32_t impulseChangeBlock1, uint32_t impulseChangeBlock2);

	// Testet, ob Konstante noch aktiv gemessen werden muss
	bool ShouldMeasureConstant(Sim::PhysicsBehaviour::Constant constant, uint32_t frictionBlock1, uint32_t frictionBlock2, uint32_t impulseChangeBlock1, uint32_t impulseChangeBlock2);

	// Sch�tzt, ob Aktion eine Bedingung erf�llt
	bool FulfillsAfterAction(Sim::PhysicsBehaviour::InputReaction const* inputReaction, Sim::PhysicsBehaviour::Condition const& condition);
#pragma endregion

	// Statusvariabeln
	bool activated;
	bool reset;
	bool resetCompleted;
	uint32_t resetCountdown; // Frames, die gewartet werden m�ssen, bis nach einem Reset alle Variabeln wieder kontinuierlich sind
	State currentState;
	uint32_t stateEndCounter; // Frames, die gewartet werden m�ssen, damit der Status gewechselt werden kann

	// Statusabh�ngige Variabeln
	bool restartPathFinding;

	// Charakter, wird nur verwendet, um Debugwerte anzuzeigen
	Sim::Character* debugCharacter;

	// Weltged�chtnis, wird verwendet, um Weg zum Ziel zu finden
	WorldMemory* worldMemory;

	// Physikged�chtnis
	// Speichert aktuelle Werte f�r phyiskalische Konstanten, ob sie bereits bekannt sind und wie verl�sslich der Wert ist
	PhysicsMemory* physicsMemory;

	// Objekt, das f�r den Wegsuchalgorithmus zust�ndig ist
	AI::PathFinding* pathFinding;

	// Objekt, das den Charakter einem Pfad entlang steuern kann
	Ai::PathController* pathController;

	// Referenz zur wirklichen Welt, wird verwendet, um Weltged�chtnis zu vervollst�ndigen
	Sim::World* world;

	// "Sichtfeld" der KI
	// Enth�lt alle Variabeln, die der KI zur verf�gung stehen
	// Ausserdem kann hier mit dem Spieler interagiert werden
	View* view;

	// Plattformen, auf der die KI gerade steht
	WorldMemory::Platform* standingPlatforms[2];

	// Plattform, die als Ziel verwendet wird
	WorldMemory::Platform* currentTarget;
};

}
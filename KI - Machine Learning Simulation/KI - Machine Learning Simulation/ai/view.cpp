#include <ai/view.h>

#include <simulation/character.h>
#include <ai/physicsMemory.h>

using namespace System;
using namespace Sim;

namespace AI {

View::View(Character* character, PhysicsMemory* physicsMemory) : character(character), physicsMemory(physicsMemory) {
	memset(this -> data, 0, PhysicsBehaviour::VARIABLE_COUNT * sizeof(double));
}

void View::Update(double deltaTime) {
	// Wird nicht ausgeführt, wenn KI nicht aktiviert ist
	// Daher ist deltaTime immer grösser als 0.0

	// Schiebe alte Werte eine Stufe nach hinten
	for(uint32_t i = 0; i < PhysicsBehaviour::VARIABLE_COUNT; i++) {
		for(uint32_t j = View::MAX_VARIABLE_HISTORY_LENGTH - 1; j > 0; j--) {
			this -> data[j][i] = this -> data[j - 1][i];
		}
	}

	/*this -> data[(uint32_t)Variable::S_POSITION_X_0] = this -> data[(uint32_t)Variable::S_POSITION_X];
	this -> data[(uint32_t)Variable::S_POSITION_Y_0] = this -> data[(uint32_t)Variable::S_POSITION_Y];
	this -> data[(uint32_t)Variable::S_VELOCITY_X_0] = this -> data[(uint32_t)Variable::S_VELOCITY_X];
	this -> data[(uint32_t)Variable::S_VELOCITY_Y_0] = this -> data[(uint32_t)Variable::S_VELOCITY_Y];
	this -> data[(uint32_t)Variable::S_ACCELERATION_X_0] = this -> data[(uint32_t)Variable::S_ACCELERATION_X];
	this -> data[(uint32_t)Variable::S_ACCELERATION_Y_0] = this -> data[(uint32_t)Variable::S_ACCELERATION_Y];
	this -> data[(uint32_t)Variable::B_COLLISION_U_0] = this -> data[(uint32_t)Variable::B_COLLISION_U];
	this -> data[(uint32_t)Variable::B_COLLISION_D_0] = this -> data[(uint32_t)Variable::B_COLLISION_D];
	this -> data[(uint32_t)Variable::B_COLLISION_L_0] = this -> data[(uint32_t)Variable::B_COLLISION_L];
	this -> data[(uint32_t)Variable::B_COLLISION_R_0] = this -> data[(uint32_t)Variable::B_COLLISION_R];
	this -> data[(uint32_t)Variable::B_IMPULSE_CHANGE_X_0] = this -> data[(uint32_t)Variable::B_IMPULSE_CHANGE_X];
	this -> data[(uint32_t)Variable::B_IMPULSE_CHANGE_Y_0] = this -> data[(uint32_t)Variable::B_IMPULSE_CHANGE_Y];
	this -> data[(uint32_t)Variable::B_INPUT_U_0] = this -> data[(uint32_t)Variable::B_INPUT_U];
	this -> data[(uint32_t)Variable::B_INPUT_L_0] = this -> data[(uint32_t)Variable::B_INPUT_L];
	this -> data[(uint32_t)Variable::B_INPUT_R_0] = this -> data[(uint32_t)Variable::B_INPUT_R];*/

	// Lade verstrichene Zeit
	this -> data[0][(uint32_t)PhysicsBehaviour::Variable::Name::S_DELTA_TIME] = deltaTime;

	// Lade Position
	Vector2 position = this -> character -> GetPosition(); // Berechne daraus nachher Geschwindigkeit und Beschleunigung
	this -> data[0][(uint32_t)PhysicsBehaviour::Variable::Name::S_POSITION_X] = position.GetX();
	this -> data[0][(uint32_t)PhysicsBehaviour::Variable::Name::S_POSITION_Y] = position.GetY();

	// Lade Geschwindigkeit
	Vector2 velocity = (position - Vector2(this -> data[1][(uint32_t)PhysicsBehaviour::Variable::Name::S_POSITION_X], this -> data[1][(uint32_t)PhysicsBehaviour::Variable::Name::S_POSITION_Y])) / deltaTime;
	this -> data[0][(uint32_t)PhysicsBehaviour::Variable::Name::S_VELOCITY_X] = velocity.GetX();
	this -> data[0][(uint32_t)PhysicsBehaviour::Variable::Name::S_VELOCITY_Y] = velocity.GetY();

	// Lade Beschleunigung
	Vector2 acceleration = (velocity - Vector2(this -> data[1][(uint32_t)PhysicsBehaviour::Variable::Name::S_VELOCITY_X], this -> data[1][(uint32_t)PhysicsBehaviour::Variable::Name::S_VELOCITY_Y])) / deltaTime;
	this -> data[0][(uint32_t)PhysicsBehaviour::Variable::Name::S_ACCELERATION_X] = acceleration.GetX();
	this -> data[0][(uint32_t)PhysicsBehaviour::Variable::Name::S_ACCELERATION_Y] = acceleration.GetY();

	// Lade Kollisionen
	this -> data[0][(uint32_t)PhysicsBehaviour::Variable::Name::B_COLLISION_U] = this -> BoolToDouble(this -> character -> GetCollision(Character::Side::TOP));
	this -> data[0][(uint32_t)PhysicsBehaviour::Variable::Name::B_COLLISION_D] = this -> BoolToDouble(this -> character -> GetCollision(Character::Side::BOTTOM));
	this -> data[0][(uint32_t)PhysicsBehaviour::Variable::Name::B_COLLISION_L] = this -> BoolToDouble(this -> character -> GetCollision(Character::Side::LEFT));
	this -> data[0][(uint32_t)PhysicsBehaviour::Variable::Name::B_COLLISION_R] = this -> BoolToDouble(this -> character -> GetCollision(Character::Side::RIGHT));

	// Lade Impulsänderungen
	this -> data[0][(uint32_t)PhysicsBehaviour::Variable::Name::B_IMPULSE_CHANGE_X] = this -> BoolToDouble(this -> character -> GetImpulseChange(Character::Axis::X_AXIS));
	this -> data[0][(uint32_t)PhysicsBehaviour::Variable::Name::B_IMPULSE_CHANGE_Y] = this -> BoolToDouble(this -> character -> GetImpulseChange(Character::Axis::Y_AXIS));

	// Lade Benutzereingaben
	this -> data[0][(uint32_t)PhysicsBehaviour::Variable::Name::B_INPUT_U] = this -> BoolToDouble(this -> character -> GetInput(Character::Input::JUMP));
	this -> data[0][(uint32_t)PhysicsBehaviour::Variable::Name::B_INPUT_L] = this -> BoolToDouble(this -> character -> GetInput(Character::Input::WALK_LEFT));
	this -> data[0][(uint32_t)PhysicsBehaviour::Variable::Name::B_INPUT_R] = this -> BoolToDouble(this -> character -> GetInput(Character::Input::WALK_RIGHT));
}

}
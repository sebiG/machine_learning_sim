#pragma once

#include <stdafx.h>

#include <system/log.h>
#include <ai/worldMemory.h>

namespace Sim {

class Character;

}

namespace AI {

class View;
class PhysicsMemory;

// Steuert KI entlang eines Pfades
class PathController {
public:
	PathController(Sim::Character* debugCharacter, View* view, PhysicsMemory* physicsMemory, WorldMemory* worldMemory);
	~PathController() = default;

	inline void SetPath(list<WorldMemory::Trajectory*> const& path) {
		this -> path = path;
		this -> pathIterator = this -> path.begin();
		this -> currentTrajectory = *this -> pathIterator;
		this -> currentPlatform = this -> currentTrajectory -> start;
		this -> t = 0.0;
		this -> currentState = State::STATIONARY;
		System::Log::Write("Path controller path set");
	}
	inline void ExplorePlatform(WorldMemory::Platform* platform) {
		this -> path.clear();
		this -> currentState = State::STATIONARY;
		this -> pathIterator = this -> path.begin();
		this -> currentTrajectory = nullptr;
		this -> currentPlatform = platform;
	}
	inline void WalkToPosition(double xWalkPosition) {
		this -> path.clear();
		this -> currentState = State::STATIONARY;
		this -> pathIterator = this -> path.begin();
		this -> currentTrajectory = nullptr;
		this -> currentPlatform = nullptr;
		this -> StartWalking(xWalkPosition);
	}

	// Gibt an, ob der Charakter gerade auf einer Plattform steht
	bool IsStationary();

	// Steuere Charakter dem Pfad entlang
	// R�ckgabewert gibt an, ob Pfad beendet ist
	bool Update(double deltaTime);
	// Zeichne Debuginformationen
	void DrawDebugInformation();

private:
	const static uint32_t DEBUG_COLORS_COUNT = 6;
	const static float DEBUG_COLOR_MAP[DEBUG_COLORS_COUNT][3];

	const static double APPROACH_BRAKE_DISTANCE; // Distanz, bei der die Ki zu bremsen beginnt, wenn sie geht
	const static double SPEED_BIAS; // Geschwindigkeit, die erreicht werden muss, bevor die KI ihre Geschwindigkeit als null sieht
	const static double AIR_SPEED_BIAS; // Gleich wie oben, nur in der Luft
	const static double DISTANCE_BIAS; // Distanz, ab der die KI Distanzen als null sieht
	const static double EPSILON;

	void StartWalking(double xWalkPosition);
	inline void StopWalking() {
		this -> walk = false;
	}

	void Walk(double deltaTime);

	// R�ckgabewert gibt an, ob Pfad zu Ende ist
	bool NextTrajectory();

	inline void EndPath() {
		this -> path.clear();
		this -> currentState = State::STATIONARY;
		this -> pathIterator = this -> path.begin();
		this -> currentTrajectory = nullptr;
		this -> currentPlatform = nullptr;
	}

	bool CheckLanding();

	// Je nachdem was f�r eine Art von Flugbahn ausgef�hrt wird, werden die Werte in Trajectory anders interpretiert
	// R�ckgabewert gibt an, ob Pfad zu Ende ist
	bool FollowAdjacentTrajectory(double deltaTime);
	bool FollowStairTrajectory(double deltaTime);
	bool FollowFallAndJumpTrajectory(double deltaTime);

	enum class State {
		EXPLORING_LEFT,
		EXPLORING_RIGHT,
		GETTING_TO_START_POSITION,
		ACCELERATING,
		FREE_FLIGHT,
		ACCELERATED_FLIGHT,
		TOUCHED_DOWN,
		STATIONARY // Wird m�glicherweise nicht verwendet
	};

	Sim::Character* debugCharacter;
	View* view;
	PhysicsMemory* physicsMemory;
	WorldMemory* worldMemory;

	list<WorldMemory::Trajectory*> path;
	list<WorldMemory::Trajectory*>::iterator pathIterator;
	WorldMemory::Trajectory* currentTrajectory;
	WorldMemory::Platform* currentPlatform;
	double t;
	State currentState;

	bool walk; // Gibt an, ob die KI zu einer gegebenen Position gehen soll
	double walkTargetX;
	double walkDirection;
	double walkDistance;
	double walkVelocity;
	double walkApproachVelocity;

	// Statusvariabeln
	double x;
	double y;
	double vX;
	double vY;
	bool collisionUp;
	bool collisionDown;
	bool collisionRight;
	bool collisionLeft;
	WorldMemory::Platform* standingPlatform1;
	WorldMemory::Platform* standingPlatform2;
	double gravitationalAcceleration;
	double horizontalForceGround;
	double horizontalForceAir;
	double jumpImpulse;
};

}
#pragma once

#include <stdafx.h>

#include <system/errors.h>
#include <simulation/vector2.h>
#include <simulation/physicsBehaviour.h>
#include <simulation/character.h>

namespace AI {

class PhysicsMemory;

// Klasse, die als Schnittstelle zwischen KI und Physiksimulation fungiert
class View {
public:
	View(Sim::Character* character, PhysicsMemory* physicsMemory);
	~View() = default;

	const static uint32_t MAX_VARIABLE_HISTORY_LENGTH = 4;

	// Wird jede Frame aufgerufen, um der KI aktualisierte Werte zu liefern
	void Update(double deltaTime);

	// Gibt Variable zur�ck
	inline double GetData(Sim::PhysicsBehaviour::Variable variable) {
		return data[variable.frame][(uint32_t)variable.name];
	}

	// Steuert den Spieler
	inline void ResetInput() {
		this -> character -> ResetInput();
	}
	inline void DoInput(Sim::Character::Input input) {
		this -> character -> SetInput(input, true);
	}

private:
	// Zugriffsfunktion

	// Funktionen, um zwischen double und bool umzurechnen
	inline double BoolToDouble(bool b) {
		return (b) ? 1.0 : 0.0;
	}

	Sim::Character* character;
	PhysicsMemory* physicsMemory;

	// Alle Werte, die die KI benutzen kann
	// Entspricht der Enumeration "Variable" in simulation\physicsBehaviour.h
	double data[View::MAX_VARIABLE_HISTORY_LENGTH][Sim::PhysicsBehaviour::VARIABLE_COUNT];
};

}
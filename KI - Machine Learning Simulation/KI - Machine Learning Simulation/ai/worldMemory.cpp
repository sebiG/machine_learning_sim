#include <ai/worldMemory.h>

#include <system/log.h>
#include <system/window.h>
#include <system/graphics.h>
#include <simulation/render.h>

using namespace System;
using namespace Sim;

namespace AI {

WorldMemory::Trajectory::Trajectory() {
	this -> Reset();
}

void WorldMemory::Trajectory::Reset() {
	this -> possible = true;
	this -> type = Type::NONE;

	this -> xDirection = 0.0;
	this -> dX = 0.0;
	this -> dY = 0.0;
	this -> dX_ = 0.0;

	this -> t = 0.0;
	this -> tA = 0.0;
	this -> v0 = 0.0;
	this -> vE = 0.0;

	this -> startPosition = 0.0;
	this -> liftoffPosition = 0.0;
	this -> touchdownPosition = 0.0;
}

WorldMemory::PlatformRelation::PlatformRelation(uint32_t id, Platform* platform1, Platform* platform2) : id(id), platform1(platform1), platform2(platform2), distance(numeric_limits<double>::infinity()) {
	this -> CalculateDistance();

	this -> trajectoryTo1.start = platform2;
	this -> trajectoryTo1.end = platform1;
	this -> trajectoryTo2.start = platform1;
	this -> trajectoryTo2.end = platform2;
}

void WorldMemory::PlatformRelation::Load(ifstream* is) {
	is -> read((char*)&this -> id, sizeof(uint32_t));

	is -> read((char*)&this -> platform1, sizeof(uint64_t));
	is -> read((char*)&this -> platform2, sizeof(uint64_t));

	is -> read((char*)&this -> trajectoryTo1, sizeof(Trajectory));
	is -> read((char*)&this -> trajectoryTo2, sizeof(Trajectory));

	is -> read((char*)&this -> distance, sizeof(double));
	is -> read((char*)&this -> heuristicDistance, sizeof(double));
}

void WorldMemory::PlatformRelation::Save(ofstream* os) {
	os -> write((char*)&this -> id, sizeof(uint32_t));

	uint64_t id = this -> platform1 -> GetID();
	os -> write((char*)&id, sizeof(uint64_t));
	id = this -> platform2 -> GetID();
	os -> write((char*)&id, sizeof(uint64_t));

	os -> write((char*)&this -> trajectoryTo1, sizeof(Trajectory));
	os -> write((char*)&this -> trajectoryTo2, sizeof(Trajectory));

	os -> write((char*)&this -> distance, sizeof(double));
	os -> write((char*)&this -> heuristicDistance, sizeof(double));
}

void WorldMemory::PlatformRelation::ResolvePointers(WorldMemory* worldMemory) {
	for(list<Platform*>::iterator i = worldMemory -> GetPlatformBeginIterator(); i != worldMemory -> GetPlatformEndIterator(); i++) {
		if((*i) -> GetID() == (uint64_t)this -> platform1) {
			this -> platform1 = (*i);
		}
		if((*i) -> GetID() == (uint64_t)this -> platform2) {
			this -> platform2 = (*i);
		}
	}

	this -> trajectoryTo1.start = this -> platform2;
	this -> trajectoryTo1.end = this -> platform1;
	this -> trajectoryTo2.start = this -> platform1;
	this -> trajectoryTo2.end = this -> platform2;
}

void WorldMemory::PlatformRelation::CalculateDistance() {
	Platform const* p1 = this -> platform1;
	Platform const* p2 = this -> platform2;

	double dX;
	double dY;

	double left1 = platform1 -> x;
	double left2 = platform2 -> x;
	double right1 = platform1 -> x + platform1 -> width;
	double right2 = platform2 -> x + platform2 -> width;

	// dY
	dY = (double)platform2 -> y - (double)platform1 -> y;

	// dX
	dX = 0.0;
	if(dY >= 0.0) {
		// Nach oben
		if(left1 < left2) {
			// Nach rechts
			dX = left2 - right1;
		} else if(right1 > right2) {
			// Nach links
			dX = left1 - right2;
		}
	} else {
		if(right1 < right2) {
			// Nach rechts
			dX = left2 - right1;
		} else if(left1 > left2) {
			// Nach links
			dX = left1 - right2;
		}
	}
	if(dX < 0.0) {
		dX = 0.0;
	}

	dX -= 1.0;
	if(dX < 0.0) {
		dX = 0.0;
	}

	this -> distance = dX * dX * dX * dX + dY * dY * dY * dY;
	this -> heuristicDistance = sqrt(dX * dX + dY * dY);
}

WorldMemory::Platform::Platform(uint32_t id) : visited(false), id(id), logicallyAccessible(false) {}

void WorldMemory::Platform::Load(ifstream* is) {
	is -> read((char*)&this -> x, sizeof(uint32_t));
	is -> read((char*)&this -> y, sizeof(uint32_t));
	is -> read((char*)&this -> width, sizeof(uint32_t));
	is -> read((char*)&this -> block, sizeof(uint32_t));

	is -> read((char*)&this -> id, sizeof(uint32_t));
	is -> read((char*)&this -> visited, sizeof(bool));
	is -> read((char*)&this -> logicallyAccessible, sizeof(bool));

	uint32_t relationsCount;
	is -> read((char*)&relationsCount, sizeof(uint32_t));
	for(uint32_t i = 0; i < relationsCount; i++) {
		uint64_t id;
		is -> read((char*)&id, sizeof(uint64_t));
		this -> relations.push_back((PlatformRelation*)id);
	}

	uint32_t chunksCount;
	is -> read((char*)&chunksCount, sizeof(uint32_t));
	for(uint32_t i = 0; i < chunksCount; i++) {
		uint64_t id;
		is -> read((char*)&id, sizeof(uint64_t));
		this -> overlappingChunks.push_back((Chunk*)id);
	}
}

void WorldMemory::Platform::Save(ofstream* os) {
	os -> write((char*)&this -> x, sizeof(uint32_t));
	os -> write((char*)&this -> y, sizeof(uint32_t));
	os -> write((char*)&this -> width, sizeof(uint32_t));
	os -> write((char*)&this -> block, sizeof(uint32_t));

	os -> write((char*)&this -> id, sizeof(uint32_t));
	os -> write((char*)&this -> visited, sizeof(bool));
	os -> write((char*)&this -> logicallyAccessible, sizeof(bool));

	uint32_t relationsCount = (uint32_t)this -> relations.size();
	os -> write((char*)&relationsCount, sizeof(uint32_t));
	for(list<PlatformRelation*>::iterator i = this -> GetRelationBeginIterator(); i != this -> GetRelationEndIterator(); i++) {
		uint64_t id = (*i) -> GetID();
		os -> write((char*)&id, sizeof(uint64_t));
	}

	uint32_t chunksCount = (uint32_t)this -> overlappingChunks.size();
	os -> write((char*)&chunksCount, sizeof(uint32_t));
	for(list<Chunk*>::iterator i = this -> GetOverlappingChunkBeginIterator(); i != this -> GetOverlappingChunkEndIterator(); i++) {
		uint64_t id = (*i) -> GetID();
		os -> write((char*)&id, sizeof(uint64_t));
	}
}

void WorldMemory::Platform::ResolvePointers(WorldMemory* worldMemory) {
	// Zusammenh�nge
	for(list<PlatformRelation*>::iterator i = this -> GetRelationBeginIterator(); i != this -> GetRelationEndIterator(); i++) {
		for(list<PlatformRelation*>::iterator j = worldMemory -> GetPlatformRelationBeginIterator(); j != worldMemory -> GetPlatformRelationEndIterator(); j++) {
			if((*j) -> GetID() == (uint64_t)(*i)) {
				(*i) = (*j);
				break;
			}
		}
	}

	// Chunks
	for(list<Chunk*>::iterator i = this -> GetOverlappingChunkBeginIterator(); i != this -> GetOverlappingChunkEndIterator(); i++) {
		(*i) = worldMemory -> GetChunk((uint32_t)((uint64_t)(*i)));
	}
}

void WorldMemory::Platform::UpdateLogicalAccessibility() {
	if(this -> logicallyAccessible) {
		return;
	}
	for(list<PlatformRelation*>::iterator i = this -> GetRelationBeginIterator(); i != this -> GetRelationEndIterator(); i++) {
		if(!(*i) -> GetTrajectoryTo(this) -> possible) {
			continue;
		}

		Platform* platform = (*i) -> GetOtherPlatform(this);

		if(platform -> visited) {
			this -> logicallyAccessible = true;
		}
	}
}

WorldMemory::Chunk::Chunk(uint32_t id) : id(id) {
	for(uint32_t i = 0; i < World::Chunk::CHUNK_SIZE * World::Chunk::CHUNK_SIZE; i++) {
		this -> blockMemory[i] = Block{ 0, Block::Flag::UNKNOWN };
	}
}

void WorldMemory::Chunk::Load(ifstream* is) {
	is -> read((char*)&this -> id, sizeof(uint32_t));

	is -> read((char*)this -> blockMemory, World::Chunk::CHUNK_SIZE * World::Chunk::CHUNK_SIZE * sizeof(Block));

	// Plattformen
	this -> platforms.clear();

	uint32_t platformCount;
	is -> read((char*)&platformCount, sizeof(uint32_t));

	for(uint32_t i = 0; i < platformCount; i++) {
		uint64_t id;
		is -> read((char*)&id, sizeof(uint64_t));
		this -> platforms.push_back((Platform*)id);
	}
}

void WorldMemory::Chunk::Save(ofstream* os) {
	os -> write((char*)&this -> id, sizeof(uint32_t));

	os -> write((char*)this -> blockMemory, World::Chunk::CHUNK_SIZE * World::Chunk::CHUNK_SIZE * sizeof(Block));

	// Plattformen
	uint32_t platformCount = (uint32_t)this -> platforms.size();
	os -> write((char*)&platformCount, sizeof(uint32_t));

	for(list<Platform*>::iterator i = this -> GetPlatformBeginIterator(); i != this -> GetPlatformEndIterator(); i++) {
		if((*i) == nullptr) {
			uint64_t id = 0xffffffffffffffff;
			os -> write((char*)&id, sizeof(uint64_t));
		} else {
			uint64_t id = (*i) -> GetID();
			os -> write((char*)&id, sizeof(uint64_t));
		}
	}
}

void WorldMemory::Chunk::ResolvePointers(WorldMemory* worldMemory) {
	for(list<Platform*>::iterator i = this -> GetPlatformBeginIterator(); i != this -> GetPlatformEndIterator(); i++) {
		for(list<Platform*>::iterator j = worldMemory -> GetPlatformBeginIterator(); j != worldMemory -> GetPlatformEndIterator(); j++) {
			if((*j) -> GetID() == (uint64_t)(*i)) {
				(*i) = (*j);
				break;
			}
		}
	}
}

WorldMemory::WorldMemory() : nextPlatformID(0), nextPlatformRelationID(0), startPlatform(nullptr), endPlatform(nullptr) {
	memset(this -> memoryChunks, 0, World::WORLD_SIZE * World::WORLD_SIZE * sizeof(Chunk*));
}

WorldMemory::~WorldMemory() {
	for(uint32_t i = 0; i < World::WORLD_SIZE * World::WORLD_SIZE; i++) {
		delete this -> memoryChunks[i];
	}
	for(list<Platform*>::iterator i = this -> platforms.begin(); i != this -> platforms.end(); i++) {
		delete *i;
	}
	this -> platforms.clear();
	for(list<PlatformRelation*>::iterator i = this -> platformRelations.begin(); i != this -> platformRelations.end(); i++) {
		delete *i;
	}
	this -> platformRelations.clear();
}

void WorldMemory::Load(string filename) {
	Log::Write("begin loading world memory from file " + filename);

	ifstream is(filename, ios::binary);
	if(!is.is_open()) {
		throw Error("Error occured in WorldMemory::Load, file not found\nFilename: " + filename);
	}

	string version;
	while(is.peek() != '\n') {
		version.push_back(is.get());
	}
	is.get();

	Log::Write("physics memory file version checked");

	if(version != WorldMemory::CURRENT_SAVE_FILE_VERSION) {
		throw Error("Error occured in WorldMemory::Load, file version is incompatible\n File Version: " + version + " Required Version: " + WorldMemory::CURRENT_SAVE_FILE_VERSION);
	}

	// Lade neue Daten nicht direkt in Klassenvariabeln, falls die Datei besch�digt ist
	uint32_t newNextPlatformID;
	uint32_t newNextPlatformRelationID;

	Platform* newStartPlatform;
	Platform* newEndPlatform;

	uint32_t newStartX;
	uint32_t newStartY;
	uint32_t newEndX;
	uint32_t newEndY;

	Chunk* newMemoryChunks[Sim::World::WORLD_SIZE * Sim::World::WORLD_SIZE];
	memset(newMemoryChunks, 0, World::WORLD_SIZE * World::WORLD_SIZE * sizeof(Chunk*));

	list<Platform*> newPlatforms;
	list<PlatformRelation*> newPlatformRelations;
	list<PlatformRelation*> newNewRelations;

	try {
		Log::Write("Reading metadata...");

		is.read((char*)&newNextPlatformID, sizeof(uint32_t));
		is.read((char*)&newNextPlatformRelationID, sizeof(uint32_t));

		// ID's werden direkt hineingeladen
		is.read((char*)&newStartPlatform, sizeof(uint64_t));
		is.read((char*)&newEndPlatform, sizeof(uint64_t));

		is.read((char*)&newStartX, sizeof(uint32_t));
		is.read((char*)&newStartY, sizeof(uint32_t));
		is.read((char*)&newEndX, sizeof(uint32_t));
		is.read((char*)&newEndY, sizeof(uint32_t));

		Log::Write("Reading chunk data...");

		for(uint32_t i = 0; i < World::WORLD_SIZE * World::WORLD_SIZE; i++) {
			bool chunkExists;
			is.read((char*)&chunkExists, sizeof(bool));
			if(chunkExists) {
				newMemoryChunks[i] = new Chunk();
				newMemoryChunks[i] -> Load(&is);
			}
			if(!is) {
				throw Error("Error occured in ifstream::read, world memory file may be corrupted\nFile: " + filename);
			}
		}

		Log::Write("Reading platform data...");

		uint32_t platformCount;
		is.read((char*)&platformCount, sizeof(uint32_t));
		for(uint32_t i = 0; i < platformCount; i++) {
			Platform* platform = new Platform();
			platform -> Load(&is);
			newPlatforms.push_back(platform);
			if(!is) {
				throw Error("Error occured in ifstream::read, world memory file may be corrupted\nFile: " + filename);
			}
		}

		Log::Write("Reading platform relation data...");

		uint32_t platformRelationCount;
		is.read((char*)&platformRelationCount, sizeof(uint32_t));
		for(uint32_t i = 0; i < platformRelationCount; i++) {
			PlatformRelation* platformRelation = new PlatformRelation();
			platformRelation -> Load(&is);
			newPlatformRelations.push_back(platformRelation);
			if(!is) {
				throw Error("Error occured in ifstream::read, world memory file may be corrupted\nFile: " + filename);
			}
		}

		is.read((char*)&platformRelationCount, sizeof(uint32_t));
		for(uint32_t i = 0; i < platformRelationCount; i++) {
			uint64_t id;
			is.read((char*)&id, sizeof(uint64_t));
			newNewRelations.push_back((PlatformRelation*)id);
		}
		if(!is) {
			throw Error("Error occured in ifstream::read, world memory file may be corrupted\nFile: " + filename);
		}

		is.close();
	} catch(Error e) {
		// L�sche bereits geladene Daten

		{
			for(uint32_t i = 0; i < World::WORLD_SIZE * World::WORLD_SIZE; i++) {
				delete newMemoryChunks[i];
			}

			for(list<Platform*>::iterator i = newPlatforms.begin(); i != newPlatforms.end(); i++) {
				delete (*i);
			}

			for(list<PlatformRelation*>::iterator i = newPlatformRelations.begin(); i != newPlatformRelations.end(); i++) {
				delete (*i);
			}
		}

		throw e;
	}

	// L�sche alte Daten

	{
		for(uint32_t i = 0; i < World::WORLD_SIZE * World::WORLD_SIZE; i++) {
			delete this -> memoryChunks[i];
		}

		for(list<Platform*>::iterator i = this -> GetPlatformBeginIterator(); i != this -> GetPlatformEndIterator(); i++) {
			delete (*i);
		}
		this -> platforms.clear();

		for(list<PlatformRelation*>::iterator i = this -> GetPlatformRelationBeginIterator(); i != this -> GetPlatformRelationEndIterator(); i++) {
			delete (*i);
		}
		this -> platformRelations.clear();
	}

	// Kopiere Daten

	{
		this -> nextPlatformID = newNextPlatformID;
		this -> nextPlatformRelationID = newNextPlatformRelationID;

		this -> startPlatform = newStartPlatform;
		this -> endPlatform = newEndPlatform;

		this -> startX = newStartX;
		this -> startY = newStartY;
		this -> endX = newEndX;
		this -> endY = newEndY;

		memcpy(this -> memoryChunks, newMemoryChunks, World::WORLD_SIZE * World::WORLD_SIZE * sizeof(Chunk*));

		this -> platforms = newPlatforms;
		this -> platformRelations = newPlatformRelations;
		this -> newRelations = newNewRelations;
	}

	// Zeiger bekommen

	{
		Log::Write("Resolving pointers...");

		// Metadaten

		if(this -> startPlatform == (Platform*)0xffffffffffffffff) {
			this -> startPlatform = nullptr;
		} else {
			for(list<Platform*>::iterator j = this -> GetPlatformBeginIterator(); j != this -> GetPlatformEndIterator(); j++) {
				if((*j) -> GetID() == (uint64_t)this -> startPlatform) {
					this -> startPlatform = (*j);
					break;
				}
			}
		}

		if(this -> endPlatform == (Platform*)0xffffffffffffffff) {
			this -> endPlatform = nullptr;
		} else {
			for(list<Platform*>::iterator j = this -> GetPlatformBeginIterator(); j != this -> GetPlatformEndIterator(); j++) {
				if((*j) -> GetID() == (uint64_t)this -> endPlatform) {
					this -> endPlatform = (*j);
					break;
				}
			}
		}

		// Chunks

		for(uint32_t i = 0; i < World::WORLD_SIZE * World::WORLD_SIZE; i++) {
			Chunk* chunk = this -> GetChunk(i);
			if(chunk != nullptr) {
				chunk -> ResolvePointers(this);
			}
		}

		// Plattformen

		for(list<Platform*>::iterator i = this -> GetPlatformBeginIterator(); i != this -> GetPlatformEndIterator(); i++) {
			(*i) -> ResolvePointers(this);
		}

		// Verbindungen

		for(list<PlatformRelation*>::iterator i = this -> GetPlatformRelationBeginIterator(); i != this -> GetPlatformRelationEndIterator(); i++) {
			(*i) -> ResolvePointers(this);
		}

		// Neue Verbindungen

		for(list<PlatformRelation*>::iterator i = this -> GetNewRelationsBegin(); i != this -> GetNewRelationsEnd(); i++) {
			for(list<PlatformRelation*>::iterator j = this -> GetPlatformRelationBeginIterator(); j != this -> GetPlatformRelationEndIterator(); j++) {
				if((*j) -> GetID() == (uint64_t)(*i)) {
					(*i) = (*j);
					break;
				}
			}
		}
	}

	Log::Write("done loading world memory");
}

void WorldMemory::Save(string filename) {
	Log::Write("begin saving world memory " + filename);

	ofstream os(filename, ios::binary | ios::trunc);
	if(!os.is_open()) {
		throw Error("Error occured in WorldMemory::Save, unable to open file\nFilename: " + filename);
	}

	os << WorldMemory::CURRENT_SAVE_FILE_VERSION << "\n" << flush;

	Log::Write("world memory file version written...");

	Log::Write("writing metadata...");

	os.write((char*)&this -> nextPlatformID, sizeof(uint32_t));
	os.write((char*)&this -> nextPlatformRelationID, sizeof(uint32_t));

	uint64_t id;
	if(this -> startPlatform == nullptr) {
		id = 0xffffffffffffffff;
	} else {
		id = (uint32_t)this -> startPlatform -> GetID();
	}
	os.write((char*)&id, sizeof(uint64_t));
	if(this -> endPlatform == nullptr) {
		id = 0xffffffffffffffff;
	} else {
		id = (uint32_t)this -> endPlatform -> GetID();
	}
	os.write((char*)&id, sizeof(uint64_t));

	os.write((char*)&this -> startX, sizeof(uint32_t));
	os.write((char*)&this -> startY, sizeof(uint32_t));
	os.write((char*)&this -> endX, sizeof(uint32_t));
	os.write((char*)&this -> endY, sizeof(uint32_t));

	Log::Write("writing chunks data...");

	for(uint32_t i = 0; i < World::WORLD_SIZE * World::WORLD_SIZE; i++) {
		Chunk* chunk = this -> GetChunk(i);
		bool chunkExists;
		if(chunk == nullptr) {
			chunkExists = false;
		} else {
			chunkExists = true;
		}
		os.write((char*)&chunkExists, sizeof(bool));
		if(chunkExists) {
			chunk -> Save(&os);
		}
	}

	Log::Write("writing platform data...");

	uint32_t platformCount = (uint32_t)this -> platforms.size();
	os.write((char*)&platformCount, sizeof(uint32_t));
	for(list<Platform*>::iterator i = this -> GetPlatformBeginIterator(); i != this -> GetPlatformEndIterator(); i++) {
		(*i) -> Save(&os);
	}

	Log::Write("writing platform relation data...");
	uint32_t platformRelationCount = (uint32_t)this -> platformRelations.size();
	os.write((char*)&platformRelationCount, sizeof(uint32_t));
	for(list<PlatformRelation*>::iterator i = this -> GetPlatformRelationBeginIterator(); i != this -> GetPlatformRelationEndIterator(); i++) {
		(*i) -> Save(&os);
	}

	platformRelationCount = (uint32_t)this -> newRelations.size();
	os.write((char*)&platformRelationCount, sizeof(uint32_t));
	for(list<PlatformRelation*>::iterator i = this -> GetNewRelationsBegin(); i != this -> GetNewRelationsEnd(); i++) {
		uint64_t id = (*i) -> GetID();
		os.write((char*)&id, sizeof(uint64_t));
	}

	os.close();

	Log::Write("done saving world memory");
}

void WorldMemory::DrawDebugInformation(Vector2 const& characterPosition) {
	// Zeichne Kreuze �ber Bl�cke, die bereits registriert wurden
	int32_t firstBlockX = (int32_t)floor((characterPosition.GetX() - (double)Window::GetWidth() / 2.0 / (double)Render::BLOCK_SIZE));
	int32_t lastBlockX = (int32_t)floor((characterPosition.GetX() + (double)Window::GetWidth() / 2.0 / (double)Render::BLOCK_SIZE));
	int32_t firstBlockY = (int32_t)floor((characterPosition.GetY() - (double)Window::GetHeight() / 2.0 / (double)Render::BLOCK_SIZE));
	int32_t lastBlockY = (int32_t)floor((characterPosition.GetY() + (double)Window::GetHeight() / 2.0 / (double)Render::BLOCK_SIZE));

	Vector2 offset = characterPosition * (double)Render::BLOCK_SIZE;
	int32_t offsetX = (int32_t)offset.GetX() - Window::GetWidth() / 2 + Render::BLOCK_SIZE / 2;
	int32_t offsetY = (int32_t)offset.GetY() - Window::GetHeight() / 2 + Render::BLOCK_SIZE / 2;

	Platform* standingPlatform1 = this -> GetPlatformAt((int32_t)floor(characterPosition.GetX()), (int32_t)round(characterPosition.GetY() - 1.0));
	Platform* standingPlatform2 = this -> GetPlatformAt((int32_t)ceil(characterPosition.GetX()), (int32_t)round(characterPosition.GetY() - 1.0));

	{
		uint32_t linesCounter = 0;
		for(int32_t i = firstBlockX; i <= lastBlockX; i++) {
			for(int32_t j = firstBlockY; j <= lastBlockY; j++) {
				WorldMemory::Block block = this -> GetBlock(i, j);

				if(block.block != 0) {
					if(block.flags == WorldMemory::Block::Flag::NONE && block.block != 0) {
						Graphics::Line cross[2] = {
							{ (int32_t)(i * Render::BLOCK_SIZE) - offsetX, (int32_t)(j * Render::BLOCK_SIZE) - offsetY, (int32_t)((i + 1) * Render::BLOCK_SIZE) - offsetX, (int32_t)((j + 1) * Render::BLOCK_SIZE) - offsetY, 0.5f, 0.0f, 0.5f },
							{ (int32_t)(i * Render::BLOCK_SIZE) - offsetX, (int32_t)((j + 1) * Render::BLOCK_SIZE) - offsetY, (int32_t)((i + 1) * Render::BLOCK_SIZE) - offsetX, (int32_t)(j * Render::BLOCK_SIZE) - offsetY, 0.5f, 0.0f, 0.5f }
						};
						Graphics::DrawLines(cross, 2);
						linesCounter += 2;
					} else if(block.flags == WorldMemory::Block::Flag::UNPROCESSED) {
						Graphics::Line cross[2] = {
							{ (int32_t)(i * Render::BLOCK_SIZE) - offsetX, (int32_t)(j * Render::BLOCK_SIZE) - offsetY, (int32_t)((i + 1) * Render::BLOCK_SIZE) - offsetX, (int32_t)((j + 1) * Render::BLOCK_SIZE) - offsetY, 0.5f, 0.5f, 0.0f },
							{ (int32_t)(i * Render::BLOCK_SIZE) - offsetX, (int32_t)((j + 1) * Render::BLOCK_SIZE) - offsetY, (int32_t)((i + 1) * Render::BLOCK_SIZE) - offsetX, (int32_t)(j * Render::BLOCK_SIZE) - offsetY, 0.5f, 0.5f, 0.0f }
						};
						Graphics::DrawLines(cross, 2);
						linesCounter += 2;
					}
				}

				if(linesCounter >= Graphics::MAX_LINE_LIST_SIZE - 2) {
					linesCounter = 0;
					Graphics::FlushLines();
				}
			}
		}
		Graphics::FlushLines();
	}

	// Zeichne Plattformen
	{
		uint32_t rectsCounter = 0;
		for(list<Platform*>::iterator i = this -> GetPlatformBeginIterator(); i != this -> GetPlatformEndIterator(); i++) {
			Graphics::Rect rect = { (int32_t)(((*i) -> x) * Render::BLOCK_SIZE) - offsetX + 10, (int32_t)(((*i) -> y) * Render::BLOCK_SIZE) - offsetY + 20, (*i) -> width * Render::BLOCK_SIZE - 20, Render::BLOCK_SIZE - 40, 0.0f, 0.5f, 1.0f };
			Graphics::FillRects(&rect, 1);
			rectsCounter++;

			if(rectsCounter >= Graphics::MAX_RECT_LIST_SIZE - 2) {
				rectsCounter = 0;
				Graphics::FlushFilledRects();
			}
		}
		Graphics::FlushFilledRects();
	}

	// Zeichne Zusammenh�nge
	{
		uint32_t linesCounter = 0;
		uint32_t circlesCounter = 0;
		for(list<PlatformRelation*>::iterator i = this -> GetPlatformRelationBeginIterator(); i != this -> GetPlatformRelationEndIterator(); i++) {
			pair<Platform*, Platform*> platforms = (*i) -> GetPlatforms();
			int32_t x1 = platforms.first -> x;
			int32_t y1 = platforms.first -> y;
			int32_t x2 = platforms.second -> x;
			int32_t y2 = platforms.second -> y;
			/*double xMid = (double)(x1 + x2) / 2.0;
			double yMid = (double)(y1 + y2) / 2.0;
			float r1, g1, b1;
			float r2, g2, b2;

			if((*i) -> GetTrajectoryTo(platforms.first) -> possible) {
				r1 = 0.0f;
				g1 = 0.0f;
				b1 = 0.4f;
			} else {
				r1 = 0.4f;
				g1 = 0.0f;
				b1 = 0.0f;
			}
			if((*i) -> GetTrajectoryTo(platforms.second) -> possible) {
				r2 = 0.0f;
				g2 = 0.0f;
				b2 = 0.4f;
			} else {
				r2 = 0.4f;
				g2 = 0.0f;
				b2 = 0.0f;
			}

			Graphics::Line lines[2] = {
				{
					(int32_t)(x1 * Render::BLOCK_SIZE) - offsetX + (int32_t)Render::BLOCK_SIZE / 2,
					(int32_t)(y1 * Render::BLOCK_SIZE) - offsetY + (int32_t)Render::BLOCK_SIZE / 2,
					(int32_t)(xMid * (double)Render::BLOCK_SIZE) - offsetX + (int32_t)Render::BLOCK_SIZE / 2,
					(int32_t)(yMid * (double)Render::BLOCK_SIZE) - offsetY + (int32_t)Render::BLOCK_SIZE / 2,
					r1, g1, b1
				},
				{
					(int32_t)(xMid * (double)Render::BLOCK_SIZE) - offsetX + (int32_t)Render::BLOCK_SIZE / 2,
					(int32_t)(yMid * (double)Render::BLOCK_SIZE) - offsetY + (int32_t)Render::BLOCK_SIZE / 2,
					(int32_t)(x2 * Render::BLOCK_SIZE) - offsetX + (int32_t)Render::BLOCK_SIZE / 2,
					(int32_t)(y2 * Render::BLOCK_SIZE) - offsetY + (int32_t)Render::BLOCK_SIZE / 2,
					r2, g2, b2
				}
			};

			Graphics::DrawLines(lines, 2);*/

			Graphics::Line line = {
				(int32_t)(x1 * Render::BLOCK_SIZE) - offsetX + (int32_t)Render::BLOCK_SIZE / 2,
				(int32_t)(y1 * Render::BLOCK_SIZE) - offsetY + (int32_t)Render::BLOCK_SIZE / 2,
				(int32_t)(x2 * Render::BLOCK_SIZE) - offsetX + (int32_t)Render::BLOCK_SIZE / 2,
				(int32_t)(y2 * Render::BLOCK_SIZE) - offsetY + (int32_t)Render::BLOCK_SIZE / 2,
				0.0f, 0.0f, 0.0f
			};

			Graphics::Circle circles[2] = {
				{ (int32_t)(x1 * Render::BLOCK_SIZE) - offsetX + (int32_t)Render::BLOCK_SIZE / 2, (int32_t)(y1 * Render::BLOCK_SIZE) - offsetY + (int32_t)Render::BLOCK_SIZE / 2, 5, 0, 0, 0.0f, 0.0f, 0.0f },
				{ (int32_t)(x2 * Render::BLOCK_SIZE) - offsetX + (int32_t)Render::BLOCK_SIZE / 2, (int32_t)(y2 * Render::BLOCK_SIZE) - offsetY + (int32_t)Render::BLOCK_SIZE / 2, 5, 0, 0, 0.0f, 0.0f, 0.0f }
			};

			Graphics::DrawLines(&line, 1);
			Graphics::FillCircles(circles, 2);

			linesCounter += 2;
			circlesCounter += 2;

			if(linesCounter >= Graphics::MAX_LINE_LIST_SIZE - 2) {
				linesCounter = 0;
				Graphics::FlushLines();
			}
			if(circlesCounter >= Graphics::MAX_CIRCLE_LIST_SIZE - 2) {
				circlesCounter = 0;
				Graphics::FlushFilledCircles();
			}
		}
		Graphics::FlushLines();
		Graphics::FlushFilledCircles();
	}

	// Zeichne Start- und Zielblock
	{
		if(this -> startPlatform != nullptr) {
			Graphics::Rect startMarkers[2] = {
				{ (int32_t)(this -> startX * Render::BLOCK_SIZE) - offsetX, (int32_t)(this -> startY * Render::BLOCK_SIZE) - offsetY + (int32_t)Render::BLOCK_SIZE / 2 - 5, Render::BLOCK_SIZE, 10, 0.0f, 0.5f, 0.0f},
				{ (int32_t)(this -> startX * Render::BLOCK_SIZE) - offsetX + (int32_t)Render::BLOCK_SIZE / 2 - 5, (int32_t)(this -> startY * Render::BLOCK_SIZE) - offsetY, 10, Render::BLOCK_SIZE, 0.0f, 0.5f, 0.0f },
			};
			Graphics::FillRects(startMarkers, 2);
		}

		if(this -> endPlatform != nullptr) {
			Graphics::Rect endMarkers[2] = {
				{ (int32_t)(this -> endX * Render::BLOCK_SIZE) - offsetX, (int32_t)(this -> endY * Render::BLOCK_SIZE) - offsetY + (int32_t)Render::BLOCK_SIZE / 2 - 5, Render::BLOCK_SIZE, 10, 1.0, 0.0f, 0.0f },
				{ (int32_t)(this -> endX * Render::BLOCK_SIZE) - offsetX + (int32_t)Render::BLOCK_SIZE / 2 - 5, (int32_t)(this -> endY * Render::BLOCK_SIZE) - offsetY, 10, Render::BLOCK_SIZE, 1.0f, 0.0f, 0.0f }
			};
			Graphics::FillRects(endMarkers, 2);
		}

		Graphics::FlushFilledRects();
	}
}

void WorldMemory::AddBlock(int32_t x, int32_t y, Block block, bool start, bool end) {
	// Pr�fe, ob die Koordinaten g�ltig sind
	if(x < 0 || x >= World::WORLD_SIZE * World::Chunk::CHUNK_SIZE || y < 0 || y >= World::WORLD_SIZE * World::Chunk::CHUNK_SIZE) {
		return;
	}

	// Wenn der Block bereits gesetzt ist, mache nichts
	if(this -> GetBlock(x, y).flags == Block::Flag::NONE) {
		return;
	}

	// F�ge Block hinzu, Chunk wird erstellt, falls er noch nicht existiert
	this -> SetBlock(x, y, block);

	// Wenn der Block Luft ist, mache nichts
	if(block.block == 0) {
		return;
	}

	// Wenn der Block Flags gesetzt hat, mache nichts
	if(block.flags != Block::Flag::NONE) {
		return;
	}

	// Pr�fe, ob direkt dar�ber ein Block ist; falls ja, mache nichts
	if(this -> GetBlock(x, y + 1).block != 0) {
		return;
	}

	// Suche Plattform, an die der Block angeschlossen werden kann
	Platform* platform = nullptr;
	Block leftBlock = this -> GetBlock(x - 1, y);
	Block rightBlock = this -> GetBlock(x + 1, y);
	if(leftBlock.flags == Block::Flag::NONE && leftBlock.block == block.block) {
		// Links befindet sich ein Block
		// Pr�fe, ob dieser Block zu einer Plattform geh�rt
		platform = this -> GetPlatformAt(x - 1, y);
		if(platform == nullptr) {
			goto endIf;
		}

		// F�ge Block hinzu
		platform -> width++;

		// Berechne alle Flugbahnen, die mit dieser Plattform zu tun haben neu
		{
			for(list<PlatformRelation*>::iterator i = platform -> GetRelationBeginIterator(); i != platform -> GetRelationEndIterator(); i++) {
				this -> AddNewRelation(*i);
			}
			// Plattformen links
			int32_t xPosition = platform -> x - 1;
			while(true) {
				Platform* nextPlatform = this -> GetPlatformAt(xPosition, platform -> y);
				if(nextPlatform != nullptr) {
					for(list<PlatformRelation*>::iterator i = nextPlatform -> GetRelationBeginIterator(); i != nextPlatform -> GetRelationEndIterator(); i++) {
						this -> AddNewRelation(*i);
					}
				} else {
					nextPlatform = this -> GetPlatformAt(xPosition - 1, platform -> y);
					if(nextPlatform != nullptr) {
						for(list<PlatformRelation*>::iterator i = nextPlatform -> GetRelationBeginIterator(); i != nextPlatform -> GetRelationEndIterator(); i++) {
							this -> AddNewRelation(*i);
						}
					}
				}
				if(nextPlatform == nullptr) {
					break;
				}
				xPosition = nextPlatform -> x - 1;
			}
			// Plattformen rechts
			xPosition = platform -> x + platform -> width;
			while(true) {
				Platform* nextPlatform = this -> GetPlatformAt(xPosition, platform -> y);
				if(nextPlatform != nullptr) {
					for(list<PlatformRelation*>::iterator i = nextPlatform -> GetRelationBeginIterator(); i != nextPlatform -> GetRelationEndIterator(); i++) {
						this -> AddNewRelation(*i);
					}
				} else {
					nextPlatform = this -> GetPlatformAt(xPosition + 1, platform -> y);
					if(nextPlatform != nullptr) {
						for(list<PlatformRelation*>::iterator i = nextPlatform -> GetRelationBeginIterator(); i != nextPlatform -> GetRelationEndIterator(); i++) {
							this -> AddNewRelation(*i);
						}
					}
				}
				if(nextPlatform == nullptr) {
					break;
				}
				xPosition = nextPlatform -> x + nextPlatform -> width;
			}
		}

		// Pr�fe, ob durch das Hinzuf�gen des Blockes ein neuer Chunk angeschnitten wird
		// Dieser Chunk ist bereits erstellt, da vorher ein Block hinzugef�gt worden ist
		Chunk* blockChunk = this -> GetChunkFromBlock(x, y);
		if(x % World::Chunk::CHUNK_SIZE == 0) {
			// Registriere neuer Chunk bei Plattform
			platform -> AddOverlappingChunk(blockChunk);

			// Registriere Plattform bei neuem Chunk
			blockChunk -> AddPlatform(platform);

			// Suche nach anderen Plattformen in neuen angrenzenden Chunks
			int32_t beginChunkY = y / World::Chunk::CHUNK_SIZE - WorldMemory::PLATFORM_RELATION_CHUNKS;
			int32_t endChunkY = y / World::Chunk::CHUNK_SIZE + WorldMemory::PLATFORM_RELATION_CHUNKS;
			int32_t chunkX = x / World::Chunk::CHUNK_SIZE + WorldMemory::PLATFORM_RELATION_CHUNKS;
			int32_t firstBlockX = chunkX * World::Chunk::CHUNK_SIZE;
			if(chunkX < World::WORLD_SIZE) {
				for(int32_t i = beginChunkY; i <= endChunkY; i++) {
					if(i < 0 || i >= World::WORLD_SIZE) {
						continue;
					}
					Chunk* chunk = this -> GetChunk(chunkX, i);

					if(chunk != nullptr) {
						// Schaue f�r jede Plattform im Chunk, ob sie nicht auch in den Chunk links davon ragt
						// Wenn nein, verbinde sie mit der k�rzlich vergr�sserten Plattform
						for(list<Platform*>::iterator j = chunk -> GetPlatformBeginIterator(); j != chunk -> GetPlatformEndIterator(); j++) {
							if((*j) -> x >= (uint32_t)firstBlockX) {
								if(*j == platform) {
									continue;
								}
								// Erstelle Zusammenhang
								PlatformRelation* relation = new PlatformRelation(this -> nextPlatformRelationID, platform, *j);
								this -> nextPlatformRelationID++;
								relation -> CalculateDistance();
								this -> platformRelations.push_back(relation);
								this -> AddNewRelation(relation);

								// Registriere Zusammenhang bei beiden Plattformen
								platform -> AddRelation(relation);
								(*j) -> AddRelation(relation);
							}
						}
					}
				}
			}
		}
	} else if(rightBlock.flags == Block::Flag::NONE && rightBlock.block == block.block) {
		// Rechts befindet sich ein Block
		// Pr�fe, ob dieser Block zu einer Plattform geh�rt
		platform = this -> GetPlatformAt(x + 1, y);
		if(platform == nullptr) {
			goto endIf;
		}

		// F�ge Block hinzu
		platform -> x--;
		platform -> width++;

		// Berechne alle Flugbahnen, die mit dieser Plattform zu tun haben neu
		{
			for(list<PlatformRelation*>::iterator i = platform -> GetRelationBeginIterator(); i != platform -> GetRelationEndIterator(); i++) {
				this -> AddNewRelation(*i);
			}
			// Plattformen links
			int32_t xPosition = platform -> x - 1;
			while(true) {
				Platform* nextPlatform = this -> GetPlatformAt(xPosition, platform -> y);
				if(nextPlatform != nullptr) {
					for(list<PlatformRelation*>::iterator i = nextPlatform -> GetRelationBeginIterator(); i != nextPlatform -> GetRelationEndIterator(); i++) {
						this -> AddNewRelation(*i);
					}
				} else {
					nextPlatform = this -> GetPlatformAt(xPosition - 1, platform -> y);
					if(nextPlatform != nullptr) {
						for(list<PlatformRelation*>::iterator i = nextPlatform -> GetRelationBeginIterator(); i != nextPlatform -> GetRelationEndIterator(); i++) {
							this -> AddNewRelation(*i);
						}
					}
				}
				if(nextPlatform == nullptr) {
					break;
				}
				xPosition = nextPlatform -> x - 1;
			}
			// Plattformen rechts
			xPosition = platform -> x + platform -> width;
			while(true) {
				Platform* nextPlatform = this -> GetPlatformAt(xPosition, platform -> y);
				if(nextPlatform != nullptr) {
					for(list<PlatformRelation*>::iterator i = nextPlatform -> GetRelationBeginIterator(); i != nextPlatform -> GetRelationEndIterator(); i++) {
						this -> AddNewRelation(*i);
					}
				} else {
					nextPlatform = this -> GetPlatformAt(xPosition + 1, platform -> y);
					if(nextPlatform != nullptr) {
						for(list<PlatformRelation*>::iterator i = nextPlatform -> GetRelationBeginIterator(); i != nextPlatform -> GetRelationEndIterator(); i++) {
							this -> AddNewRelation(*i);
						}
					}
				}
				if(nextPlatform == nullptr) {
					break;
				}
				xPosition = nextPlatform -> x + nextPlatform -> width;
			}
		}

		// Pr�fe, ob durch das Hinzuf�gen des Blockes ein neuer Chunk angeschnitten wird
		// Dieser Chunk ist bereits erstellt, da vorher ein Block hinzugef�gt worden ist
		Chunk* blockChunk = this -> GetChunkFromBlock(x, y);
		if(x % World::Chunk::CHUNK_SIZE == World::Chunk::CHUNK_SIZE - 1) {
			// Registriere neuer Chunk bei Plattform
			platform -> AddOverlappingChunk(blockChunk);

			// Registriere Plattform bei neuem Chunk
			blockChunk -> AddPlatform(platform);

			// Suche nach anderen Plattformen in neuen angrenzenden Chunks
			int32_t beginChunkY = y / World::Chunk::CHUNK_SIZE - WorldMemory::PLATFORM_RELATION_CHUNKS;
			int32_t endChunkY = y / World::Chunk::CHUNK_SIZE + WorldMemory::PLATFORM_RELATION_CHUNKS;
			int32_t chunkX = x / World::Chunk::CHUNK_SIZE - WorldMemory::PLATFORM_RELATION_CHUNKS;
			int32_t lastBlockX = (chunkX + 1) * World::Chunk::CHUNK_SIZE;
			if(chunkX >= 0) {
				for(int32_t i = beginChunkY; i <= endChunkY; i++) {
					if(i < 0 || i >= World::WORLD_SIZE) {
						continue;
					}
					Chunk* chunk = this -> GetChunk(chunkX, i);

					if(chunk != nullptr) {
						// Schaue f�r jede Plattform im Chunk, ob sie nicht auch in den Chunk rechts davon ragt
						// Wenn nein, verbinde sie mit der k�rzlich vergr�sserten Plattform
						for(list<Platform*>::iterator j = chunk -> GetPlatformBeginIterator(); j != chunk -> GetPlatformEndIterator(); j++) {
							if((*j) -> x + (*j) -> width <= (uint32_t)lastBlockX) {
								if(*j == platform) {
									continue;
								}
								// Erstelle Zusammenhang
								PlatformRelation* relation = new PlatformRelation(this -> nextPlatformRelationID, platform, *j);
								this -> nextPlatformRelationID++;
								relation -> CalculateDistance();
								this -> platformRelations.push_back(relation);
								this -> AddNewRelation(relation);

								// Registriere Zusammenhang bei beiden Plattformen
								platform -> AddRelation(relation);
								(*j) -> AddRelation(relation);
							}
						}
					}
				}
			}
		}
	}
endIf:

	// Keine Plattform gefunden, erstelle neue
	if(platform == nullptr) {
		// Erstelle neue Plattform
		platform = new Platform(this -> nextPlatformID);
		platform -> x = x;
		platform -> y = y;
		platform -> width = 1;
		platform -> block = block.block;
		this -> nextPlatformID++;
		this -> platforms.push_back(platform);

		// Dieser Chunk ist bereits erstellt, da vorher ein Block hinzugef�gt worden ist
		Chunk* blockChunk = this -> GetChunkFromBlock(x, y);

		// Registriere Plattform bei Chunk
		blockChunk -> AddPlatform(platform);

		// Registriere Chunk bei Plattform
		platform -> AddOverlappingChunk(blockChunk);

		// Suche nach Plattformen in angrenzenden Chunks
		int32_t beginChunkX = x / World::Chunk::CHUNK_SIZE - WorldMemory::PLATFORM_RELATION_CHUNKS;
		int32_t endChunkX = x / World::Chunk::CHUNK_SIZE + WorldMemory::PLATFORM_RELATION_CHUNKS;
		int32_t beginChunkY = y / World::Chunk::CHUNK_SIZE - WorldMemory::PLATFORM_RELATION_CHUNKS;
		int32_t endChunkY = y / World::Chunk::CHUNK_SIZE + WorldMemory::PLATFORM_RELATION_CHUNKS;
		for(int32_t i = beginChunkX; i <= endChunkX; i++) {
			if(i >= 0 && i < World::WORLD_SIZE) {
				for(int32_t j = beginChunkY; j <= endChunkY; j++) {
					if(j >= 0 && j < World::WORLD_SIZE) {
						Chunk* chunk = this -> GetChunk(i, j);

						if(chunk != nullptr) {
							list<Platform*> alreadyRelatedPlatforms;

							for(list<Platform*>::iterator k = chunk -> GetPlatformBeginIterator(); k != chunk -> GetPlatformEndIterator(); k++) {
								if(*k == platform) {
									continue;
								}
								// Teste, ob die Plattform nicht bereits verzeichnet ist
								if(find(alreadyRelatedPlatforms.begin(), alreadyRelatedPlatforms.end(), *k) == alreadyRelatedPlatforms.end()) {
									// Erstelle Zusammenhang
									PlatformRelation* relation = new PlatformRelation(this -> nextPlatformRelationID, platform, *k);
									this -> nextPlatformRelationID++;
									relation -> CalculateDistance();
									this -> platformRelations.push_back(relation);
									this -> AddNewRelation(relation);

									// Registriere Zusammenhang bei beiden Plattformen
									platform -> AddRelation(relation);
									(*k) -> AddRelation(relation);

									// Verzeichne Plattform, so dass sie nicht mehrmals verwendet wird
									alreadyRelatedPlatforms.push_back(*k);
									alreadyRelatedPlatforms.sort();
								}
							}
						}
					}
				}
			}
		}

		// Berechne alle Flugbahnen, die mit dieser Plattform zu tun haben neu
		{
			// Plattformen links
			int32_t xPosition = platform -> x - 1;
			while(true) {
				Platform* nextPlatform = this -> GetPlatformAt(xPosition, platform -> y);
				if(nextPlatform != nullptr) {
					for(list<PlatformRelation*>::iterator i = nextPlatform -> GetRelationBeginIterator(); i != nextPlatform -> GetRelationEndIterator(); i++) {
						this -> AddNewRelation(*i);
					}
				} else {
					nextPlatform = this -> GetPlatformAt(xPosition - 1, platform -> y);
					if(nextPlatform != nullptr) {
						for(list<PlatformRelation*>::iterator i = nextPlatform -> GetRelationBeginIterator(); i != nextPlatform -> GetRelationEndIterator(); i++) {
							this -> AddNewRelation(*i);
						}
					}
				}
				if(nextPlatform == nullptr) {
					break;
				}
				xPosition = nextPlatform -> x - 1;
			}
			// Plattformen rechts
			xPosition = platform -> x + platform -> width;
			while(true) {
				Platform* nextPlatform = this -> GetPlatformAt(xPosition, platform -> y);
				if(nextPlatform != nullptr) {
					for(list<PlatformRelation*>::iterator i = nextPlatform -> GetRelationBeginIterator(); i != nextPlatform -> GetRelationEndIterator(); i++) {
						this -> AddNewRelation(*i);
					}
				} else {
					nextPlatform = this -> GetPlatformAt(xPosition + 1, platform -> y);
					if(nextPlatform != nullptr) {
						for(list<PlatformRelation*>::iterator i = nextPlatform -> GetRelationBeginIterator(); i != nextPlatform -> GetRelationEndIterator(); i++) {
							this -> AddNewRelation(*i);
						}
					}
				}
				if(nextPlatform == nullptr) {
					break;
				}
				xPosition = nextPlatform -> x + nextPlatform -> width;
			}
		}
	}

	// Setzte Start- bzw. Endplattform falls die jeweiligen Flags gesetzt sind
	if(start && platform != nullptr) {
		this -> startPlatform = platform;
		this -> startX = x;
		this -> startY = y;
	}
	if(end && platform != nullptr) {
		this -> endPlatform = platform;
		this -> endX = x;
		this -> endY = y;
	}
}

void WorldMemory::CreateTargetList(Sim::Vector2 const& characterPosition) {
	this -> targetList.clear();
	for(list<Platform*>::iterator i = this -> GetPlatformBeginIterator(); i != this -> GetPlatformEndIterator(); i++) {
		// Aktualisiere logische Erreichbarkeit
		(*i) -> UpdateLogicalAccessibility();
		if(!(*i) -> HasBeenVisited() && (*i) -> IsLogicallyAccessible()) {
			// Plattform kommt als Ziel in Frage

			// F�ge Plattform nur hinzu, wenn sie nicht die Endplattform ist
			// Die Endplattform wird zuvorderst in die Liste eingef�gt
			if(*i != this -> endPlatform) {
				this -> targetList.push_back(*i);
			}
		}
	}
	// F�ge wenn m�glich immer die Zeilplattform in die List ein
	if(this -> endPlatform != nullptr && this -> endPlatform -> IsLogicallyAccessible()) {
		this -> targetList.push_front(this -> endPlatform);
	}
	this -> targetList.sort(TargetCompare(characterPosition));
}

WorldMemory::Platform* WorldMemory::GetPlatformAt(int32_t x, int32_t y) {
	if(x < 0 || y < 0 || x >= World::WORLD_SIZE * World::Chunk::CHUNK_SIZE || y >= World::WORLD_SIZE * World::Chunk::CHUNK_SIZE) {
		return nullptr;
	}
	Platform* platform = nullptr;
	Chunk* chunk = this -> GetChunkFromBlock(x, y);
	if(chunk != nullptr) {
		for(list<Platform*>::iterator i = chunk -> GetPlatformBeginIterator(); i != chunk -> GetPlatformEndIterator(); i++) {
			if((*i) -> y == y) {
				if((*i) -> x <= (uint32_t)x && (*i) -> x + (*i) -> width > (uint32_t)x) {
					platform = *i;
				}
			}
		}
	}
	return platform;
}

bool WorldMemory::TargetCompare::operator()(Platform const* a, Platform const* b) const {
	double distanceA = (a -> x - this -> referencePosition.GetX()) * (a -> x - this -> referencePosition.GetX()) + (a -> y - this -> referencePosition.GetY()) * (a -> y - this -> referencePosition.GetY());
	double distanceB = (b -> x - this -> referencePosition.GetX()) * (b -> x - this -> referencePosition.GetX()) + (b -> y - this -> referencePosition.GetY()) * (b -> y - this -> referencePosition.GetY());
	return distanceA < distanceB;
}

const string WorldMemory::CURRENT_SAVE_FILE_VERSION = "v1.0";

}
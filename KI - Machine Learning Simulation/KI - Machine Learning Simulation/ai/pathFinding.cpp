#include <ai/pathFinding.h>

#include <system/errors.h>

using namespace System;

namespace AI {

// Pr�dikat, um Knoten zu sortieren
bool CompareNodes(PathFinding::Node* n1, PathFinding::Node* n2) {
	return n1 -> f < n2 -> f;
}

PathFinding::PathFinding(WorldMemory* worldMemory) : worldMemory(worldMemory) {

}

PathFinding::~PathFinding() {

}

void PathFinding::UpdateNodes() {
	// Erstelle neue Liste mit Knoten
	this -> nodes.clear();

	for(list<WorldMemory::Platform*>::iterator i = this -> worldMemory -> GetPlatformBeginIterator(); i != this -> worldMemory -> GetPlatformEndIterator(); i++) {
		this -> nodes.push_back(new Node(*i));
	}
}

list<WorldMemory::Trajectory*> PathFinding::FindPath(WorldMemory::Platform* start, WorldMemory::Platform* end) {
	this -> UpdateNodes();

	// Erstelle alle h-Werte
	for(uint32_t i = 0; i < this -> nodes.size(); i++) {
		WorldMemory::PlatformRelation relation(0, this -> nodes[i] -> platform, end);
		relation.CalculateDistance();

		this -> nodes[i] -> h = relation.GetHeuristicDistance();
	}

	// Erstelle Open-List
	this -> openList.clear();

	Node* startNode = this -> NodeFromPlatform(start);

	startNode -> open = true;
	this -> openList.push_back(startNode);

	bool found = false;

	while(!this -> openList.empty()) {
		Node* currentNode = this -> openList.front();
		this -> openList.pop_front();
		currentNode -> open = false;

		if(currentNode -> platform == end) {
			// Pfad gefunden
			found = true;
			break;
		}

		currentNode -> closed = true;

		this -> ExpandNode(currentNode);
		this -> openList.sort([](PathFinding::Node* n1, PathFinding::Node* n2) {return n1 -> f < n2 -> f; });
	}

	// Erstelle Weg
	list<WorldMemory::Trajectory*> path;

	if(!found) {
		// Gib leerer Weg zur�ck
		return path;
	}

	Node* currentNode = this -> NodeFromPlatform(end);
	while(currentNode != startNode) {
		path.push_front(currentNode -> trajectory);
		currentNode = currentNode -> predecessor;
	}

	return path;
}

void PathFinding::ExpandNode(Node* currentNode) {
	for(list<WorldMemory::PlatformRelation*>::iterator i = currentNode -> platform -> GetRelationBeginIterator(); i != currentNode -> platform -> GetRelationEndIterator(); i++) {
		WorldMemory::Platform* successorPlatform = (*i) -> GetOtherPlatform(currentNode -> platform);
		WorldMemory::Trajectory* trajectory = (*i) -> GetTrajectoryTo(successorPlatform);
		Node* successorNode = this -> NodeFromPlatform(successorPlatform);

		if(!trajectory -> possible) {
			continue;
		}

		if(successorNode -> closed) {
			continue;
		}

		(*i) -> CalculateDistance();
		double newG = currentNode -> g + (*i) -> GetDistance();
		if(successorNode -> open && newG >= successorNode -> g) {
			continue;
		}

		successorNode -> predecessor = currentNode;
		successorNode -> trajectory = trajectory;
		successorNode -> g = newG;

		successorNode -> f = newG + successorNode -> h;
		if(!successorNode -> open) {
			successorNode -> open = true;
			this -> openList.push_back(successorNode);
		}
	}
}

PathFinding::Node* PathFinding::NodeFromPlatform(WorldMemory::Platform* platform) {
	if(platform -> GetID() >= this -> nodes.size()) {
		throw Error("Error occured in PathFinding::NodeFromPlatform: id too big");
	}

	Node* node = this -> nodes[platform -> GetID()];
	if(node -> platform != platform) {
		throw Error("Error occured in PathFinding::NodeFromPlatform: platform id does not correspond with node index. The world graph may be corrupted");
	}

	return node;
}

}
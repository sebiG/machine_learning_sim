#include <ai/ai.h>

#include <system/errors.h>
#include <system/graphics.h>
#include <simulation/render.h>
#include <ai/physicsMemory.h>
#include <ai/view.h>
#include <ai/pathFinding.h>
#include <ai/pathController.h>

using namespace System;
using namespace Sim;

namespace AI {

Ai::Ai(World* world, Sim::Character* character) : debugCharacter(character), world(world) {
	this -> reset = true;
	this -> resetCompleted = false;

	this -> restartPathFinding = true;

	this -> worldMemory = new WorldMemory();
	this -> physicsMemory = new PhysicsMemory();
	this -> view = new View(character, this -> physicsMemory);

	this -> pathFinding = new PathFinding(this -> worldMemory);
	this -> pathController = new PathController(this -> debugCharacter, this -> view, this -> physicsMemory, this -> worldMemory);
}

Ai::~Ai() {
	delete this -> worldMemory;
	delete this -> physicsMemory;
	delete this -> view;
}

void Ai::ClearWorldMemory() {
	// Ersetze Weltged�chtnis
	this -> worldMemory -> ~WorldMemory();
	this -> worldMemory = new(this -> worldMemory) WorldMemory();
	Log::Write("AI world memory cleared");
}

void Ai::LoadWorldMemory(string filename) {
	this -> worldMemory -> Load(filename);
}

void Ai::SaveWorldMemory(string filename) {
	this -> worldMemory -> Save(filename);
}

void Ai::ClearPhysicsMemory() {
	// Ersetze Physikged�chtnis
	this -> physicsMemory -> ~PhysicsMemory();
	this -> physicsMemory = new(this -> physicsMemory) PhysicsMemory();
	Log::Write("AI physics memory cleared");
}

void Ai::LoadPhysicsMemory(string filename) {
	this -> physicsMemory -> Load(filename);
}

void Ai::SavePhysicsMemory(string filename) {
	this -> physicsMemory -> Save(filename);
}

void Ai::Update(double deltaTime) {
	if(!this -> activated) {
		return;
	}

	if(this -> reset) {
		this -> reset = false;
		// Setze Reset-Timer
		this -> resetCountdown = View::MAX_VARIABLE_HISTORY_LENGTH;
		// Setze aktueller Status zu MEASURING; Dies ist nicht unbedingt n�tig, erm�glicht jedoch, dass Pathfinding korrekt gestartet wird
		this -> currentState = State::MEASURING;
		this -> stateEndCounter = 0;
	}

	// Aktionen, die immer ausgef�hrt werden, solange die KI aktiv ist

	// Sammle Werte f�r Variabeln (Falls reset gesetzt ist, werden hier ung�ltige Werte geladen; jedoch verhindert resetCountdown, dass diese Werte verwendet werden)
	this -> view -> Update(deltaTime);

	if(this -> resetCountdown > 0) {
		resetCountdown--;
		if(this -> resetCountdown == 0) {
			this -> resetCompleted = true;
		}
	} else {
		// Setzte zur�ck, falls die Y-Position zu tief ist
		if(this -> view -> GetData(PhysicsBehaviour::Variable(PhysicsBehaviour::Variable::Name::S_POSITION_Y, 0)) < -10.0) {
			Log::Write("Ai fell out of the world");
			this -> SendEvent(Event(Ai::EVENT_REQUEST_RESET, EventParam(), EventParam()));
			return;
		}

		// Messe zuerst immer allf�llige Daten

		// Registriere Umgebung in Weltged�chtnis
		this -> restartPathFinding |= this -> RegisterEnvironment();

		// Finde Plattformen, auf der die KI gerade steht
		this -> RegisterStandingPlatforms();

		// Berechne Wichtigkeiten f�r physikalische Konstanten
		this -> physicsMemory -> CalculateImportances();

		// Teste Eingabereaktionen
		bool* inputReactionsPossible = this -> TestInputReactions();

		// Messe Konstanten
		bool recalculateTrajectories = false;
		bool* actions = this -> MeasureData(inputReactionsPossible, &recalculateTrajectories);

		// Berechne Flugbahnen neu, falls eine Konstante gewechselt wurde
		if(recalculateTrajectories) {
			this -> worldMemory -> ResetTrajectories();
		}

		// Wechsle Frame, indem die Eingaben zur�ckgesetzt werden
		this -> view -> ResetInput();

		// F�hre eventuelle Experimente durch
		bool didExperiment = this -> DoExperiments(actions);

		// Falls aktiv ein Experiment durchgef�hrt wurde, setzte Status auf MEASURING
		// Dadurch kann die KI ausser Kontrolle geraten zugunsten der Messung, dies spielt aber keine Rolle, da die KI sich jederzeit zur�cksetzten kann
		if(didExperiment) {
			this -> currentState = State::MEASURING;
		}

		// F�hre anhand des aktuellen Status verschiedene Aktionen aus

		switch(this -> currentState) {
		case State::SEARCHING_PATH:
		// Suche m�glichen Weg
		{
			// Suche Startplatform
			WorldMemory::Platform* startPlatform = nullptr;
			if(this -> standingPlatforms[1] != nullptr && !this -> standingPlatforms[1] -> HasBeenVisited()) {
				startPlatform = this -> standingPlatforms[1];
			}
			if(this -> standingPlatforms[0] != nullptr && !this -> standingPlatforms[0] -> HasBeenVisited()) {
				startPlatform = this -> standingPlatforms[0];
			}
			if(startPlatform == nullptr) {
				if(this -> standingPlatforms[1] != nullptr) {
					startPlatform = this -> standingPlatforms[1];
				}
				if(this -> standingPlatforms[0] != nullptr) {
					startPlatform = this -> standingPlatforms[0];
				}
			}

			if(startPlatform == nullptr) {
				// KI steht auf keiner Plattform (m�glicherweise Fehler)
				this -> currentState = State::RECOVERING;
				break;
			}

			bool foundTarget = false;

			// Wenn aktuelle Plattform noch nicht besucht wurde, mache das jetzt
			if(!startPlatform -> HasBeenVisited()) {
				foundTarget = true;
				this -> pathController -> ExplorePlatform(startPlatform);
			} else {
				if(startPlatform == this -> worldMemory -> GetEndPlatform()) {
					// KI befindet sich auf Zielplattform
					// Sollte im Normalfall bereits vorher gel�st sein
					this -> pathController -> WalkToPosition(this -> worldMemory -> GetEndPosition().GetX());
					foundTarget = true;
				} else {
					for(list<WorldMemory::Platform*>::iterator i = this -> worldMemory -> GetTargetListBegin(); i != this -> worldMemory -> GetTargetListEnd(); i++) {
						this -> currentTarget = (*i);

						// Suche Pfad
						list<WorldMemory::Trajectory*> path = this -> pathFinding -> FindPath(startPlatform, *i);

						if(path.size() > 0) {
							this -> pathController -> SetPath(path);
							foundTarget = true;
							break;
						}
					}
				}
			}
			if(foundTarget) {
				// Ziel gefunden, wechsle KI-Status
				this -> currentState = Ai::State::FOLLOWING_PATH;
			} else {
				// Kein Ziel gefunden, Reset
				Log::Write("Ai: no possible target found, resetting");
				this -> SendEvent(Event(Ai::EVENT_REQUEST_RESET, EventParam(), EventParam()));
			}

			break;
		}
		case State::FOLLOWING_PATH:
			// Wenn der Weg neu berechnet werden muss und die KI steht,
			// wechsle Status
			if(this -> pathController -> IsStationary() && this -> restartPathFinding) {
				this -> restartPathFinding = false;
				this -> currentState = State::SEARCHING_PATH;
			} else {
				if(this -> pathController -> Update(deltaTime)) {
					this -> currentState = State::SEARCHING_PATH;
				}
			}

			break;
		case State::RECOVERING:
		// Setze Status auf SEARCHING_PATH falls sich die KI View::MAX_VARIABLE_HISTORY_LENGTH lang nicht bewegt hat
		{
			// Setze Status auf SEARCHING_PATH, wenn sich die KI nicht mehr bewegt
			if(this -> stateEndCounter == View::MAX_VARIABLE_HISTORY_LENGTH) {
				this -> currentState = State::SEARCHING_PATH;
				this -> stateEndCounter = 0;
			}
			if(this -> view -> GetData(PhysicsBehaviour::Variable(PhysicsBehaviour::Variable::Name::S_VELOCITY_X, 0)) == 0.0
				&& this -> view -> GetData(PhysicsBehaviour::Variable(PhysicsBehaviour::Variable::Name::S_VELOCITY_Y, 0)) == 0.0) {
				this -> stateEndCounter++;
			} else {
				this -> stateEndCounter = 0;
			}
			break;
		}
		case State::MEASURING:
		// Setze Status auf RECOVERING, falls View::MAX_VARIABLE_HISTORY_LENGTH Frames lang sich die KI nicht bewegt hat
		// oder die KI nach einem Reset View::MAX_VARIABLE_HISTORY_LENGTH Frames lang im freien Fall gewesen ist
		{
			if(this -> stateEndCounter == View::MAX_VARIABLE_HISTORY_LENGTH) {
				this -> currentState = State::RECOVERING; // Setze status auf RECOVERING, damit die KI zum stillstand kommt und SEARCHING_PATH korrekt durchgef�hrt werden kann
				this -> stateEndCounter = 0;
			}

			// Erh�he Z�hler, wenn kein aktives Experiment durchegf�hrt wurde
			if(!didExperiment && this -> stateEndCounter > 0) {
				this -> stateEndCounter++;
			}
			if(!didExperiment
				&& this -> view -> GetData(PhysicsBehaviour::Variable(PhysicsBehaviour::Variable::Name::S_VELOCITY_X, 0)) == 0.0
				&& this -> view -> GetData(PhysicsBehaviour::Variable(PhysicsBehaviour::Variable::Name::S_VELOCITY_Y, 0)) == 0.0) {
				if(this -> stateEndCounter == 0) {
					this -> stateEndCounter = 1;
				}
			} else if(!didExperiment && this -> resetCompleted) {
				if(this -> stateEndCounter == 0) {
					this -> stateEndCounter = 1;
				}
			}
			break;
		}
		}

		delete inputReactionsPossible;
		delete actions;

		this -> resetCompleted = false;
	}
}

void Ai::DrawDebugInformation() {	
	// Zeichne Weltged�chtnis
	this -> worldMemory -> DrawDebugInformation(this -> debugCharacter -> GetPosition());

	// Zeichne aktueller Pfad
	this -> pathController -> DrawDebugInformation();

	// Zeichne Physikged�chtnis
	this -> physicsMemory -> DrawDebugInformation();

	// Zeichne Sichtfeld
	Graphics::Rect fieldOfView = { (int32_t)(Window::GetWidth() / 2 - (int32_t)((Ai::VISION_DISTANCE + 0.5) * Render::BLOCK_SIZE)), (int32_t)(Window::GetHeight() / 2 - (int32_t)((Ai::VISION_DISTANCE + 0.5) * Render::BLOCK_SIZE)), (uint32_t)((Ai::VISION_DISTANCE + 0.5) * Render::BLOCK_SIZE * 2), (uint32_t)((Ai::VISION_DISTANCE + 0.5) * Render::BLOCK_SIZE * 2), 0.5f, 0.0f, 0.5f };
	Graphics::DrawRects(&fieldOfView, 1);
	Graphics::FlushRects();

	// Schreibe Statusinformationen
	wstring currentStateString = L"KI-Status: ";
	if(this -> activated) {
		switch(this -> currentState) {
		case State::SEARCHING_PATH:
			currentStateString += L"Pfad suchen";
			break;
		case State::FOLLOWING_PATH:
			currentStateString += L"Pfad folgen";
			break;
		case State::RECOVERING:
			currentStateString += L"Erholen";
			break;
		case State::MEASURING:
			currentStateString += L"messen";
			break;
		}
	} else {
		currentStateString += L"deaktiviert";
	}
	Graphics::AddTextToQueue(currentStateString, 400, 20, 0.0f, 1.0f, 0.0f);
}

#pragma region pathFinding

bool Ai::RegisterEnvironment() {
	Vector2 characterPosition(this -> view -> GetData(PhysicsBehaviour::Variable(PhysicsBehaviour::Variable::Name::S_POSITION_X, 0)), this -> view -> GetData(PhysicsBehaviour::Variable(PhysicsBehaviour::Variable::Name::S_POSITION_Y, 0)));
	// Koordinaten der ersten bzw. letzten Sichtbaren Bl�cken
	int32_t firstBlockX = (int32_t)floor(characterPosition.GetX() - Ai::VISION_DISTANCE);
	int32_t lastBlockX = (int32_t)ceil(characterPosition.GetX() + Ai::VISION_DISTANCE);
	int32_t firstBlockY = (int32_t)floor(characterPosition.GetY() - Ai::VISION_DISTANCE);
	int32_t lastBlockY = (int32_t)ceil(characterPosition.GetY() + Ai::VISION_DISTANCE);

	for(int32_t i = firstBlockX; i <= lastBlockX; i++) {
		for(int32_t j = lastBlockY; j >= firstBlockY; j--) {
			if(j == lastBlockY) {
				this -> worldMemory -> AddBlock(i, j, WorldMemory::Block{ this -> world -> GetBlock(i, j), WorldMemory::Block::Flag::UNPROCESSED });
			} else {
				bool start = false;
				bool end = false;
				if(i == this -> world -> GetStartX() && j == this -> world -> GetStartY()) {
					start = true;
				}
				if(i == this -> world -> GetEndX() && j == this -> world -> GetEndY()) {
					end = true;
				}

				this -> worldMemory -> AddBlock(i, j, WorldMemory::Block{ this -> world -> GetBlock(i, j), WorldMemory::Block::Flag::NONE }, start, end);
			}
		}
	}

	bool returnValue = this -> worldMemory -> HasNewRelations();

	for(list<WorldMemory::PlatformRelation*>::iterator i = this -> worldMemory -> GetNewRelationsBegin(); i != this -> worldMemory -> GetNewRelationsEnd(); i++) {
		this -> CalculateTrajectories(*i);
	}
	this -> worldMemory -> ResetNewRelationsList();

	this -> worldMemory -> CreateTargetList(characterPosition);

	return returnValue;
}

void Ai::RegisterStandingPlatforms() {
	this -> standingPlatforms[0] = nullptr;
	this -> standingPlatforms[1] = nullptr;

	if(this -> view -> GetData(PhysicsBehaviour::Variable(PhysicsBehaviour::Variable::Name::B_COLLISION_D, 0)) != 1.0) {
		return;
	}

	Vector2 characterPosition(this -> view -> GetData(PhysicsBehaviour::Variable(PhysicsBehaviour::Variable::Name::S_POSITION_X, 0)), this -> view -> GetData(PhysicsBehaviour::Variable(PhysicsBehaviour::Variable::Name::S_POSITION_Y, 0)));
	int32_t x1 = (int32_t)floor(characterPosition.GetX());
	int32_t x2 = (int32_t)ceil(characterPosition.GetX());
	int32_t y = (int32_t)round(characterPosition.GetY()) - 1;

	WorldMemory::Block b1 = this -> worldMemory -> GetBlock(x1, y);
	WorldMemory::Block b2 = this -> worldMemory -> GetBlock(x2, y);

	if(!(b1.flags & WorldMemory::Block::Flag::UNKNOWN) && (b1.block != 0)) {
		WorldMemory::Chunk* chunk = this -> worldMemory -> GetChunkFromBlock((uint32_t)x1, (uint32_t)y);
		for(list<WorldMemory::Platform*>::iterator i = chunk -> GetPlatformBeginIterator(); i != chunk -> GetPlatformEndIterator(); i++) {
			if((*i) -> IsInPlatform(x1, y)) {
				this -> standingPlatforms[0] = (*i);
			}
		}
	}
	if(!(b2.flags & WorldMemory::Block::Flag::UNKNOWN) && (b2.block != 0)) {
		WorldMemory::Chunk* chunk = this -> worldMemory -> GetChunkFromBlock((uint32_t)x2, (uint32_t)y);
		for(list<WorldMemory::Platform*>::iterator i = chunk -> GetPlatformBeginIterator(); i != chunk -> GetPlatformEndIterator(); i++) {
			if((*i) -> IsInPlatform(x2, y)) {
				this -> standingPlatforms[1] = (*i);
			}
		}
	}
}

void Ai::CalculateTrajectories(WorldMemory::PlatformRelation* relation) {
	pair<WorldMemory::Platform*, WorldMemory::Platform*> platforms = relation -> GetPlatforms();

	this -> CalculateTrajectory(relation -> GetTrajectoryTo(platforms.first));
	this -> CalculateTrajectory(relation -> GetTrajectoryTo(platforms.second));
}

void Ai::CalculateTrajectory(WorldMemory::Trajectory * trajectory) {
	// Manche Flugbahnen werden als m�glich dargestellt, sind aber offensichtlich unm�glich.
	// Diese Flugbahnen sind meistens suboptimal und werden gar nie in einem Pfad vorkommen.
	// Falls trotzdem eine solche ausgef�hrt wird, merkt die KI, dass nicht alles nach Plan verl�uft und markiert die Flugbahn nachtr�glich als unm�glich

	// Distanzberechnungen analog zu WorldMemory::PlatformRelation::CalculateDistance() in worldMemory.cpp

	trajectory -> Reset();

	double left1 = trajectory -> start -> x;
	double left2 = trajectory -> end -> x;
	double right1 = trajectory -> start -> x + trajectory -> start -> width;
	double right2 = trajectory -> end -> x + trajectory -> end -> width;

	// dY
	trajectory -> dY = (double)trajectory -> end -> y - (double)trajectory -> start -> y;

	// Unten wird zwischen rechts und links entschieden
	// Ist eine Flugbahn in beide Richtungen m�glich wird standardm�ssig nur links mit angegeben
	// Trifft nur f�r Flugbahnen vom Typ 2 und 3 zu
	bool bothDirectionsPossible = false;

	// dX
	trajectory -> dX = 0.0;
	trajectory -> xDirection = 0.0;
	if(trajectory -> dY >= 0.0) {
		// Nach oben
		if(left1 < left2) {
			// Nach rechts
			trajectory -> dX = left2 - right1;
			trajectory -> xDirection = 1.0;
		}
		if(right1 > right2) {
			// Nach links
			if(trajectory -> xDirection > 0.0) {
				bothDirectionsPossible = true;
			}
			trajectory -> dX = left1 - right2;
			trajectory -> xDirection = -1.0;
		}
	} else {
		// Nach unten
		if(right1 < right2) {
			// Nach rechts
			trajectory -> dX = left2 - right1;
			trajectory -> xDirection = 1.0;
		}
		if(left1 > left2) {
			// Nach links
			if(trajectory -> xDirection > 0.0) {
				bothDirectionsPossible = true;
			}
			trajectory -> dX = left1 - right2;
			trajectory -> xDirection = -1.0;
		}
	}
	if(trajectory -> dX < 0.0) {
		trajectory -> dX = 0.0;
	}
	if(trajectory -> xDirection == 0.0) {
		trajectory -> dX = 0.0;
		trajectory -> possible = false;
	}

	// dX_
	trajectory -> dX_ = trajectory -> dX - 1.0 + Ai::CHARACTER_SIZE_BIAS;
	if(trajectory -> dX_ < 0.0) {
		trajectory -> dX_ = 0.0; // Plattformen �berschneiden sich
	}

	// Wenn Flugbahn bereits jetzt als unm�glich eingestuft wird, ist sie wirklich unm�glich
	if(!trajectory -> possible) {
		return;
	}

	// Suche Wissen zu verwendeten Konstanten
	double gravitationalAcceleration = abs(this -> physicsMemory -> GetConstantKnowledge(PhysicsBehaviour::Constant::S_GRAVITY) -> value);
	double airAcceleration = this -> physicsMemory -> GetConstantKnowledge(PhysicsBehaviour::Constant::S_HORIZONTAL_FORCE_AIR) -> value;
	double groundForce = this -> physicsMemory -> GetConstantKnowledge(PhysicsBehaviour::Constant::S_HORIZONTAL_FORCE_GROUND) -> value;
	double maxHorizontalVelocity = this -> physicsMemory -> GetConstantKnowledge(PhysicsBehaviour::Constant::S_MAX_HORIZONTAL_VELOCITY) -> value;
	double jumpImpulse = this -> physicsMemory -> GetConstantKnowledge(PhysicsBehaviour::Constant::S_JUMP_IMPULSE) -> value;
	double groundFrictionStart = this -> physicsMemory -> GetConstantKnowledge(PhysicsBehaviour::Constant::S_PER_MATERIAL_FRICTION_COEFFICIENT, trajectory -> start -> block) -> value;
	double groundFrictionEnd = this -> physicsMemory -> GetConstantKnowledge(PhysicsBehaviour::Constant::S_PER_MATERIAL_FRICTION_COEFFICIENT, trajectory -> end -> block) -> value;

	// Setze possible auf false, sp�ter wird je nachdem wieder auf true gesetzt
	trajectory -> possible = false;

	// Teste, ob sich die KI auf der Startplattform �berhaupt bewegen kann
	// In diesem Fall ist gar nichts mehr m�glich
	if(gravitationalAcceleration > groundForce) {
		trajectory -> possible = false;
		// Berechnung abgeschlossen, kein Typ kommt in Frage
		return;
	}

	// 1. Typ: Plattformen sind auf der gleichen H�he und nicht mehr als ein Block voneinander entfernt
	if(!trajectory -> possible && trajectory -> end -> y == trajectory -> start -> y && trajectory -> dX <= 1.0) {
		trajectory -> t = 0.0;
		trajectory -> tA = 0.0;
		trajectory -> v0 = 0.0;
		trajectory -> vE = 0.0;
		if(trajectory -> xDirection > 0.0) {
			trajectory -> startPosition = trajectory -> end -> x;
		} else {
			trajectory -> startPosition = trajectory -> end -> x + trajectory -> end -> width - 1.0;
		}
		trajectory -> liftoffPosition = 0.0;
		trajectory -> touchdownPosition = 0.0;

		// Teste, ob Bl�cke im Weg sind
		if(this -> worldMemory -> GetBlock((int32_t)round(trajectory -> startPosition - trajectory -> xDirection), trajectory -> start -> y + 1).block != 0) {
			trajectory -> possible = false;
			return;
		}

		trajectory -> type = WorldMemory::Trajectory::Type::ADJACENT;
		trajectory -> possible = true;
	}

	// 2. Typ: Plattformen bilden eine Stufe, Flugbahn geht nach oben
	if(!trajectory -> possible && trajectory -> dY > 0.0 && trajectory -> dX_ == 0.0) {
		// h(t) = h0 + v0 * t - 0.5 * g * t^2
		// h'(t) = v0 - g * t
		// t = (vY0 + sqrt(vY0^2 - 2 * g * (h - h0))) / g
		// tA = t - vY0 / g
		double vY0 = jumpImpulse;

		if(2.0 * gravitationalAcceleration * (trajectory -> dY + Ai::CHARACTER_SIZE_BIAS) > pow(vY0, 2)) {
			// KI kann nicht genug hoch springen
			return;
		}

		trajectory -> t = (vY0 + sqrt(pow(vY0, 2) - 2.0 * gravitationalAcceleration * (trajectory -> dY + Ai::CHARACTER_SIZE_BIAS))) / gravitationalAcceleration;
		trajectory -> tA = trajectory -> t - vY0 / gravitationalAcceleration;
		trajectory -> v0 = 10.0 * Ai::CHARACTER_SIZE_BIAS / trajectory -> tA;
		trajectory -> vE = 0.0;

		this -> CalculateTrajectoryType2(trajectory);
		if(!trajectory -> possible && bothDirectionsPossible) {
			trajectory -> xDirection = 1.0;
			this -> CalculateTrajectoryType2(trajectory);
		}
		trajectory -> type = WorldMemory::Trajectory::Type::STAIR;

		// Ob m�glich oder nicht, weiterer Code kann daran nichts mehr �ndern
		return;
	}

	// 3. Typ: end liegt unterhalb der Startplattform und kann m�glicherweise ohne Sprung erreicht werden
	// Pr�fe diesn Fall nur, wenn vorher keine g�ltige Flugbahn gefunden wurde
	// Alle Flugbahnen, die mit Typ 3 beschrieben werden k�nnen lassen sich auch als Typ 4 beschreiben
	if(!trajectory -> possible && trajectory -> dY < 0.0) {
		// Pr�fen, ob Bl�cke im Weg sind
		{
			uint32_t checkBlockX;
			if(trajectory -> xDirection > 0.0) {
				checkBlockX = trajectory -> start -> x + trajectory -> start -> width;
			} else {
				checkBlockX = trajectory -> start -> x - 1;
			}
			if(this -> worldMemory -> GetBlock(checkBlockX, trajectory -> start -> y).block != 0 || this -> worldMemory -> GetBlock(checkBlockX, trajectory -> start -> y + 1).block != 0) {
				return; // Es existiert ein k�rzerer Weg, die letzte Variant wird nicht verwendet
			}
		}

		// Zeit, die ben�tigt wird, um den Weg vertikal zur�ckzulegen
		double t = sqrt(2.0 * abs(trajectory -> dY) / gravitationalAcceleration);
		double tA = 0.0;
		double v0 = 0.0;
		double vE = 0.0;
		if(t * t >= 2.0 * trajectory -> dX_ / airAcceleration) {
			// Fall A: Die KI kann im Flug abbremsen
			v0 = airAcceleration * t - airAcceleration * sqrt(t * t - 2.0 * trajectory -> dX_ / airAcceleration);
			tA = v0 / airAcceleration;
		} else {
			// Fall B: Die KI kommt mit einer Restgeschwindigkeit an
			v0 = (airAcceleration * t * t + 2.0 * trajectory -> dX_) / (2.0 * t);
			tA = t;
			vE = v0 - airAcceleration * t;
		}

		if(v0 < Ai::EPSILON) {
			trajectory -> t = t;

			this -> CalculateTrajectoryType3(trajectory);
			if(!trajectory -> possible && bothDirectionsPossible) {
				trajectory -> xDirection = 1.0;
				this -> CalculateTrajectoryType3(trajectory);
			}

			trajectory -> type = WorldMemory::Trajectory::Type::FALL;

			// Ob m�glich oder nicht, weiterer Code kann daran nichts mehr �ndern
			return;
		}

		// Fall C: Wenn v0 zu gross is, nimm v0 maximal und f�ge die maximale Bremszeit hinzu
		// EDIT: Da maxHorizontalVelocity == inf, treffen diese F�lle nie ein
		if(v0 > maxHorizontalVelocity) {
			if(maxHorizontalVelocity * t >= trajectory -> dX_) {
				v0 = maxHorizontalVelocity;
				tA = sqrt(2.0 * (v0 * t - trajectory -> dX_) / airAcceleration);
				vE = v0 - airAcceleration * tA;
			} else {
				// Fall D: Flugbahn ist mit diesem Typen unm�glich
				goto skipCase3;
			}
		}

		// Enposition ist Kante der Plattform
		double liftoffPosition;
		if(trajectory -> xDirection > 0.0) {
			liftoffPosition = trajectory -> start -> x + trajectory -> start -> width + Ai::CHARACTER_SIZE_BIAS;
		} else {
			liftoffPosition = trajectory -> start -> x - 1.0 - Ai::CHARACTER_SIZE_BIAS;
		}

		// Berechne Startposition
		double startPosition = 0.0;
		if(!this -> CalculateTrajectoryStartPosition(trajectory, trajectory -> start, liftoffPosition, v0, &startPosition)) {
			// Fall E: KI kann auf Plattform nicht starten, mache v0 minimal
			v0 = trajectory -> dX_ / t;
			tA = 0.0;
			vE = v0;

			if(!this -> CalculateTrajectoryStartPosition(trajectory, trajectory -> start, liftoffPosition, v0, &startPosition)) {
				goto skipCase3;
			}
		}

		// Landung
		double touchdownPosition = liftoffPosition + trajectory -> dX_ * trajectory -> xDirection;

		// Wenn m�glich, speichere Daten
		trajectory -> type = WorldMemory::Trajectory::Type::FALL;
		trajectory -> possible = true;

		trajectory -> t = t;
		trajectory -> tA = tA;
		trajectory -> v0 = v0;
		trajectory -> vE = vE;

		trajectory -> startPosition = startPosition;
		trajectory -> liftoffPosition = liftoffPosition;
		trajectory -> touchdownPosition = touchdownPosition;
	}
skipCase3:

	// 4. Typ: Ein Sprung muss verwendet werden
	// Es besteht eine L�cke zwischen den beiden Plattformen
	if(!trajectory -> possible) {
		trajectory -> dX_ += 4.0 * Ai::CHARACTER_SIZE_BIAS;

		double vY0 = jumpImpulse;

		if(2.0 * gravitationalAcceleration * (trajectory -> start -> y - trajectory -> end -> y) + pow(vY0, 2) < 0.0) {
			// KI kann nicht genug hoch springen
			return;
		}

		double t = (vY0 + sqrt(2.0 * gravitationalAcceleration * ((double)trajectory -> start -> y - (double)trajectory -> end -> y) + pow(vY0, 2))) / gravitationalAcceleration;

		double tA = 0.0;
		double v0 = 0.0;
		double vE = 0.0;
		if(t * t >= 2.0 * trajectory -> dX_ / airAcceleration) {
			// Fall A: Die KI kann im Flug abbremsen
			v0 = airAcceleration * t - airAcceleration * sqrt(t * t - 2.0 * trajectory -> dX_ / airAcceleration);
			tA = v0 / airAcceleration;
		} else {
			// Fall B: Die KI kommt mit einer Restgeschwindigkeit an
			v0 = (airAcceleration * t * t + 2.0 * trajectory -> dX_) / (2.0 * t);
			tA = t;
			vE = v0 - airAcceleration * t;
		}

		// Fall C: Wenn v0 zu gross is, nimm v0 maximal und f�ge die maximale Bremszeit hinzu
		// EDIT: Da maxHorizontalVelocity == inf, treffen diese F�lle nie ein
		if(v0 > maxHorizontalVelocity) {
			if(maxHorizontalVelocity * t >= trajectory -> dX_) {
				v0 = maxHorizontalVelocity;
				tA = sqrt(2.0 * (v0 * t - trajectory -> dX_) / airAcceleration);
				vE = v0 - airAcceleration * tA;
			} else {
				// Fall D: Flugbahn ist unm�glich
				return;
			}
		}

		// Berechne Positionen
		double touchdownPosition;
		if(trajectory -> xDirection > 0.0) {
			touchdownPosition = trajectory -> end -> x - 1.0 + 3.0 * Ai::CHARACTER_SIZE_BIAS;
		} else {
			touchdownPosition = trajectory -> end -> x + trajectory -> end -> width - 3.0 * Ai::CHARACTER_SIZE_BIAS;
		}

		double liftoffPosition = touchdownPosition - trajectory -> dX_ * trajectory -> xDirection;

		double startPosition;

		if(!this -> CalculateTrajectoryStartPosition(trajectory, trajectory -> start, liftoffPosition, v0, &startPosition)) {
			// Fall E: KI kann auf Plattform nicht starten, mache v0 minimal
			v0 = trajectory -> dX_ / t;
			tA = 0.0;
			vE = v0;

			if(!this -> CalculateTrajectoryStartPosition(trajectory, trajectory -> start, liftoffPosition, v0, &startPosition)) {
				return;
			}
		}

		// Wenn m�glich, speichere Daten
		trajectory -> type = WorldMemory::Trajectory::Type::JUMP;
		trajectory -> possible = true;

		trajectory -> t = t;
		trajectory -> tA = tA;
		trajectory -> v0 = v0;
		trajectory -> vE = vE;

		trajectory -> startPosition = startPosition;
		trajectory -> liftoffPosition = liftoffPosition;
		trajectory -> touchdownPosition = touchdownPosition;
	}

	return;
}

void Ai::CalculateTrajectoryType2(WorldMemory::Trajectory* trajectory) {
	if(trajectory -> xDirection > 0.0) {
		trajectory -> startPosition = trajectory -> end -> x - 1.0;
	} else {
		trajectory -> startPosition = trajectory -> end -> x + trajectory -> end -> width;
	}

	bool blockPerfect; // Gibt an, ob die KI einen Block verwenden kann, um sich perfekt auszurichten
	trajectory -> liftoffPosition = trajectory -> startPosition;
	trajectory -> touchdownPosition = trajectory -> startPosition;
	if(this -> worldMemory -> GetBlock((int32_t)round(trajectory -> startPosition) + 1, trajectory -> start -> y + 1).block != 0) {
		trajectory -> startPosition += Ai::CHARACTER_SIZE_BIAS;
		blockPerfect = true;
	} else if(this -> worldMemory -> GetBlock((int32_t)round(trajectory -> startPosition) - 1, trajectory -> start -> y + 1).block != 0) {
		trajectory -> startPosition -= Ai::CHARACTER_SIZE_BIAS;
		blockPerfect = true;
	} else {
		trajectory -> startPosition -= trajectory -> xDirection * Ai::CHARACTER_SIZE_BIAS;
		blockPerfect = false;
	}

	// Teste Flugbahn
	uint32_t testY = trajectory -> start -> y + 1;
	uint32_t maxY = trajectory -> end -> y + 2;
	for(; testY <= maxY; testY++) {
		if(this -> worldMemory -> GetBlock((int32_t)round(trajectory -> liftoffPosition), testY).block != 0) {
			trajectory -> possible = false;
			return;
		}
		if(!blockPerfect) {
			if(this -> worldMemory -> GetBlock((int32_t)round(trajectory -> liftoffPosition - trajectory -> xDirection), testY).block != 0) {
				trajectory -> possible = false;
				return;
			}
		}
	}

	trajectory -> possible = true;
}

void Ai::CalculateTrajectoryType3(WorldMemory::Trajectory* trajectory) {
	// KI muss sich nur fallen lassen
	trajectory -> tA = 0.0;
	trajectory -> v0 = 0.0;
	trajectory -> vE = 0.0;

	double liftoffPosition;
	if(trajectory -> xDirection > 0.0) {
		liftoffPosition = trajectory -> start -> x + trajectory -> start -> width;
	} else {
		liftoffPosition = trajectory -> start -> x - 1.0;
	}
	trajectory -> startPosition = liftoffPosition + trajectory -> xDirection * Ai::CHARACTER_SIZE_BIAS;
	trajectory -> liftoffPosition = liftoffPosition + trajectory -> xDirection * Ai::CHARACTER_SIZE_BIAS;
	trajectory -> touchdownPosition = liftoffPosition + trajectory -> xDirection * Ai::CHARACTER_SIZE_BIAS;

	// Teste Flugbahn
	uint32_t testY = trajectory -> start -> y + 1;
	uint32_t minY = trajectory -> end -> y + 1;
	for(; testY >= minY; testY--) {
		if(this -> worldMemory -> GetBlock((int32_t)round(liftoffPosition), testY).block != 0) {
			trajectory -> possible = false;
			return;
		}
	}

	trajectory -> possible = true;
}

bool Ai::CalculateTrajectoryStartPosition(WorldMemory::Trajectory* trajectory, WorldMemory::Platform* start, double liftoffPosition, double v0, double* startPosition) {
	*startPosition = liftoffPosition;

	double remainingV = v0;
	double length;
	WorldMemory::Platform* currentPlatform = start;

	// Berechne erster �berlappbereich
	double currentFrictionCoefficient = this -> physicsMemory -> GetConstantKnowledge(PhysicsBehaviour::Constant::S_PER_MATERIAL_FRICTION_COEFFICIENT, currentPlatform -> block) -> value;
	length = 1.0;
	double startV = this -> StartVelocityOnPlatform(currentFrictionCoefficient, &length, remainingV);
	*startPosition -= trajectory -> xDirection * length;
	remainingV = startV;

	while(true) {
		if(remainingV < Ai::EPSILON) {
			break;
		}

		currentFrictionCoefficient = this -> physicsMemory -> GetConstantKnowledge(PhysicsBehaviour::Constant::S_PER_MATERIAL_FRICTION_COEFFICIENT, currentPlatform -> block) -> value;

		// Berechne zuerst maximale Geschwindigkeit, die auf einer Plattform erreicht werden kann
		length = currentPlatform -> width - 1; // Entspricht der L�nge der Plattform minus 1, da im vorherigen Schritt bereits die �berlappzone beschrieben wurde
		startV = this -> StartVelocityOnPlatform(currentFrictionCoefficient, &length, remainingV);
		*startPosition -= trajectory -> xDirection * length;
		remainingV = startV;
		if(remainingV < Ai::EPSILON) {
			break;
		}

		// Berechne danach die maximale Geschwidigkeit die am Ende der Plattform erreicht werden kann
		int32_t nextBlockX = (int32_t)round(*startPosition) - (int32_t)round(trajectory -> xDirection);

		if(this -> worldMemory -> GetBlock(nextBlockX, currentPlatform -> y + 1).block != 0) {
			// Block ist im Weg, es ist somit zu wenig Platz zum Beschleunigen vorhanden
			return false;
		}

		WorldMemory::Platform* nextPlatform = this -> worldMemory -> GetPlatformAt(nextBlockX, currentPlatform -> y);
		if(nextPlatform != nullptr) {
			// Direkt angrenzende Plattform gefunden
			double nextFrictionCoefficient = this -> physicsMemory -> GetConstantKnowledge(PhysicsBehaviour::Constant::S_PER_MATERIAL_FRICTION_COEFFICIENT, nextPlatform -> block) -> value;

			length = 1.0;
			startV = this -> StartVelocityOnPlatform(max(currentFrictionCoefficient, nextFrictionCoefficient), &length, remainingV);
			*startPosition -= trajectory -> xDirection * length;
			remainingV = startV;
		} else {
			nextPlatform = this -> worldMemory -> GetPlatformAt(nextBlockX - (int32_t)round(trajectory -> xDirection), currentPlatform -> y);
			if(nextPlatform != nullptr) {
				// Angrenzende Plattform mit einem Block Abstand gefunden
				double nextFrictionCoefficient = this -> physicsMemory -> GetConstantKnowledge(PhysicsBehaviour::Constant::S_PER_MATERIAL_FRICTION_COEFFICIENT, nextPlatform -> block) -> value;

				length = 1.0;
				startV = this -> StartVelocityOnPlatform(currentFrictionCoefficient, &length, remainingV);
				*startPosition -= trajectory -> xDirection * length;
				remainingV = startV;

				length = 1.0;
				startV = this -> StartVelocityOnPlatform(nextFrictionCoefficient, &length, remainingV);
				*startPosition -= trajectory -> xDirection * length;
				remainingV = startV;
			} else {
				// Berechne Beschleunigung auf Kante

				length = 1.0 - Ai::CHARACTER_SIZE_BIAS;
				startV = this -> StartVelocityOnPlatform(currentFrictionCoefficient, &length, remainingV);
				*startPosition -= trajectory -> xDirection * length;
				remainingV = startV;

				if(remainingV < Ai::EPSILON) {
					return true;
				} else {
					return false;
				}
			}
		}

		// Die ben�tigte Distanz wurde am �bergang zwischen zwei Plattformen erreicht
		if(remainingV < Ai::EPSILON) {
			break;
		}

		// Falls dieser Code erreicht wird, ist eine weitere Plattform gefunden worden, die zum Beschleunigen verwendet werden kann
		currentPlatform = nextPlatform;
	}

	return true;
}

double Ai::StartVelocityOnPlatform(double frictionCoefficient, double* length, double vE) {
	double gravitationalAcceleration = abs(this -> physicsMemory -> GetConstantKnowledge(PhysicsBehaviour::Constant::S_GRAVITY) -> value);
	double horizontalForceGround = this -> physicsMemory -> GetConstantKnowledge(PhysicsBehaviour::Constant::S_HORIZONTAL_FORCE_GROUND) -> value;

	if(2.0 * (horizontalForceGround - gravitationalAcceleration) * frictionCoefficient * *length > vE * vE) {
		double minLength = vE * vE / (2.0 * (horizontalForceGround - gravitationalAcceleration) * frictionCoefficient) + Ai::CHARACTER_SIZE_BIAS;
		*length = min(*length, minLength);
		return 0.0;
	} else {
		return sqrt(vE * vE - 2.0 * (horizontalForceGround - gravitationalAcceleration) * frictionCoefficient * *length);
	}
}

#pragma endregion

#pragma region physicsMeasurment

bool* Ai::TestInputReactions() {
	bool* inputReactionsPossible = new bool[PhysicsBehaviour::GetInputReactionsCount()];
	::memset(inputReactionsPossible, (int32_t)true, PhysicsBehaviour::GetInputReactionsCount() * sizeof(bool));

	for(uint32_t i = 0; i < PhysicsBehaviour::GetInputReactionsCount(); i++) {
		PhysicsBehaviour::InputReaction const* reaction = PhysicsBehaviour::GetInputReaction(i);

		for(uint32_t j = 0; j < reaction -> inputConditionsCount; j++) {
			ConditionCheckReturn* returnValue = this -> CheckCondition(reaction -> inputConditions[j]);

			if(!returnValue -> allConstantsKnown) {
				inputReactionsPossible[i] = false;
			}

			if(!returnValue -> fulfilled) {
				inputReactionsPossible[i] = false;
			}

			delete returnValue;
		}
	}

	return inputReactionsPossible;
}

bool* Ai::MeasureData(bool* inputReactionsPossible, bool* recalculateTrajectories) {
	// Position, die f�r die phyiskalischen Berechnungen in der letzten Frame relevant war
	Vector2 physicsRelevantPosition(this -> view -> GetData(PhysicsBehaviour::Variable(PhysicsBehaviour::Variable::Name::S_POSITION_X, 1)), this -> view -> GetData(PhysicsBehaviour::Variable(PhysicsBehaviour::Variable::Name::S_POSITION_Y, 1)));

	bool* actions = new bool[4];
	::memset(actions, 0, 4 * sizeof(bool));
	bool actionsChosen = false;

	// Pr�fe alle Formeln
	// Falls eine Formel nicht verwendet werden kann, pr�fe ob sie durch eine Aktion verwendet werden kann
	for(uint32_t i = 0; i < PhysicsBehaviour::GetConstantFormulaeCount(); i++) {
		PhysicsBehaviour::ConstantFormula const* formula = PhysicsBehaviour::GetConstantFormula(i);

		// Statusvariabeln
		bool allConstantsKnown = true;
		bool allConditionsMet = true;
		Character::Side collisionSide = Character::Side::TOP; // Wird verwendet, um herauszufinden, welcher Reibungskoeffizient gemeint ist
		Character::Axis impulseAxis = Character::Axis::X_AXIS; // Wird verwendet, um herauszufinden, welcher Impulserhaltungskoeffizient gemeint ist

		// Teste, ob alle als Bedingung benutzten Konstanten bekannt sind
		// Falls ja, teste, ob die Bedingung erf�llt ist
		// Falls die Bedingung nicht erf�llt ist, teste, ob sie nach einer Aktion erf�llt ist
		
		// Zeigt an, ob die gesammte Formel gel�st werden kann
		bool completelyPossible = true;

		bool localActions[4];
		::memset(localActions, 0, 4 * sizeof(bool));

		for(uint32_t j = 0; j < formula -> conditionsCount; j++) {
			PhysicsBehaviour::Condition condition = formula -> conditions[j];

			ConditionCheckReturn* returnValue = this -> CheckCondition(condition);

			// Breche Test f�r Formel ab, wenn nicht alle verwendeten Konstanten bekannt sind
			if(!returnValue -> allConstantsKnown) {
				allConstantsKnown = false;
				goto abortTests;
			}

			if(!returnValue -> fulfilled) {
				allConditionsMet = false;
			}
			if(!actionsChosen && (!returnValue -> fulfilled || returnValue -> needsInputNextFrame)) {
				bool conditionSolvable = false;
				
				for(uint32_t k = 0; k < PhysicsBehaviour::GetInputReactionsCount(); k++) {
					if(inputReactionsPossible[k]) {
						PhysicsBehaviour::InputReaction const* reaction = PhysicsBehaviour::GetInputReaction(k);

						if(this -> FulfillsAfterAction(reaction, condition)) {
							//localActionCount[(uint32_t)reaction -> triggerInput]++;
							localActions[(uint32_t)reaction -> triggerInput] = true;
							conditionSolvable = true;
							break;
						}
					}
				}
				if(!conditionSolvable) {
					completelyPossible = false;
				}
			}

			// Speichere m�glicherweise vorhandene zus�tzliche Information
			if(returnValue -> givesCollisionInformation) {
				collisionSide = returnValue -> collisionSide;
			}
			if(returnValue -> givesImpulseChangeInformation) {
				impulseAxis = returnValue -> impulseChangeAxis;
			}

			delete returnValue;
		}

		// Suche Block-IDs f�r Bl�cke, die f�r den Reibungskoeffizienten und Impulserhaltungskoeffizienten wichtig sind
		uint32_t frictionBlock1;
		uint32_t frictionBlock2;
		uint32_t impulseChangeBlock1;
		uint32_t impulseChangeBlock2;
		// Positionsberechnungen analog zu den Reibungsberechnungen in physicsEngine.cpp
		switch(collisionSide) {
		case Character::Side::TOP:
			frictionBlock1 = this -> world -> GetBlock((int32_t)floor(physicsRelevantPosition.GetX()), (int32_t)(physicsRelevantPosition.GetY() + 1.0));
			frictionBlock2 = this -> world -> GetBlock((int32_t)floor(physicsRelevantPosition.GetX() + 1.0), (int32_t)(physicsRelevantPosition.GetY() + 1.0));
			break;
		case Character::Side::BOTTOM:
			frictionBlock1 = this -> world -> GetBlock((int32_t)floor(physicsRelevantPosition.GetX()), (int32_t)(physicsRelevantPosition.GetY() - 1.0));
			frictionBlock2 = this -> world -> GetBlock((int32_t)floor(physicsRelevantPosition.GetX() + 1.0), (int32_t)(physicsRelevantPosition.GetY() - 1.0));
			break;
		case Character::Side::RIGHT:
			frictionBlock1 = this -> world -> GetBlock((int32_t)(physicsRelevantPosition.GetX() + 1.0), (int32_t)floor(physicsRelevantPosition.GetY()));
			frictionBlock2 = this -> world -> GetBlock((int32_t)(physicsRelevantPosition.GetX() + 1.0), (int32_t)floor(physicsRelevantPosition.GetY() + 1.0));
			break;
		case Character::Side::LEFT:
			frictionBlock1 = this -> world -> GetBlock((int32_t)(physicsRelevantPosition.GetX() - 1.0), (int32_t)floor(physicsRelevantPosition.GetY()));
			frictionBlock2 = this -> world -> GetBlock((int32_t)(physicsRelevantPosition.GetX() - 1.0), (int32_t)floor(physicsRelevantPosition.GetY() + 1.0));
			break;
		}
		if(impulseAxis == Character::Axis::X_AXIS) {
			if(this -> view -> GetData(PhysicsBehaviour::Variable(PhysicsBehaviour::Variable::Name::B_COLLISION_L, 1))) {
				impulseChangeBlock1 = this -> world -> GetBlock((int32_t)(physicsRelevantPosition.GetX() - 1.0), (int32_t)floor(physicsRelevantPosition.GetY()));
				impulseChangeBlock2 = this -> world -> GetBlock((int32_t)(physicsRelevantPosition.GetX() - 1.0), (int32_t)floor(physicsRelevantPosition.GetY() + 1.0));
			} else {
				impulseChangeBlock1 = this -> world -> GetBlock((int32_t)(physicsRelevantPosition.GetX() + 1.0), (int32_t)floor(physicsRelevantPosition.GetY()));
				impulseChangeBlock2 = this -> world -> GetBlock((int32_t)(physicsRelevantPosition.GetX() + 1.0), (int32_t)floor(physicsRelevantPosition.GetY() + 1.0));
			}
		} else {
			if(this -> view -> GetData(PhysicsBehaviour::Variable(PhysicsBehaviour::Variable::Name::B_COLLISION_U, 1))) {
				impulseChangeBlock1 = this -> world -> GetBlock((int32_t)floor(physicsRelevantPosition.GetX()), (int32_t)(physicsRelevantPosition.GetY() + 1.0));
				impulseChangeBlock2 = this -> world -> GetBlock((int32_t)floor(physicsRelevantPosition.GetX() + 1.0), (int32_t)(physicsRelevantPosition.GetY() + 1.0));
			} else {
				impulseChangeBlock1 = this -> world -> GetBlock((int32_t)floor(physicsRelevantPosition.GetX()), (int32_t)(physicsRelevantPosition.GetY() - 1.0));
				impulseChangeBlock2 = this -> world -> GetBlock((int32_t)floor(physicsRelevantPosition.GetX() + 1.0), (int32_t)(physicsRelevantPosition.GetY() - 1.0));
			}
		}

		// F�lle Parameter ein und teste,
		// ob alle als Parameter verwendeten Konstanten bekannt sind

		double* inputValues = new double[formula -> parametersCount];
		for(uint32_t j = 0; j < formula -> parametersCount; j++) {
			switch(formula -> parameters[j].type) {
			case PhysicsBehaviour::Value::Type::CONSTANT:
			{
				// knowledge1 und knowledge2 unterscheiden sich nur, falls eine materialabh�ngige Konstante verwendet wird
				PhysicsMemory::ConstantKnowledge* knowledge1 = this -> physicsMemory -> GetConstantKnowledge(formula -> parameters[j].constantValue, frictionBlock1);
				PhysicsMemory::ConstantKnowledge* knowledge2 = this -> physicsMemory -> GetConstantKnowledge(formula -> parameters[j].constantValue, frictionBlock2);

				PhysicsMemory::ConstantKnowledge::State state1 = PhysicsMemory::ConstantKnowledge::State::NONE;
				PhysicsMemory::ConstantKnowledge::State state2 = PhysicsMemory::ConstantKnowledge::State::NONE;
				double value1 = -numeric_limits<double>::infinity();
				double value2 = -numeric_limits<double>::infinity();
				double importance1 = 4.0;
				double importance2 = 4.0;
				if(knowledge1 != nullptr) {
					state1 = knowledge1 -> state;
					value1 = knowledge1 -> value;
					importance1 = knowledge1 -> importance;
				}
				if(knowledge2 != nullptr) {
					state2 = knowledge2 -> state;
					value2 = knowledge2 -> value;
					importance2 = knowledge2 -> importance;
				}

				// Teste, ob Konstante bereits bekannt ist
				// Falls eine Materialabh�ngige Konstante gepr�ft wird, erm�glicht der &&-Operator das Pr�fen der zwei verschiedenen Bl�cken, die den Spieler ber�hren
				if(!((state1 == PhysicsMemory::ConstantKnowledge::State::UNSURE || state2 == PhysicsMemory::ConstantKnowledge::State::UNSURE) || (importance1 < 1.0 || importance2 < 1.0))) {
					// Formel kann nicht verwendet werden, verlasse Schleife
					allConstantsKnown = false;
					goto abortTests;
				}
				// Bei materialabh�ngigen Konstanten wird immer der gr�ssere Wert genommen, ansosten besteht kein Unterschied
				inputValues[j] = max(value1, value2);
				break;
			}
			case PhysicsBehaviour::Value::Type::VARIABLE:
			{
				inputValues[j] = this -> view -> GetData(formula -> parameters[j].variableValue);
				break;
			}
			case PhysicsBehaviour::Value::Type::LITERAL:
			{
				inputValues[j] = formula -> parameters[j].doubleValue;
				break;
			}
			}
		}

		// Benutze Formel, um einen Wert f�r eine Konstante zu messen, falls alle Bedingungen erf�llt sind und alle verwendeten Konstanten bekannt sind
		// F�ge Aktion der globalen Aktionsliste hinzu, falls eine gefunden wurde, die m�glicherweise das Problem (teilweise) l�st
		// Ansonsten mache gar nichts

		if(allConstantsKnown) {
			if(allConditionsMet) {
				// Berechne Formel
				if(this -> CalculateConstantFromFormula(formula, inputValues, frictionBlock1, frictionBlock2, impulseChangeBlock1, impulseChangeBlock2)) {
					*recalculateTrajectories = true;
				}
			} else if(completelyPossible && !actionsChosen) {
				if(this -> ShouldMeasureConstant(formula -> constant, frictionBlock1, frictionBlock2, impulseChangeBlock1, impulseChangeBlock2)) {
					memcpy(actions, localActions, 4 * sizeof(bool));
					actionsChosen = true;
				}
			}
		}
		delete inputValues;

	abortTests:
		continue;
	}

	return actions;
}

bool Ai::DoExperiments(bool* actions) {
	bool moved = false;
	if(actions[(uint32_t)PhysicsBehaviour::Action::RELEASE]) {
		this -> view -> ResetInput(); // m�glicherweise �berfl�ssig
		moved = true;
	}
	if(actions[(uint32_t)PhysicsBehaviour::Action::RIGHT]) {
		this -> view -> DoInput(Character::Input::WALK_RIGHT);
		moved = true;
	}
	if(actions[(uint32_t)PhysicsBehaviour::Action::LEFT]) {
		this -> view -> DoInput(Character::Input::WALK_LEFT);
		moved = true;
	}
	if(actions[(uint32_t)PhysicsBehaviour::Action::JUMP]) {
		this -> view -> DoInput(Character::Input::JUMP);
		moved = true;
	}

	return moved;
}

Ai::ConditionCheckReturn* Ai::CheckCondition(PhysicsBehaviour::Condition const& condition) {
	ConditionCheckReturn* returnValue = new ConditionCheckReturn();

	returnValue -> fulfilled = false;
	returnValue -> needsInputNextFrame = false;
	returnValue -> allConstantsKnown = true;
	returnValue -> givesCollisionInformation = false;
	returnValue -> givesImpulseChangeInformation = false;

	double conditionValue;
	double referenceValue;

	PhysicsBehaviour::Variable conditionVariable = condition.conditionalVariable;
	conditionValue = this -> view -> GetData(conditionVariable);

	if(conditionVariable.frame == 0 && condition.op == PhysicsBehaviour::LogicalOperator::EQUAL_TO && condition.referenceValue.type == PhysicsBehaviour::Value::Type::LITERAL && condition.referenceValue.doubleValue == 1.0) {
		if(conditionVariable.name == PhysicsBehaviour::Variable::Name::B_INPUT_L || conditionVariable.name == PhysicsBehaviour::Variable::Name::B_INPUT_R || conditionVariable.name == PhysicsBehaviour::Variable::Name::B_INPUT_U) {
			returnValue -> needsInputNextFrame = true;
		}
	}

	switch(condition.referenceValue.type) {
	case PhysicsBehaviour::Value::Type::CONSTANT:
	{
		PhysicsBehaviour::Constant constant = condition.referenceValue.constantValue;
		if(constant == PhysicsBehaviour::Constant::S_PER_MATERIAL_FRICTION_COEFFICIENT || constant == PhysicsBehaviour::Constant::S_PER_MATERIAL_IMPULSE_CONSERVATION_COEFFICIENT) {
			throw Error("Ai::CheckCondition: Cannot use per material constant as reference Value in condition");
		}
		PhysicsMemory::ConstantKnowledge *knowledge = this -> physicsMemory -> GetConstantKnowledge(constant);
		if(knowledge -> state == PhysicsMemory::ConstantKnowledge::State::NONE) {
			// Formel kann nicht verwendet werden, setze allConstantsKnown auf false
			returnValue -> allConstantsKnown = false;
			return returnValue;
		}
		referenceValue = knowledge -> value;
		if(condition.referenceValue.constantValue == PhysicsBehaviour::Constant::S_MAX_HORIZONTAL_NEG_VELOCITY) {
			referenceValue = -referenceValue;
		}
		break;
	}
	case PhysicsBehaviour::Value::Type::VARIABLE:
	{
		referenceValue = this -> view -> GetData(condition.referenceValue.variableValue);
		break;
	}
	case PhysicsBehaviour::Value::Type::LITERAL:
	{
		// Pr�fe, ob Werte f�r collisionSide oder impulseAxis gewonnen werden k�nnen
		// Boolsche Variabeln k�nnen nur mit einem Literal verglichen werden
		double value = condition.referenceValue.doubleValue;
		if(value == 1.0) {
			if(conditionVariable == PhysicsBehaviour::Variable(PhysicsBehaviour::Variable::Name::B_COLLISION_U, 0)) {
				returnValue -> givesCollisionInformation = true;
				returnValue -> collisionSide = Character::Side::TOP;
			} else if(conditionVariable == PhysicsBehaviour::Variable(PhysicsBehaviour::Variable::Name::B_COLLISION_D, 0)) {
				returnValue -> givesCollisionInformation = true;
				returnValue -> collisionSide = Character::Side::BOTTOM;
			} else if(conditionVariable == PhysicsBehaviour::Variable(PhysicsBehaviour::Variable::Name::B_COLLISION_L, 0)) {
				returnValue -> givesCollisionInformation = true;
				returnValue -> collisionSide = Character::Side::LEFT;
			} else if(conditionVariable == PhysicsBehaviour::Variable(PhysicsBehaviour::Variable::Name::B_COLLISION_R, 0)) {
				returnValue -> givesCollisionInformation = true;
				returnValue -> collisionSide = Character::Side::RIGHT;
			} else if(conditionVariable == PhysicsBehaviour::Variable(PhysicsBehaviour::Variable::Name::B_IMPULSE_CHANGE_X, 1)) {
				returnValue -> givesImpulseChangeInformation = true;
				returnValue -> impulseChangeAxis = Character::Axis::X_AXIS;
			} else if(conditionVariable == PhysicsBehaviour::Variable(PhysicsBehaviour::Variable::Name::B_IMPULSE_CHANGE_Y, 1)) {
				returnValue -> givesImpulseChangeInformation = true;
				returnValue -> impulseChangeAxis = Character::Axis::Y_AXIS;
			}
		}

		referenceValue = value;
		break;
	}
	}

	bool result = false;
	switch(condition.op) {
	case PhysicsBehaviour::LogicalOperator::EQUAL_TO:
		result = abs(conditionValue - referenceValue) < Ai::EPSILON;
		break;
	case PhysicsBehaviour::LogicalOperator::NOT_EQUAL_TO:
		result = abs(conditionValue - referenceValue) > Ai::EPSILON;
		break;
	case PhysicsBehaviour::LogicalOperator::SMALLER_THAN:
		result = referenceValue - conditionValue > Ai::EPSILON;
		break;
	case PhysicsBehaviour::LogicalOperator::BIGGER_THAN:
		result = conditionValue - referenceValue > Ai::EPSILON;
		break;
	case PhysicsBehaviour::LogicalOperator::SAME_SIGN_THAN:
		if((conditionValue > 0.0 && referenceValue > 0.0) || (conditionValue < 0.0 && referenceValue < 0.0) || (conditionValue == referenceValue)) {
			result = true;
		} else {
			result = false;
		}
		break;
	case PhysicsBehaviour::LogicalOperator::NOT_SAME_SIGN_THAN:
		if((conditionValue > 0.0 && referenceValue > 0.0) || (conditionValue < 0.0 && referenceValue < 0.0) || (conditionValue == referenceValue)) {
			result = false;
		} else {
			result = true;
		}
		break;
	}

	returnValue -> fulfilled = result;

	return returnValue;
}

bool Ai::CalculateConstantFromFormula(PhysicsBehaviour::ConstantFormula const* formula, double* inputValues, uint32_t frictionBlock1, uint32_t frictionBlock2, uint32_t impulseChangeBlock1, uint32_t impulseChangeBlock2) {
	bool returnValue = false;
	if((uint32_t)formula -> constant <= (uint32_t)PhysicsBehaviour::Constant::S_MAX_HORIZONTAL_NEG_VELOCITY) {
		// Normale Konstante (Einmalige und max. horizontale Geschwindigkeit)
		PhysicsMemory::ConstantKnowledge* knowledge = this -> physicsMemory -> GetConstantKnowledge(formula -> constant);
		double value = formula -> Evaluate(inputValues);
		if(knowledge -> state != PhysicsMemory::ConstantKnowledge::State::MEASURED) {
			if(knowledge -> ChangeState(PhysicsMemory::ConstantKnowledge::State::MEASURED, value)) {
				returnValue = true;
			}
		} else {
			if(knowledge -> AddValue(value)) {
				returnValue = true;
			}
		}
	} else if(formula -> constant == PhysicsBehaviour::Constant::S_PER_MATERIAL_FRICTION_COEFFICIENT) {
		// Materialabh�ngiger Reibungskoeffizient
		PhysicsMemory::ConstantKnowledge* knowledge1 = this -> physicsMemory -> GetConstantKnowledge(formula -> constant, frictionBlock1);
		PhysicsMemory::ConstantKnowledge* knowledge2 = this -> physicsMemory -> GetConstantKnowledge(formula -> constant, frictionBlock2);

		double value = formula -> Evaluate(inputValues);
		// Zwei Bl�cke sind beteiligt
		if(knowledge1 != nullptr && knowledge2 != nullptr) {
			if(knowledge1 == knowledge2) {
				// Beide Bl�cke sind identisch, Block kann ermittelt werden
				if(knowledge1 -> state != PhysicsMemory::ConstantKnowledge::State::MEASURED) {
					knowledge1 -> ChangeState(PhysicsMemory::ConstantKnowledge::State::MEASURED, value);
					returnValue = true;
				} else {
					if(knowledge1 -> AddValue(value)) {
						returnValue = true;
					}
				}
			} else {
				// Bl�cke sind verschieden
				if(knowledge1 -> state != PhysicsMemory::ConstantKnowledge::State::MEASURED && knowledge2 -> state != PhysicsMemory::ConstantKnowledge::State::MEASURED) {
					// Bl�cke wurden beide noch nicht gemessen
					knowledge1 -> ChangeState(PhysicsMemory::ConstantKnowledge::State::UNSURE, value);
					knowledge2 -> ChangeState(PhysicsMemory::ConstantKnowledge::State::UNSURE, value);
					returnValue = true;
				} else if(knowledge1 -> state == PhysicsMemory::ConstantKnowledge::State::MEASURED && knowledge2 -> state != PhysicsMemory::ConstantKnowledge::State::MEASURED && abs(knowledge1 -> value - value) > Ai::EPSILON) {
					// Einer von zwei Bl�cken wurde bereits bestimmt, je nachdem, ob value verschieden dem bereits gemessenen Wert ist, kann der zweite Block auch bestimmt werden
					knowledge2 -> ChangeState(PhysicsMemory::ConstantKnowledge::State::MEASURED, value);
					returnValue = true;
				} else if(knowledge1 -> state != PhysicsMemory::ConstantKnowledge::State::MEASURED && knowledge2 -> state == PhysicsMemory::ConstantKnowledge::State::MEASURED && abs(knowledge2 -> value - value) > Ai::EPSILON) {
					// Einer von zwei Bl�cken wurde bereits bestimmt, je nachdem, ob value verschieden dem bereits gemessenen Wert ist, kann der zweite Block auch bestimmt werden
					knowledge1 -> ChangeState(PhysicsMemory::ConstantKnowledge::State::MEASURED, value);
					returnValue = true;
				}
			}
		}
		// Nur ein Block ist beteiligt, Block kann ermittelt werden
		else if(knowledge1 != nullptr) {
			if(knowledge1 -> state != PhysicsMemory::ConstantKnowledge::State::MEASURED) {
				knowledge1 -> ChangeState(PhysicsMemory::ConstantKnowledge::State::MEASURED, value);
				returnValue = true;
			} else {
				if(knowledge1 -> AddValue(value)) {
					returnValue = true;
				}
			}
		} else if(knowledge2 != nullptr) {
			if(knowledge2 -> state != PhysicsMemory::ConstantKnowledge::State::MEASURED) {
				knowledge2 -> ChangeState(PhysicsMemory::ConstantKnowledge::State::MEASURED, value);
				returnValue = true;
			} else {
				if(knowledge2 -> AddValue(value)) {
					returnValue = true;
				}
			}
		}
	} else if(formula -> constant == PhysicsBehaviour::Constant::S_PER_MATERIAL_IMPULSE_CONSERVATION_COEFFICIENT) {
		// Materialabh�ngiger Impulserhaltungskoeffizient

		PhysicsMemory::ConstantKnowledge* knowledge1 = this -> physicsMemory -> GetConstantKnowledge(formula -> constant, impulseChangeBlock1);
		PhysicsMemory::ConstantKnowledge* knowledge2 = this -> physicsMemory -> GetConstantKnowledge(formula -> constant, impulseChangeBlock2);

		double value = formula -> Evaluate(inputValues);

		// Zwei Bl�cke sind beteiligt
		if(knowledge1 != nullptr && knowledge2 != nullptr) {
			if(knowledge1 == knowledge2) {
				// Beide Bl�cke sind identisch, Block kann ermittelt werden
				if(knowledge1 -> state != PhysicsMemory::ConstantKnowledge::State::MEASURED) {
					knowledge1 -> ChangeState(PhysicsMemory::ConstantKnowledge::State::MEASURED, value);
					returnValue = true;
				} else {
					if(knowledge1 -> AddValue(value)) {
						returnValue = true;
					}
				}
			} else {
				// Bl�cke sind verschieden
				if(knowledge1 -> state != PhysicsMemory::ConstantKnowledge::State::MEASURED && knowledge2 -> state != PhysicsMemory::ConstantKnowledge::State::MEASURED) {
					// Bl�cke wurden beide noch nicht gemessen
					knowledge1 -> ChangeState(PhysicsMemory::ConstantKnowledge::State::UNSURE, value);
					knowledge2 -> ChangeState(PhysicsMemory::ConstantKnowledge::State::UNSURE, value);
					returnValue = true;
				} else if(knowledge1 -> state == PhysicsMemory::ConstantKnowledge::State::MEASURED && knowledge2 -> state != PhysicsMemory::ConstantKnowledge::State::MEASURED && abs(knowledge1 -> value - value) > Ai::EPSILON) {
					// Einer von zwei Bl�cken wurde bereits bestimmt, je nachdem, ob value verschieden dem bereits gemessenen Wert ist, kann der zweite Block auch bestimmt werden
					knowledge2 -> ChangeState(PhysicsMemory::ConstantKnowledge::State::MEASURED, value);
					returnValue = true;
				} else if(knowledge1 -> state != PhysicsMemory::ConstantKnowledge::State::MEASURED && knowledge2 -> state == PhysicsMemory::ConstantKnowledge::State::MEASURED && abs(knowledge2 -> value - value) > Ai::EPSILON) {
					// Einer von zwei Bl�cken wurde bereits bestimmt, je nachdem, ob value verschieden dem bereits gemessenen Wert ist, kann der zweite Block auch bestimmt werden
					knowledge1 -> ChangeState(PhysicsMemory::ConstantKnowledge::State::MEASURED, value);
					returnValue = true;
				}
			}
		}
		// Nur ein Block ist beteiligt, Block kann ermittelt werden
		else if(knowledge1 != nullptr) {
			if(knowledge1 -> state != PhysicsMemory::ConstantKnowledge::State::MEASURED) {
				knowledge1 -> ChangeState(PhysicsMemory::ConstantKnowledge::State::MEASURED, value);
				returnValue = true;
			} else {
				if(knowledge1 -> AddValue(value)) {
					returnValue = true;
				}
			}
		} else if(knowledge2 != nullptr) {
			if(knowledge2 -> state != PhysicsMemory::ConstantKnowledge::State::MEASURED) {
				knowledge2 -> ChangeState(PhysicsMemory::ConstantKnowledge::State::MEASURED, value);
				returnValue = true;
			} else {
				if(knowledge2 -> AddValue(value)) {
					returnValue = true;
				}
			}
		}
	}
	return returnValue;
}

bool Ai::ShouldMeasureConstant(PhysicsBehaviour::Constant constant, uint32_t frictionBlock1, uint32_t frictionBlock2, uint32_t impulseChangeBlock1, uint32_t impulseChangeBlock2) {
	if((uint32_t)constant <= (uint32_t)PhysicsBehaviour::Constant::S_JUMP_IMPULSE) {
		// Einmalige Konstanten
		// Wenn Wichtigkeit �ber 1 dann true sonst false
		PhysicsMemory::ConstantKnowledge* knowledge = this -> physicsMemory -> GetConstantKnowledge(constant);
		if(knowledge -> importance < 1.0) {
			return false;
		}
	} else if(constant == PhysicsBehaviour::Constant::S_MAX_HORIZONTAL_VELOCITY || constant == PhysicsBehaviour::Constant::S_MAX_HORIZONTAL_NEG_VELOCITY) {
		// Maximale horizontale Geschwindigkeit
		// Diese Konstante wird passiv gemessen
		return false;
	} else if(constant == PhysicsBehaviour::Constant::S_PER_MATERIAL_FRICTION_COEFFICIENT) {
		// Materialabh�ngiger Reibungskoeffizient
		PhysicsMemory::ConstantKnowledge* knowledge1 = this -> physicsMemory -> GetConstantKnowledge(constant, frictionBlock1);
		PhysicsMemory::ConstantKnowledge* knowledge2 = this -> physicsMemory -> GetConstantKnowledge(constant, frictionBlock2);

		// Zwei Bl�cke sind beteiligt
		if(knowledge1 != nullptr && knowledge2 != nullptr) {
			if(knowledge1 == knowledge2) {
				// Beide Bl�cke gleich
				// Wenn Wichtigkeit �ber 1 dann true sonst false
				if(knowledge1 -> importance < 1.0) {
					return false;
				}
			} else {
				// Bl�cke verschieden
				// Wenn beide KnowledgeState::NONE sind: true sonst false
				if(knowledge1 -> state != PhysicsMemory::ConstantKnowledge::State::NONE || knowledge2 -> state != PhysicsMemory::ConstantKnowledge::State::NONE) {
					return false;
				}
			}
		}
		// Nur ein Block ist beteiligt
		// Wenn Wichtigkeit �ber 1 dann true sonst false
		else if(knowledge1 != nullptr) {
			if(knowledge1 -> importance < 1.0) {
				return false;
			}
		} else if(knowledge2 != nullptr) {
			if(knowledge2 -> importance < 1.0) {
				return false;
			}
		}
	} else if(constant == PhysicsBehaviour::Constant::S_PER_MATERIAL_IMPULSE_CONSERVATION_COEFFICIENT) {
		// Materialabh�ngiger Impulserhaltungskoeffizient
		// Diese Konstante wird passiv gemessen
		return false;
	}

	return true;
}

bool Ai::FulfillsAfterAction(PhysicsBehaviour::InputReaction const* inputReaction, PhysicsBehaviour::Condition const& condition) {
	for(uint32_t i = 0; i < inputReaction -> outputConditionsCount; i++) {
		if(condition.conditionalVariable.name == inputReaction -> outputConditions[i].conditionalVariable.name) {
			// Perfekte �bereinstimmung
			if(condition.op == inputReaction -> outputConditions[i].op && condition.referenceValue.CausalEqual(inputReaction -> outputConditions[i].referenceValue)) {
				return true;
			}
			// Nicht-Gleich muss bei gegebener Gleichheit gepr�ft werden
			// Nicht-Gleich wird bei allen ungenauen Folgen als erf�llt betrachtet
			if(condition.op == PhysicsBehaviour::LogicalOperator::NOT_EQUAL_TO) {
				if(inputReaction -> outputConditions[i].op == PhysicsBehaviour::LogicalOperator::EQUAL_TO) {
					if(condition.referenceValue.CausalEqual(inputReaction -> outputConditions[i].referenceValue)) {
						return false;
					} else {
						return true;
					}
				} else {
					return true;
				}
			}
			// Bei Vergleichen ist es m�glich durch Folgen wie Vx0 > Vx0 (nachher gr�sser als vorher) die Bedingung zu l�ssen
			if(condition.op == PhysicsBehaviour::LogicalOperator::BIGGER_THAN) {
				if(inputReaction -> outputConditions[i].op == PhysicsBehaviour::LogicalOperator::BIGGER_THAN && inputReaction -> outputConditions[i].referenceValue.type == PhysicsBehaviour::Value::Type::VARIABLE && inputReaction -> outputConditions[i].referenceValue.variableValue == condition.conditionalVariable) {
					return true;
				}
			}
			if(condition.op == PhysicsBehaviour::LogicalOperator::SMALLER_THAN) {
				if(inputReaction -> outputConditions[i].op == PhysicsBehaviour::LogicalOperator::SMALLER_THAN && inputReaction -> outputConditions[i].referenceValue.type == PhysicsBehaviour::Value::Type::VARIABLE && inputReaction -> outputConditions[i].referenceValue.variableValue == condition.conditionalVariable) {
					return true;
				}
			}
		}
	}

	return false;
}

#pragma endregion

const double Ai::EPSILON = 0.000000001;
const double Ai::CHARACTER_SIZE_BIAS = 0.05;

}
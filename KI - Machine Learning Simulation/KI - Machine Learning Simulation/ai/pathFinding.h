#pragma once

#include <stdafx.h>

#include <ai/worldMemory.h>

namespace AI {

// Klasse, um den schnellsten Weg zum Ziel zu finden
// Verwendet den A* Algorithmus
class PathFinding {
public:
	// Enth�llt zus�tzliche Informationen zu Knoten
	struct Node {
		Node(WorldMemory::Platform* platform) : platform(platform), f(numeric_limits<double>::infinity()), g(0.0), h(numeric_limits<double>::infinity()), predecessor(nullptr), closed(false), open(false), trajectory(nullptr) {}

		bool operator <(Node const& other) const {
			return this -> f < other.f;
		}

		double f; // Gesch�tzte Distanz zum Ziel vom Start �ber diesen Knoten
		double g; // Bisherige Distanz
		double h; // Gesch�tzte Distanz zum Ziel
		bool closed;
		bool open;

		Node* predecessor;

		WorldMemory::Platform* platform;
		WorldMemory::Trajectory* trajectory;
	};

	PathFinding(WorldMemory* worldMemory);
	~PathFinding();

	// Aktualisiert Knoten
	void UpdateNodes();

	// Sucht schnellsten Weg
	list<WorldMemory::Trajectory*> FindPath(WorldMemory::Platform* start, WorldMemory::Platform* end);

private:
	void ExpandNode(Node* currentNode);

	Node* NodeFromPlatform(WorldMemory::Platform* platform);

	WorldMemory* worldMemory;

	// Knoten
	vector<Node*> nodes;

	// Daten, die vom A*-Algorithmus gebraucht werden
	list<Node*> openList;
};

}
#include <ai/physicsMemory.h>

#include <system/log.h>
#include <system/window.h>
#include <system/graphics.h>
#include <simulation/blockRegistry.h>

using namespace System;
using namespace Sim;

namespace AI {

bool PhysicsMemory::ConstantKnowledge::AddValue(double value) {
	bool returnValue = false;
	if(abs(this -> value - value) < ConstantKnowledge::ERROR_MEASUREMENT_EPSILON) {
		this -> value = (this -> value * (this -> recordedValuesCount) + value) / (double)(this -> recordedValuesCount + 1);
		this -> recordedValuesCount++;
		this -> errorValue = 0.0;
		this -> errorValueCount = 0;
	} else {
		if(abs(this -> errorValue - value) < ConstantKnowledge::ERROR_MEASUREMENT_EPSILON) {
			this -> errorValueCount++;
		}
		this -> errorValue = value;
		if(this -> errorValueCount > ConstantKnowledge::MAX_SUCCESSIVE_ERROR_MEASUREMENTS[(uint32_t)this -> constant]) {
			this -> value = this -> errorValue;
			this -> recordedValuesCount = 0;
			this -> errorValue = 0.0;
			this -> errorValueCount = 0;
			returnValue = true;
			stringstream ss;
			ss << (uint32_t)this -> constant << ", set to new value " << value;
			Log::Write("Detected change in constant " + ss.str());
		}
	}
	return returnValue;
}

PhysicsMemory::PhysicsMemory() : maxHorizontalVelocityKnowledge(PhysicsBehaviour::Constant::S_MAX_HORIZONTAL_VELOCITY, numeric_limits<double>::infinity()) {
	// Erstelle liste mit Platz f�r Wissen �ber einalige Konstanten
	this -> uniqueConstantKnowledge.push_back(ConstantKnowledge(PhysicsBehaviour::Constant::S_GRAVITY, 1.0));
	this -> uniqueConstantKnowledge.push_back(ConstantKnowledge(PhysicsBehaviour::Constant::S_HORIZONTAL_FORCE_AIR, 1.0));
	this -> uniqueConstantKnowledge.push_back(ConstantKnowledge(PhysicsBehaviour::Constant::S_HORIZONTAL_FORCE_GROUND, 1.0));
	this -> uniqueConstantKnowledge.push_back(ConstantKnowledge(PhysicsBehaviour::Constant::S_JUMP_IMPULSE, 1.0));

	// Erstelle Liste mit Platz f�r materialabh�ngiges Wissen
	// Lasse luft aus
	for(uint32_t i = 1; i < BlockRegistry::GetBlockCount(); i++) {
		this -> materialKnowledge.push_back({ i, ConstantKnowledge(PhysicsBehaviour::Constant::S_PER_MATERIAL_FRICTION_COEFFICIENT, 1.0), ConstantKnowledge(PhysicsBehaviour::Constant::S_PER_MATERIAL_IMPULSE_CONSERVATION_COEFFICIENT, 0.0) });
	}

	// Erstelle Todo-Liste
	this -> CalculateImportances();
}

PhysicsMemory::~PhysicsMemory() {
	this -> uniqueConstantKnowledge.clear();
	this -> materialKnowledge.clear();
}

PhysicsMemory::ConstantKnowledge* PhysicsMemory::GetConstantKnowledge(PhysicsBehaviour::Constant c, uint32_t blockID) {
	if((uint32_t)c <= (uint32_t)PhysicsBehaviour::Constant::S_JUMP_IMPULSE) {
		// Einmalige Konstanten
		return this -> GetUniqueConstantKnowledge(c);
	} else if((uint32_t)c <= (uint32_t)PhysicsBehaviour::Constant::S_MAX_HORIZONTAL_NEG_VELOCITY) {
		// Maximale horizontale Geschwindigkeit
		return this -> GetMaxHorizontalVelocityKnowledge();
	} else if((uint32_t)c <= (uint32_t)PhysicsBehaviour::Constant::S_PER_MATERIAL_IMPULSE_CONSERVATION_COEFFICIENT) {
		// Materialabh�ngige Konstanten
		if(blockID == 0) {
			return nullptr;
		}
		if(c == PhysicsBehaviour::Constant::S_PER_MATERIAL_FRICTION_COEFFICIENT) {
			return &this -> GetMaterialKnowledge(blockID) -> frictionCoefficient;
		} else if(c == PhysicsBehaviour::Constant::S_PER_MATERIAL_IMPULSE_CONSERVATION_COEFFICIENT) {
			return &this -> GetMaterialKnowledge(blockID) -> impulseCoefficient;
		}
	}
	return nullptr;
}

void PhysicsMemory::Load(string filename) {
	Log::Write("begin loading physics memory from file " + filename);

	ifstream is(filename, ios::binary);
	if(!is.is_open()) {
		throw Error("Error occured in PhysicsMemory::Load, file not found\nFilename: " + filename);
	}

	string version;
	while(is.peek() != '\n') {
		version.push_back(is.get());
	}
	is.get();

	Log::Write("physics memory file version checked");

	if(version != PhysicsMemory::CURRENT_SAVE_FILE_VERSION) {
		throw Error("Error occured in PhysicsMemory::Load, file version is incompatible\n File Version: " + version + " Required Version: " + PhysicsMemory::CURRENT_SAVE_FILE_VERSION);
	}

	// Lade neue Daten nicht direkt in Klassenvariabeln, falls die Datei besch�digt ist

	vector<ConstantKnowledge> newUniqueConstantKnowledge;
	ConstantKnowledge newMaxHorizontalVelocityKnowledge;
	vector<MaterialKnowledge> newMaterialKnowledge;

	// Dieser Block kann Fehler werfen, falls die Datei besch�digt ist
	// Da alle Daten im Stack gespeichert sind, muss in dann nichts aufger�umt werden
	// und ein try-catch-Block ist �berfl�ssig
	{
		Log::Write("reading unique constant knowledge data...");

		// Lese Wissen zu einmaligen Konstanten
		uint32_t uniqueConstantCount;
		is.read((char*)&uniqueConstantCount, sizeof(uint32_t));
		if(!is) {
			throw Error("Error occured in ifstream::read (unique constant knowledge count), physics memory file may be corrupted\nFile: " + filename);
		}
		newUniqueConstantKnowledge.reserve(uniqueConstantCount);
		for(uint32_t i = 0; i < uniqueConstantCount; i++) {
			ConstantKnowledge c;
			is.read((char*)&c, sizeof(ConstantKnowledge));
			if(!is) {
				throw Error("Error occured in ifstream::read (unique constant knowledge), physics memory file may be corrupted\nFile: " + filename);
			}
			newUniqueConstantKnowledge.push_back(c);
		}

		Log::Write("reading max horizontal velocity knowledge data...");

		// Lese Wissen zur maximalen horizontalen Geschwindigkeit

		is.read((char*)&newMaxHorizontalVelocityKnowledge, sizeof(ConstantKnowledge));
		if(!is) {
			throw Error("Error occured in ifstream::read (max. hor. velocity knowledge), physics memory file may be corrupted\nFile: " + filename);
		}

		Log::Write("reading material knowledge data...");

		// Lese Wissen zu materialabh�ngigen Konstanten

		uint32_t materialKnowledgeCount;
		is.read((char*)&materialKnowledgeCount, sizeof(uint32_t));
		if(!is) {
			throw Error("Error occured in ifstream::read (material knowledge count), physics memory file may be corrupted\nFile: " + filename);
		}
		newMaterialKnowledge.reserve(materialKnowledgeCount);
		for(uint32_t i = 0; i < materialKnowledgeCount; i++) {
			MaterialKnowledge m;
			is.read((char*)&m, sizeof(MaterialKnowledge));
			if(!is) {
				throw Error("Error occured in ifstream::read (material knowledge), physics memory file may be corrupted\nFile: " + filename);
			}
			newMaterialKnowledge.push_back(m);
		}

		is.close();
	}

	// Kopiere Daten

	this -> uniqueConstantKnowledge = newUniqueConstantKnowledge;
	this -> maxHorizontalVelocityKnowledge = newMaxHorizontalVelocityKnowledge;
	this -> materialKnowledge = newMaterialKnowledge;

	// L�sche tempor�re Daten

	newUniqueConstantKnowledge.clear();
	newMaterialKnowledge.clear();

	Log::Write("done loading physics memory");
}

void PhysicsMemory::Save(string filename) {
	Log::Write("begin saving physics memory " + filename);

	ofstream os(filename, ios::binary | ios::trunc);
	if(!os.is_open()) {
		throw Error("Error occured in PhysicsMemory::Save, unable to open file\nFilename: " + filename);
	}

	os << PhysicsMemory::CURRENT_SAVE_FILE_VERSION << "\n" << flush;

	Log::Write("physics memory file version written...");

	Log::Write("writing unique constant knowledge data...");

	// Schreibe Wissen zu einmaligen Konstanten

	uint32_t uniqueConstantKnowledgeCount = (uint32_t)this -> uniqueConstantKnowledge.size();
	os.write((char*)&uniqueConstantKnowledgeCount, sizeof(uint32_t));
	for(uint32_t i = 0; i < uniqueConstantKnowledgeCount; i++) {
		os.write((char*)&this -> uniqueConstantKnowledge[i], sizeof(ConstantKnowledge));
	}

	Log::Write("writing max horizontal velocity knowledge data...");

	// Schreibe Wissen zu maximaler horizontalen Geschwindigkeit

	os.write((char*)&this -> maxHorizontalVelocityKnowledge, sizeof(ConstantKnowledge));

	Log::Write("writing material knwoledg data...");

	uint32_t materialKnowledgeCount = (uint32_t)this -> materialKnowledge.size();
	os.write((char*)&materialKnowledgeCount, sizeof(uint32_t));
	for(uint32_t i = 0; i < materialKnowledgeCount; i++) {
		os.write((char*)&this -> materialKnowledge[i], sizeof(MaterialKnowledge));
	}

	os.close();

	Log::Write("done saving physics memory");
}

void PhysicsMemory::CalculateImportances() {
	//Berechne Wichtigkeit f�r alle Konstanten
	for(uint32_t i = 0; i < this -> uniqueConstantKnowledge.size(); i++) {
		this -> CalculateImportance(&this -> uniqueConstantKnowledge[i]);
	}
	this -> CalculateImportance(&this -> maxHorizontalVelocityKnowledge);
	for(uint32_t i = 0; i < this -> materialKnowledge.size(); i++) {
		this -> CalculateImportance(&this -> materialKnowledge[i].frictionCoefficient);
		this -> CalculateImportance(&this -> materialKnowledge[i].impulseCoefficient);
	}
}

void PhysicsMemory::DrawDebugInformation() {
	// Zeichnet Liste mit Konstanten und der gemessenen Werten
	wstringstream ss;

	ss << left << setw(20) << L"Konstante" << setw(12) << L"Rang" << setw(0) << L" " << setw(13) << L"Wert" << setw(0) << L" " << setw(12) << L"Messungen" << setw(0) << endl;
	ss << L"--------------------------------------------------------" << endl;
	for(uint32_t i = 0; i < this -> uniqueConstantKnowledge.size(); i++) {
		ss << setw(20) << PhysicsMemory::UNIQUE_CONSTANT_NAMES[i];
		ss << setw(12) << this -> uniqueConstantKnowledge[i].importance << setw(0) << L" ";
		if(this -> uniqueConstantKnowledge[i].state == PhysicsMemory::ConstantKnowledge::State::NONE) {
			ss << L"-" << setw(12) << L" " << setw(0) << L" ";
		} else if(this -> uniqueConstantKnowledge[i].state == PhysicsMemory::ConstantKnowledge::State::UNSURE) {
			ss << L"!" << setw(12) << this -> uniqueConstantKnowledge[i].value << setw(0) << L" ";
		} else if(this -> uniqueConstantKnowledge[i].state == PhysicsMemory::ConstantKnowledge::State::MEASURED) {
			ss << L" " << setw(12) << this -> uniqueConstantKnowledge[i].value << setw(0) << L" ";
		}
		ss << setw(12) << this -> uniqueConstantKnowledge[i].recordedValuesCount << setw(0) << endl;
	}

	ss << setw(20) << PhysicsMemory::MAX_H_VELOCITY_CONSTANT_NAME;
	ss << setw(12) << this -> maxHorizontalVelocityKnowledge.importance << setw(0) << L" ";
	if(this -> maxHorizontalVelocityKnowledge.state == PhysicsMemory::ConstantKnowledge::State::NONE) {
		ss << L"-" << setw(12) << L" " << setw(0) << L" ";
	} else if(this -> maxHorizontalVelocityKnowledge.state == PhysicsMemory::ConstantKnowledge::State::UNSURE) {
		ss << L"!" << setw(12) << this -> maxHorizontalVelocityKnowledge.value << setw(0) << L" ";
	} else if(this -> maxHorizontalVelocityKnowledge.state == PhysicsMemory::ConstantKnowledge::State::MEASURED) {
		ss << L" " << setw(12) << this -> maxHorizontalVelocityKnowledge.value << setw(0) << L" ";
	}
	ss << setw(12) << this -> maxHorizontalVelocityKnowledge.recordedValuesCount << setw(0) << endl;

	for(uint32_t i = 0; i < this -> materialKnowledge.size(); i++) {
		ss << setw(20) << (BlockRegistry::GetBlock(i + 1).name + L" R.");
		ss << setw(12) << this -> materialKnowledge[i].frictionCoefficient.importance << setw(0) << L" ";
		if(this -> materialKnowledge[i].frictionCoefficient.state == PhysicsMemory::ConstantKnowledge::State::NONE) {
			ss << L"-" << setw(12) << L" " << setw(0) << L" ";
		} else if(this -> materialKnowledge[i].frictionCoefficient.state == PhysicsMemory::ConstantKnowledge::State::UNSURE) {
			ss << L"!" << setw(12) << this -> materialKnowledge[i].frictionCoefficient.value << setw(0) << L" ";
		} else if(this -> materialKnowledge[i].frictionCoefficient.state == PhysicsMemory::ConstantKnowledge::State::MEASURED) {
			ss << L" " << setw(12) << this -> materialKnowledge[i].frictionCoefficient.value << setw(0) << L" ";
		}
		ss << setw(12) << this -> materialKnowledge[i].frictionCoefficient.recordedValuesCount << setw(0) << endl;

		ss << left << setw(20) << (BlockRegistry::GetBlock(i + 1).name + L" I.");
		ss << setw(12) << this -> materialKnowledge[i].impulseCoefficient.importance << setw(0) << L" ";
		if(this -> materialKnowledge[i].impulseCoefficient.state == PhysicsMemory::ConstantKnowledge::State::NONE) {
			ss << L"-" << setw(12) << L" " << setw(0) << L" ";
		} else if(this -> materialKnowledge[i].impulseCoefficient.state == PhysicsMemory::ConstantKnowledge::State::UNSURE) {
			ss << L"!" << setw(12) << this -> materialKnowledge[i].impulseCoefficient.value << setw(0) << L" ";
		} else if(this -> materialKnowledge[i].impulseCoefficient.state == PhysicsMemory::ConstantKnowledge::State::MEASURED) {
			ss << L" " << setw(12) << this -> materialKnowledge[i].impulseCoefficient.value << setw(0) << L" ";
		}
		ss << setw(12) << this -> materialKnowledge[i].impulseCoefficient.recordedValuesCount << setw(0) << endl;
	}

	Graphics::AddTextToQueue(ss.str(), Window::GetWidth() - 500, Window::GetHeight() - 10, 1.0f, 1.0f, 1.0f);
}

void PhysicsMemory::CalculateImportance(ConstantKnowledge* knowledge) {
	switch(knowledge -> state) {
	case PhysicsMemory::ConstantKnowledge::State::NONE:
		knowledge -> importance = 4.0;
		break;
	case PhysicsMemory::ConstantKnowledge::State::UNSURE:
		knowledge -> importance = 3.0;
		break;
	case PhysicsMemory::ConstantKnowledge::State::MEASURED:
		// Skala: y = 2.0 * r ^ x, r < 0
		knowledge -> importance = 2.0 * pow(PhysicsMemory::RECORD_IMPORTANCE_R[(uint32_t)knowledge -> constant], (double)knowledge -> recordedValuesCount);
		break;
	}
}

const double PhysicsMemory::ConstantKnowledge::ERROR_MEASUREMENT_EPSILON = 0.05;

const uint32_t PhysicsMemory::ConstantKnowledge::MAX_SUCCESSIVE_ERROR_MEASUREMENTS[] = {
	5, // Schwerkaft
	5, // Hor. Kraft Luft
	5, // Hor. Kraft Boden
	1, // Sprungimpuls
	5, // Max. hor. Geschwindigkeit
	5, // Max. hor. Geschwindigkeit
	10, // Reibungskoeffizient
	2 // Impulserhaltungskoeffizient
};

const string PhysicsMemory::CURRENT_SAVE_FILE_VERSION = "v1.1";

const uint32_t PhysicsMemory::OK_RECORD_COUNTS[] = {
	100, // Schwerkraft
	100, // Horizontale Kraft Luft
	100, // Horizontale Kraft Boden
	5, // Sprungimpuls
	50, // Max. horizontale Geschwindigkeit
	50, // Max. neg. horizontale Geschwindigkeit
	100, // Reibungskoeffizient
	5 // Impulserhaltungskoeffizient
};

const double PhysicsMemory::RECORD_IMPORTANCE_R[]{
	pow(2.0, -1.0 / (double)PhysicsMemory::OK_RECORD_COUNTS[0]),
	pow(2.0, -1.0 / (double)PhysicsMemory::OK_RECORD_COUNTS[1]),
	pow(2.0, -1.0 / (double)PhysicsMemory::OK_RECORD_COUNTS[2]),
	pow(2.0, -1.0 / (double)PhysicsMemory::OK_RECORD_COUNTS[3]),
	pow(2.0, -1.0 / (double)PhysicsMemory::OK_RECORD_COUNTS[4]),
	pow(2.0, -1.0 / (double)PhysicsMemory::OK_RECORD_COUNTS[5]),
	pow(2.0, -1.0 / (double)PhysicsMemory::OK_RECORD_COUNTS[6]),
	pow(2.0, -1.0 / (double)PhysicsMemory::OK_RECORD_COUNTS[7])
};

const wstring PhysicsMemory::UNIQUE_CONSTANT_NAMES[] = {
	L"Gravitation", L"H. Kraft Luft", L"H. Kraft Boden", L"Sprungimpuls"
};

const wstring PhysicsMemory::MAX_H_VELOCITY_CONSTANT_NAME = L"max. h. Geschw.";

}
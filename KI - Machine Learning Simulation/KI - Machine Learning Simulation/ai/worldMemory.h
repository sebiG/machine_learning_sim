#pragma once

#include <stdafx.h>

#include <system/errors.h>
#include <simulation/world.h>

namespace AI {

// Klasse f�r Weltged�chtnis
class WorldMemory {
public:
	class Platform;
	class Chunk;

	// Klasse f�r Flugbahn/Weg zwischen zwei Plattformen
	class Trajectory {
	public:
		Trajectory();

		void Reset();

		enum Type {
			NONE, // Nicht spezifiziert
			ADJACENT, // KI muss nur gerade aus laufen
			STAIR, // KI muss senkrecht nach oben springen und dann kurz zur Seite gehen
			FALL, // KI muss mit einer bestimmten Geschwindigkeit �ber den Rand laufen
			JUMP // KI muss laufen und springen
		};

		bool possible;
		Type type;

		// Daten
		Platform* start;
		Platform* end;

		double xDirection; // Richtung der Bewegung
		double dX; // Zur�ckzulegender Weg (Skalar)
		double dY; // Zur�ckzulegender Weg (Vektor)
		double dX_; // Tats�chlich zur�ckzulegender Weg (Skalar)

		double t; // Zeit, um dY im freien Fall zur�ckzulegen
		double tA; // Bremszeit in der luft
		double v0; // Startgeschwindigkeit
		double vE; // Endgeschwindigkeit

		double startPosition; // Position, an der die KI zu beschleunigen beginnen muss
		double liftoffPosition; // Position, an der die KI genau v0 erreicht haben muss
		double touchdownPosition; // Landeposition
		// Bremsen ist danach trivial
	};

	// Klasse f�r logische Verbindungen zwischen Platformen
	class PlatformRelation {
	public:
		PlatformRelation() = default;
		PlatformRelation(uint32_t id, Platform* platform1, Platform* platform2);

		void Load(ifstream* is);
		void Save(ofstream* os);
		void ResolvePointers(WorldMemory* worldMemory);

		inline uint32_t GetID() {
			return this -> id;
		}

		// Berechnet distanz zwischen Plattformen
		void CalculateDistance();

		// Zugriffsmethoden
		inline pair<Platform*, Platform*> GetPlatforms() {
			return pair<Platform*, Platform*>(this -> platform1, this -> platform2);
		}
		inline Platform* GetOtherPlatform(Platform* platform) {
			return (platform == this -> platform1) ? this -> platform2 : this -> platform1;
		}
		inline pair<Trajectory*, Trajectory*> GetTrajectories() {
			return pair<Trajectory*, Trajectory*>(&this -> trajectoryTo1, &this -> trajectoryTo2);
		}
		inline Trajectory* GetTrajectoryTo(Platform* platform) {
			return (platform == this -> platform1) ? &this -> trajectoryTo1 : &this -> trajectoryTo2;
		}
		inline double GetDistance() {
			return this -> distance;
		}
		inline double GetHeuristicDistance() {
			return this -> heuristicDistance;
		}

	private:
		//const static double OVERLAP_DIFFICULTY_CONSTANT;

		uint32_t id;

		Platform* platform1;
		Platform* platform2;
		Trajectory trajectoryTo1;
		Trajectory trajectoryTo2;
		double distance;
		double heuristicDistance;
	};

	// Klasse f�r Plattform
	// Wird als Knoten im Weltgraphen verwendet
	class Platform {
	public:
		Platform() = default;
		Platform(uint32_t id);

		void Load(ifstream* is);
		void Save(ofstream* os);
		void ResolvePointers(WorldMemory* worldMemory);

		uint32_t x;
		uint32_t y;
		uint32_t width;
		uint32_t block;

		// Gibt true zur�ck, falls x und y Teil der Platform sind
		inline bool IsInPlatform(uint32_t x, uint32_t y) {
			if(y == this -> y && x >= this -> x && x < this -> x + this -> width) {
				return true;
			}
			return false;
		}

		// Gibt eindeutige Identifikationsnummer zur�ck
		inline uint32_t GetID() {
			return this -> id;
		}

		// Gibt an, ob Plattform logisch erreichbar ist
		inline bool IsLogicallyAccessible() {
			return this -> logicallyAccessible;
		}
		// Aktualisiert logische Erreichbarkeit
		void UpdateLogicalAccessibility();

		// Gibt an, ob Plattform bereits besucht wurde
		inline bool HasBeenVisited() {
			return this -> visited;
		}
		// Besuche Plattform
		inline void Visit() {
			this -> visited = true;
			this -> logicallyAccessible = true;
		}

		// Zugriffsmethoden f�r Zusammenh�nge
		inline list<PlatformRelation*>::iterator GetRelationBeginIterator() {
			return this -> relations.begin();
		}
		inline list<PlatformRelation*>::iterator GetRelationEndIterator() {
			return this -> relations.end();
		}
		inline void AddRelation(PlatformRelation* relation) {
			this -> relations.push_back(relation);
		}
		inline void RemoveRelation(PlatformRelation* relation) {
			this -> relations.remove(relation);
		}

		// Zugriffsmethoden f�r Chunks, die von der Plattform �berschnitten werden
		inline list<Chunk*>::iterator GetOverlappingChunkBeginIterator() {
			return this -> overlappingChunks.begin();
		}
		inline list<Chunk*>::iterator GetOverlappingChunkEndIterator() {
			return this -> overlappingChunks.end();
		}
		inline void AddOverlappingChunk(Chunk* chunk) {
			this -> overlappingChunks.push_back(chunk);
		}
		inline void RemoveOverlappingChunk(Chunk* chunk) {
			this -> overlappingChunks.remove(chunk);
		}

	private:
		uint32_t id;

		bool visited;
		bool logicallyAccessible;

		list<PlatformRelation*> relations;
		list<Chunk*> overlappingChunks;
	};

	// Struktur f�r Block im Weltged�chtnis
	struct Block {
		enum Flag {
			NONE = 0,
			UNKNOWN = 1,
			UNPROCESSED = 2
		};

		uint32_t block;
		uint8_t flags;
	};

	// Klasse f�r Teil des Weltged�chtnis
	// Entspricht Chunk in World
	// Wird verwendet, um einen schnellen Zugriff auf nahe Strukturen zu haben, ohne bereits Teil des Weltgraphen zu sein
	// Enth�lt eine Liste mit allen Strukturen, die sich im Chunk befinden oder �berlappen
	class Chunk {
	public:
		Chunk(uint32_t id);
		Chunk() : Chunk(0) {}
		~Chunk() = default; // Standarddestruktor

		inline uint32_t GetID() {
			return this -> id;
		}

		void Load(ifstream* file);
		void Save(ofstream* file);
		void ResolvePointers(WorldMemory* worldMemory);

		// Gibt Block an x- und y-Koordinate zur�ck
		// Ung�ltige Werte geben BLOCK_UNKNOWN zur�ck
		inline Block GetBlock(uint32_t x, uint32_t y) {
			if(x >= Sim::World::Chunk::CHUNK_SIZE || y >= Sim::World::Chunk::CHUNK_SIZE) {
				return Block{ 0, Block::Flag::UNKNOWN };
			}
			return this -> blockMemory[y * Sim::World::Chunk::CHUNK_SIZE + x];
		}
		// Gibt Block an Index zur�ck
		// F�r ung�ltige Werte ist das Verhalten undefiniert
		inline Block GetBlock(uint32_t index) {
			return this -> blockMemory[index];
		}

		// Setzt Block
		// F�r ung�ltige Werte ist das Verhalten undefiniert
		inline void SetBlock(uint32_t x, uint32_t y, Block block) {
			this -> blockMemory[y * Sim::World::Chunk::CHUNK_SIZE + x] = block;
		}
		inline void SetBlock(uint32_t index, Block block) {
			this -> blockMemory[index] = block;
		}

		// Zugriffsmethoden f�r Plattformen
		inline list<Platform*>::iterator GetPlatformBeginIterator() {
			return this -> platforms.begin();
		}
		inline list<Platform*>::iterator GetPlatformEndIterator() {
			return this -> platforms.end();
		}
		inline void AddPlatform(Platform* platform) {
			this -> platforms.push_back(platform);
		}
		inline void RemovePlatform(Platform* platform) {
			this -> platforms.remove(platform);
		}

	private:
		uint32_t id;

		Block blockMemory[Sim::World::Chunk::CHUNK_SIZE * Sim::World::Chunk::CHUNK_SIZE];

		list<Platform*> platforms;
	};

	WorldMemory();
	~WorldMemory();

	const static string CURRENT_SAVE_FILE_VERSION; // string, der in jeder Weltged�chtnisdatei zuoberts vorkommt. So kann gepr�ft werden, ob die Version des Programms mit der Version der Datei �bereinstimmt.

	// L�dt Weltged�chtnis von Datei
	void Load(string filename);
	// Speicher Weltged�chtnis in Datei
	void Save(string filename);

	// Zeichnet Debuginformationen
	void DrawDebugInformation(Sim::Vector2 const& characterPosition);

	// Gibt Weltge�chtnisteil zur�ck
	// F�r ung�ltige Werte ist das Verhalten undefiniert
	inline Chunk* GetChunk(uint32_t x, uint32_t y) {
		return this -> memoryChunks[y * Sim::World::WORLD_SIZE + x];
	}
	inline Chunk* GetChunk(uint32_t index) {
		return this -> memoryChunks[index];
	}
	inline Chunk* GetChunkFromBlock(uint32_t x, uint32_t y) {
		uint32_t chunkX = x / Sim::World::Chunk::CHUNK_SIZE;
		uint32_t chunkY = y / Sim::World::Chunk::CHUNK_SIZE;
		return this -> GetChunk(chunkX, chunkY);
	}

	// Gibt Block an x- und y-Koordinate zur�ck
	// F�r ung�ltige Werte wird WorldMemoryChunk::BLOCK_UNKNOWN zur�ckgegeben
	inline Block GetBlock(int32_t x, int32_t y) {
		if(x < 0 || y < 0 || x >= Sim::World::WORLD_SIZE * Sim::World::Chunk::CHUNK_SIZE || y >= Sim::World::WORLD_SIZE * Sim::World::Chunk::CHUNK_SIZE) {
			return Block{ 0, Block::Flag::UNKNOWN };
		}
		
		uint32_t chunkX = x / Sim::World::Chunk::CHUNK_SIZE;
		uint32_t chunkY = y / Sim::World::Chunk::CHUNK_SIZE;
		uint32_t blockX = x % Sim::World::Chunk::CHUNK_SIZE;
		uint32_t blockY = y % Sim::World::Chunk::CHUNK_SIZE;
		if(this -> memoryChunks[chunkY * Sim::World::WORLD_SIZE + chunkX] == nullptr) {
			return Block{ 0, Block::Flag::UNKNOWN };
		}
		return this -> memoryChunks[chunkY * Sim::World::WORLD_SIZE + chunkX] -> GetBlock(blockX, blockY);
	}

	// F�gt neuer Block hinzu
	// Das Weltged�chtnis gliedert ihn im Weltgraphen ein, indem es eine neue Platform erstellt oder eine bestehende vergr�ssert
	// start und stop geben an, ob es sich beim Block um den Start- oder Zielblock handelt
	void AddBlock(int32_t x, int32_t y, Block block, bool start = false, bool end = false);

	// Erstellt Liste mit logisch erreichbaren Plattformen
	// Muss aufgerufen werden, wenn der Spieler eine neue Plattform betritt oder neue Plattformen gefunden wurden
	// Die angegebene Position bestimmt, in welcher Reihenfolge die Plattformen in der Liste vorkommen; N�here Plattformen sind vor weiter entfernten
	void CreateTargetList(Sim::Vector2 const& characterPosition);

	// Gibt Liste mit logisch erreichbaren Plattformen zur�ck
	inline list<Platform*>::iterator GetTargetListBegin() {
		return this -> targetList.begin();
	}
	inline list<Platform*>::iterator GetTargetListEnd() {
		return this -> targetList.end();
	}

	// Gibt Liste mit neuen Zusammenh�ngen zur�ck
	inline list<PlatformRelation*>::iterator GetNewRelationsBegin() {
		return this -> newRelations.begin();
	}
	inline list<PlatformRelation*>::iterator GetNewRelationsEnd() {
		return this -> newRelations.end();
	}
	inline bool HasNewRelations() {
		return this -> newRelations.size() > 0;
	}

	// Zugriffsmethoden f�r Plattformen
	inline list<Platform*>::iterator GetPlatformBeginIterator() {
		return this -> platforms.begin();
	}
	inline list<Platform*>::iterator GetPlatformEndIterator() {
		return this -> platforms.end();
	}
	Platform* GetPlatformAt(int32_t x, int32_t y);

	// Zugriffsmethoden f�r Zusammenh�nge
	inline list<PlatformRelation*>::iterator GetPlatformRelationBeginIterator() {
		return this -> platformRelations.begin();
	}
	inline list<PlatformRelation*>::iterator GetPlatformRelationEndIterator() {
		return this -> platformRelations.end();
	}

	// Setzt alle Flugbahnen zur�ck (wenn eine physikalische Konstante sich ge�ndert hat)
	inline void ResetTrajectories() {
		this -> newRelations.clear();
		for(list<PlatformRelation*>::iterator i = this -> GetPlatformRelationBeginIterator(); i != this -> GetPlatformRelationEndIterator(); i++) {
			this -> AddNewRelation(*i);
			pair<Trajectory*, Trajectory*> trajectories = (*i) -> GetTrajectories();
			trajectories.first -> Reset();
			trajectories.second -> Reset();
		}
	}

	// Setzt Liste mit neuen Zusammenh�ngen zur�ck
	inline void ResetNewRelationsList() {
		this -> newRelations.clear();
	}

	// Gibt Start- und Zeilplattformen zur�ck
	// Wenn diese nicht bekannt sind, wird nullptr zur�ckgegeben
	inline Platform* GetStartPlatform() {
		return this -> startPlatform;
	}
	inline Platform* GetEndPlatform() {
		return this -> endPlatform;
	}

	inline Sim::Vector2 GetStartPosition() {
		return Sim::Vector2((double)this -> startX, (double)this -> startY);
	}
	inline Sim::Vector2 GetEndPosition() {
		return Sim::Vector2((double)this -> endX, (double)this -> endY);
	}

private:
	// Spezielle Vergleichsstruktur
	struct TargetCompare {
		TargetCompare(Sim::Vector2 const& referencePosition) : referencePosition(referencePosition) {}
		bool operator() (Platform const* a, Platform const* b) const;

		Sim::Vector2 referencePosition;
	};

	// Radius von an eine Plattform angrenzenden Chunks, in denen Zusammenh�nge mit Plattformen bestehen
	const static uint32_t PLATFORM_RELATION_CHUNKS = 1;

	// Setzt Block an x- und y-Koordinate relativ zum Nullpunkt der Welt
	// Falls der Teilbereich des Ged�chtnis noch nicht existiert, wird er erstellt
	// F�r ung�ltige Werte bleibt die Welt unver�ndert
	inline void SetBlock(uint32_t x, uint32_t y, Block block) {
		if(x >= Sim::World::WORLD_SIZE * Sim::World::Chunk::CHUNK_SIZE || y >= Sim::World::WORLD_SIZE * Sim::World::Chunk::CHUNK_SIZE) {
			return;
		}

		uint32_t chunkX = x / Sim::World::Chunk::CHUNK_SIZE;
		uint32_t chunkY = y / Sim::World::Chunk::CHUNK_SIZE;
		uint32_t blockX = x % Sim::World::Chunk::CHUNK_SIZE;
		uint32_t blockY = y % Sim::World::Chunk::CHUNK_SIZE;
		if(this -> GetChunk(chunkX, chunkY) == nullptr) {
			this -> SetChunk(chunkX, chunkY, new Chunk(chunkY * Sim::World::WORLD_SIZE + chunkX));
		}
		this -> GetChunk(chunkX, chunkY) -> SetBlock(blockX, blockY, block);
	}

	// Setzt Weltged�chtnisteil
	// F�r ung�ltige Werte ist das Verhalten undefiniert
	// nullptr f�r WorldMemoryChunk* ist erlaubt
	inline void SetChunk(uint32_t x, uint32_t y, Chunk* chunk) {
		if(this -> memoryChunks[y * Sim::World::WORLD_SIZE + x] != nullptr) {
			delete this -> memoryChunks[y * Sim::World::WORLD_SIZE + x];
		}
		this -> memoryChunks[y * Sim::World::WORLD_SIZE + x] = chunk;
	}
	inline void SetChunk(uint32_t index, Chunk* chunk) {
		if(this -> memoryChunks[index] != nullptr) {
			delete this -> memoryChunks[index];
		}
		this -> memoryChunks[index] = chunk;
	}

	inline void AddNewRelation(PlatformRelation* relation) {
		if(find(this -> newRelations.begin(), this -> newRelations.end(), relation) == this -> newRelations.end()) {
			this -> newRelations.push_back(relation);
		}
	}

#pragma region Serialisierte Daten
	// N�chste eindeutige IDs
	uint32_t nextPlatformID;
	uint32_t nextPlatformRelationID;

	Platform* startPlatform;
	Platform* endPlatform;

	uint32_t startX;
	uint32_t startY;
	uint32_t endX;
	uint32_t endY;

	Chunk* memoryChunks[Sim::World::WORLD_SIZE * Sim::World::WORLD_SIZE]; // Daten werden hier serialisiert

	list<Platform*> platforms; // Daten werden hier serialisiert
	list<PlatformRelation*> platformRelations; // Daten werden hier serialisiert
	list<PlatformRelation*> newRelations;
#pragma endregion

	// Liste mit m�glichen Zielen
	list<Platform*> targetList;
};

}
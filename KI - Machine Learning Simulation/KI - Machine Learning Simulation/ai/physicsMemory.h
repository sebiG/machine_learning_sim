#pragma once

#include <stdafx.h>

#include <system/errors.h>
#include <simulation/physicsBehaviour.h>

namespace AI {

// Klasse f�r Physikged�chtnis
// Speichert aktuelle Werte f�r phyiskalische Konstanten, ob sie bereits bekannt sind und wie verl�sslich der Wert ist
class PhysicsMemory {
public:
	// Struktur f�r Wissenstand �ber phyiskalische Konstante
	struct ConstantKnowledge {
		// Enumeration f�r die Verschiedenen Zust�nde von Wissen
		enum class State {
			NONE,
			MEASURED,
			UNSURE
		};

		ConstantKnowledge() {}
		ConstantKnowledge(Sim::PhysicsBehaviour::Constant constant, double defaultValue) {
			this -> constant = constant;
			this -> state = State::UNSURE;
			this -> value = defaultValue;
			this -> recordedValuesCount = 0;
			this -> errorValue = 0.0;
			this -> errorValueCount = 0;
		}
		// Wechselt Status und setzt Wert auf Standardwert zur�ck
		// R�ckgabewert gibt an, ob der Wert der Konstante sich ge�ndert hat
		inline bool ChangeState(State state, double defaultValue) {
			bool returnValue = (this -> value != defaultValue);
			this -> state = state;
			this -> value = defaultValue;
			this -> recordedValuesCount = 0;
			return returnValue;
		}

		const static double ERROR_MEASUREMENT_EPSILON;
		const static uint32_t MAX_SUCCESSIVE_ERROR_MEASUREMENTS[Sim::PhysicsBehaviour::VARIABLE_COUNT];

		// Berechnet Durchschnitt mit neuem Wert (nur wenn state == KnowledgeState::MEASURED)
		// R�ckgabewert gibt an, ob die Konstante ge�ndert wurde
		bool AddValue(double value);

		Sim::PhysicsBehaviour::Constant constant;

		State state;
		double importance;
		double value;
		uint32_t recordedValuesCount;

		double errorValue;
		uint32_t errorValueCount;
	};

	// Struktur f�r Wissenstand �ber Material
	struct MaterialKnowledge {
		uint32_t materialId;
		ConstantKnowledge frictionCoefficient;
		ConstantKnowledge impulseCoefficient;
	};

	PhysicsMemory();
	~PhysicsMemory();

	// Gibt Wissensstand allgemein zur�ck
	ConstantKnowledge* GetConstantKnowledge(Sim::PhysicsBehaviour::Constant c, uint32_t blockID = 0);
	// Gibt Wissenstand zur�ck (Nur einalige Konstanten)
	inline ConstantKnowledge* GetUniqueConstantKnowledge(Sim::PhysicsBehaviour::Constant constant) {
		if((uint32_t)constant >= this -> uniqueConstantKnowledge.size()) {
			throw System::Error("PhysicsMemory::GetConstantKnowledge: Nur einmalige physikalische Konstanten erlaubt");
		}
		return &this -> uniqueConstantKnowledge[(uint32_t)constant];
	}
	// Gibt Wissenstand zu Maximalgeschwindigkeit zur�ck
	inline ConstantKnowledge* GetMaxHorizontalVelocityKnowledge() {
		return &this -> maxHorizontalVelocityKnowledge;
	}
	// Gibt Wissenstand zu Material zur�ck
	inline MaterialKnowledge* GetMaterialKnowledge(uint32_t id) {
		if(id == 0) {
			throw System::Error("PhysicsMemory::GetMaterialKnowledge: id 0, air has no constants");
		}
		if(id > this -> materialKnowledge.size()) {
			throw System::Error("PhysicsMemory::GetMaterialKnowledge: id too big, this material doesn't exist");
		}
		return &this -> materialKnowledge[id - 1];
	}

	const static string CURRENT_SAVE_FILE_VERSION;

	// L�dt Wissen von Datei
	void Load(string filename);
	// Speichert Wissen in Datei
	void Save(string filename);

	// Erstellt Liste mit nach Wichtigkeit geordneten Experimenten je nach bereits vorhandenem Wissen
	void CalculateImportances();

	// Zeichnet Debug-Infomration
	void DrawDebugInformation();

private:
	const static uint32_t OK_RECORD_COUNTS[Sim::PhysicsBehaviour::VARIABLE_COUNT];
	const static double RECORD_IMPORTANCE_R[Sim::PhysicsBehaviour::VARIABLE_COUNT];

	const static wstring UNIQUE_CONSTANT_NAMES[4];
	const static wstring MAX_H_VELOCITY_CONSTANT_NAME;

	void CalculateImportance(ConstantKnowledge* knowledge);

	// Verschiedene Arten von Wissen �ber Konstanten, die verschieden behandelt werden
	vector<ConstantKnowledge> uniqueConstantKnowledge;
	ConstantKnowledge maxHorizontalVelocityKnowledge;
	vector<MaterialKnowledge> materialKnowledge;
};

}
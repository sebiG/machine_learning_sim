#include <editor/editor.h>

#include <system/errors.h>
#include <system/log.h>
#include <system/window.h>
#include <system/graphics.h>
#include <system/input.h>
#include <system/program.h>
#include <ui/ui.h>
#include <ui/textbox.h>
#include <ui/button.h>
#include <simulation/world.h>
#include <editor/worldViewer.h>
#include <editor/blockList.h>

using namespace System;
using namespace UI;
using namespace Sim;

namespace Edit {

void Editor::Update(double deltaTime) {
	if(this -> uiMode == UiMode::UI_NONE) {
		// Verlasse Editor falls Ctrl + Q gedr�ckt wird
		if(((Input::GetKeyboardState(DIK_LCONTROL) == Input::KeyState::PRESSED) || (Input::GetKeyboardState(DIK_RCONTROL) == Input::KeyState::PRESSED)) && Input::GetKeyboardState(DIK_Q) == Input::KeyState::DOWN) {
			Log::Write("Recorded Ctrl + Q, simulation exited");
			Program::SwitchProgramMode(Program::ProgramModeEnum::PROGRAM_MODE_MAIN_MENU);
		}

		// Richtungstasten
		if(this -> editMode != EditMode::EDIT_CHOOSE_BLOCK) {
			if(Input::GetKeyboardState(DIK_UP) == Input::KeyState::DOWN) {
				this -> worldViewer -> Up();
			}
			if(Input::GetKeyboardState(DIK_DOWN) == Input::KeyState::DOWN) {
				this -> worldViewer -> Down();
			}
			if(Input::GetKeyboardState(DIK_RIGHT) == Input::KeyState::DOWN) {
				this -> worldViewer -> Right();
			}
			if(Input::GetKeyboardState(DIK_LEFT) == Input::KeyState::DOWN) {
				this -> worldViewer -> Left();
			}
		} else {
			if(Input::GetKeyboardState(DIK_UP) == Input::KeyState::DOWN) {
				this -> blockList -> Previous();
			}
			if(Input::GetKeyboardState(DIK_DOWN) == Input::KeyState::DOWN) {
				this -> blockList -> Next();
			}
		}

		uint32_t cursorX = this -> worldViewer -> GetCursorX();
		uint32_t cursorY = this -> worldViewer -> GetCursorY();

		if(this -> editMode == EditMode::EDIT_NONE) {
			// Nur im Standardbearbeitungsmodus

			// L�schen
			if(Input::GetKeyboardState(DIK_DELETE) == Input::KeyState::DOWN) {
				if(this -> world -> GetBlock(cursorX, cursorY) != 0) {
					bool deleteBlock = true;
					if((cursorX == this -> world -> GetStartX() && cursorY == this -> world -> GetStartY()) || (cursorX == this -> world -> GetEndX() && cursorY == this -> world -> GetEndY())) {
						deleteBlock = false;
					}
					if(deleteBlock) {
						this -> world -> SetBlock(cursorX, cursorY, 0);
					} else {
						this -> statusBar.SetStatus(L"Block unter Start- oder Endposition kann nicht gel�scht werden.");
					}
				}
			}

			// Kopieren
			if(Input::GetKeyboardState(DIK_C) == Input::KeyState::DOWN) {
				this -> clipboard = this -> world -> GetBlock(cursorX, cursorY);
			}

			// Einf�gen solange Taste gedr�ckt wird
			if(Input::GetKeyboardState(DIK_V) == Input::KeyState::PRESSED) {
				bool setBlock = true;
				if(this -> clipboard == 0) {
					if((cursorX == this -> world -> GetStartX() && cursorY == this -> world -> GetStartY()) || (cursorX == this -> world -> GetEndX() && cursorY == this -> world -> GetEndY())) {
						setBlock = false;
					}
				}
				if(setBlock) {
					this -> world -> SetBlock(cursorX, cursorY, this -> clipboard);
				} else {
					this -> statusBar.SetStatus(L"Block unter Start- oder Endposition kann nicht mit Luft ersetzt werden.");
				}
			}

			// Wechsle zu Startpositionsmodus
			if(Input::GetKeyboardState(DIK_S) == Input::KeyState::DOWN) {
				this -> editMode = EditMode::EDIT_SET_START;
			}

			// Wechsle zu Endpositionsmodus
			if(Input::GetKeyboardState(DIK_E) == Input::KeyState::DOWN) {
				this -> editMode = EditMode::EDIT_SET_END;
			}

			// Wechsle zu Blockauswahlmodus
			if(Input::GetKeyboardState(DIK_Q) == Input::KeyState::DOWN) {
				this -> editMode = EditMode::EDIT_CHOOSE_BLOCK;
				this -> blockList -> StartSelect();
			}
		} else if(this -> editMode == EditMode::EDIT_SET_START) {
			// Nur im Startpositionsmodus

			// Wechsle zur�ck zu normalem Modus
			if(Input::GetKeyboardState(DIK_ESCAPE) == Input::KeyState::DOWN) {
				this -> editMode = EditMode::EDIT_NONE;
			}
			// Setze Startposition
			if(Input::GetKeyboardState(DIK_RETURN) == Input::KeyState::DOWN) {
				this -> world -> SetStartX(cursorX);
				this -> world -> SetStartY(cursorY);
				// Setze Plattform, falls noch nicht vorhanden
				if(this -> world -> GetBlock(cursorX, cursorY) == 0) {
					this -> world -> SetBlock(cursorX, cursorY, 1);
				}
				this -> editMode = EditMode::EDIT_NONE;
			}
		} else if(this -> editMode == EditMode::EDIT_SET_END) {
			// Nur im Zielpositionmodus

			// Wechsle zur�ck zu normalem Modus
			if(Input::GetKeyboardState(DIK_ESCAPE) == Input::KeyState::DOWN) {
				this -> editMode = EditMode::EDIT_NONE;
			}
			// Setze Zeilposition
			if(Input::GetKeyboardState(DIK_RETURN) == Input::KeyState::DOWN) {
				this -> world -> SetEndX(cursorX);
				this -> world -> SetEndY(cursorY);
				// Setze Plattform, falls noch nicht vorhanden
				if(this -> world -> GetBlock(cursorX, cursorY) == 0) {
					this -> world -> SetBlock(cursorX, cursorY, 1);
				}
				this -> editMode = EditMode::EDIT_NONE;
			}
		} else if(this -> editMode == EditMode::EDIT_CHOOSE_BLOCK) {
			// Nur im Blockauswahlmodus

			// Wechsle zur�ck zu normalem Modus
			if(Input::GetKeyboardState(DIK_ESCAPE) == Input::KeyState::DOWN) {
				this -> blockList -> StopSelect();
				this -> editMode = EditMode::EDIT_NONE;
			}
			// W�hle Block aus
			if(Input::GetKeyboardState(DIK_RETURN) == Input::KeyState::DOWN) {
				this -> selectedBlock = this -> blockList -> StopSelect();
				this -> editMode = EditMode::EDIT_SET_BLOCK;
				this -> ignoreInput = true; // Verhindere das sofortige Platzieren des Blockes
			}
		}  else if(this -> editMode == EditMode::EDIT_SET_BLOCK) {
			// Nur im Blockplatziermodus

			// Wechsle zur�ck zu normalem Modus
			if(Input::GetKeyboardState(DIK_ESCAPE) == Input::KeyState::DOWN) {
				this -> editMode = EditMode::EDIT_NONE;
			}
			// Platziere Bl�cke solange RETURN gedr�ckt wird
			if(Input::GetKeyboardState(DIK_RETURN) == Input::KeyState::DOWN) {
				this -> ignoreInput = false;
			}
			if(Input::GetKeyboardState(DIK_RETURN) == Input::KeyState::PRESSED && !this -> ignoreInput) {
				bool setBlock = true;
				if(this -> selectedBlock == 0) {
					if((cursorX == this -> world -> GetStartX() && cursorY == this -> world -> GetStartY()) || (cursorX == this -> world -> GetEndX() && cursorY == this -> world -> GetEndY())) {
						setBlock = false;
					}
				}
				if(setBlock) {
					this -> world -> SetBlock(cursorX, cursorY, this -> selectedBlock);
				} else {
					this -> statusBar.SetStatus(L"Block unter Start- oder Endposition kann nicht mit Luft ersetzt werden.");
				}
			}
		}
	}

	// Aktualisiere Weltanzeige
	this -> worldViewer -> Update(deltaTime);
	this -> worldViewer -> Draw();

	// Zeichne Blockliste
	this -> blockList -> Draw();

	// Zeichne Cursor
	if(this -> editMode == EditMode::EDIT_NONE || this -> editMode == EditMode::EDIT_CHOOSE_BLOCK) {
		Graphics::Line cursor[2] = { { (int32_t)Window::GetWidth() / 2 - 15, (int32_t)Window::GetHeight() / 2, (int32_t)Window::GetWidth() / 2 + 15, (int32_t)Window::GetHeight() / 2, 0.0f, 1.0f, 1.0f },
		{ (int32_t)Window::GetWidth() / 2, (int32_t)Window::GetHeight() / 2 - 15, (int32_t)Window::GetWidth() / 2, (int32_t)Window::GetHeight() / 2 + 15, 0.0f, 1.0f, 1.0f } };
		Graphics::DrawLines(cursor, 2);
		Graphics::FlushLines();
	} else if(this -> editMode == EditMode::EDIT_SET_START) {
		Graphics::Line cursor[2] = { { (int32_t)Window::GetWidth() / 2 - 15, (int32_t)Window::GetHeight() / 2, (int32_t)Window::GetWidth() / 2 + 15, (int32_t)Window::GetHeight() / 2, 0.0f, 0.5f, 0.0f },
		{ (int32_t)Window::GetWidth() / 2, (int32_t)Window::GetHeight() / 2 - 15, (int32_t)Window::GetWidth() / 2, (int32_t)Window::GetHeight() / 2 + 15, 0.0f, 0.5f, 0.0f } };
		Graphics::DrawLines(cursor, 2);
		Graphics::FlushLines();
	} else if(this -> editMode == EditMode::EDIT_SET_END) {
		Graphics::Line cursor[2] = { { (int32_t)Window::GetWidth() / 2 - 15, (int32_t)Window::GetHeight() / 2, (int32_t)Window::GetWidth() / 2 + 15, (int32_t)Window::GetHeight() / 2, 1.0f, 0.0f, 0.0f },
		{ (int32_t)Window::GetWidth() / 2, (int32_t)Window::GetHeight() / 2 - 15, (int32_t)Window::GetWidth() / 2, (int32_t)Window::GetHeight() / 2 + 15, 1.0f, 0.0f, 0.0f } };
		Graphics::DrawLines(cursor, 2);
		Graphics::FlushLines();
	} else if(this -> editMode == EditMode::EDIT_SET_BLOCK) {
		Graphics::TexturedSquare cursor = { (int32_t)Window::GetWidth() / 2 - 12, (int32_t)Window::GetHeight() / 2 - 12, 24, this -> selectedBlock + 16 };
		Graphics::Rect cursorFrame = { (int32_t)Window::GetWidth() / 2 - 12, (int32_t)Window::GetHeight() / 2 - 12, 24, 24, 1.0f, 1.0f, 1.0f };
		Graphics::DrawTexturedSquares(&cursor, 1);
		Graphics::DrawRects(&cursorFrame, 1);

		Graphics::FlushTexturedSquares();
		Graphics::FlushRects();
	}

	// Aktualisiere Benutzeroberfl�che
	this -> UpdateUi(deltaTime);
}

void Editor::Init() {
	Log::WriteImportant("Editor: begin Init");

	// Erstelle Objekte
	this -> world = new World();
	this -> worldViewer = new WorldViewer(this -> world);
	this -> blockList = new BlockList();

	this -> clipboard = 0;
	this -> uiMode = UiMode::UI_NONE;
	this -> editMode = EditMode::EDIT_NONE;
	this -> ignoreInput = false;

	// Benutzeroberfl�che f�r Speichern und Laden
	{
		this -> fileUi = new Ui();
		this -> fileUi -> x = Window::GetWidth() - 330;
		this -> fileUi -> y = 10;
		this -> fileUi -> width = 320;
		this -> fileUi -> height = 184;
		this -> fileUi -> text = L"Welt Speichern / Laden";
		this -> fileUi -> active = false;
		this -> fileUi -> visible = false;

		this -> filePathTextbox = new Textbox();
		this -> filePathTextbox -> x = 10;
		this -> filePathTextbox -> y = 58;
		this -> filePathTextbox -> width = 300;
		this -> filePathTextbox -> height = 44;
		this -> filePathTextbox -> text = L"Dateipfad (.world)";
		this -> filePathTextbox -> maxCharacters = 35;
		this -> fileUi -> AddUiElement(this -> filePathTextbox);

		this -> fileSaveButton = new Button();
		this -> fileSaveButton -> x = 10;
		this -> fileSaveButton -> y = 34;
		this -> fileSaveButton -> width = 300;
		this -> fileSaveButton -> height = 20;
		this -> fileSaveButton -> text = L"Speichern";
		this -> fileUi -> AddUiElement(this -> fileSaveButton);

		this -> fileLoadButton = new Button();
		this -> fileLoadButton -> x = 10;
		this -> fileLoadButton -> y = 10;
		this -> fileLoadButton -> width = 300;
		this -> fileLoadButton -> height = 20;
		this -> fileLoadButton -> text = L"Laden";
		this -> fileUi -> AddUiElement(this -> fileLoadButton);
	}

	// Registriere f�r Ereignisse
	Window::Register(this, Window::WINDOW_SIZE);
	
	this -> fileSaveButton -> Register(this, Button::BUTTON_ACTIVATE);
	this -> fileLoadButton -> Register(this, Button::BUTTON_ACTIVATE);

	Log::WriteImportant("Editor: Init terminated");
}

void Editor::DeInit() {
	Log::WriteImportant("Editor: begin DeInit");

	// L�sche Objekte
	delete this -> world;
	delete this -> worldViewer;
	delete this -> blockList;

	delete this -> filePathTextbox;
	delete this -> fileSaveButton;
	delete this -> fileLoadButton;

	this -> uiMode = UiMode::UI_NONE;

	Log::WriteImportant("Editor: DeInit terminated");
}

void Editor::OnEvent(Event const& e) {
	EventSender* sender = e.param1.Decode<EventSender*>();

	if(e.eventID == Window::WINDOW_SIZE) {
		// Positioniere Benutzeroberfl�che neu
		this -> fileUi -> x = e.param1.Decode<uint32_t>() - 330;
	} else if(e.eventID == Button::BUTTON_ACTIVATE) {
		if(sender == this -> fileSaveButton) { // Speichere Welt
			StringConverter converter;
			string file;
			try {
				file = converter.to_bytes(this -> filePathTextbox -> enteredText);
			} catch(range_error re) {
				Log::WriteImportant("Save World: filename is in wrong format");
				this -> statusBar.SetStatus(L"Speichern von Welt: Dateiname liegt im falschen Format vor");
			}
			try {
				this -> world -> Save(file);
				Log::WriteImportant("Saved World to \"" + file + "\"");
				this -> statusBar.SetStatus(L"Welt in Datei \"" + converter.from_bytes(file) + L"\" gespeichert");
			} catch(Error error) {
				Log::WriteImportant("Couldn't save world to \"" + file + "\"");
				this -> statusBar.SetStatus(L"Welt konnte nicht in Datei \"" + converter.from_bytes(file) + L"\" gespeichert werden");
			}
		} else if(sender == this -> fileLoadButton) { // Lade Welt
			StringConverter converter;
			string file;
			try {
				file = converter.to_bytes(this -> filePathTextbox -> enteredText);
			} catch(range_error re) {
				Log::WriteImportant("load World: filename is in wrong format");
				this -> statusBar.SetStatus(L"Laden von Welt: Dateiname liegt im falschen Format vor");
			}
			try {
				this -> world -> Load(file);
				Log::WriteImportant("Loaded World from \"" + file + "\"");
				this -> statusBar.SetStatus(L"Welt von Datei \"" + converter.from_bytes(file) + L"\" geladen");
			} catch(Error error) {
				Log::WriteImportant("Couldn't load world from \"" + file + "\"");
				this -> statusBar.SetStatus(L"Welt konnte nicht von Datei \"" + converter.from_bytes(file) + L"\" geladen werden");
			}
		}
	}
}

void Editor::UpdateUi(double deltaTime) {
	if(Input::GetKeyboardState(DIK_ESCAPE) == Input::KeyState::DOWN) {
		// Schliesse aktuelle Benutzeroberfl�che, falls sie aktiv ist
		switch(this -> uiMode) {
		case UiMode::UI_FILE_MENU:
			if(this -> fileUi-> active) {
				this -> fileUi -> active = false;
				this -> fileUi -> visible = false;
				this -> uiMode = UiMode::UI_NONE;
			}
			break;
		}
	}

	if(this -> uiMode == UiMode::UI_NONE) {
		if(Input::GetKeyboardState(DIK_F1) == Input::KeyState::DOWN) {
			this -> fileUi -> active = true;
			this -> fileUi -> visible = true;
			this -> uiMode = UiMode::UI_FILE_MENU;
		}
	}

	this -> fileUi -> Update(deltaTime);
	this -> statusBar.Update(deltaTime);
	
	this -> fileUi -> Draw();
	this -> statusBar.Draw();

	Graphics::FlushFilledRects();
	Graphics::FlushRects();
	Graphics::FlushLines();
	Graphics::DrawTextQueue();
}

}
#pragma once

#include <stdafx.h>

#include <system/programMode.h>
#include <system/event.h>
#include <ui/statusBar.h>

namespace UI {

class Ui;
class Button;
class Textbox;

}

namespace Sim {

class World;

}

namespace Edit {

class WorldViewer;
class BlockList;

// Klasse für Editorprogrammmodus
class Editor : public System::ProgramMode, public System::EventReciever {
public:
	Editor() = default; // Standardkonstruktor
	~Editor() = default; // Standarddestruktor

	virtual void Update(double deltaTime);
	virtual void Init();
	virtual void DeInit();

	virtual void OnEvent(System::Event const& e);

private:
	// Mögliche Benutzeroberflächenmodi
	enum UiMode {
		UI_NONE,
		UI_FILE_MENU
	};

	// Mögliche Bearbeitungsmodi
	enum EditMode {
		EDIT_NONE,
		EDIT_SET_START,
		EDIT_SET_END,
		EDIT_CHOOSE_BLOCK,
		EDIT_SET_BLOCK
	};

	// Hilfefunktion, wird von Update aufgerufen
	void UpdateUi(double deltaTime);

	UiMode uiMode;
	EditMode editMode;
	bool ignoreInput; // Wird verwendet, damit nachdem ein Block ausgewählt wurde, dieser nicht sofort platziert wird
	UI::StatusBar statusBar;

	Sim::World* world;
	WorldViewer* worldViewer;
	BlockList* blockList;

	uint32_t clipboard;
	uint32_t selectedBlock;

	UI::Ui* fileUi;
	UI::Textbox* filePathTextbox;
	UI::Button* fileSaveButton;
	UI::Button* fileLoadButton;
};

}
#include <editor/worldViewer.h>

#include <system/window.h>
#include <system/graphics.h>
#include <system/input.h>

using namespace System;
using namespace Sim;

namespace Edit {

void WorldViewer::Update(double deltaTime) {
	
}

void WorldViewer::Draw() {
	// Zeichne Gitter

	// Berechne, wie viele Bereiche gezeichnet werden m�ssen

	int32_t firstBlockX = (int32_t)floor((double)this -> cursorX - ((double)Window::GetWidth() / 2.0 / WorldViewer::BLOCK_SIZE));
	int32_t lastBlockX = (int32_t)ceil((double)this -> cursorX + ((double)Window::GetWidth() / 2.0 / WorldViewer::BLOCK_SIZE));
	int32_t firstBlockY = (int32_t)floor((double)this -> cursorY - ((double)Window::GetHeight() / 2.0 / WorldViewer::BLOCK_SIZE));
	int32_t lastBlockY = (int32_t)ceil((double)this -> cursorY + ((double)Window::GetHeight() / 2.0 / WorldViewer::BLOCK_SIZE));

	if(firstBlockX < 0 && lastBlockX >= 0) {
		firstBlockX = 0;
	} else if(firstBlockX < 0 && lastBlockX < 0) {
		firstBlockX = 0;
		lastBlockX = 0;
	}
	if(firstBlockY < 0 && lastBlockY >= 0) {
		firstBlockY = 0;
	} else if(firstBlockY < 0 && lastBlockY < 0) {
		firstBlockY = 0;
		lastBlockY = 0;
	}
	uint32_t firstChunkX = (uint32_t)firstBlockX / World::Chunk::CHUNK_SIZE;
	uint32_t lastChunkX = (uint32_t)lastBlockX / World::Chunk::CHUNK_SIZE;
	uint32_t firstChunkY = (uint32_t)firstBlockY / World::Chunk::CHUNK_SIZE;
	uint32_t lastChunkY = (uint32_t)lastBlockY / World::Chunk::CHUNK_SIZE;

	int32_t offsetX = (int32_t)(this -> cursorX * WorldViewer::BLOCK_SIZE) - Window::GetWidth() / 2 + WorldViewer::BLOCK_SIZE / 2;
	int32_t offsetY = (int32_t)(this -> cursorY * WorldViewer::BLOCK_SIZE) - Window::GetHeight() / 2 + WorldViewer::BLOCK_SIZE / 2;

	for(uint32_t x = firstChunkX; x <= lastChunkX; x++) {
		if(x >= World::WORLD_SIZE) {
			continue;
		}
		uint32_t xChunkOffset = x * World::Chunk::CHUNK_SIZE;
		for(uint32_t y = firstChunkY; y <= lastChunkY; y++) {
			if(y >= World::WORLD_SIZE) {
				continue;
			}
			uint32_t yChunkOffset = y * World::Chunk::CHUNK_SIZE;

			// Zeichne Bl�cke
			World::Chunk* chunk = this -> world -> GetChunk(x, y);
			if(chunk != nullptr) {
				uint32_t squaresCounter = 0;
				for(uint32_t i = 0; i < World::Chunk::CHUNK_SIZE; i++) {
					for(uint32_t j = 0; j < World::Chunk::CHUNK_SIZE; j++) {
						uint32_t block = chunk -> GetBlock(i, j);

						Graphics::TexturedSquare square = { (int32_t)((xChunkOffset + i) * WorldViewer::BLOCK_SIZE) - offsetX, (int32_t)((yChunkOffset + j) * WorldViewer::BLOCK_SIZE) - offsetY, WorldViewer::BLOCK_SIZE, block + 16 };
						Graphics::DrawTexturedSquares(&square, 1);

						squaresCounter++;
						if(squaresCounter == Graphics::MAX_SQUARE_LIST_SIZE - 1) {
							squaresCounter = 0;
							Graphics::FlushTexturedSquares();
						}
					}
				}
				Graphics::FlushTexturedSquares();
			}

			// Zeichne Gitter
			Graphics::Line grid[34];
			for(uint32_t i = 0; i < 17; i++) {
				grid[i] = Graphics::Line{ (int32_t)((xChunkOffset + i) * WorldViewer::BLOCK_SIZE) - offsetX, (int32_t)(yChunkOffset * WorldViewer::BLOCK_SIZE) - offsetY, (int32_t)((xChunkOffset + i) * WorldViewer::BLOCK_SIZE) - offsetX, (int32_t)((yChunkOffset + 16) * WorldViewer::BLOCK_SIZE) - offsetY, 1.0f, 1.0f, 1.0f };
				grid[i + 17] = Graphics::Line{ (int32_t)((xChunkOffset)* WorldViewer::BLOCK_SIZE) - offsetX, (int32_t)((yChunkOffset + i) * WorldViewer::BLOCK_SIZE) - offsetY, (int32_t)((xChunkOffset + 16) * WorldViewer::BLOCK_SIZE) - offsetX, (int32_t)((yChunkOffset + i) * WorldViewer::BLOCK_SIZE) - offsetY, 1.0f, 1.0f, 1.0f };
			}
			Graphics::DrawLines(grid, 34);
			Graphics::FlushLines();
		}
	}

	// Markiere Startposition
	Graphics::Circle startEndMarkers[2] = { 
		{ ((int32_t)this -> world -> GetStartX() * (int32_t)WorldViewer::BLOCK_SIZE) - offsetX + (int32_t)WorldViewer::BLOCK_SIZE / 2, ((int32_t)this -> world -> GetStartY() * (int32_t)WorldViewer::BLOCK_SIZE) - offsetY + (int32_t)WorldViewer::BLOCK_SIZE / 2, 5, 0, 0, 0.0f, 0.5f, 0.0f },
		{ ((int32_t)this -> world -> GetEndX() * (int32_t)WorldViewer::BLOCK_SIZE) - offsetX + (int32_t)WorldViewer::BLOCK_SIZE / 2, ((int32_t)this -> world -> GetEndY() * (int32_t)WorldViewer::BLOCK_SIZE) - offsetY + (int32_t)WorldViewer::BLOCK_SIZE / 2, 5, 0, 0, 0.8f, 0.0f, 0.0f }
	};
	Graphics::FillCircles(startEndMarkers, 2);
	Graphics::FlushFilledCircles();

	// Schreibe Koordinaten
	for(uint32_t i = (uint32_t)firstBlockX; i < (uint32_t)lastBlockX; i += 2) {
		wstringstream ss;
		ss << i;
		if((i * WorldViewer::BLOCK_SIZE) - offsetX + 10 < 0) {
			continue;
		}
		Graphics::AddTextToQueue(ss.str(), (uint32_t)((i * WorldViewer::BLOCK_SIZE) - offsetX + 10), 20, 1.0f, 0.0f, 0.0f);
	}
	for(uint32_t i = (uint32_t)firstBlockY; i < (uint32_t)lastBlockY; i += 2) {
		wstringstream ss;
		ss << i;
		if((i * WorldViewer::BLOCK_SIZE) - offsetY + 30 < 0) {
			continue;
		}
		Graphics::AddTextToQueue(ss.str(), 10, (uint32_t)((i * WorldViewer::BLOCK_SIZE) - offsetY + 30), 1.0f, 0.0f, 0.0f);
	}
	Graphics::DrawTextQueue();
}

}
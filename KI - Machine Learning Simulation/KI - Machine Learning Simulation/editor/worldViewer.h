#pragma once

#include <stdafx.h>

#include <simulation/world.h>

namespace Sim {

class World;

}

namespace Edit {

// Klasse zum anzeigen der Welt und zum daring navigieren
class WorldViewer {
public:
	WorldViewer(Sim::World* world) : world(world), cursorX(0), cursorY(0) {};
	~WorldViewer() = default; // Standarddestruktor

	const static uint32_t BLOCK_SIZE = 48;

	// Schnittstelle
	inline uint32_t GetCursorX() {
		return this -> cursorX;
	}
	inline uint32_t GetCursorY() {
		return this -> cursorY;
	}

	inline void Up() {
		if(this -> cursorY < Sim::World::WORLD_SIZE * Sim::World::Chunk::CHUNK_SIZE - 1) {
			this -> cursorY++;
		}
	}
	inline void Down() {
		if(this -> cursorY > 0) {
			this -> cursorY--;
		}
	}
	inline void Right() {
		if(this -> cursorX < Sim::World::WORLD_SIZE * Sim::World::Chunk::CHUNK_SIZE - 1) {
			this -> cursorX++;
		}
	}
	inline void Left() {
		if(this -> cursorX > 0) {
			this -> cursorX--;
		}
	}

	// Wird jede Frame aufgerufen
	void Update(double deltaTime);
	void Draw();

private:
	Sim::World* world;

	uint32_t cursorX;
	uint32_t cursorY;
};

}
#pragma once

#include <stdafx.h>

namespace Edit {

// Klasse f�r Liste mit allen Bl�cken
class BlockList {
public:
	BlockList() : selectedBlock(0), selecting(false) {};
	~BlockList() = default; // Standarddesktruktor

	// Beginne mit Blockauswahl
	void StartSelect();
	// Beende Blockauswahl, R�ckgabewert ist die ID des ausgew�hlten Blocks
	uint32_t StopSelect();
	// �ndere Auswahl
	void Previous();
	void Next();

	// Wird jede Frame aufgerufen
	void Draw();

private:
	uint32_t selectedBlock;
	bool selecting;
};

}
#include <editor/blockList.h>

#include <system/window.h>
#include <system/graphics.h>
#include <simulation/blockRegistry.h>

using namespace System;
using namespace Sim;

namespace Edit {

void BlockList::StartSelect() {
	this -> selecting = true;
}

uint32_t BlockList::StopSelect() {
	this -> selecting = false;
	return this -> selectedBlock;
}

void BlockList::Previous() {
	if(this -> selectedBlock > 0) {
		this -> selectedBlock--;
	}
}

void BlockList::Next() {
	if(this -> selectedBlock < BlockRegistry::GetBlockCount() - 1) {
		this -> selectedBlock++;
	}
}

void BlockList::Draw() {
	if(!this -> selecting) {
		return;
	}

	// Zeichne Fenster
	Graphics::Rect rect = { (int32_t)Window::GetWidth() - 100, 0, 100, Window::GetHeight(), 0.9f, 0.9f, 0.9f };
	Graphics::FillRects(&rect, 1);
	rect.color.red = 0.0f;
	rect.color.green = 0.0f;
	rect.color.blue = 0.0f;
	Graphics::DrawRects(&rect, 1);

	// Zeichne Rahmen um ausgewählten Block

	Graphics::Rect border = { (int32_t)Window::GetWidth() - 90, (int32_t)Window::GetHeight() - 90, 80, 80, 0.0f, 0.5f, 0.0f };
	Graphics::DrawRects(&border, 1);

	Graphics::FlushFilledRects();
	Graphics::FlushRects();

	// Zeichne Blockvorschauen
	Graphics::TexturedSquare* blockPreviews;
	blockPreviews = new Graphics::TexturedSquare[BlockRegistry::GetBlockCount() - this -> selectedBlock];

	for(uint32_t i = this -> selectedBlock; i < BlockRegistry::GetBlockCount(); i++) {
		blockPreviews[i - this -> selectedBlock] = { (int32_t)Window::GetWidth() - 74, (int32_t)(Window::GetHeight() - 74 - (i - this -> selectedBlock) * 100), 48, i + 16 };
		Graphics::AddTextToQueue(BlockRegistry::GetBlock(i).name, Window::GetWidth() - 80, Window::GetHeight() - 74 - (i - this -> selectedBlock) * 100, 0.1f, 0.1f, 0.1f);
	}

	Graphics::DrawTexturedSquares(blockPreviews, BlockRegistry::GetBlockCount() - this -> selectedBlock);
	Graphics::FlushTexturedSquares();
	Graphics::DrawTextQueue();

	delete blockPreviews;
}

}
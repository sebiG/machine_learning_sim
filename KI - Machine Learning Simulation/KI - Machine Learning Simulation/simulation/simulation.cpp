#include <simulation/simulation.h>

#include <system/log.h>
#include <system/errors.h>
#include <system/window.h>
#include <system/input.h>
#include <system/program.h>
#include <simulation/world.h>
#include <simulation/settings.h>
#include <simulation/character.h>
#include <simulation/physicsEngine.h>
#include <simulation/render.h>
#include <ai/ai.h>
#include <ui/ui.h>
#include <ui/button.h>
#include <ui/textbox.h>
#include <ui/scrollbar.h>

using namespace System;
using namespace UI;
using namespace AI;

namespace Sim {

void Simulation::Update(double deltaTime) {
	if(this -> uiMode == UiMode::UI_NONE) {
		// Verlasse Simulation falls Ctrl + Q gedr�ckt wird
		if(((Input::GetKeyboardState(DIK_LCONTROL) == Input::KeyState::PRESSED) || (Input::GetKeyboardState(DIK_RCONTROL) == Input::KeyState::PRESSED)) && Input::GetKeyboardState(DIK_Q) == Input::KeyState::DOWN) {
			Log::Write("Simulation exited (Ctrl + Q)");
			Program::SwitchProgramMode(Program::ProgramModeEnum::PROGRAM_MODE_MAIN_MENU);
		}

		// Starte/Stoppe Simulation falls SPACE gedr�ckt wird
		if(Input::GetKeyboardState(DIK_SPACE) == Input::KeyState::DOWN) {
			if(this -> physicsEngine -> IsRunning()) {
				// Stoppe alle laufenden Komponenten
				Log::Write("Simulation manually stopped");
				this -> Stop();
				this -> statusBar.SetStatus(L"Simulation manuell gestoppt (Space)");
			} else {
				// Starte alle Komponenten
				Log::Write("Simulation manually started");
				this -> Start();
				this -> statusBar.SetStatus(L"Simulation manuell gestartet (Space)");
			}
		}

		// Setze Simulation zur�ck falls Ctrl + R gedr�ckt wird
		if(((Input::GetKeyboardState(DIK_LCONTROL) == Input::KeyState::PRESSED) || (Input::GetKeyboardState(DIK_RCONTROL) == Input::KeyState::PRESSED)) && Input::GetKeyboardState(DIK_R) == Input::KeyState::DOWN) {
			// Setze Spieler zur�ck an Startposition, registriere neue Kollisionen und signalisiere der KI, dass ein Reset stattgefunden hat
			Log::Write("Simulation manually reset (Ctrl + R)");
			this -> character -> Reset(Vector2(this -> world -> GetStartX(), this -> world -> GetStartY() + 1));
			this -> physicsEngine -> RecognizeCollisions();
			this -> ai -> Reset();
			this -> statusBar.SetStatus(L"Simulation manuell zur�ckgesetzt");
		}

		// Wechsle Kontrollmodus falls Ctrl + M gedr�ckt wird
		if(((Input::GetKeyboardState(DIK_LCONTROL) == Input::KeyState::PRESSED) || (Input::GetKeyboardState(DIK_RCONTROL) == Input::KeyState::PRESSED)) && Input::GetKeyboardState(DIK_M) == Input::KeyState::DOWN) {
			if(this -> controlMode == ControlMode::CONTROL_MANUAL) {
				Log::Write("Set control mode to intelligent (Ctrl + M)");
				// Aktiviere KI falls die Physiksimulation l�uft
				if(this -> physicsEngine -> IsRunning()) {
					this -> ai -> Activate();
				}
				// Setze KI zur�ck, so dass sie mit den neuen Positionswerten arbeiten kann
				this -> ai -> Reset();
				this -> controlMode = ControlMode::CONTROL_INTELLIGENT;
				this -> statusBar.SetStatus(L"Steuerungsmodus auf intelligent gestellt");
			} else if(this -> controlMode == ControlMode::CONTROL_INTELLIGENT) {
				Log::Write("Set control mode to manual (Ctrl + M)");
				// Deaktiviere KI, falls sie immer noch aktiviert ist
				this -> ai -> Deactivate();
				this -> controlMode = ControlMode::CONTROL_MANUAL;
				this -> statusBar.SetStatus(L"Steuerungsmodus auf manuell gestellt");
			}
		}

		// Steure Spieler mittels A, W, D falls mnueller Kontrollmodus eingeschaltet ist
		if(this -> controlMode == ControlMode::CONTROL_MANUAL) {
			this -> character -> ResetInput();
			if(Input::GetKeyboardState(DIK_W) == Input::KeyState::PRESSED) {
				this -> character -> SetInput(Character::Input::JUMP, true);
			}
			if(Input::GetKeyboardState(DIK_A) == Input::KeyState::PRESSED) {
				this -> character -> SetInput(Character::Input::WALK_LEFT, true);
			}
			if(Input::GetKeyboardState(DIK_D) == Input::KeyState::PRESSED) {
				this -> character -> SetInput(Character::Input::WALK_RIGHT, true);
			}
		}
	}

	// Aktualisiere KI und Physiksimulation
	if(deltaTime < Simulation::MAX_FRAME_LENGTH) {
		this -> physicsEngine -> Update(deltaTime);
		this -> physicsEngine -> RecognizeCollisions();

		if(ai -> IsActivated()) {
			this -> ai -> Update(deltaTime);

			// Stoppe Simulation falls Ziel erreicht wurde
			if(this -> character -> GetCollision(Character::Side::BOTTOM)) {
				if((int32_t)round(this -> character -> GetPosition().GetY()) - 1 == this -> world -> GetEndY()) {
					if((int32_t)floor(this -> character -> GetPosition().GetX() + 0.5) == this -> world -> GetEndX() || (int32_t)ceil(this -> character -> GetPosition().GetX() - 0.5) == this -> world -> GetEndX()) {
						Log::Write("Simulation automatically stopped");
						this -> Stop();
						this -> statusBar.SetStatus(L"Ziel erreicht, Simulation gestoppt");
					}
				}
			}

			// Stoppe Simulation, falls KI mehrere Male hintereinander zur�ckgesetzt wurde
		}
	} else {
		stringstream ss;
		ss << deltaTime << " max: " << Simulation::MAX_FRAME_LENGTH << endl;
		Log::Write("Omitting frame: frametime too high, measured: " + ss.str());
	}

	this -> render -> Draw();
	this -> ai -> DrawDebugInformation();
	this -> physicsEngine -> DrawDebugInformation();

	// Aktualisiere Benutzeroberfl�che
	this -> UpdateUI(deltaTime);
}

void Simulation::Init() {
	Log::WriteImportant("Simulation: begin Init");

	// Erstelle Objekte
	this -> world = new World();
	this -> settings = new Settings();
	this -> character = new Character(Vector2(this -> world -> GetStartX(), this -> world -> GetStartY() + 1));
	this -> physicsEngine = new PhysicsEngine(this -> world, this -> settings, this -> character);
	this -> render = new Render(this -> world, this -> character);

	this -> ai = new Ai(this -> world, this -> character);
	this -> ai -> Register(this, Ai::EVENT_REQUEST_RESET);

	this -> controlMode = ControlMode::CONTROL_MANUAL;

	this -> physicsEngine -> RecognizeCollisions();

	// Benutzeroberfl�che f�r Simulationseinstellungen
	{
		this -> settingsUi = new Ui();
		this -> settingsUi -> x = Window::GetWidth() - 330;
		this -> settingsUi -> y = 10;
		this -> settingsUi -> width = 320;
		this -> settingsUi -> height = 448;
		this -> settingsUi -> text = L"Simulationseinstellungen";
		this -> settingsUi -> active = false;
		this -> settingsUi -> visible = false;

		this -> characterMassTextbox = new Textbox();
		this -> characterMassTextbox -> x = 10;
		this -> characterMassTextbox -> y = 348;
		this -> characterMassTextbox -> width = 300;
		this -> characterMassTextbox -> height = 44;
		this -> characterMassTextbox -> text = L"Spielermasse [kg]";
		this -> characterMassTextbox -> maxCharacters = 12;
		this -> settingsUi -> AddUiElement(this -> characterMassTextbox);

		this -> gravityAccelerationTextbox = new Textbox();
		this -> gravityAccelerationTextbox -> x = 10;
		this -> gravityAccelerationTextbox -> y = 300;
		this -> gravityAccelerationTextbox -> width = 300;
		this -> gravityAccelerationTextbox -> height = 44;
		this -> gravityAccelerationTextbox -> text = L"Fallbeschleunigung [m/s�]";
		this -> gravityAccelerationTextbox -> maxCharacters = 12;
		this -> settingsUi -> AddUiElement(this -> gravityAccelerationTextbox);

		this -> maxWalkingSpeedTextbox = new Textbox();
		this -> maxWalkingSpeedTextbox -> x = 10;
		this -> maxWalkingSpeedTextbox -> y = 252;
		this -> maxWalkingSpeedTextbox -> width = 300;
		this -> maxWalkingSpeedTextbox -> height = 44;
		this -> maxWalkingSpeedTextbox -> text = L"Maximale Laufgeschwindigkeit [m/s]";
		this -> maxWalkingSpeedTextbox -> maxCharacters = 12;
		this -> settingsUi -> AddUiElement(this -> maxWalkingSpeedTextbox);

		this -> horizontalForceTextbox = new Textbox();
		this -> horizontalForceTextbox -> x = 10;
		this -> horizontalForceTextbox -> y = 204;
		this -> horizontalForceTextbox -> width = 300;
		this -> horizontalForceTextbox -> height = 44;
		this -> horizontalForceTextbox -> text = L"Horizontale Kraft [N]";
		this -> horizontalForceTextbox -> maxCharacters = 12;
		this -> settingsUi -> AddUiElement(this -> horizontalForceTextbox);

		this -> airHorizontalForceTextbox = new Textbox();
		this -> airHorizontalForceTextbox -> x = 10;
		this -> airHorizontalForceTextbox -> y = 156;
		this -> airHorizontalForceTextbox -> width = 300;
		this -> airHorizontalForceTextbox -> height = 44;
		this -> airHorizontalForceTextbox -> text = L"Horizontale Kraft in der Luft [N]";
		this -> airHorizontalForceTextbox -> maxCharacters = 12;
		this -> settingsUi -> AddUiElement(this -> airHorizontalForceTextbox);

		this -> jumpImpulseTextbox = new Textbox();
		this -> jumpImpulseTextbox -> x = 10;
		this -> jumpImpulseTextbox -> y = 108;
		this -> jumpImpulseTextbox -> width = 300;
		this -> jumpImpulseTextbox -> height = 44;
		this -> jumpImpulseTextbox -> text = L"Sprungimpuls [kg m/s]";
		this -> jumpImpulseTextbox -> maxCharacters = 12;
		this -> settingsUi -> AddUiElement(this -> jumpImpulseTextbox);

		this -> settingsPathTextbox = new Textbox();
		this -> settingsPathTextbox -> x = 10;
		this -> settingsPathTextbox -> y = 58;
		this -> settingsPathTextbox -> width = 300;
		this -> settingsPathTextbox -> height = 44;
		this -> settingsPathTextbox -> text = L"Dateipfad Einstellungen (.sconf)";
		this -> settingsPathTextbox -> maxCharacters = 35;
		this -> settingsUi -> AddUiElement(this -> settingsPathTextbox);

		this -> settingsSaveButton = new Button();
		this -> settingsSaveButton -> x = 10;
		this -> settingsSaveButton -> y = 34;
		this -> settingsSaveButton -> width = 300;
		this -> settingsSaveButton -> height = 20;
		this -> settingsSaveButton -> text = L"Einstellungen speichern";
		this -> settingsUi -> AddUiElement(this -> settingsSaveButton);

		this -> settingsLoadButton = new Button();
		this -> settingsLoadButton -> x = 10;
		this -> settingsLoadButton -> y = 10;
		this -> settingsLoadButton -> width = 300;
		this -> settingsLoadButton -> height = 20;
		this -> settingsLoadButton -> text = L"Einstellungen laden";
		this -> settingsUi -> AddUiElement(this -> settingsLoadButton);
	}

	// Benutzeroberfl�che f�r Welt
	{
		this -> worldUi = new Ui();
		this -> worldUi -> x = Window::GetWidth() - 330;
		this -> worldUi -> y = 10;
		this -> worldUi -> width = 320;
		this -> worldUi -> height = 140;
		this -> worldUi -> text = L"Welt Laden";
		this -> worldUi -> active = false;
		this -> worldUi -> visible = false;

		this -> worldPathTextbox = new Textbox();
		this -> worldPathTextbox -> x = 10;
		this -> worldPathTextbox -> y = 34;
		this -> worldPathTextbox -> width = 300;
		this -> worldPathTextbox -> height = 44;
		this -> worldPathTextbox -> text = L"Dateipfad (.world)";
		this -> worldPathTextbox -> maxCharacters = 35;
		this -> worldUi -> AddUiElement(this -> worldPathTextbox);

		this -> worldLoadButton = new Button();
		this -> worldLoadButton -> x = 10;
		this -> worldLoadButton -> y = 10;
		this -> worldLoadButton -> width = 300;
		this -> worldLoadButton -> height = 20;
		this -> worldLoadButton -> text = L"Welt laden";
		this -> worldUi -> AddUiElement(this -> worldLoadButton);
	}

	// Benutzeroberfl�che f�r KI
	{
		this -> aiUi = new Ui();
		this -> aiUi -> x = Window::GetWidth() - 330;
		this -> aiUi -> y = 10;
		this -> aiUi -> width = 320;
		this -> aiUi -> height = 350;
		this -> aiUi -> text = L"KI-Einstellungen und -Datenbank";
		this -> aiUi -> active = false;
		this -> aiUi -> visible = false;

		this -> physicsMemoryPathTextbox = new Textbox();
		this -> physicsMemoryPathTextbox -> x = 10;
		this -> physicsMemoryPathTextbox -> y = 222;
		this -> physicsMemoryPathTextbox -> width = 300;
		this -> physicsMemoryPathTextbox -> height = 44;
		this -> physicsMemoryPathTextbox -> text = L"Dateipfad Physikged�chtnis (.pmem)";
		this -> physicsMemoryPathTextbox -> maxCharacters = 35;
		this -> aiUi -> AddUiElement(this -> physicsMemoryPathTextbox);

		this -> physicsMemorySaveButton = new Button();
		this -> physicsMemorySaveButton -> x = 10;
		this -> physicsMemorySaveButton -> y = 198;
		this -> physicsMemorySaveButton -> width = 300;
		this -> physicsMemorySaveButton -> height = 20;
		this -> physicsMemorySaveButton -> text = L"Physikged�chtnis speichern";
		this -> aiUi -> AddUiElement(this -> physicsMemorySaveButton);

		this -> physicsMemoryLoadButton = new Button();
		this -> physicsMemoryLoadButton -> x = 10;
		this -> physicsMemoryLoadButton -> y = 174;
		this -> physicsMemoryLoadButton -> width = 300;
		this -> physicsMemoryLoadButton -> height = 20;
		this -> physicsMemoryLoadButton -> text = L"Physikged�chtnis laden";
		this -> aiUi -> AddUiElement(this -> physicsMemoryLoadButton);

		this -> physicsMemoryClearButton = new Button();
		this -> physicsMemoryClearButton -> x = 10;
		this -> physicsMemoryClearButton -> y = 150;
		this -> physicsMemoryClearButton -> width = 300;
		this -> physicsMemoryClearButton -> height = 20;
		this -> physicsMemoryClearButton -> text = L"Physikged�chtnis l�schen";
		this -> aiUi -> AddUiElement(this -> physicsMemoryClearButton);

		this -> worldMemoryPathTextbox = new Textbox();
		this -> worldMemoryPathTextbox -> x = 10;
		this -> worldMemoryPathTextbox -> y = 82;
		this -> worldMemoryPathTextbox -> width = 300;
		this -> worldMemoryPathTextbox -> height = 44;
		this -> worldMemoryPathTextbox -> text = L"Dateipfad Weltged�chtnis (.wmem)";
		this -> worldMemoryPathTextbox -> maxCharacters = 35;
		this -> aiUi -> AddUiElement(this -> worldMemoryPathTextbox);

		this -> worldMemorySaveButton = new Button();
		this -> worldMemorySaveButton -> x = 10;
		this -> worldMemorySaveButton -> y = 58;
		this -> worldMemorySaveButton -> width = 300;
		this -> worldMemorySaveButton -> height = 20;
		this -> worldMemorySaveButton -> text = L"Weltged�chtnis speichern";
		this -> aiUi -> AddUiElement(this -> worldMemorySaveButton);

		this -> worldMemoryLoadButton = new Button();
		this -> worldMemoryLoadButton -> x = 10;
		this -> worldMemoryLoadButton -> y = 34;
		this -> worldMemoryLoadButton -> width = 300;
		this -> worldMemoryLoadButton -> height = 20;
		this -> worldMemoryLoadButton -> text = L"Weltged�chtnis laden";
		this -> aiUi -> AddUiElement(this -> worldMemoryLoadButton);

		this -> worldMemoryClearButton = new Button();
		this -> worldMemoryClearButton -> x = 10;
		this -> worldMemoryClearButton -> y = 10;
		this -> worldMemoryClearButton -> width = 300;
		this -> worldMemoryClearButton -> height = 20;
		this -> worldMemoryClearButton -> text = L"Weltged�chtnis l�schen";
		this -> aiUi -> AddUiElement(this -> worldMemoryClearButton);
	}

	this -> ReloadUIValues();

	this -> uiMode = UiMode::UI_NONE;

	// Registriere f�r Ereignisse
	Window::Register(this, Window::WINDOW_SIZE);

	this -> characterMassTextbox -> Register(this, Textbox::TEXTBOX_CHANGE);
	this -> gravityAccelerationTextbox -> Register(this, Textbox::TEXTBOX_CHANGE);
	this -> maxWalkingSpeedTextbox -> Register(this, Textbox::TEXTBOX_CHANGE);
	this -> horizontalForceTextbox -> Register(this, Textbox::TEXTBOX_CHANGE);
	this -> airHorizontalForceTextbox -> Register(this, Textbox::TEXTBOX_CHANGE);
	this -> jumpImpulseTextbox -> Register(this, Textbox::TEXTBOX_CHANGE);
	this -> settingsSaveButton -> Register(this, Button::BUTTON_ACTIVATE);
	this -> settingsLoadButton -> Register(this, Button::BUTTON_ACTIVATE);

	this -> worldLoadButton -> Register(this, Button::BUTTON_ACTIVATE);

	this -> physicsMemorySaveButton -> Register(this, Button::BUTTON_ACTIVATE);
	this -> physicsMemoryLoadButton -> Register(this, Button::BUTTON_ACTIVATE);
	this -> physicsMemoryClearButton -> Register(this, Button::BUTTON_ACTIVATE);
	this -> worldMemorySaveButton -> Register(this, Button::BUTTON_ACTIVATE);
	this -> worldMemoryLoadButton -> Register(this, Button::BUTTON_ACTIVATE);
	this -> worldMemoryClearButton -> Register(this, Button::BUTTON_ACTIVATE);

	Log::WriteImportant("Simulation: DeInit terminated");
}

void Simulation::DeInit() {
	Log::WriteImportant("Simulation: begin DeInit");

	// L�sche Objekte
	delete this -> render;
	delete this -> physicsEngine;
	delete this -> character;
	delete this -> settings;
	delete this -> world;

	delete this -> settingsUi;
	delete this -> characterMassTextbox;
	delete this -> gravityAccelerationTextbox;
	delete this -> maxWalkingSpeedTextbox;
	delete this -> horizontalForceTextbox;
	delete this -> airHorizontalForceTextbox;
	delete this -> jumpImpulseTextbox;
	delete this -> settingsPathTextbox;
	delete this -> settingsSaveButton;
	delete this -> settingsLoadButton;

	delete this -> worldUi;
	delete this -> worldPathTextbox;
	delete this -> worldLoadButton;

	delete this -> aiUi;
	delete this -> physicsMemoryPathTextbox;
	delete this -> physicsMemorySaveButton;
	delete this -> physicsMemoryLoadButton;
	delete this -> physicsMemoryClearButton;
	delete this -> worldMemoryPathTextbox;
	delete this -> worldMemorySaveButton;
	delete this -> worldMemoryLoadButton;
	delete this -> worldMemoryClearButton;

	this -> uiMode = UiMode::UI_NONE;

	Log::WriteImportant("Simulation: DeInit terminated");
}

void Simulation::OnEvent(Event const& e) {
	EventSender* sender = e.param1.Decode<EventSender*>();
	
	if(e.eventID == Ai::EVENT_REQUEST_RESET) {
		// Wenn der Charekter immer noch am Start steht, stoppe die Simulation
		// Die KI kann nichts mehr unternehmen
		if(abs(this -> character -> GetPosition().GetX() - (double)this -> world -> GetStartX()) < Ai::EPSILON && abs(this -> character -> GetPosition().GetY() - (double)(this -> world -> GetStartY() + 1)) < Ai::EPSILON) {
			// Setze Simulation zur�ck und stoppe Simulation
			Log::Write("Simulation automatically reset");
			Log::Write("AI is unable to find new path");
			Log::Write("Simulation automatically stopped");
			this -> character -> Reset(Vector2(this -> world -> GetStartX(), this -> world -> GetStartY() + 1));
			this -> physicsEngine -> RecognizeCollisions();
			this -> ai -> Reset();
			this -> Stop();
			this -> statusBar.SetStatus(L"Simulation automatisch zur�ckgesetzt und gestoppt, da KI kei Ziel ausmachen kann");
		} else {
			// Setze Simulation zur�ck
			Log::Write("Simulation automatically reset");
			this -> character -> Reset(Vector2(this -> world -> GetStartX(), this -> world -> GetStartY() + 1));
			this -> physicsEngine -> RecognizeCollisions();
			this -> ai -> Reset();
			this -> statusBar.SetStatus(L"Simulation automatisch zur�ckgesetzt");
		}
	} else if(e.eventID == Window::WINDOW_SIZE) {
		// Positioniere Benutzeroberfl�che neu
		this -> settingsUi -> x = e.param1.Decode<uint32_t>() - 330;
		this -> worldUi -> x = e.param1.Decode<uint32_t>() - 330;
		this -> aiUi -> x = e.param1.Decode<uint32_t>() - 330;
	} else if(e.eventID == Textbox::TEXTBOX_CHANGE) {
		wstringstream ss;
		ss.str(L"");
		if(sender == this -> characterMassTextbox) { // Spieler �ndern
			ss.str(this -> characterMassTextbox -> enteredText);
			double d;
			if(ss >> d) {
				this -> settings -> SetCharacterMass(d);
			}
			this -> ReloadUIValues();
		} else if(sender == this -> gravityAccelerationTextbox) { // Gravitatinsbeschleunigung �ndern
			ss.str(this -> gravityAccelerationTextbox -> enteredText);
			double d;
			if(ss >> d) {
				this -> settings -> SetGravityAcceleration(d);
			}
			this -> ReloadUIValues();
		} else if(sender == this -> maxWalkingSpeedTextbox) { // Maximale Laufgeschwindigkeit �ndern
			ss.str(this -> maxWalkingSpeedTextbox -> enteredText);
			double d;
			if(ss >> d) {
				this -> settings -> SetMaxWalkingSpeed(d);
			}
			this -> ReloadUIValues();
		} else if(sender == this -> horizontalForceTextbox) { // Horizontale Kraft �ndern
			ss.str(this -> horizontalForceTextbox -> enteredText);
			double d;
			if(ss >> d) {
				this -> settings -> SetHorizontalForce(d);
			}
			this -> ReloadUIValues();
		} else if(sender == this -> airHorizontalForceTextbox) { // Horizontale Kraft in der Luft �ndern
			ss.str(this -> airHorizontalForceTextbox -> enteredText);
			double d;
			if(ss >> d) {
				this -> settings -> SetAirHorizontalForce(d);
			}
			this -> ReloadUIValues();
		} else if(sender == this -> jumpImpulseTextbox) { // Sprungimpuls �ndern
			ss.str(this -> jumpImpulseTextbox -> enteredText);
			double d;
			if(ss >> d) {
				this -> settings -> SetJumpImpulse(d);
			}
			this -> ReloadUIValues();
		}
	} else if(e.eventID == Button::BUTTON_ACTIVATE) {
		if(sender == this -> settingsSaveButton) {
			// Simulationseinstellungen speichern
			this -> SaveSimulationSettings();
		} else if(sender == this -> settingsLoadButton) {
			// Simulationseinstellungen laden
			this -> LoadSimulationSettings();
		} else if(sender == this -> worldLoadButton) {
			// Welt Laden
			this -> LoadWorld();
		} else if(sender == this -> physicsMemorySaveButton) {
			// Physikged�chtnis speichern
			this -> SavePhysicsMemory();
		} else if(sender == this -> physicsMemoryLoadButton) {
			// Physikged�chtnis laden
			this -> LoadPhysicsMemory();
		} else if(sender == this -> physicsMemoryClearButton) {
			// Physikged�chtnis l�schen
			this -> ai -> ClearPhysicsMemory();
			this -> statusBar.SetStatus(L"Physikged�chtnis gel�scht");
		} else if(sender == this -> worldMemorySaveButton) {
			// Weltged�chtnis speichern
			this -> SaveWorldMemory();
		} else if(sender == this -> worldMemoryLoadButton) {
			// Weltged�chtnis laden
			this -> LoadWorldMemory();
		} else if(sender == this -> worldMemoryClearButton) {
			// Weltged�chtnis l�schen
			this -> ai -> ClearWorldMemory();
			this -> statusBar.SetStatus(L"Weltged�chtnis gel�scht");
		}
	}
}

void Simulation::Start() {
	this -> physicsEngine -> Start();
	if(this -> controlMode == ControlMode::CONTROL_INTELLIGENT) {
		this -> ai -> Activate();
	}
}

void Simulation::Stop() {
	this -> physicsEngine -> Stop();
	if(this -> controlMode == ControlMode::CONTROL_INTELLIGENT) {
		this -> ai -> Deactivate();
	}
}

void Simulation::SaveSimulationSettings() {
	StringConverter converter;
	string file;
	try {
		file = converter.to_bytes(this -> settingsPathTextbox -> enteredText);
	} catch(range_error re) {
		Log::WriteImportant("Save simulation settings: filename is in wrong format");
		this -> statusBar.SetStatus(L"Speichern von Simulationseinstellungen: Dateiname liegt im falschen Format vor");
	}
	try {
		this -> settings -> Save(file);
		Log::WriteImportant("Saved simulation settings to \"" + file + "\"");
		this -> statusBar.SetStatus(L"Simulationseinstellungen in Datei \"" + converter.from_bytes(file) + L"\" gespeichert");
	} catch(Error error) {
		Log::WriteImportant("Couldn't save simulation settings to \"" + file + "\"");
		this -> statusBar.SetStatus(L"Simulationseinstellungen konnten nicht in Datei \"" + converter.from_bytes(file) + L"\" gespeichert werden");
	}
}

void Simulation::LoadSimulationSettings() {
	StringConverter converter;
	string file;
	try {
		file = converter.to_bytes(this -> settingsPathTextbox -> enteredText);
	} catch(range_error re) {
		Log::WriteImportant("Load simulation settings: filename is in wrong format");
		this -> statusBar.SetStatus(L"Laden von Simulationseinstellungen: Dateiname liegt im falschen Format vor");
	}
	try {
		this -> settings -> Load(file);
		Log::WriteImportant("Loaded simulation settings from \"" + file + "\"");
		this -> statusBar.SetStatus(L"Simulationseinstellungen von Datei \"" + converter.from_bytes(file) + L"\" geladen");

		this -> ReloadUIValues();
	} catch(Error error) {
		Log::WriteImportant("Couldn't load simulation settings from \"" + file + "\" Error: " + error.What());
		this -> statusBar.SetStatus(L"Simulationseinstellungen konnten nicht von Datei \"" + converter.from_bytes(file) + L"\" geladen werden");
	}
}

void Simulation::LoadWorld() {
	StringConverter converter;
	string file;
	try {
		file = converter.to_bytes(this -> worldPathTextbox -> enteredText);
	} catch(range_error re) {
		Log::WriteImportant("Load world: filename is in wrong format");
		this -> statusBar.SetStatus(L"Laden von Welt: Dateiname liegt im falschen Format vor");
	}
	try {
		this -> Stop();
		this -> world -> Load(file);
		this -> character -> Reset(Vector2(this -> world -> GetStartX(), this -> world -> GetStartY() + 1));
		this -> physicsEngine -> RecognizeCollisions();
		this -> ai -> Reset();
		this -> ai -> ClearWorldMemory();
		Log::WriteImportant("Loaded world from \"" + file + "\"");
		this -> statusBar.SetStatus(L"Welt von Datei \"" + converter.from_bytes(file) + L"\" geladen");
	} catch(Error error) {
		Log::WriteImportant("Couldn't load world from \"" + file + "\" Error: " + error.What());
		this -> statusBar.SetStatus(L"Welt konnte nicht von Datei \"" + converter.from_bytes(file) + L"\" geladen werden");
	}
}

void Simulation::SaveWorldMemory() {
	StringConverter converter;
	string file;
	try {
		file = converter.to_bytes(this -> worldMemoryPathTextbox -> enteredText);
	} catch(range_error re) {
		Log::WriteImportant("Save world memory: filename is in wrong format");
		this -> statusBar.SetStatus(L"Speichern von Weltged�chtnis: Dateiname liegt im falschen Format vor");
	}
	try {
		this -> ai -> SaveWorldMemory(file);
		Log::WriteImportant("Saved world memory to \"" + file + "\"");
		this -> statusBar.SetStatus(L"Weltged�chtnis in Datei \"" + converter.from_bytes(file) + L"\" gespeichert");
	} catch(Error error) {
		Log::WriteImportant("Couldn't save world memory to \"" + file + "\"");
		this -> statusBar.SetStatus(L"Weltged�chnis konnten nicht in Datei \"" + converter.from_bytes(file) + L"\" gespeichert werden");
	}
}

void Simulation::LoadWorldMemory() {
	StringConverter converter;
	string file;
	try {
		file = converter.to_bytes(this -> worldMemoryPathTextbox -> enteredText);
	} catch(range_error re) {
		Log::WriteImportant("Load world memory: filename is in wrong format");
		this -> statusBar.SetStatus(L"Laden von Weltged�chtnis: Dateiname liegt im falschen Format vor");
	}
	try {
		this -> ai -> LoadWorldMemory(file);
		Log::WriteImportant("Loaded world memory from \"" + file + "\"");
		this -> statusBar.SetStatus(L"Weltged�chtnis von Datei \"" + converter.from_bytes(file) + L"\" geladen");
	} catch(Error error) {
		Log::WriteImportant("Couldn't load world memory from \"" + file + "\" Error: " + error.What());
		this -> statusBar.SetStatus(L"Weltged�chtnis konnten nicht von Datei \"" + converter.from_bytes(file) + L"\" geladen werden");
	}
}

void Simulation::SavePhysicsMemory() {
	StringConverter converter;
	string file;
	try {
		file = converter.to_bytes(this -> physicsMemoryPathTextbox -> enteredText);
	} catch(range_error re) {
		Log::WriteImportant("Save physics memory: filename is in wrong format");
		this -> statusBar.SetStatus(L"Speichern von Physikged�chtnis: Dateiname liegt im falschen Format vor");
	}
	try {
		this -> ai -> SavePhysicsMemory(file);
		Log::WriteImportant("Saved physics memory to \"" + file + "\"");
		this -> statusBar.SetStatus(L"Physikged�chtnis in Datei \"" + converter.from_bytes(file) + L"\" gespeichert");
	} catch(Error error) {
		Log::WriteImportant("Couldn't save physics memory to \"" + file + "\"");
		this -> statusBar.SetStatus(L"Physikged�chtnis konnte nicht in Datei \"" + converter.from_bytes(file) + L"\" gespeichert wertden");
	}
}

void Simulation::LoadPhysicsMemory() {
	StringConverter converter;
	string file;
	try {
		file = converter.to_bytes(this -> physicsMemoryPathTextbox -> enteredText);
	} catch(range_error re) {
		Log::WriteImportant("Load physics memory: filename is in wrong format");
		this -> statusBar.SetStatus(L"Laden von Physikged�chtnis: Dateiname liegt im falschen Format vor");
	}
	try {
		this -> ai -> LoadPhysicsMemory(file);
		Log::WriteImportant("Loaded physics memory from \"" + file + "\"");
		this -> statusBar.SetStatus(L"Physikged�chtnis von Datei \"" + converter.from_bytes(file) + L"\" geladen");
	} catch(Error error) {
		Log::WriteImportant("Couldn't load physics memory from \"" + file + "\" Error: " + error.What());
		this -> statusBar.SetStatus(L"Physikged�chtnis konnten nicht von Datei \"" + converter.from_bytes(file) + L"\" geladen werden");
	}
}

void Simulation::UpdateUI(double deltaTime) {
	// Aktualisiere Benutzeroberfl�che

	if(Input::GetKeyboardState(DIK_ESCAPE) == Input::KeyState::DOWN) {
		// Schliesse aktuelle Benutzeroberfl�che, falls sie aktiv ist
		switch(this -> uiMode) {
		case UiMode::UI_SIM_SETTINGS:
			if(this -> settingsUi -> active) {
				this -> settingsUi -> active = false;
				this -> settingsUi -> visible = false;
				this -> uiMode = UiMode::UI_NONE;
			}
			break;
		case UiMode::UI_WORLD_SETTINGS:
			if(this -> worldUi -> active) {
				this -> worldUi -> active = false;
				this -> worldUi -> visible = false;
				this -> uiMode = UiMode::UI_NONE;
			}
			break;
		case UiMode::UI_AI:
			if(this -> aiUi -> active) {
				this -> aiUi -> active = false;
				this -> aiUi -> visible = false;
				this -> uiMode = UiMode::UI_NONE;
			}
			break;
		}
	}

	if(this -> uiMode == UiMode::UI_NONE) {
		if(Input::GetKeyboardState(DIK_F1) == Input::KeyState::DOWN) {
			Log::Write("Simulation automatically stopped");
			this -> settingsUi -> active = true;
			this -> settingsUi -> visible = true;
			this -> Stop();
			this -> statusBar.SetStatus(L"Simulation automatisch gestoppt");
			this -> uiMode = UiMode::UI_SIM_SETTINGS;
		}
		if(Input::GetKeyboardState(DIK_F2) == Input::KeyState::DOWN) {
			Log::Write("Simulation automatically stopped");
			this -> worldUi -> active = true;
			this -> worldUi -> visible = true;
			this -> Stop();
			this -> statusBar.SetStatus(L"Simulation automatisch gestoppt");
			this -> uiMode = UiMode::UI_WORLD_SETTINGS;
		}
		if(Input::GetKeyboardState(DIK_F3) == Input::KeyState::DOWN) {
			Log::Write("Simulation automatically stopped");
			this -> aiUi -> active = true;
			this -> aiUi -> visible = true;
			this -> Stop();
			this -> statusBar.SetStatus(L"Simulation automatisch gestoppt");
			this -> uiMode = UiMode::UI_AI;
		}
	}

	this -> settingsUi -> Update(deltaTime);
	this -> worldUi -> Update(deltaTime);
	this -> aiUi -> Update(deltaTime);

	this -> statusBar.Update(deltaTime);

	this -> settingsUi -> Draw();
	this -> worldUi -> Draw();
	this -> aiUi -> Draw();

	// Zeichne Informationen �ber Modi
	Graphics::AddTextToQueue(wstring((this -> physicsEngine -> IsRunning()) ? L"Gestartet" : L"Gestoppt") + wstring(L"\nKontrollmodus: ") + wstring((this -> controlMode == ControlMode::CONTROL_MANUAL) ? L"Manuell" : L"Intelligent"), 10, 190, 1.0f, 1.0f, 1.0f);

	this -> statusBar.Draw();

	Graphics::FlushFilledRects();
	Graphics::FlushRects();
	Graphics::FlushLines();
	Graphics::DrawTextQueue();
}

void Simulation::ReloadUIValues() {
	wstringstream ss;
	ss << this -> settings -> GetCharacterMass();
	this -> characterMassTextbox -> enteredText = ss.str();

	ss.str(L"");
	ss << this -> settings -> GetGravityAcceleration();
	this -> gravityAccelerationTextbox -> enteredText = ss.str();

	ss.str(L"");
	ss << this -> settings -> GetMaxWalkingSpeed();
	this -> maxWalkingSpeedTextbox -> enteredText = ss.str();

	ss.str(L"");
	ss << this -> settings -> GetHorizontalForce();
	this -> horizontalForceTextbox -> enteredText = ss.str();

	ss.str(L"");
	ss << this -> settings -> GetAirHorizontalForce();
	this -> airHorizontalForceTextbox -> enteredText = ss.str();

	ss.str(L"");
	ss << this -> settings -> GetJumpImpulse();
	this -> jumpImpulseTextbox -> enteredText = ss.str();
}

const double Simulation::MAX_FRAME_LENGTH = 1.0 / 24.0;

}
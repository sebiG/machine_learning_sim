#pragma once

#include <stdafx.h>

#include <simulation/vector2.h>

namespace Sim {

// Klasse f�r physiklaisches Objekt, welches den Spieler darstellt
// Dient als Schnittstelle zwischen KI, Physiksimulation und anderen Komponenten, die am aktuellen Zustand des Spielers interessiert sind
// Beschleunigung und aussere Kraft wurde ausgelassen, da diese w�rend der Frame mehrmals den Wert �ndern und so hier nicht geeignet sind
class Character {
public:
	Character(Vector2 const& position) : position(position), velocity(Vector2::Zero()) {
		memset(this -> collisionList, 0, 4 * sizeof(bool));
		memset(this -> impulseChange, 0, 2 * sizeof(bool));
		memset(this -> input, 0, 4 * sizeof(bool));
	};
	~Character() = default;

	// Enumeration f�r die verschiedene Seiten des Spielers
	enum class Side {
		TOP = 0,
		BOTTOM = 1,
		RIGHT = 2,
		LEFT = 3
	};

	// Enumeration f�r verschiedene Benutzereingaben
	enum class Input {
		WALK_RIGHT = 0,
		WALK_LEFT = 1,
		JUMP = 2
	};

	// Enumeration f�r Achsen
	enum class Axis {
		X_AXIS = 0,
		Y_AXIS = 1
	};

	// Zugriffsfunktionen

	// Position [m]
	inline Vector2 const& GetPosition() {
		return this -> position;
	}
	inline void SetPosition(Vector2 const& position) {
		this -> position = position;
	}
	// Geschwindigkeit [m/s]
	inline Vector2 const& GetVelocity() {
		return this -> velocity;
	}
	inline void SetVelocity(Vector2 const& velocity) {
		this -> velocity = velocity;
	}
	// Normalkraft [N]
	inline Vector2 const& GetNormalForce() {
		return this -> normalForce;
	}
	inline void SetNormalForce(Vector2 const& normalForce) {
		this -> normalForce = normalForce;
	}
	// Beschleunigung [m/s�]
	inline Vector2 const& GetAcceleration() {
		return this -> acceleration;
	}
	inline void SetAcceleration(Vector2 const& acceleration) {
		this -> acceleration = acceleration;
	}
	// Kollisionen
	inline bool GetCollision(Side side) {
		return this -> collisionList[(uint32_t)side];
	}
	inline void SetCollision(Side side, bool collision) {
		this -> collisionList[(uint32_t)side] = collision;
	}
	// Impuls�nderungen bei Kollisionen
	inline bool GetImpulseChange(Axis axis) {
		return this -> impulseChange[(uint32_t)axis];
	}
	inline void SetImpulseChange(Axis axis, bool impulseChange) {
		this -> impulseChange[(uint32_t)axis] = impulseChange;
	}
	// Benutzereingaben
	inline bool GetInput(Input input) {
		return this -> input[(uint32_t)input];
	}
	inline void SetInput(Input input, bool value) {
		if(input == Character::Input::WALK_RIGHT && !value) {
			cout << "test" << endl;
		}
		this -> input[(uint32_t)input] = value;
	}
	inline void ResetInput() {
		memset(this -> input, 0, 3 * sizeof(bool));
	}

	// Setzt Spieler zur�ck
	inline void Reset(Vector2 const& position) {
		this -> position = position;
		this -> velocity = Vector2::Zero();
		this -> normalForce = Vector2::Zero();
		memset(this -> collisionList, 0, sizeof(bool) * 4);
		memset(this -> impulseChange, 0, sizeof(bool) * 2);
		memset(this -> input, 0, sizeof(bool) * 3);
	}

private:
	Vector2 position;
	Vector2 velocity;
	Vector2 normalForce;
	Vector2 acceleration;
	bool collisionList[4];
	bool impulseChange[2];
	bool input[3];
};

}
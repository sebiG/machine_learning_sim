#pragma once

#include <stdafx.h>

#include <simulation/vector2.h>

// Diese Header-Datei enth�lt Definitionen f�r Datenklassen.
// Die eigentliche Funktionalit�t wird ausserhalb implementiert.

namespace Sim {

// Klasse f�r Simulationswelt
class World {
public:
	// Klasse f�r Teilbereich der Welt
	class Chunk {
	public:
		Chunk();
		~Chunk() = default; // Standarddestruktor
		void Load(ifstream* is); // Lade Daten von Dateistream
		void Save(ofstream* os); // Speichere Daten in Dateistream

		const static uint32_t CHUNK_SIZE = 16; // Seitenl�nge jedes Teilbereichs

											   // Gibt Block an x- und y-Koordinate zur�ck (Ung�ltige Werte geben 0 zur�ck)
		inline uint32_t GetBlock(uint32_t x, uint32_t y) {
			if(x >= Chunk::CHUNK_SIZE || y >= Chunk::CHUNK_SIZE) {
				return 0;
			}
			return this -> blocks[y * Chunk::CHUNK_SIZE + x];
		}
		// Gibt Block an Index zur�ck (F�r ung�ltige Werte ist das Verhalten undefiniert)
		inline uint32_t GetBlock(uint32_t index) {
			return this -> blocks[index];
		}

		// Setzt Block an x- und y-Koordinate
		inline void SetBlock(uint32_t x, uint32_t y, uint32_t block) {
			this -> blocks[y * Chunk::CHUNK_SIZE + x] = block;
		}
		// Setzt Block an Index
		inline void SetBlock(uint32_t index, uint32_t block) {
			this -> blocks[index] = block;
		}

	private:
		uint32_t blocks[Chunk::CHUNK_SIZE * Chunk::CHUNK_SIZE];
	};

	World(); // Erstellt eine leere Welt
	~World(); // L�scht alle Elemente
	void Load(string filename); // L�dt Welt aus Datei
	void Save(string filename); // Speichert Welt in Datei

	const static string CURRENT_SAVE_FILE_VERSION; // string, der in jeder Weltdatei zuoberst vorkommt. So kann gepr�ft werden, ob die Version des Programms mit der Version der Datei �bereinstimmt.

	const static uint32_t WORLD_SIZE = 16; // Seitenl�nge der Welt in Teilbereichen

	// Gibt Block an x- und y-Koordinate relativ zum Nullpunkt der Welt zur�ck (Ung�ltige Werte geben 0 zur�ck)
	inline uint32_t GetBlock(int32_t x, int32_t y) {
		if(x < 0 || y < 0 || x >= World::WORLD_SIZE * Chunk::CHUNK_SIZE || y >= World::WORLD_SIZE * Chunk::CHUNK_SIZE) {
			return 0;
		}

		uint32_t chunkX = x / Chunk::CHUNK_SIZE;
		uint32_t chunkY = y / Chunk::CHUNK_SIZE;
		uint32_t blockX = x % Chunk::CHUNK_SIZE;
		uint32_t blockY = y % Chunk::CHUNK_SIZE;
		if(this -> GetChunk(chunkX, chunkY) == nullptr) {
			return 0;
		}
		return this -> GetChunk(chunkX, chunkY) -> GetBlock(blockX, blockY);
	}
	// Setzt Block an x- und y-Koordinate relative zum Nullpunkt der Welt
	// Falls der betroffene Teilbereich noch nicht existiert, wird er erstellt (F�r ung�ltige Werte bleibt die Welt unver�ndert)
	inline void SetBlock(int32_t x, int32_t y, uint32_t block) {
		if(x < 0 || y < 0 || x >= World::WORLD_SIZE * Chunk::CHUNK_SIZE || y >= World::WORLD_SIZE * Chunk::CHUNK_SIZE) {
			return;
		}

		uint32_t chunkX = x / Chunk::CHUNK_SIZE;
		uint32_t chunkY = y / Chunk::CHUNK_SIZE;
		uint32_t blockX = x % Chunk::CHUNK_SIZE;
		uint32_t blockY = y % Chunk::CHUNK_SIZE;
		if(this -> GetChunk(chunkX, chunkY) == nullptr) {
			this -> SetChunk(chunkX, chunkY, new Chunk());
		}
		this -> GetChunk(chunkX, chunkY) -> SetBlock(blockX, blockY, block);
	}
	// Gibt Chunk an x- und y-Koordinate zur�ck
	inline Chunk* GetChunk(uint32_t x, uint32_t y) {
		return this -> chunks[y * World::WORLD_SIZE + x];
	}
	// Gibt Chunk an Index zur�ck
	inline Chunk* GetChunk(uint32_t index) {
		return this -> chunks[index];
	}
	// Setzt Chunk an x- und y-Koordinate (Falls bereits ein Chunk dort existiert, wird dieser gel�scht)
	inline void SetChunk(uint32_t x, uint32_t y, Chunk* chunk) {
		if(this -> chunks[y * World::WORLD_SIZE + x] != nullptr) {
			delete this -> chunks[y * World::WORLD_SIZE + x];
		}
		this -> chunks[y * World::WORLD_SIZE + x] = chunk;
	}
	// Setzt Chunk an Index (Falls bereits ein Chunk dort existiert, wird dieser gel�scht)
	inline void SetChunk(uint32_t index, Chunk* chunk) {
		if(this -> chunks[index] != nullptr) {
			delete this -> chunks[index];
		}
		this -> chunks[index] = chunk;
	}

	// Start- und Zielpositionen (Spieler steht darauf)
	inline uint32_t GetStartX() {
		return this -> startX;
	}
	inline uint32_t GetStartY() {
		return this -> startY;
	}
	inline uint32_t GetEndX() {
		return this -> endX;
	}
	inline uint32_t GetEndY() {
		return this -> endY;
	}
	inline void SetStartX(uint32_t x) {
		this -> startX = x;
	}
	inline void SetStartY(uint32_t startY) {
		this -> startY = startY;
	}
	inline void SetEndX(uint32_t endX) {
		this -> endX = endX;
	}
	inline void SetEndY(uint32_t endY) {
		this -> endY = endY;
	}
	
private:
	Chunk* chunks[World::WORLD_SIZE * World::WORLD_SIZE];
	uint32_t startX;
	uint32_t startY;
	uint32_t endX;
	uint32_t endY;
};

}
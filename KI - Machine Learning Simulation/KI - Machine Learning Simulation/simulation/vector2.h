#pragma once

#include <stdafx.h>

namespace Sim {

// Klasse f�r SIMD-gest�tzte Vektoren
class Vector2 {
public:
	Vector2() : Vector2(0.0, 0.0) {}; // Konstruktor
	// Konstruktor mit Werten
	Vector2(double x, double y) {
		__declspec(align(16)) double data[2] = { x, y };
		this -> data = _mm_load_pd(data);
	};
	Vector2(Vector2 const& vector) {
		this -> data = vector.data;
	}
private:
	// SIMD-Konstruktor
	Vector2(__m128d const& simd) : data(simd) {}
public:
	// Null-Vektor
	inline static Vector2 Zero() {
		return Vector2();
	}

	// Vergleich
	inline bool operator==(const Vector2 &vector) {
		__m128d comp = _mm_cmpeq_pd(this -> data, vector.data);
		__declspec(align(16)) int64_t res[2];
		_mm_store_pd((double*)res, this -> data);
		return res[0] && res[1];
	};
	inline bool operator!=(const Vector2 &vector) {
		__m128d comp = _mm_cmpneq_pd(this -> data, vector.data);
		__declspec(align(16)) int64_t res[2];
		_mm_store_pd((double*)res, this -> data);
		return res[0] || res[1];
	};
	// Zuweisung
	inline Vector2& operator=(Vector2 const &vector) {
		this -> data = vector.data;
		return *this;
	};
	// Arithmetische Operatoren
	inline Vector2 operator+(Vector2 const &vector) const {
		return Vector2(_mm_add_pd(this -> data, vector.data));
	};
	inline Vector2 operator-(Vector2 const &vector) const {
		return Vector2(_mm_sub_pd(this -> data, vector.data));
	};
	inline Vector2 operator*(double const &scalar) const {
		__m128d scalar_ = _mm_set1_pd(scalar);
		return Vector2(_mm_mul_pd(this -> data, scalar_));
	};
	inline double operator*(Vector2 const &vector) const {
		return this -> Dot(vector);
	};
	inline Vector2 operator/(double const &scalar) const {
		if(scalar == 0) {
			throw new invalid_argument("Divide by zero");
		}
		__m128d scalar_ = _mm_set1_pd(scalar);
		return Vector2(_mm_div_pd(this -> data, scalar_));
	};
	inline Vector2 operator-() const {
		__m128d zero;
		return Vector2(_mm_sub_pd(zero, this -> data));
	};
	inline Vector2 operator+=(Vector2 const &vector) {
		this -> data = _mm_add_pd(this -> data, vector.data);
		return *this;
	};
	inline Vector2& operator-=(Vector2 const &vector) {
		this -> data = _mm_sub_pd(this -> data, vector.data);
		return *this;
	};
	inline Vector2& operator*=(double const &scalar) {
		__m128d scalar_ = _mm_set1_pd(scalar);
		this -> data = _mm_mul_pd(this -> data, scalar_);
		return *this;
	};
	inline Vector2& operator/=(double const &scalar) {
		__m128d scalar_ = _mm_set1_pd(scalar);
		this -> data = _mm_div_pd(this -> data, scalar_);
		return *this;
	};

	// Magnitude
	inline double Length() const {
		__m128d square = _mm_mul_pd(this -> data, this -> data);
		__declspec(align(16)) double ret[2];
		_mm_store_pd(ret, square);
		__m128d sum = _mm_set1_pd(ret[0] + ret[1]);
		_mm_store_pd(ret, _mm_sqrt_pd(sum));
		return ret[0];
	};
	// Magnitude im Quadrat (schneller, da keine Quadratwurzel gebraucht wird)
	inline double LengthSquared() const {
		__m128d square = _mm_mul_pd(this -> data, this -> data);
		__declspec(align(16)) double ret[2];
		_mm_store_pd(ret, square);
		return ret[0] + ret[1];
	};
	// Skalarprodukt
	inline double Dot(Vector2 const &vector) const {
		__m128d mult = _mm_mul_pd(this -> data, vector.data);
		__declspec(align(16)) double ret[2];
		_mm_store_pd(ret, mult);
		return ret[0] + ret[1];
	};
	// Winkel zwischen Vektoren
	inline double Angle(Vector2 const &vector) const {
		Vector2 v = this -> Normalize();
		return acos(v.Dot(vector.Normalize()));
	};
	// Normailisieren
	inline Vector2 Normalize() const {
		double length = this -> Length();
		__m128d length_ = _mm_set1_pd(length);
		return Vector2(_mm_div_pd(this -> data, length_));
	};

	// Wertzugriff
	inline void SetX(double x) {
		__declspec(align(16)) double ret[2];
		_mm_store_pd(ret, this -> data);
		ret[0] = x;
		this -> data = _mm_load_pd(ret);
	};
	inline void SetY(double y) {
		__declspec(align(16)) double ret[2];
		_mm_store_pd(ret, this -> data);
		ret[1] = y;
		this -> data = _mm_load_pd(ret);
	};
	inline double GetX() const {
		__declspec(align(16)) double ret[2];
		_mm_store_pd(ret, this -> data);
		return ret[0];
	};
	inline double GetY() const {
		__declspec(align(16)) double ret[2];
		_mm_store_pd(ret, this -> data);
		return ret[1];
	};

	// Freunddeklaration, f�r kommutative Skalarmultiplikation
	friend Vector2 operator*(double const &scalar, Vector2 const& object);

private:
	__m128d data;
};

// reversed operators

inline Vector2 operator*(double const &scalar, Vector2 const& object) {
	__m128d scalar_ = _mm_set1_pd(scalar);
	return Vector2(_mm_mul_pd(object.data, scalar_));
};

}
#pragma once

#include <stdafx.h>

#include <system/errors.h>
#include <simulation/character.h>

namespace Sim {



// Diese Klasse enth�llt alle wichtigen Objekte aus den oben deklarierten Strukturen
// Singleton
class PhysicsBehaviour {
public:
	PhysicsBehaviour() = delete; // Verhindere Konstruktion
	~PhysicsBehaviour() = default;

	// Logische Operatoren um konditionelle Variabel mit Referenzwert zu verbinden
	enum class LogicalOperator {
		EQUAL_TO, // Gleich Referenzwert
		NOT_EQUAL_TO, // Ungleich Referenzwert
		SMALLER_THAN, // Kleiner als Referenzwert
		BIGGER_THAN, // Gr�sser als Referenzwert
		SAME_SIGN_THAN, // Gleiches Vorzeichen
		NOT_SAME_SIGN_THAN // Anderes Vorzeichen
	};

	// Variable
	const static uint32_t VARIABLE_COUNT = 16; //31;
	struct Variable {
		// Enumeration der Variabelnamen, die der KI direkt oder indirekt zur Verf�gung stehen
		// S_* sind Skalare
		// B_* sind Boolsche Werte {0.0, 1.0}
		// *_0 sind Werte aus der letzten Frame, sie werden verwendet um Unterschiede zu erkennen. Diese Werte werden bei Eingabereaktionen nicht verwendet
		enum class Name : uint16_t {
			S_DELTA_TIME,
			S_POSITION_X,
			S_POSITION_Y,
			//S_POSITION_X_0,
			//S_POSITION_Y_0,
			S_VELOCITY_X,
			S_VELOCITY_Y,
			//S_VELOCITY_X_0,
			//S_VELOCITY_Y_0,
			S_ACCELERATION_X,
			S_ACCELERATION_Y,
			//S_ACCELERATION_X_0,
			//S_ACCELERATION_Y_0,
			B_COLLISION_U,
			B_COLLISION_D,
			B_COLLISION_L,
			B_COLLISION_R,
			//B_COLLISION_U_0,
			//B_COLLISION_D_0,
			//B_COLLISION_L_0,
			//B_COLLISION_R_0,
			B_IMPULSE_CHANGE_X,
			B_IMPULSE_CHANGE_Y,
			//B_IMPULSE_CHANGE_X_0,
			//B_IMPULSE_CHANGE_Y_0,
			B_INPUT_U,
			B_INPUT_L,
			B_INPUT_R,
			//B_INPUT_U_0,
			//B_INPUT_L_0,
			//B_INPUT_R_0
		};

		Variable() = default;
		Variable(Name variable, uint16_t frame) : name(variable), frame(frame) {}

		bool operator ==(Variable const& object) const {
			return (this -> frame == object.frame) && (this -> name == object.name);
		}

		Name name;
		uint16_t frame;
	};

	// Enumeration der Konstanten, die zur Laufzeit ermittelt werden m�ssen
	const static uint32_t CONSTANT_COUNT = 8;
	enum class Constant {
		// Einmalige Konstanten
		S_GRAVITY,
		S_HORIZONTAL_FORCE_AIR,
		S_HORIZONTAL_FORCE_GROUND,
		S_JUMP_IMPULSE,
		// Konstanten, die auf spezielle Weise ermittelt werden
		S_MAX_HORIZONTAL_VELOCITY,
		S_MAX_HORIZONTAL_NEG_VELOCITY, // Gleicher Wert, nur mit anderem Vorzeichen
									   // Konstanten, die Pro Material ermittelt werden
		S_PER_MATERIAL_FRICTION_COEFFICIENT,
		S_PER_MATERIAL_IMPULSE_CONSERVATION_COEFFICIENT
	};

	struct Value {
		// Verschiedene Arten von Werten
		enum class Type {
			VARIABLE,
			CONSTANT,
			LITERAL
		};

		Value() = default;
		Value(double value) {
			this -> type = Type::LITERAL;
			this -> doubleValue = value;
		}
		Value(Variable value) {
			this -> type = Type::VARIABLE;
			this -> variableValue = value;
		}
		Value(Constant value) {
			this -> type = Type::CONSTANT;
			this -> constantValue = value;
		}

		// Gleich bis auf Zeit (im Falle einer Variable)
		inline bool CausalEqual(Value const& object) const {
			if(this -> type == object.type) {
				if(this -> type == Type::CONSTANT) {
					return this -> constantValue == object.constantValue;
				}
				if(this -> type == Type::VARIABLE) {
					return this -> variableValue.name == object.variableValue.name;
				}
				if(this -> type == Type::LITERAL) {
					return this -> doubleValue == object.doubleValue;
				}
			}
			return false;
		}

		Type type;
		union {
			double doubleValue;
			Variable variableValue;
			Constant constantValue;
		};
	};

	// Struktur f�r Bedingung, die Gelten muss, damit eine Formel angewendet werden kann
	struct Condition {
		Variable conditionalVariable;

		LogicalOperator op;

		Value referenceValue;
	};

	// Aktionen, die die KI machen kann
	enum class Action {
		RIGHT = 0,
		LEFT = 1,
		JUMP = 2,
		RELEASE = 3, // L�sst alle Tasten los
		NONE = 4
	};

	// Struktur, die Beschreibt, wie auf eine Benutzereingabe reagiert wird
	// Hiermit kann die KI herausfinden, was sie tun muss, um eine Gew�nschte Situation hervorzurufen
	// �ber diese Daten l�sst sich jedoch nicht herausfinden, was die Konkreten Werte von Konstanten sind
	struct InputReaction {
		uint32_t inputConditionsCount;
		Condition* inputConditions;

		Action triggerInput;

		uint32_t outputConditionsCount;
		Condition* outputConditions;
	};

	// Struktur f�r Formel, die verwendet werden kann, um Konstanten zu berechnen
	// Hiermit kann die KI konkrete Werte f�r Konstanten berechnen, falls die Bedingungen erf�llt sind
	struct ConstantFormula {
		uint32_t conditionsCount;
		Condition* conditions;

		Constant constant;

		uint32_t parametersCount;
		Value* parameters;

		double(*Evaluate)(double* values);
	};

	inline static bool IsCreated() {
		return PhysicsBehaviour::isCreated;
	}
	static void Create(); // Erstelle Objekte
	static void Destroy(); // L�sche Objekte

	inline static uint32_t GetInputReactionsCount() {
		return (uint32_t)PhysicsBehaviour::inputReactions.size();
	}
	inline static InputReaction const* GetInputReaction(uint32_t id) {
		if(id >= PhysicsBehaviour::inputReactions.size()) {
			stringstream ss;
			ss << id << "max: " << PhysicsBehaviour::inputReactions.size() - 1;
			throw System::Error("PhysicsBehaviour::GetInputReaction(): id too big, given: " + ss.str());
		}
		return &PhysicsBehaviour::inputReactions[id];
	}

	inline static uint32_t GetConstantFormulaeCount() {
		return (uint32_t)PhysicsBehaviour::constantFormulae.size();
	}
	inline static ConstantFormula const* GetConstantFormula(uint32_t id) {
		if(id >= PhysicsBehaviour::constantFormulae.size()) {
			stringstream ss;
			ss << id << "max: " << PhysicsBehaviour::constantFormulae.size() - 1;
			throw System::Error("PhysicsBehaviour::GetConstantFormula(): id too big, given: " + ss.str());
		}
		return &PhysicsBehaviour::constantFormulae[id];
	}

private:
	static bool isCreated;

	static vector<InputReaction> inputReactions;
	static vector<ConstantFormula> constantFormulae;
};

}
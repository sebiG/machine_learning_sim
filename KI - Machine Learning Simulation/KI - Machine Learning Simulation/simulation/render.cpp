#include <simulation/render.h>

#include <system/window.h>
#include <system/graphics.h>
#include <simulation/world.h>
#include <simulation/character.h>
#include <simulation/vector2.h>

using namespace System;

namespace Sim {

void Render::Draw() {
	// Zeichne sichtbare Teile der Welt
	Vector2 characterPos = this -> character -> GetPosition();
	int32_t firstBlockX = (int32_t)floor((characterPos.GetX() - (double)Window::GetWidth() / 2.0 / (double)Render::BLOCK_SIZE));
	int32_t lastBlockX = (int32_t)ceil((characterPos.GetX() + (double)Window::GetWidth() / 2.0 / (double)Render::BLOCK_SIZE));
	int32_t firstBlockY = (int32_t)floor((characterPos.GetY() - (double)Window::GetHeight() / 2.0 / (double)Render::BLOCK_SIZE));
	int32_t lastBlockY = (int32_t)ceil((characterPos.GetY() + (double)Window::GetHeight() / 2.0 / (double)Render::BLOCK_SIZE));

	Vector2 offset = characterPos * (double)Render::BLOCK_SIZE;
	int32_t offsetX = (int32_t)offset.GetX() - Window::GetWidth() / 2 + Render::BLOCK_SIZE / 2;
	int32_t offsetY = (int32_t)offset.GetY() - Window::GetHeight() / 2 + Render::BLOCK_SIZE / 2;

	uint32_t squaresCounter = 0;
	for(int32_t i = firstBlockX; i <= lastBlockX; i++) {
		for(int32_t j = firstBlockY; j <= lastBlockY; j++) {
			uint32_t block = this -> world -> GetBlock(i, j);

			Graphics::TexturedSquare square = { (int32_t)(i * Render::BLOCK_SIZE) - offsetX, (int32_t)(j * Render::BLOCK_SIZE) - offsetY, Render::BLOCK_SIZE, block + 16 };
			Graphics::DrawTexturedSquares(&square, 1);

			squaresCounter++;
			if(squaresCounter == Graphics::MAX_SQUARE_LIST_SIZE - 1) {
				squaresCounter = 0;
				Graphics::FlushTexturedSquares();
			}
		}
	}
	Graphics::FlushTexturedSquares();

	// Alter Code
	/*
	if(firstBlockX < 0 && lastBlockX >= 0) {
		firstBlockX = 0;
	} else if(firstBlockX < 0 && lastBlockX < 0) {
		firstBlockX = 0;
		lastBlockX = 0;
	}
	if(firstBlockY < 0 && lastBlockY >= 0) {
		firstBlockY = 0;
	} else if(firstBlockY < 0 && lastBlockY < 0) {
		firstBlockY = 0;
		lastBlockY = 0;
	}
	uint32_t firstChunkX = (uint32_t)firstBlockX / Chunk::CHUNK_SIZE;
	uint32_t lastChunkX = (uint32_t)lastBlockX / Chunk::CHUNK_SIZE;
	uint32_t firstChunkY = (uint32_t)firstBlockY / Chunk::CHUNK_SIZE;
	uint32_t lastChunkY = (uint32_t)lastBlockY / Chunk::CHUNK_SIZE;

	Vector2 offset = characterPos * (double)Render::BLOCK_SIZE;
	int32_t offsetX = (int32_t)offset.GetX() - Window::GetWidth() / 2 + Render::BLOCK_SIZE / 2;
	int32_t offsetY = (int32_t)offset.GetY() - Window::GetHeight() / 2 + Render::BLOCK_SIZE / 2;

	for(uint32_t x = firstChunkX; x <= lastChunkX; x++) {
		if(x >= World::WORLD_SIZE) {
			continue;
		}
		uint32_t xChunkOffset = x * Chunk::CHUNK_SIZE;
		for(uint32_t y = firstChunkY; y <= lastChunkY; y++) {
			if(y >= World::WORLD_SIZE) {
				continue;
			}
			uint32_t yChunkOffset = y * Chunk::CHUNK_SIZE;

			Chunk* chunk = this -> world -> GetChunk(x, y);
			if(chunk == nullptr) {
				continue;
			}
			uint32_t squaresCounter = 0;
			for(uint32_t i = 0; i < Chunk::CHUNK_SIZE; i++) {
				for(uint32_t j = 0; j < Chunk::CHUNK_SIZE; j++) {
					uint32_t block = chunk -> GetBlock(i, j);

					Graphics::TexturedSquare square = { (int32_t)((xChunkOffset + i) * Render::BLOCK_SIZE) - offsetX, (int32_t)((yChunkOffset + j) * Render::BLOCK_SIZE) - offsetY, Render::BLOCK_SIZE, block + 16 };
					Graphics::DrawTexturedSquares(&square, 1);

					squaresCounter++;
					if(squaresCounter == Graphics::MAX_SQUARE_LIST_SIZE - 1) {
						squaresCounter = 0;
						Graphics::FlushTexturedSquares();
					}
				}
			}
			Graphics::FlushTexturedSquares();
		}
	}
	*/

	// Zeichne Spieler
	Graphics::TexturedSquare character = { (int32_t)(Window::GetWidth() - Render::BLOCK_SIZE) / 2, (int32_t)(Window::GetHeight() - Render::BLOCK_SIZE) / 2, Render::BLOCK_SIZE, 0 };
	Graphics::DrawTexturedSquares(&character, 1);
	Graphics::FlushTexturedSquares();
}

}
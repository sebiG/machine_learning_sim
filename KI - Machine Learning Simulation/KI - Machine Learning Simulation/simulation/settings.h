#pragma once

#include <stdafx.h>

#include <system/errors.h>

#include <simulation/vector2.h>

namespace Sim {

// Klasse f�r Simulationseinstellungen
// Enth�llt allgemeine Einstellungen zur Physik und Spieler
class Settings {
public:
	Settings();
	~Settings() = default; // Standarddestruktor
	// Lade Einstellungen von Datei
	void Load(string filename);
	// Speichere Einstellungen in Datei
	void Save(string filename);

	const static string CURRENT_SAVE_FILE_VERSION; // string, der in jeder Einstellungsdatei vorkommt. So kann gepr�ft werden, ob die Version des Programms mit der Version der Datei �bereinstimmt.

	// Spielermasse
	inline double GetCharacterMass() {
		return this -> characterMass;
	}
	inline void SetCharacterMass(double characterMass) {
		if(characterMass <= 0.0) {
			throw System::Error("Error ocuured in Settings::SetCharacterMass, characterMass must be bigger than zero");
		}
		this -> characterMass = characterMass;
	}
	// Schwerkraft
	inline double GetGravityAcceleration() {
		return this -> gravityAcceleration;
	}
	inline void SetGravityAcceleration(double gravity) {
		this -> gravityAcceleration = gravity;
	}
	// Maximale Laufgeschwindigkeit
	inline double GetMaxWalkingSpeed() {
		return this -> maxWalkingSpeed;
	}
	inline void SetMaxWalkingSpeed(double maxWalkingSpeed) {
		this -> maxWalkingSpeed = maxWalkingSpeed;
	}
	// Horizontale Kraft
	inline double GetHorizontalForce() {
		return this -> horizontalForce;
	}
	inline void SetHorizontalForce(double horizontalForce) {
		this -> horizontalForce = horizontalForce;
	}
	// Horizontale Kraft in der Luft
	inline double GetAirHorizontalForce() {
		return this -> airHorizontalForce;
	}
	inline void SetAirHorizontalForce(double airHorizontalForce) {
		this -> airHorizontalForce = airHorizontalForce;
	}
	// Sprungimpuls
	inline double GetJumpImpulse() {
		return this -> jumpImpulse;
	}
	inline void SetJumpImpulse(double jumpImpulse) {
		this -> jumpImpulse = jumpImpulse;
	}

private:
	double characterMass;

	double gravityAcceleration; // Wird m�glicherweise zu einem Vektor ge�ndert

	double maxWalkingSpeed;
	double horizontalForce;
	double airHorizontalForce;
	double jumpImpulse;
};

}
#include <simulation/world.h>

#include <system/errors.h>
#include <system/log.h>
#include <simulation/blockRegistry.h>

using namespace System;

namespace Sim {

// Chunk

World::Chunk::Chunk() {
	memset(this -> blocks, 0, Chunk::CHUNK_SIZE * Chunk::CHUNK_SIZE * sizeof(uint32_t));
}

void World::Chunk::Load(ifstream* is) {
	is -> read((char*)this -> blocks, Chunk::CHUNK_SIZE * Chunk::CHUNK_SIZE * sizeof(uint32_t));
}

void World::Chunk::Save(ofstream* os) {
	os -> write((char*)this -> blocks, Chunk::CHUNK_SIZE * Chunk::CHUNK_SIZE * sizeof(uint32_t));
}

// World

World::World() {
	this -> startX = 0;
	this -> startY = 0;
	this -> endX = 1;
	this -> endY = 0;
	memset(this -> chunks, 0, World::WORLD_SIZE * World::WORLD_SIZE * sizeof(Chunk*));
	this -> SetBlock(0, 0, 1);
	this -> SetBlock(1, 0, 1);
}

World::~World() {
	for(uint32_t i = 0; i < World::WORLD_SIZE * World::WORLD_SIZE; i++) {
		delete (this -> chunks[i]);
	}
}

void World::Load(string filename) {
	Log::Write("begin loading world from file " + filename);

	ifstream is(filename, ios::binary);
	if(!is.is_open()) {
		throw Error("Error occured in World::Load, file not found\nFilename: " + filename);
	}

	string version;
	while(is.peek() != '\n') {
		version.push_back(is.get());
	}
	is.get();

	Log::Write("worldfile version checked");

	if(version != World::CURRENT_SAVE_FILE_VERSION) {
		throw Error("Error occured in World::Load, file version is incompatible\nFile Version: " + version + " Required Version: " + World::CURRENT_SAVE_FILE_VERSION);
	}

	// Sichere alte Daten f�r den Fall, dass die Datei besch�digt ist
	uint32_t oldStartX = this -> startX;
	uint32_t oldStartY = this -> startY;
	uint32_t oldEndX = this -> endX;
	uint32_t oldEndY = this -> endY;
	Chunk* oldChunks[World::WORLD_SIZE * World::WORLD_SIZE];
	memcpy(oldChunks, this -> chunks, sizeof(Chunk*) * World::WORLD_SIZE * World::WORLD_SIZE);

	// L�sche alle alten Daten
	memset(this -> chunks, 0, sizeof(Chunk*) * World::WORLD_SIZE * World::WORLD_SIZE);

	try {
		Log::Write("reading character data...");

		// Lese Spielerposition
		is.read((char*)&this -> startX, sizeof(uint32_t));
		is.read((char*)&this -> startY, sizeof(uint32_t));
		is.read((char*)&this -> endX, sizeof(uint32_t));
		is.read((char*)&this -> endY, sizeof(uint32_t));

		Log::Write("reading chunk data...");

		// Lese Weltdaten
		for(uint32_t i = 0; i < World::WORLD_SIZE * World::WORLD_SIZE; i++) {
			bool chunkExists;
			is.read((char*)&chunkExists, sizeof(bool));
			if(chunkExists) {
				this -> chunks[i] = new Chunk();
				this -> chunks[i] -> Load(&is);
			}
			if(!is) {
				throw Error("Error occured in ifstream::read, worldfile may be corrupted\nFile: " + filename);
			}
		}

		is.close();

	} catch(Error error) {
		// L�sche fehlerhaft Daten
		for(uint32_t i = 0; i < World::WORLD_SIZE * World::WORLD_SIZE; i++) {
			delete (this -> chunks[i]);
		}

		// Lade Sicherung
		this -> startX = oldStartX;
		this -> startY = oldStartY;
		this -> endX = oldEndX;
		this -> endY = oldEndY;
		memcpy(this -> chunks, oldChunks, sizeof(Chunk*) * World::WORLD_SIZE * World::WORLD_SIZE);

		throw error;
	}

	// L�sche Sicherung
	for(uint32_t i = 0; i < World::WORLD_SIZE * World::WORLD_SIZE; i++) {
		delete (oldChunks[i]);
	}

	Log::Write("done loading world");
}

void World::Save(string filename) {
	Log::Write("begin saving world to " + filename);

	ofstream os(filename, ios::binary | ios::trunc);
	if(!os.is_open()) {
		throw Error("Error occured in World::Save, unable to open file\nFilename: " + filename);
	}

	os << World::CURRENT_SAVE_FILE_VERSION << "\n" << flush;

	Log::Write("savefile version written");

	Log::Write("writing character data...");

	os.write((char*)&this -> startX, sizeof(uint32_t));
	os.write((char*)&this -> startY, sizeof(uint32_t));
	os.write((char*)&this -> endX, sizeof(uint32_t));
	os.write((char*)&this -> endY, sizeof(uint32_t));

	Log::Write("writing chunk data...");

	// Schreibe Weltdaten
	for(uint32_t i = 0; i < World::WORLD_SIZE * World::WORLD_SIZE; i++) {
		if(this -> chunks[i] != nullptr) {
			bool b = true;
			os.write((char*)&b, sizeof(bool));

			this -> chunks[i] -> Save(&os);
		} else {
			bool b = false;
			os.write((char*)&b, sizeof(bool));
		}
	}

	os.close();

	Log::Write("done saving world");
}

// Version ist definiert als eine Kombination der eigenen Version und der Version des Blockregisters
const string World::CURRENT_SAVE_FILE_VERSION = "v1.1_" + BlockRegistry::CURRENT_BLOCK_FILE_VERSION;

}
#include <simulation/blockRegistry.h>

#include <system/log.h>

using namespace System;

namespace Sim {

void BlockRegistry::Create() {
	if(BlockRegistry::isCreated) {
		// Programm bereits erstellt
		throw FatalError("Attempting to create already created block registry singleton");
	}
	BlockRegistry::isCreated = true;

	Log::Write("begin loading block data from file " + BlockRegistry::BLOCK_FILE);

	// Setze Standarddaten f�r leerer Block (Luft)
	BlockData emptyBlock = { L"Luft", 0.0, 0.0 };
	BlockRegistry::blockData.push_back(emptyBlock);

	wifstream is(BlockRegistry::BLOCK_FILE, ios::binary);
	if(!is.is_open()) {
		throw Error("Error occured in BlockRegistry::Load, file not found\nFilename: " + BlockRegistry::BLOCK_FILE);
	}

	wstring version;
	string asciiVersion;
	while(is.peek() != '\r' && is.peek() != '\n') {
		version.push_back(is.get());
	}
	while(is.peek() == '\r' || is.peek() == '\n') {
		is.get();
	}

	Log::Write("Block file version checked");

	StringConverter converter;
	try {
		asciiVersion = converter.to_bytes(version);
	} catch(range_error e) {
		throw Error("Error occured in BlockRegistry::Load, file version has invalid format");
	}
	if(asciiVersion != BlockRegistry::CURRENT_BLOCK_FILE_VERSION) {
		throw Error("Error occured in BlockRegistry::Load, file version is incompatible\nFile Version: " + asciiVersion + " Required Version: " + BlockRegistry::CURRENT_BLOCK_FILE_VERSION);
	}

	Log::Write("reading block data...");

	// Format
	// ID [Name] Reibungskoeffizient Impulserhaltung
	wstringstream ss;
	wstring line;

	while(!is.eof()) {
		getline(is, line);
		ss.str(line);

		BlockData block;

		uint32_t id; // Wird sp�ter nciht verwendet
		if(!(ss >> id)) {
			throw Error("Error occured in BlockRegistry::Load, invalid format of file: " + BlockRegistry::BLOCK_FILE);
		}

		while(ss.peek() != L'[') {
			ss.get();
		}
		ss.get();
		while(ss.peek() != L']') {
			block.name.push_back(ss.get());
		}
		ss.get();
		if(!ss) {
			throw Error("Error occured in BlockRegistry::Load, invalid format of file: " + BlockRegistry::BLOCK_FILE);
		}

		if(!(ss >> block.frictionCoefficent)) {
			throw Error("Error occured in BlockRegistry::Load, invalid format of file: " + BlockRegistry::BLOCK_FILE);
		}

		if(!(ss >> block.conservedImpulse)) {
			throw Error("Error occured in BlockRegistry::Load, invalid format of file: " + BlockRegistry::BLOCK_FILE);
		}

		BlockRegistry::blockData.push_back(block);
	}

	Log::Write("done loading block data");

	Log::WriteImportant("Block registry created");
}

void BlockRegistry::Destroy() {
	if(!BlockRegistry::isCreated) {
		// Programm bereits zerst�rt
		throw FatalError("Attempting to destroy already destroyed block registry singleton");
	}
	BlockRegistry::isCreated = false;

	BlockRegistry::blockData.clear();

	Log::WriteImportant("Block registry destroyed");
}

const string BlockRegistry::BLOCK_FILE = "settings/blocks.cfg";
const string BlockRegistry::CURRENT_BLOCK_FILE_VERSION = "v1.0";

bool BlockRegistry::isCreated = false;

vector<BlockData> BlockRegistry::blockData;

}
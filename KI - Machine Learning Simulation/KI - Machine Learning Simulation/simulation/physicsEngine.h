#pragma once

#include <stdafx.h>

#include <system/graphics.h>
#include <system/log.h>
#include <simulation/vector2.h>

namespace Sim {

class World;
class Settings;
class Character;

// Klasse f�r Physiksimulation
class PhysicsEngine {
public:
	PhysicsEngine(World* world, Settings* settings, Character* character) : world(world), settings(settings), character(character), running(false) {};
	~PhysicsEngine() = default;

	const static uint32_t DEBUG_VECTOR_UNIT_LENGTH = 2;

	// Statuskontrolle
	inline bool IsRunning() {
		return this -> running;
	}
	inline void Start() {
		this -> running = true;
		System::Log::Write("Physics engine started");
	}
	inline void Stop() {
		this -> running = false;
		System::Log::Write("Physics engine stopped");
	}

	// Wird jede Frame aufgerufen
	void Update(double deltaTime);
	// Berechnet Kollisionen neu
	void RecognizeCollisions();
	// Zeichnet informationen �ber den aktuellen physikalischen Zustand
	void DrawDebugInformation();

private:
	const static double COLLISION_RAY_TRACE_T;
	const static double MIN_BOUNCE_VELOCITY;
	const static double EPSILON; // Unsch�rfe bei Kollisionen

	// Hilfefunktion
	void DrawVector(System::Graphics::Line* line, System::Graphics::Circle* circle, int32_t offsetX, int32_t offsetY, Vector2 const& vector, float red, float green, float blue);

	World* world;
	Settings* settings;
	Character* character;

	Vector2 debugGravityAcceleration;
	Vector2 debugInternalAcceleration;
	Vector2 debugPrimaryAcceleration;
	Vector2 debugVelocity;
	Vector2 debugPosition;
	Vector2 debugFrictionAcceleration;

	bool running;
};

}
#pragma once

#include <stdafx.h>

#include <system/programMode.h>
#include <system/event.h>
#include <ui/statusBar.h>

namespace UI {

class Ui;
class Button;
class Textbox;

}

namespace AI {

class Ai;

}

namespace Sim {

class World;
class Settings;
class Character;
class PhysicsEngine;
class Render;

// Klasse für Simualtions-Programmmodus
class Simulation : public System::ProgramMode, public System::EventReciever {
public:
	Simulation() = default;
	virtual ~Simulation() = default;

	const static double MAX_FRAME_LENGTH; // Ist eine Frame länger als dieser Grenzwert, wird sie nicht simuliert

	virtual void Update(double deltaTime);
	virtual void Init();
	virtual void DeInit();

	virtual void OnEvent(System::Event const& e);

private:
	// Mögliche Steuerungsmodi
	enum ControlMode {
		CONTROL_MANUAL,
		CONTROL_INTELLIGENT
	};

	// Mögliche Benutzeroberflächenmodi
	enum UiMode {
		UI_NONE,
		UI_SIM_SETTINGS,
		UI_WORLD_SETTINGS,
		UI_AI
	};

	// Hilfefunktionen, um alle Teile der Simulation zu starten bzw. zu stoppen
	void Start();
	void Stop();

	// Hilfefunktionen für laden und speichern von Dateien
	void SaveSimulationSettings();
	void LoadSimulationSettings();
	void LoadWorld();
	void SaveWorldMemory();
	void LoadWorldMemory();
	void SavePhysicsMemory();
	void LoadPhysicsMemory();

	// Teilfunktion von Update
	void UpdateUI(double deltaTime);

	// Aktualisiert Werte der Benutzeroberfläche
	void ReloadUIValues();

	World* world;
	Settings* settings;
	Character* character;
	PhysicsEngine* physicsEngine;
	Render* render;

	AI::Ai* ai;

	ControlMode controlMode;

	UiMode uiMode;
	UI::StatusBar statusBar;

	UI::Ui* settingsUi;
	UI::Textbox* characterMassTextbox;
	UI::Textbox* gravityAccelerationTextbox;
	UI::Textbox* maxWalkingSpeedTextbox;
	UI::Textbox* horizontalForceTextbox;
	UI::Textbox* airHorizontalForceTextbox;
	UI::Textbox* jumpImpulseTextbox;
	UI::Textbox* settingsPathTextbox;
	UI::Button* settingsSaveButton;
	UI::Button* settingsLoadButton;

	UI::Ui* worldUi;
	UI::Textbox* worldPathTextbox;
	UI::Button* worldLoadButton;

	UI::Ui* aiUi;
	UI::Textbox* physicsMemoryPathTextbox;
	UI::Button* physicsMemorySaveButton;
	UI::Button* physicsMemoryLoadButton;
	UI::Button* physicsMemoryClearButton;
	UI::Textbox* worldMemoryPathTextbox;
	UI::Button* worldMemorySaveButton;
	UI::Button* worldMemoryLoadButton;
	UI::Button* worldMemoryClearButton;
};

}
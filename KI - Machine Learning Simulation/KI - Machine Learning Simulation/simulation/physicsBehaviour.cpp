#include <simulation/physicsBehaviour.h>

#include <system/log.h>

using namespace System;

namespace Sim {

// Anonymer Namensbereich f�r Formelfunktionen
namespace {

// Gravitation
double EvalGravity(double* values) {
	return values[0];
}

// Maximale horizontale Geschwindigkeit
double EvalMaxHorizontalVelocity(double* values) {
	return abs(values[0]);
}

// Horizontale Kraft in der Luft
double EvalHorizontalForceAir(double* values) {
	return abs(values[0]);
}

// Horizontale Kraft am Boden
double EvalHorizontalForce(double* values) {
	//double d = (values[0] + abs(values[1]) * values[2] * (values[3]) / abs(values[3])) / (values[2] * values[4]);
	return (abs(values[0]) / values[2]) + abs(values[1]);
}

// Sprungimpuls
double EvalJumpImpulse(double* values) {
	return values[0] - values[1] * values[2];
}

// Reibungskoeffizient in X-Richtung
double EvalFrictionCoefficientX(double* values) {
	return abs(values[0] / values[1]);
}

// Reibungskoeffizient in Y-Richtung
double EvalFrictionCoefficientY(double* values) {
	return abs((values[0] - values[1]) / values[3]);
}

// Impulserhaltungskoeffizient in X-Richtung
double EvalImpulseCoefficientX(double* values) {
	return -1.0 * values[0] / values[1];
}

// Impulserhaltungskoeffizient in Y-Richtung
double EvalImpulseCoefficientY(double* values) {
	return -1.0 * values[0] / (values[1] + values[2] * values[3]);
}

}

void PhysicsBehaviour::Create() {
	if(PhysicsBehaviour::isCreated) {
		// Datenbank bereits erstellt
		throw string("Attempting to create already created physics behaviour database singleton");
	}
	PhysicsBehaviour::isCreated = true;

	Log::Write("begin physics behaviour database creation");

	// F�ge Eingabereaktion ein

	// Sprung
	// wenn Iu
	//		Cd == 1
	//		Cu == 0
	// dann Vy > 0
	//		Cd == 0
	//		Iu == 1
	{
		InputReaction reactionJump;
		reactionJump.inputConditionsCount = 2;
		reactionJump.inputConditions = new Condition[2];
		reactionJump.inputConditions[0] = { Variable(Variable::Name::B_COLLISION_D, 0), LogicalOperator::EQUAL_TO, 1.0 };
		reactionJump.inputConditions[1] = { Variable(Variable::Name::B_COLLISION_U, 0), LogicalOperator::EQUAL_TO, 0.0 };
		reactionJump.triggerInput = Action::JUMP;
		reactionJump.outputConditionsCount = 3;
		reactionJump.outputConditions = new Condition[3];
		reactionJump.outputConditions[0] = { Variable(Variable::Name::S_VELOCITY_Y, 0), LogicalOperator::BIGGER_THAN, 0.0 };
		reactionJump.outputConditions[1] = { Variable(Variable::Name::B_COLLISION_D, 0), LogicalOperator::EQUAL_TO, 0.0 };
		reactionJump.outputConditions[2] = { Variable(Variable::Name::B_INPUT_U, 0), LogicalOperator::EQUAL_TO, 1.0 };
		PhysicsBehaviour::inputReactions.push_back(reactionJump);
	}

	// Bewegung nach rechts in der Luft
	// wenn Ir
	//		Cd == 0
	//		Cr == 0
	//		Px == 0
	//		Vx < Vmax
	// dann Vx > Vx (Nacher gr�sser als vorher)
	//		Ir == 1
	//		Cl == 0
	{
		InputReaction reactionDiveRight;
		reactionDiveRight.inputConditionsCount = 4;
		reactionDiveRight.inputConditions = new Condition[4];
		reactionDiveRight.inputConditions[0] = { Variable(Variable::Name::B_COLLISION_D, 0), LogicalOperator::EQUAL_TO, 0.0 };
		reactionDiveRight.inputConditions[1] = { Variable(Variable::Name::B_COLLISION_R, 0), LogicalOperator::EQUAL_TO, 0.0 };
		reactionDiveRight.inputConditions[2] = { Variable(Variable::Name::B_IMPULSE_CHANGE_X, 0), LogicalOperator::EQUAL_TO, 0.0 };
		reactionDiveRight.inputConditions[3] = { Variable(Variable::Name::S_VELOCITY_X, 0), LogicalOperator::SMALLER_THAN, Constant::S_MAX_HORIZONTAL_VELOCITY };
		reactionDiveRight.triggerInput = Action::RIGHT;
		reactionDiveRight.outputConditionsCount = 3;
		reactionDiveRight.outputConditions = new Condition[3];
		reactionDiveRight.outputConditions[0] = { Variable(Variable::Name::S_VELOCITY_X, 0), LogicalOperator::BIGGER_THAN, Variable(Variable::Name::S_VELOCITY_X, 0) };
		reactionDiveRight.outputConditions[1] = { Variable(Variable::Name::B_INPUT_R, 0), LogicalOperator::EQUAL_TO, 1.0 };
		reactionDiveRight.outputConditions[2] = { Variable(Variable::Name::B_COLLISION_L, 0), LogicalOperator::EQUAL_TO, 0.0 };
		PhysicsBehaviour::inputReactions.push_back(reactionDiveRight);
	}

	// Bewegung nach links in der Luft
	// wenn Il
	//		Cd == 0
	//		Cl == 0
	//		Px == 0
	//		Vx > -Vmax
	// dann Vx < Vx (Nacher kleiner als vorher)
	//		Il == 1
	//		Cr == 0
	{
		InputReaction reactionDiveLeft;
		reactionDiveLeft.inputConditionsCount = 4;
		reactionDiveLeft.inputConditions = new Condition[4];
		reactionDiveLeft.inputConditions[0] = { Variable(Variable::Name::B_COLLISION_D, 0), LogicalOperator::EQUAL_TO, 0.0 };
		reactionDiveLeft.inputConditions[1] = { Variable(Variable::Name::B_COLLISION_L, 0), LogicalOperator::EQUAL_TO, 0.0 };
		reactionDiveLeft.inputConditions[2] = { Variable(Variable::Name::B_IMPULSE_CHANGE_X, 0), LogicalOperator::EQUAL_TO, 0.0 };
		reactionDiveLeft.inputConditions[3] = { Variable(Variable::Name::S_VELOCITY_X, 0), LogicalOperator::BIGGER_THAN, Constant::S_MAX_HORIZONTAL_NEG_VELOCITY };
		reactionDiveLeft.triggerInput = Action::LEFT;
		reactionDiveLeft.outputConditionsCount = 3;
		reactionDiveLeft.outputConditions = new Condition[3];
		reactionDiveLeft.outputConditions[0] = { Variable(Variable::Name::S_VELOCITY_X, 0), LogicalOperator::SMALLER_THAN, Variable(Variable::Name::S_VELOCITY_X, 0) };
		reactionDiveLeft.outputConditions[1] = { Variable(Variable::Name::B_INPUT_L, 0), LogicalOperator::EQUAL_TO, 1.0 };
		reactionDiveLeft.outputConditions[2] = { Variable(Variable::Name::B_COLLISION_R, 0), LogicalOperator::EQUAL_TO, 0.0 };
		PhysicsBehaviour::inputReactions.push_back(reactionDiveLeft);
	}

	// Bewegung nach rechts am Boden
	// !Dies funktioniert nur bei einem g�ltigen Reibungskoeffizienten. Jedoch sollte ein Reibungskoeffizient immer so gew�hlt sein, dass der Spieler sich immer noch fortbewegen kann!
	// wenn Ir
	//		Cd == 1
	//		Cr == 0
	//		Px == 0
	//		Vx < Vmax
	// dann Vx > Vx (Nacher gr�sser als vorher)
	//		Ir == 1
	//		Cl == 0
	{
		InputReaction reactionWalkRight;
		reactionWalkRight.inputConditionsCount = 4;
		reactionWalkRight.inputConditions = new Condition[4];
		reactionWalkRight.inputConditions[0] = { Variable(Variable::Name::B_COLLISION_D, 0), LogicalOperator::EQUAL_TO, 1.0 };
		reactionWalkRight.inputConditions[1] = { Variable(Variable::Name::B_COLLISION_R, 0), LogicalOperator::EQUAL_TO, 0.0 };
		reactionWalkRight.inputConditions[2] = { Variable(Variable::Name::B_IMPULSE_CHANGE_X, 0), LogicalOperator::EQUAL_TO, 0.0 };
		reactionWalkRight.inputConditions[3] = { Variable(Variable::Name::S_VELOCITY_X, 0), LogicalOperator::SMALLER_THAN, Constant::S_MAX_HORIZONTAL_VELOCITY };
		reactionWalkRight.triggerInput = Action::RIGHT;
		reactionWalkRight.outputConditionsCount = 3;
		reactionWalkRight.outputConditions = new Condition[3];
		reactionWalkRight.outputConditions[0] = { Variable(Variable::Name::S_VELOCITY_X, 0), LogicalOperator::BIGGER_THAN, Variable(Variable::Name::S_VELOCITY_X, 0) };
		reactionWalkRight.outputConditions[1] = { Variable(Variable::Name::B_INPUT_R, 0), LogicalOperator::EQUAL_TO, 1.0 };
		reactionWalkRight.outputConditions[2] = { Variable(Variable::Name::B_COLLISION_L, 0), LogicalOperator::EQUAL_TO, 0.0 };
		PhysicsBehaviour::inputReactions.push_back(reactionWalkRight);
	}

	// Bewegung nach links am Boden
	// !Dies funktioniert nur bei einem g�ltigen Reibungskoeffizienten. Jedoch sollte ein Reibungskoeffizient immer so gew�hlt sein, dass der Spieler sich immer noch fortbewegen kann!
	// wenn Il
	//		Cd == 1
	//		Cl == 0
	//		Px == 0
	//		Vx > -Vmax
	// dann Vx < Vx (Nacher kleiner als vorher)
	//		Il == 1
	//		Cr == 0
	{
		InputReaction reactionWalkLeft;
		reactionWalkLeft.inputConditionsCount = 4;
		reactionWalkLeft.inputConditions = new Condition[4];
		reactionWalkLeft.inputConditions[0] = { Variable(Variable::Name::B_COLLISION_D, 0), LogicalOperator::EQUAL_TO, 1.0 };
		reactionWalkLeft.inputConditions[1] = { Variable(Variable::Name::B_COLLISION_L, 0), LogicalOperator::EQUAL_TO, 0.0 };
		reactionWalkLeft.inputConditions[2] = { Variable(Variable::Name::B_IMPULSE_CHANGE_X, 0), LogicalOperator::EQUAL_TO, 0.0 };
		reactionWalkLeft.inputConditions[3] = { Variable(Variable::Name::S_VELOCITY_X, 0), LogicalOperator::BIGGER_THAN, Constant::S_MAX_HORIZONTAL_NEG_VELOCITY };
		reactionWalkLeft.triggerInput = Action::LEFT;
		reactionWalkLeft.outputConditionsCount = 3;
		reactionWalkLeft.outputConditions = new Condition[3];
		reactionWalkLeft.outputConditions[0] = { Variable(Variable::Name::S_VELOCITY_X, 0), LogicalOperator::SMALLER_THAN, Variable(Variable::Name::S_VELOCITY_X, 0) };
		reactionWalkLeft.outputConditions[1] = { Variable(Variable::Name::B_INPUT_L, 0), LogicalOperator::EQUAL_TO, 1.0 };
		reactionWalkLeft.outputConditions[2] = { Variable(Variable::Name::B_COLLISION_R, 0), LogicalOperator::EQUAL_TO, 0.0 };
		PhysicsBehaviour::inputReactions.push_back(reactionWalkLeft);
	}

	// Loslassen von allen Tasten
	// wenn !Il
	//		!Ir
	//		!Iu
	// dann Il == 0
	//		Ir == 0
	//		Iu == 0
	{
		InputReaction reactionRelease;
		reactionRelease.inputConditionsCount = 0;
		reactionRelease.inputConditions = nullptr;
		reactionRelease.triggerInput = Action::RELEASE;
		reactionRelease.outputConditionsCount = 3;
		reactionRelease.outputConditions = new Condition[3];
		reactionRelease.outputConditions[0] = { Variable(Variable::Name::B_INPUT_L, 0), LogicalOperator::EQUAL_TO, 0.0 };
		reactionRelease.outputConditions[1] = { Variable(Variable::Name::B_INPUT_R, 0), LogicalOperator::EQUAL_TO, 0.0 };
		reactionRelease.outputConditions[2] = { Variable(Variable::Name::B_INPUT_U, 0), LogicalOperator::EQUAL_TO, 0.0 };
		PhysicsBehaviour::inputReactions.push_back(reactionRelease);
	}

	// F�ge Funktionen f�r Konstanten ein

	// Gravitationsbeschleunigung
	// g = ay
	// wenn Cu1 == 0
	//		Cd1 == 0
	//		Cl1 == 0
	//		Cr1 == 0
	//		Cu == 0
	//		Cd == 0
	//		Cl == 0
	//		Cr == 0
	//		Py == 0
	{
		ConstantFormula gravityFormula;
		gravityFormula.conditionsCount = 9;
		gravityFormula.conditions = new Condition[9];
		gravityFormula.conditions[0] = { Variable(Variable::Name::B_COLLISION_D, 1), LogicalOperator::EQUAL_TO, 0.0 };
		gravityFormula.conditions[1] = { Variable(Variable::Name::B_COLLISION_U, 1), LogicalOperator::EQUAL_TO, 0.0 };
		gravityFormula.conditions[2] = { Variable(Variable::Name::B_COLLISION_L, 1), LogicalOperator::EQUAL_TO, 0.0 };
		gravityFormula.conditions[3] = { Variable(Variable::Name::B_COLLISION_R, 1), LogicalOperator::EQUAL_TO, 0.0 };
		gravityFormula.conditions[4] = { Variable(Variable::Name::B_COLLISION_D, 0), LogicalOperator::EQUAL_TO, 0.0 };
		gravityFormula.conditions[5] = { Variable(Variable::Name::B_COLLISION_U, 0), LogicalOperator::EQUAL_TO, 0.0 };
		gravityFormula.conditions[6] = { Variable(Variable::Name::B_COLLISION_L, 0), LogicalOperator::EQUAL_TO, 0.0 };
		gravityFormula.conditions[7] = { Variable(Variable::Name::B_COLLISION_R, 0), LogicalOperator::EQUAL_TO, 0.0 };
		gravityFormula.conditions[8] = { Variable(Variable::Name::B_IMPULSE_CHANGE_Y, 0), LogicalOperator::EQUAL_TO, 0.0 };
		gravityFormula.constant = Constant::S_GRAVITY;
		gravityFormula.parametersCount = 1;
		gravityFormula.parameters = new Value[1];
		gravityFormula.parameters[0] = Variable(Variable::Name::S_ACCELERATION_Y, 0);
		gravityFormula.Evaluate = EvalGravity;
		PhysicsBehaviour::constantFormulae.push_back(gravityFormula);
	}

	// Maximale horizontale Geschwindigkeit nach rechts gemessen
	// wenn Cr == 0
	//		Px == 0
	//		Ir == 1
	//		Il == 0
	//		Vx == Vx1
	//		Vx > 0
	{
		ConstantFormula maxHorizontalSpeedFormula1;
		maxHorizontalSpeedFormula1.conditionsCount = 6;
		maxHorizontalSpeedFormula1.conditions = new Condition[6];
		maxHorizontalSpeedFormula1.conditions[0] = { Variable(Variable::Name::B_COLLISION_R, 0), LogicalOperator::EQUAL_TO, 0.0 };
		maxHorizontalSpeedFormula1.conditions[1] = { Variable(Variable::Name::B_IMPULSE_CHANGE_X, 0), LogicalOperator::EQUAL_TO, 0.0 };
		maxHorizontalSpeedFormula1.conditions[2] = { Variable(Variable::Name::B_INPUT_R, 0), LogicalOperator::EQUAL_TO, 1.0 };
		maxHorizontalSpeedFormula1.conditions[3] = { Variable(Variable::Name::B_INPUT_L, 0), LogicalOperator::EQUAL_TO, 0.0 };
		maxHorizontalSpeedFormula1.conditions[4] = { Variable(Variable::Name::S_VELOCITY_X, 0), LogicalOperator::EQUAL_TO, Variable(Variable::Name::S_VELOCITY_X, 1) };
		maxHorizontalSpeedFormula1.conditions[5] = { Variable(Variable::Name::S_VELOCITY_X, 0), LogicalOperator::BIGGER_THAN, 0.0 };
		maxHorizontalSpeedFormula1.constant = Constant::S_MAX_HORIZONTAL_VELOCITY;
		maxHorizontalSpeedFormula1.parametersCount = 1;
		maxHorizontalSpeedFormula1.parameters = new Value[1];
		maxHorizontalSpeedFormula1.parameters[0] = Variable(Variable::Name::S_VELOCITY_X, 0);
		maxHorizontalSpeedFormula1.Evaluate = EvalMaxHorizontalVelocity;
		PhysicsBehaviour::constantFormulae.push_back(maxHorizontalSpeedFormula1);
	}

	// Maximale horizontale Geschwindigkeit nach links gemessen
	// wenn Cl == 0
	//		Px == 0
	//		Ir == 1
	//		Il == 0
	//		Vx == Vx1
	//		Vx < 0
	{
		ConstantFormula maxHorizontalSpeedFormula1;
		maxHorizontalSpeedFormula1.conditionsCount = 6;
		maxHorizontalSpeedFormula1.conditions = new Condition[6];
		maxHorizontalSpeedFormula1.conditions[0] = { Variable(Variable::Name::B_COLLISION_L, 0), LogicalOperator::EQUAL_TO, 0.0 };
		maxHorizontalSpeedFormula1.conditions[1] = { Variable(Variable::Name::B_IMPULSE_CHANGE_X, 0), LogicalOperator::EQUAL_TO, 0.0 };
		maxHorizontalSpeedFormula1.conditions[2] = { Variable(Variable::Name::B_INPUT_L, 0), LogicalOperator::EQUAL_TO, 1.0 };
		maxHorizontalSpeedFormula1.conditions[3] = { Variable(Variable::Name::B_INPUT_R, 0), LogicalOperator::EQUAL_TO, 0.0 };
		maxHorizontalSpeedFormula1.conditions[4] = { Variable(Variable::Name::S_VELOCITY_X, 0), LogicalOperator::EQUAL_TO, Variable(Variable::Name::S_VELOCITY_X, 1) };
		maxHorizontalSpeedFormula1.conditions[5] = { Variable(Variable::Name::S_VELOCITY_X, 0), LogicalOperator::SMALLER_THAN, 0.0 };
		maxHorizontalSpeedFormula1.constant = Constant::S_MAX_HORIZONTAL_VELOCITY;
		maxHorizontalSpeedFormula1.parametersCount = 1;
		maxHorizontalSpeedFormula1.parameters = new Value[1];
		maxHorizontalSpeedFormula1.parameters[0] = Variable(Variable::Name::S_VELOCITY_X, 0);
		maxHorizontalSpeedFormula1.Evaluate = EvalMaxHorizontalVelocity;
		PhysicsBehaviour::constantFormulae.push_back(maxHorizontalSpeedFormula1);
	}

	// Horizontale Kraft in der Luft (nach rechts gemessen)
	// FLuft = |ax * m|, m = 1
	// FLuft = |ax|
	// wenn Cd == 0
	//		Cu == 0
	//		Cr == 0
	//		Cl == 0
	//		Cd1 == 0
	//		Cu1 == 0
	//		Cr1 == 0
	//		Cl1 == 0
	//		Px == 0
	//		Py == 0
	//		Ir == 1
	//		Il == 0
	//		Vx < Vmax
	{
		ConstantFormula horizontalForceAirFormula1;
		horizontalForceAirFormula1.conditionsCount = 13;
		horizontalForceAirFormula1.conditions = new Condition[13];
		horizontalForceAirFormula1.conditions[0] = { Variable(Variable::Name::B_COLLISION_D, 0), LogicalOperator::EQUAL_TO, 0.0 };
		horizontalForceAirFormula1.conditions[1] = { Variable(Variable::Name::B_COLLISION_U, 0), LogicalOperator::EQUAL_TO, 0.0 };
		horizontalForceAirFormula1.conditions[2] = { Variable(Variable::Name::B_COLLISION_R, 0), LogicalOperator::EQUAL_TO, 0.0 };
		horizontalForceAirFormula1.conditions[3] = { Variable(Variable::Name::B_COLLISION_L, 0), LogicalOperator::EQUAL_TO, 0.0 };
		horizontalForceAirFormula1.conditions[4] = { Variable(Variable::Name::B_COLLISION_U, 1), LogicalOperator::EQUAL_TO, 0.0 };
		horizontalForceAirFormula1.conditions[5] = { Variable(Variable::Name::B_COLLISION_D, 1), LogicalOperator::EQUAL_TO, 0.0 };
		horizontalForceAirFormula1.conditions[6] = { Variable(Variable::Name::B_COLLISION_R, 1), LogicalOperator::EQUAL_TO, 0.0 };
		horizontalForceAirFormula1.conditions[7] = { Variable(Variable::Name::B_COLLISION_L, 1), LogicalOperator::EQUAL_TO, 0.0 };
		horizontalForceAirFormula1.conditions[8] = { Variable(Variable::Name::B_IMPULSE_CHANGE_X, 0), LogicalOperator::EQUAL_TO, 0.0 };
		horizontalForceAirFormula1.conditions[9] = { Variable(Variable::Name::B_IMPULSE_CHANGE_Y, 0), LogicalOperator::EQUAL_TO, 0.0 };
		horizontalForceAirFormula1.conditions[10] = { Variable(Variable::Name::B_INPUT_R, 0), LogicalOperator::EQUAL_TO, 1.0 };
		horizontalForceAirFormula1.conditions[11] = { Variable(Variable::Name::B_INPUT_L, 0), LogicalOperator::EQUAL_TO, 0.0 };
		horizontalForceAirFormula1.conditions[12] = { Variable(Variable::Name::S_VELOCITY_X, 0), LogicalOperator::SMALLER_THAN, Constant::S_MAX_HORIZONTAL_VELOCITY };
		horizontalForceAirFormula1.constant = Constant::S_HORIZONTAL_FORCE_AIR;
		horizontalForceAirFormula1.parametersCount = 1;
		horizontalForceAirFormula1.parameters = new Value[1];
		horizontalForceAirFormula1.parameters[0] = Variable(Variable::Name::S_ACCELERATION_X, 0);
		horizontalForceAirFormula1.Evaluate = EvalHorizontalForceAir;
		PhysicsBehaviour::constantFormulae.push_back(horizontalForceAirFormula1);
	}

	// Horizontale Kraft in der Luft (nach links gemessen)
	// FLuft = |ax * m|, m = 1
	// FLuft = |ax|
	// wenn Cd == 0
	//		Cu == 0
	//		Cl == 0
	//		Cr == 0
	//		Cu1 == 0
	//		Cd1 == 0
	//		Cr1 == 0
	//		Cl1 == 0
	//		Px == 0
	//		Py == 0
	//		Il == 1
	//		Ir == 0
	//		Vx > -Vmax
	{
		ConstantFormula horizontalForceAirFormula2;
		horizontalForceAirFormula2.conditionsCount = 13;
		horizontalForceAirFormula2.conditions = new Condition[13];
		horizontalForceAirFormula2.conditions[0] = { Variable(Variable::Name::B_COLLISION_D, 0), LogicalOperator::EQUAL_TO, 0.0 };
		horizontalForceAirFormula2.conditions[1] = { Variable(Variable::Name::B_COLLISION_U, 0), LogicalOperator::EQUAL_TO, 0.0 };
		horizontalForceAirFormula2.conditions[2] = { Variable(Variable::Name::B_COLLISION_L, 0), LogicalOperator::EQUAL_TO, 0.0 };
		horizontalForceAirFormula2.conditions[3] = { Variable(Variable::Name::B_COLLISION_R, 0), LogicalOperator::EQUAL_TO, 0.0 };
		horizontalForceAirFormula2.conditions[4] = { Variable(Variable::Name::B_COLLISION_U, 1), LogicalOperator::EQUAL_TO, 0.0 };
		horizontalForceAirFormula2.conditions[5] = { Variable(Variable::Name::B_COLLISION_D, 1), LogicalOperator::EQUAL_TO, 0.0 };
		horizontalForceAirFormula2.conditions[6] = { Variable(Variable::Name::B_COLLISION_R, 1), LogicalOperator::EQUAL_TO, 0.0 };
		horizontalForceAirFormula2.conditions[7] = { Variable(Variable::Name::B_COLLISION_L, 1), LogicalOperator::EQUAL_TO, 0.0 };
		horizontalForceAirFormula2.conditions[8] = { Variable(Variable::Name::B_IMPULSE_CHANGE_X, 0), LogicalOperator::EQUAL_TO, 0.0 };
		horizontalForceAirFormula2.conditions[9] = { Variable(Variable::Name::B_IMPULSE_CHANGE_Y, 0), LogicalOperator::EQUAL_TO, 0.0 };
		horizontalForceAirFormula2.conditions[10] = { Variable(Variable::Name::B_INPUT_L, 0), LogicalOperator::EQUAL_TO, 1.0 };
		horizontalForceAirFormula2.conditions[11] = { Variable(Variable::Name::B_INPUT_R, 0), LogicalOperator::EQUAL_TO, 0.0 };
		horizontalForceAirFormula2.conditions[12] = { Variable(Variable::Name::S_VELOCITY_X, 0), LogicalOperator::BIGGER_THAN, Constant::S_MAX_HORIZONTAL_NEG_VELOCITY };
		horizontalForceAirFormula2.constant = Constant::S_HORIZONTAL_FORCE_AIR;
		horizontalForceAirFormula2.parametersCount = 1;
		horizontalForceAirFormula2.parameters = new Value[1];
		horizontalForceAirFormula2.parameters[0] = Variable(Variable::Name::S_ACCELERATION_X, 0);
		horizontalForceAirFormula2.Evaluate = EvalHorizontalForceAir;
		PhysicsBehaviour::constantFormulae.push_back(horizontalForceAirFormula2);
	}

	// Horizontale Kraft am Boden (nach rechts gemessen)
	// FBoden = a / mu + g
	// wenn Cd == 1
	//		Cr == 0
	//		Cl == 0
	//		Cr1 == 0
	//		Cl1 == 0
	//		Px == 0
	//		Py == 0
	//		Ir == 1
	//		Il == 0
	//		Vx < Vmax
	//		Vx > 0
	//		Vx1 > 0
	{
		ConstantFormula horizontalForceFormula;
		horizontalForceFormula.conditionsCount = 12;
		horizontalForceFormula.conditions = new Condition[12];
		horizontalForceFormula.conditions[0] = { Variable(Variable::Name::B_COLLISION_D, 0), LogicalOperator::EQUAL_TO, 1.0 };
		horizontalForceFormula.conditions[1] = { Variable(Variable::Name::B_COLLISION_R, 0), LogicalOperator::EQUAL_TO, 0.0 };
		horizontalForceFormula.conditions[2] = { Variable(Variable::Name::B_COLLISION_L, 0), LogicalOperator::EQUAL_TO, 0.0 };
		horizontalForceFormula.conditions[3] = { Variable(Variable::Name::B_COLLISION_R, 1), LogicalOperator::EQUAL_TO, 0.0 };
		horizontalForceFormula.conditions[4] = { Variable(Variable::Name::B_COLLISION_L, 1), LogicalOperator::EQUAL_TO, 0.0 };
		horizontalForceFormula.conditions[5] = { Variable(Variable::Name::B_IMPULSE_CHANGE_X, 0), LogicalOperator::EQUAL_TO, 0.0 };
		horizontalForceFormula.conditions[6] = { Variable(Variable::Name::B_IMPULSE_CHANGE_Y, 0), LogicalOperator::EQUAL_TO, 0.0 };
		horizontalForceFormula.conditions[7] = { Variable(Variable::Name::B_INPUT_R, 0), LogicalOperator::EQUAL_TO, 1.0 };
		horizontalForceFormula.conditions[8] = { Variable(Variable::Name::B_INPUT_L, 0), LogicalOperator::EQUAL_TO, 0.0 };
		horizontalForceFormula.conditions[9] = { Variable(Variable::Name::S_VELOCITY_X, 0), LogicalOperator::SMALLER_THAN, Constant::S_MAX_HORIZONTAL_VELOCITY };
		horizontalForceFormula.conditions[10] = { Variable(Variable::Name::S_VELOCITY_X, 0), LogicalOperator::BIGGER_THAN, 0.5 };
		horizontalForceFormula.conditions[11] = { Variable(Variable::Name::S_VELOCITY_X, 1), LogicalOperator::BIGGER_THAN, 0.5 };
		//horizontalForceFormula.conditions[12] = { Variable(Variable::Name::S_VELOCITY_X, 0), LogicalOperator::SAME_SIGN_THAN, Variable(Variable::Name::S_VELOCITY_X, 1) };
		horizontalForceFormula.constant = Constant::S_HORIZONTAL_FORCE_GROUND;
		horizontalForceFormula.parametersCount = 3;
		horizontalForceFormula.parameters = new Value[3];
		horizontalForceFormula.parameters[0] = Variable(Variable::Name::S_ACCELERATION_X, 0);
		horizontalForceFormula.parameters[1] = Constant::S_GRAVITY;
		horizontalForceFormula.parameters[2] = Constant::S_PER_MATERIAL_FRICTION_COEFFICIENT;
		//horizontalForceFormula.parameters[3] = Variable(Variable::Name::S_VELOCITY_X, 1);
		//horizontalForceFormula.parameters[4] = 1.0;
		horizontalForceFormula.Evaluate = EvalHorizontalForce;
		PhysicsBehaviour::constantFormulae.push_back(horizontalForceFormula);
	}

	// Horizontale Kraft am Boden (nach links gemessen)
	// FBoden = a / mu + g
	// wenn Cd == 1
	//		Cl == 0
	//		Cr == 0
	//		Cl1 == 0
	//		Cr1 == 0
	//		Px == 0
	//		Py == 0
	//		Il == 1
	//		Ir == 0
	//		Vx > -Vmax
	//		Vx < 0
	//		Vx1 < 0
	{
		ConstantFormula horizontalForceFormula;
		horizontalForceFormula.conditionsCount = 12;
		horizontalForceFormula.conditions = new Condition[12];
		horizontalForceFormula.conditions[0] = { Variable(Variable::Name::B_COLLISION_D, 0), LogicalOperator::EQUAL_TO, 1.0 };
		horizontalForceFormula.conditions[1] = { Variable(Variable::Name::B_COLLISION_L, 0), LogicalOperator::EQUAL_TO, 0.0 };
		horizontalForceFormula.conditions[2] = { Variable(Variable::Name::B_COLLISION_R, 0), LogicalOperator::EQUAL_TO, 0.0 };
		horizontalForceFormula.conditions[3] = { Variable(Variable::Name::B_COLLISION_L, 1), LogicalOperator::EQUAL_TO, 0.0 };
		horizontalForceFormula.conditions[4] = { Variable(Variable::Name::B_COLLISION_R, 1), LogicalOperator::EQUAL_TO, 0.0 };
		horizontalForceFormula.conditions[5] = { Variable(Variable::Name::B_IMPULSE_CHANGE_X, 0), LogicalOperator::EQUAL_TO, 0.0 };
		horizontalForceFormula.conditions[6] = { Variable(Variable::Name::B_IMPULSE_CHANGE_Y, 0), LogicalOperator::EQUAL_TO, 0.0 };
		horizontalForceFormula.conditions[7] = { Variable(Variable::Name::B_INPUT_L, 0), LogicalOperator::EQUAL_TO, 1.0 };
		horizontalForceFormula.conditions[8] = { Variable(Variable::Name::B_INPUT_R, 0), LogicalOperator::EQUAL_TO, 0.0 };
		horizontalForceFormula.conditions[9] = { Variable(Variable::Name::S_VELOCITY_X, 0), LogicalOperator::BIGGER_THAN, Constant::S_MAX_HORIZONTAL_NEG_VELOCITY };
		horizontalForceFormula.conditions[10] = { Variable(Variable::Name::S_VELOCITY_X, 0), LogicalOperator::SMALLER_THAN, -0.5 };
		horizontalForceFormula.conditions[11] = { Variable(Variable::Name::S_VELOCITY_X, 1), LogicalOperator::SMALLER_THAN, -0.5 };
		//horizontalForceFormula.conditions[12] = { Variable(Variable::Name::S_VELOCITY_X, 0), LogicalOperator::SAME_SIGN_THAN, Variable(Variable::Name::S_VELOCITY_X, 1) };
		horizontalForceFormula.constant = Constant::S_HORIZONTAL_FORCE_GROUND;
		horizontalForceFormula.parametersCount = 3;
		horizontalForceFormula.parameters = new Value[3];
		horizontalForceFormula.parameters[0] = Variable(Variable::Name::S_ACCELERATION_X, 0);
		horizontalForceFormula.parameters[1] = Constant::S_GRAVITY;
		horizontalForceFormula.parameters[2] = Constant::S_PER_MATERIAL_FRICTION_COEFFICIENT;
		//horizontalForceFormula.parameters[3] = Variable(Variable::Name::S_VELOCITY_X, 1);
		//horizontalForceFormula.parameters[4] = -1.0;
		horizontalForceFormula.Evaluate = EvalHorizontalForce;
		PhysicsBehaviour::constantFormulae.push_back(horizontalForceFormula);
	}

	// Sprungimpuls
	// PSprung = Vx - g * t
	// wenn Cd1 == 1
	//		Cd == 0
	//		Cu == 0
	//		Cl == 0
	//		Cr == 0
	//		Iu == 1
	{
		ConstantFormula jumpImpulseFormula;
		jumpImpulseFormula.conditionsCount = 4;
		jumpImpulseFormula.conditions = new Condition[4];
		jumpImpulseFormula.conditions[0] = { Variable(Variable::Name::B_COLLISION_D, 1), LogicalOperator::EQUAL_TO, 1.0 };
		jumpImpulseFormula.conditions[1] = { Variable(Variable::Name::B_COLLISION_D, 0), LogicalOperator::EQUAL_TO, 0.0 };
		jumpImpulseFormula.conditions[2] = { Variable(Variable::Name::B_COLLISION_U, 0), LogicalOperator::EQUAL_TO, 0.0 };
		jumpImpulseFormula.conditions[3] = { Variable(Variable::Name::B_INPUT_U, 0), LogicalOperator::EQUAL_TO, 1.0 };
		jumpImpulseFormula.constant = Constant::S_JUMP_IMPULSE;
		jumpImpulseFormula.parametersCount = 3;
		jumpImpulseFormula.parameters = new Value[3];
		jumpImpulseFormula.parameters[0] = Variable(Variable::Name::S_VELOCITY_Y, 0);
		jumpImpulseFormula.parameters[1] = Constant::S_GRAVITY;
		jumpImpulseFormula.parameters[2] = Variable(Variable::Name::S_DELTA_TIME, 0);
		jumpImpulseFormula.Evaluate = EvalJumpImpulse;
		PhysicsBehaviour::constantFormulae.push_back(jumpImpulseFormula);
	}

	// Reibungskoeffizient in X-Richtung
	// mu = |ax / g|
	// wenn Cl == 0
	//		Cr == 0
	//		Cd == 1
	//		Px == 0
	//		Py == 0
	//		Il == 0
	//		Ir == 0
	//		Vx != 0
	{
		ConstantFormula frictionFormulaX;
		frictionFormulaX.conditionsCount = 8;
		frictionFormulaX.conditions = new Condition[8];
		frictionFormulaX.conditions[0] = { Variable(Variable::Name::B_COLLISION_L, 0), LogicalOperator::EQUAL_TO, 0.0 };
		frictionFormulaX.conditions[1] = { Variable(Variable::Name::B_COLLISION_R, 0), LogicalOperator::EQUAL_TO, 0.0 };
		frictionFormulaX.conditions[2] = { Variable(Variable::Name::B_COLLISION_D, 0), LogicalOperator::EQUAL_TO, 1.0 };
		frictionFormulaX.conditions[3] = { Variable(Variable::Name::B_IMPULSE_CHANGE_X, 0), LogicalOperator::EQUAL_TO, 0.0 };
		frictionFormulaX.conditions[4] = { Variable(Variable::Name::B_IMPULSE_CHANGE_Y, 0), LogicalOperator::EQUAL_TO, 0.0 };
		frictionFormulaX.conditions[5] = { Variable(Variable::Name::B_INPUT_L, 0), LogicalOperator::EQUAL_TO, 0.0 };
		frictionFormulaX.conditions[6] = { Variable(Variable::Name::B_INPUT_R, 0), LogicalOperator::EQUAL_TO, 0.0 };
		frictionFormulaX.conditions[7] = { Variable(Variable::Name::S_VELOCITY_X, 0), LogicalOperator::NOT_EQUAL_TO, 0.0 };
		frictionFormulaX.constant = Constant::S_PER_MATERIAL_FRICTION_COEFFICIENT;
		frictionFormulaX.parametersCount = 2;
		frictionFormulaX.parameters = new Value[2];
		frictionFormulaX.parameters[0] = Variable(Variable::Name::S_ACCELERATION_X, 0);
		frictionFormulaX.parameters[1] = Constant::S_GRAVITY;
		frictionFormulaX.Evaluate = EvalFrictionCoefficientX;
		PhysicsBehaviour::constantFormulae.push_back(frictionFormulaX);
	}

	// Impulserhaltungskoeffizient in X-Richtung
	// c = -Vx / Vx2
	// wenn Cu == 0
	//		Cd == 0
	//		Cl1 != Cr1
	//		Cu2 == 0
	//		Cd2 == 0
	//		Px == 0
	//		Px1 == 1
	//		Px2 == 0
	//		Il == 0
	//		Il1 == 0
	//		IL2 == 0
	//		Ir == 0
	//		Ir1 == 0
	//		Ir2 == 0
	//		Vx2 != 0
	{
		ConstantFormula impulseCoefficientX;
		impulseCoefficientX.conditionsCount = 15;
		impulseCoefficientX.conditions = new Condition[15];
		impulseCoefficientX.conditions[0] = { Variable(Variable::Name::B_COLLISION_U, 0), LogicalOperator::EQUAL_TO, 0.0 };
		impulseCoefficientX.conditions[1] = { Variable(Variable::Name::B_COLLISION_D, 0), LogicalOperator::EQUAL_TO, 0.0 };
		impulseCoefficientX.conditions[2] = { Variable(Variable::Name::B_COLLISION_L, 1), LogicalOperator::NOT_EQUAL_TO, Variable(Variable::Name::B_COLLISION_R, 1) };
		impulseCoefficientX.conditions[3] = { Variable(Variable::Name::B_COLLISION_U, 2), LogicalOperator::EQUAL_TO, 0.0 };
		impulseCoefficientX.conditions[4] = { Variable(Variable::Name::B_COLLISION_D, 2), LogicalOperator::EQUAL_TO, 0.0 };
		impulseCoefficientX.conditions[5] = { Variable(Variable::Name::B_IMPULSE_CHANGE_X, 0), LogicalOperator::EQUAL_TO, 0.0 };
		impulseCoefficientX.conditions[6] = { Variable(Variable::Name::B_IMPULSE_CHANGE_X, 1), LogicalOperator::EQUAL_TO, 1.0 };
		impulseCoefficientX.conditions[7] = { Variable(Variable::Name::B_IMPULSE_CHANGE_X, 2), LogicalOperator::EQUAL_TO, 0.0 };
		impulseCoefficientX.conditions[8] = { Variable(Variable::Name::B_INPUT_L, 0), LogicalOperator::EQUAL_TO, 0.0 };
		impulseCoefficientX.conditions[9] = { Variable(Variable::Name::B_INPUT_L, 1), LogicalOperator::EQUAL_TO, 0.0 };
		impulseCoefficientX.conditions[10] = { Variable(Variable::Name::B_INPUT_L, 2), LogicalOperator::EQUAL_TO, 0.0 };
		impulseCoefficientX.conditions[11] = { Variable(Variable::Name::B_INPUT_R, 0), LogicalOperator::EQUAL_TO, 0.0 };
		impulseCoefficientX.conditions[12] = { Variable(Variable::Name::B_INPUT_R, 1), LogicalOperator::EQUAL_TO, 0.0 };
		impulseCoefficientX.conditions[13] = { Variable(Variable::Name::B_INPUT_R, 2), LogicalOperator::EQUAL_TO, 0.0 };
		impulseCoefficientX.conditions[14] = { Variable(Variable::Name::S_VELOCITY_X, 2), LogicalOperator::NOT_EQUAL_TO, 0.0 };
		impulseCoefficientX.constant = Constant::S_PER_MATERIAL_IMPULSE_CONSERVATION_COEFFICIENT;
		impulseCoefficientX.parametersCount = 2;
		impulseCoefficientX.parameters = new Value[2];
		impulseCoefficientX.parameters[0] = Variable(Variable::Name::S_VELOCITY_X, 0);
		impulseCoefficientX.parameters[1] = Variable(Variable::Name::S_VELOCITY_X, 2);
		impulseCoefficientX.Evaluate = EvalImpulseCoefficientX;
		PhysicsBehaviour::constantFormulae.push_back(impulseCoefficientX);
	}

	// Impulserhaltungskoeffizient in Y-Richtung
	// c = -Vy / (Vy2 + g * t)
	// wenn Cl == 0
	//		Cr == 0
	//		Cu1 != Cd1
	//		Cl2 == 0
	//		Cr2 == 0
	//		Py == 0
	//		Py1 == 1
	//		Py2 == 0
	//		Iu == 0
	//		Iu1 == 0
	//		Iu2 == 0
	//		Vy2 != 0
	{
		ConstantFormula impulseCoefficientY;
		impulseCoefficientY.conditionsCount = 12;
		impulseCoefficientY.conditions = new Condition[12];
		impulseCoefficientY.conditions[0] = { Variable(Variable::Name::B_COLLISION_L, 0), LogicalOperator::EQUAL_TO, 0.0 };
		impulseCoefficientY.conditions[1] = { Variable(Variable::Name::B_COLLISION_R, 0), LogicalOperator::EQUAL_TO, 0.0 };
		impulseCoefficientY.conditions[2] = { Variable(Variable::Name::B_COLLISION_U, 1), LogicalOperator::NOT_EQUAL_TO, Variable(Variable::Name::B_COLLISION_D, 1) };
		impulseCoefficientY.conditions[3] = { Variable(Variable::Name::B_COLLISION_L, 2), LogicalOperator::EQUAL_TO, 0.0 };
		impulseCoefficientY.conditions[4] = { Variable(Variable::Name::B_COLLISION_R, 2), LogicalOperator::EQUAL_TO, 0.0 };
		impulseCoefficientY.conditions[5] = { Variable(Variable::Name::B_IMPULSE_CHANGE_Y, 0), LogicalOperator::EQUAL_TO, 0.0 };
		impulseCoefficientY.conditions[6] = { Variable(Variable::Name::B_IMPULSE_CHANGE_Y, 1), LogicalOperator::EQUAL_TO, 1.0 };
		impulseCoefficientY.conditions[7] = { Variable(Variable::Name::B_IMPULSE_CHANGE_Y, 2), LogicalOperator::EQUAL_TO, 0.0 };
		impulseCoefficientY.conditions[8] = { Variable(Variable::Name::B_INPUT_U, 0), LogicalOperator::EQUAL_TO, 0.0 };
		impulseCoefficientY.conditions[9] = { Variable(Variable::Name::B_INPUT_U, 1), LogicalOperator::EQUAL_TO, 0.0 };
		impulseCoefficientY.conditions[10] = { Variable(Variable::Name::B_INPUT_U, 2), LogicalOperator::EQUAL_TO, 0.0 };
		impulseCoefficientY.conditions[11] = { Variable(Variable::Name::S_VELOCITY_Y, 2), LogicalOperator::NOT_EQUAL_TO, 0.0 };
		impulseCoefficientY.constant = Constant::S_PER_MATERIAL_IMPULSE_CONSERVATION_COEFFICIENT;
		impulseCoefficientY.parametersCount = 4;
		impulseCoefficientY.parameters = new Value[4];
		impulseCoefficientY.parameters[0] = Variable(Variable::Name::S_VELOCITY_Y, 0);
		impulseCoefficientY.parameters[1] = Variable(Variable::Name::S_VELOCITY_Y, 2);
		impulseCoefficientY.parameters[2] = Constant::S_GRAVITY;
		impulseCoefficientY.parameters[3] = Variable(Variable::Name::S_DELTA_TIME, 0);
		impulseCoefficientY.Evaluate = EvalImpulseCoefficientY;
		PhysicsBehaviour::constantFormulae.push_back(impulseCoefficientY);
	}

	Log::Write("physics behaviour database creation terminated");
}

void PhysicsBehaviour::Destroy() {
	if(!PhysicsBehaviour::isCreated) {
		// Datenbank bereits gel�scht
		throw string("Attempting to destroy already destroyed physics behaviour database singleton");
	}
	PhysicsBehaviour::isCreated = true;

	Log::Write("begin physics behaviour database destruction");

	for(uint32_t i = 0; i < PhysicsBehaviour::inputReactions.size(); i++) {
		delete PhysicsBehaviour::inputReactions[i].inputConditions;
		delete PhysicsBehaviour::inputReactions[i].outputConditions;
	}
	PhysicsBehaviour::inputReactions.clear();

	for(uint32_t i = 0; i < PhysicsBehaviour::constantFormulae.size(); i++) {
		delete PhysicsBehaviour::constantFormulae[i].conditions;
		delete PhysicsBehaviour::constantFormulae[i].parameters;
	}
	PhysicsBehaviour::constantFormulae.clear();

	Log::Write("physics behaviour database destruction terminated");
}

bool PhysicsBehaviour::isCreated = false;
vector<PhysicsBehaviour::InputReaction> PhysicsBehaviour::inputReactions;
vector<PhysicsBehaviour::ConstantFormula> PhysicsBehaviour::constantFormulae;

}
#include <simulation/physicsEngine.h>

#include <system/window.h>
#include <simulation/vector2.h>
#include <simulation/world.h>
#include <simulation/settings.h>
#include <simulation/character.h>
#include <simulation/render.h>
#include <simulation/blockRegistry.h>

using namespace System;

namespace Sim {

void PhysicsEngine::Update(double deltaTime) {
	if(!this -> running) {
		// Setze deltaTime auf 0, so dass nichts ver�ndert wird
		// Anstelle von Update einfach auszulassen, werden die Berechnungen fortgesetzt, damit die Debug-Anzeige korrekte Daten anzeigen
		deltaTime = 0.0;
	}

	// Zwischenspeicher
	double mass = this -> settings -> GetCharacterMass();
	Vector2 gravityAcceleration(0.0, -this -> settings -> GetGravityAcceleration());
	double maxWalkingSpeed = numeric_limits<double>::infinity();// this -> settings -> GetMaxWalkingSpeed();
	double horizontalForce = this -> settings -> GetHorizontalForce();
	double airHorizontalForce = this -> settings -> GetAirHorizontalForce();
	double jumpImpulse = this -> settings -> GetJumpImpulse();

	Vector2 oldVelocity = this -> character -> GetVelocity();

	this -> debugGravityAcceleration = gravityAcceleration;

	Vector2 innerForce = Vector2::Zero();
	bool wentRight = false;
	bool wentLeft = false;

	// Reagiere auf Benutzereingaben
	{
		Vector2 position = this -> character -> GetPosition();
		if(deltaTime > 0.0) {
			if(this -> character -> GetCollision(Character::Side::BOTTOM)) {
				BlockData b1 = BlockRegistry::GetBlock(this -> world -> GetBlock((int32_t)floor(position.GetX()), (int32_t)(position.GetY() - 1.0)));
				BlockData b2 = BlockRegistry::GetBlock(this -> world -> GetBlock((int32_t)floor(position.GetX() + 1.0), (int32_t)(position.GetY() - 1.0)));
				double frictionCoefficent = max(b1.frictionCoefficent, b2.frictionCoefficent);

				// Spieler hat Kontakt zum Boden
				if(this -> character -> GetInput(Character::Input::WALK_RIGHT)) {
					if(oldVelocity.GetX() <= maxWalkingSpeed) {
						innerForce += Vector2(horizontalForce * frictionCoefficent, 0.0);
						wentRight = true;
					} else if(oldVelocity.GetX() == maxWalkingSpeed) {
						wentRight = true;
					}
				}
				if(this -> character -> GetInput(Character::Input::WALK_LEFT)) {
					if(oldVelocity.GetX() >= -maxWalkingSpeed) {
						innerForce += Vector2(-horizontalForce * frictionCoefficent, 0.0);
						wentLeft = true;
					} else if(oldVelocity.GetX() == -maxWalkingSpeed) {
						wentLeft = true;
					}
				}
				if(this -> character -> GetInput(Character::Input::JUMP) && !this -> character -> GetImpulseChange(Character::Axis::Y_AXIS)) {
					// Sprungimpuls wird sofort angewendet
					this -> character -> SetVelocity(this -> character -> GetVelocity() + Vector2(0.0, jumpImpulse / mass));
				}
			} else {
				// Spieler ist in der Luft
				if(this -> character -> GetInput(Character::Input::WALK_RIGHT)) {
					if(oldVelocity.GetX() <= maxWalkingSpeed) {
						innerForce += Vector2(airHorizontalForce, 0.0);
						wentRight = true;
					} else if(oldVelocity.GetX() == maxWalkingSpeed) {
						wentRight = true;
					}
				}
				if(this -> character -> GetInput(Character::Input::WALK_LEFT)) {
					if(oldVelocity.GetX() >= -maxWalkingSpeed) {
						innerForce += Vector2(-airHorizontalForce, 0.0);
						wentLeft = true;
					} else if(oldVelocity.GetX() == -maxWalkingSpeed) {
						wentLeft = true;
					}
				}
			}
		}
	} // Benutzereingaben

	// Setze Impuls�nderungen zur�ck
	this -> character -> SetImpulseChange(Character::Axis::X_AXIS, false);
	this -> character -> SetImpulseChange(Character::Axis::Y_AXIS, false);

	// Integriere prim�re Kr�fte
	Vector2 internalAcceleration = innerForce / mass;
	Vector2 primaryAcceleration = internalAcceleration + gravityAcceleration;
	this -> debugInternalAcceleration = innerForce / mass;
	this -> debugPrimaryAcceleration = primaryAcceleration;

	// Integriere prim�re Beschleunigung
	// Hierbei wird beachtet, dass nicht in Richtung von kollidierenden Kanten beschleunigt werden kann
	{
		Vector2 velocityChange;

		if(primaryAcceleration.GetX() > 0 && !this -> character -> GetCollision(Character::Side::RIGHT)) {
			velocityChange.SetX(primaryAcceleration.GetX() * deltaTime);
		} else if(primaryAcceleration.GetX() < 0 && !this -> character -> GetCollision(Character::Side::LEFT)) {
			velocityChange.SetX(primaryAcceleration.GetX() * deltaTime);
		}
		if(primaryAcceleration.GetY() > 0 && !this -> character -> GetCollision(Character::Side::TOP)) {
			velocityChange.SetY(primaryAcceleration.GetY() * deltaTime);
		} else if(primaryAcceleration.GetY() < 0 && !this -> character -> GetCollision(Character::Side::BOTTOM)) {
			velocityChange.SetY(primaryAcceleration.GetY() * deltaTime);
		}
		this -> character -> SetVelocity(this -> character -> GetVelocity() + velocityChange);
	} // Integriere prim�re Beschleunigungen

	// Reibung
	{
		if(deltaTime > 0.0) {
			Vector2 velocityChange = Vector2::Zero();
			Vector2 position = this -> character -> GetPosition();
			if(this -> character -> GetCollision(Character::Side::TOP) || this -> character -> GetCollision(Character::Side::BOTTOM)) {
				// Kollision oben oder unten
				double velocityX = this -> character -> GetVelocity().GetX();
				double normalForce = innerForce.GetY() + (gravityAcceleration * mass).GetY();
				double frictionVelocityChangeX = 0.0;
				
				if((this -> character -> GetCollision(Character::Side::TOP) && normalForce > 0.0) || (this -> character -> GetCollision(Character::Side::BOTTOM) && normalForce < 0.0)) {
					// Suche h�chsten Reibungskoeffizienten
					double frictionCoefficent = 0.0;
					if(normalForce > 0.0) {
						BlockData b1 = BlockRegistry::GetBlock(this -> world -> GetBlock((int32_t)floor(position.GetX()), (int32_t)(round(position.GetY() + 1.0))));
						BlockData b2 = BlockRegistry::GetBlock(this -> world -> GetBlock((int32_t)floor(position.GetX() + 1.0), (int32_t)round((position.GetY() + 1.0))));
						frictionCoefficent = max(frictionCoefficent, max(b1.frictionCoefficent, b2.frictionCoefficent));
					} else if(normalForce < 0.0) {
						BlockData b1 = BlockRegistry::GetBlock(this -> world -> GetBlock((int32_t)floor(position.GetX()), (int32_t)(round(position.GetY() - 1.0))));
						BlockData b2 = BlockRegistry::GetBlock(this -> world -> GetBlock((int32_t)floor(position.GetX() + 1.0), (int32_t)(round(position.GetY() - 1.0))));
						frictionCoefficent = max(frictionCoefficent, max(b1.frictionCoefficent, b2.frictionCoefficent));
					}
					frictionVelocityChangeX = (velocityX / abs(velocityX)) * abs(normalForce) * frictionCoefficent / mass * deltaTime;

					if(frictionVelocityChangeX != 0.0) {
						if((velocityX > 0.0 && velocityX - frictionVelocityChangeX > 0.0 && frictionVelocityChangeX > 0.0) || (velocityX < 0.0 && velocityX - frictionVelocityChangeX < 0.0 && frictionVelocityChangeX < 0.0)) {
							velocityChange.SetX(-frictionVelocityChangeX);
						} else {
							velocityChange.SetX(-velocityX);
						}
					}
				}
			}
			if(this -> character -> GetCollision(Character::Side::RIGHT) || this -> character -> GetCollision(Character::Side::LEFT)) {
				// Kollision rechts oder links
				double velocityY = this -> character -> GetVelocity().GetY();
				double normalForce = innerForce.GetX() + (gravityAcceleration * mass).GetX(); // Gravitation k�nnte hier vernachl�ssigt werden
				double frictionVelocityChangeY = 0.0;
				if((this -> character -> GetCollision(Character::Side::RIGHT) && normalForce > 0.0) || (this -> character -> GetCollision(Character::Side::LEFT) && normalForce < 0.0)) {
					// Suche h�chsten Reibungskoeffizienten
					double frictionCoefficent = 0.0;
					if(normalForce > 0.0) {
						BlockData b1 = BlockRegistry::GetBlock(this -> world -> GetBlock((int32_t)(round(position.GetX() + 1.0)), (int32_t)floor(position.GetY())));
						BlockData b2 = BlockRegistry::GetBlock(this -> world -> GetBlock((int32_t)(round(position.GetX() + 1.0)), (int32_t)floor(position.GetY() + 1.0)));
						frictionCoefficent = max(frictionCoefficent, max(b1.frictionCoefficent, b2.frictionCoefficent));
					} else if(normalForce < 0.0) {
						BlockData b1 = BlockRegistry::GetBlock(this -> world -> GetBlock((int32_t)(position.GetX() - 1.0), (int32_t)floor(position.GetY())));
						BlockData b2 = BlockRegistry::GetBlock(this -> world -> GetBlock((int32_t)(position.GetX() - 1.0), (int32_t)floor(position.GetY() + 1.0)));
						frictionCoefficent = max(frictionCoefficent, max(b1.frictionCoefficent, b2.frictionCoefficent));
					}
					frictionVelocityChangeY = (velocityY / abs(velocityY)) * abs(normalForce) * frictionCoefficent / mass * deltaTime;

					if(frictionVelocityChangeY != 0.0) {
						if((velocityY > 0.0 && velocityY - frictionVelocityChangeY > 0.0 && frictionVelocityChangeY > 0.0) || (velocityY < 0.0 && velocityY - frictionVelocityChangeY < 0.0 && frictionVelocityChangeY < 0.0)) {
							velocityChange.SetY(-frictionVelocityChangeY);
						} else {
							velocityChange.SetY(-velocityY);
						}
					}
				}
			}
			this -> debugFrictionAcceleration = (velocityChange.LengthSquared() > 0.0 && deltaTime > 0.0) ? velocityChange / deltaTime : Vector2::Zero();
			this -> character -> SetVelocity(this -> character -> GetVelocity() + velocityChange);
		}
	} // Reibung

	// Setze horizontale Komponente auf maximale Laufgeschwindigkeit falls n�tig
	{
		Vector2 velocity = this -> character -> GetVelocity();
		if(wentRight && velocity.GetX() >= maxWalkingSpeed) {
			velocity.SetX(maxWalkingSpeed);
		}
		if(wentLeft && velocity.GetX() <= -maxWalkingSpeed) {
			velocity.SetX(-maxWalkingSpeed);
		}
		this -> character -> SetVelocity(velocity);
	} // Passe horizontale Komponente an

	this -> debugVelocity = this -> character -> GetVelocity();

	// Integriere Geschwindigkeit und l�se Kollisionen
	{
		if(this -> character -> GetVelocity().LengthSquared() > 0.0) {
			// Erstelle Strahlen f�r jede Ecke (ri: vertices[i] + t*v)
			Vector2 velocity = this -> character -> GetVelocity();
			Vector2 position = this -> character -> GetPosition();
			// Um sicherzugehen, dass keine Bl�cke �bersprungen werden, wird f�r t += 1 die Distanz 1 zur�ckgelegt
			Vector2 v = this -> character -> GetVelocity().Normalize();
			double vX = v.GetX();
			double vY = v.GetY();
			Vector2 vertices[4];
			vertices[0] = position + Vector2(-0.5, -0.5); // unten links
			vertices[1] = position + Vector2(-0.5, 0.5); // oben links
			vertices[2] = position + Vector2(0.5, 0.5); // oben rechts
			vertices[3] = position + Vector2(0.5, -0.5); // unten rechts
			double maxT = deltaTime * velocity.Length();
			double t = 0.0;
			double vAngle = (v.GetY() > 0.0) ? v.Angle(Vector2(0, 1)) : v.Angle(Vector2(0, -1)); // Die F�lle vAngle = 90� und vAngle = 0� werden sp�ter herausgefiltert

			// Position, nachdem die Kollision aufgel�st wurde
			Vector2 newPosition;
			// Seite, auf der die Kollision statt findet
			Character::Side collisionSide;
			// Zeigt an, ob �berhaupt eine Kollision stattfand
			bool collided = false;

			// Indizes spezieller Ecken
			// leadingVertex ist in Richtung der Bewegung die vordeste Ecke
			// verticalTrailingVertex ist �ber oder unter leadingVertex
			// horizontalTrailingVertex ist rechts oder links von leadingVertex
			// Falls vAngle = 90� oder vAngle = 0�, werden diese Indizes nicht verwendet und sind undefiniert
			uint32_t leadingVertex;
			uint32_t verticalTrailingVertex;
			uint32_t horizontalTrailingVertex;
			if(vX < 0.0) {
				if(vY < 0.0) {
					leadingVertex = 0;
				} else {
					leadingVertex = 1;
				}
			} else {
				if(vY < 0.0) {
					leadingVertex = 3;
				} else {
					leadingVertex = 2;
				}
			}
			switch(leadingVertex) {
			case 0:
				verticalTrailingVertex = 1;
				horizontalTrailingVertex = 3;
				break;
			case 1:
				verticalTrailingVertex = 0;
				horizontalTrailingVertex = 2;
				break;
			case 2:
				verticalTrailingVertex = 3;
				horizontalTrailingVertex = 1;
				break;
			case 3:
				verticalTrailingVertex = 2;
				horizontalTrailingVertex = 0;
				break;
			}

			// Laufe die Strahlen Schrittweise ab
			while(true) {
				// Erh�he t oder setze es auf maxT
				if(t < maxT - PhysicsEngine::COLLISION_RAY_TRACE_T) {
					t += PhysicsEngine::COLLISION_RAY_TRACE_T;
				} else {
					t = maxT;
				}
				// Berechne die Positionen von allen Ecken im aktuellen Schritt
				Vector2 newPositions[4];
				for(uint32_t i = 0; i < 4; i++) {
					newPositions[i] = vertices[i] + v * t;
				}
				// Enth�llt sp�ter die l�nste (und somit st�rkste) Kollision. Falls keine Kollision stattfindet, wird dieser Wert auf -1.0 belassen
				double collisionDistance = -1.0;
				// Pr�fe alle Ecken einzeln
				for(uint32_t i = 0; i < 4; i++) {
					double nPX = newPositions[i].GetX();
					double nPY = newPositions[i].GetY();
					// Position des Blockes, in dem sich die Ecke befindet
					double bPX;
					double bPY;
					if(i == 0 || i == 1) {
						bPX = round(nPX + PhysicsEngine::EPSILON);
					} else {
						bPX = round(nPX - PhysicsEngine::EPSILON);
					}
					if(i == 0 || i == 3) {
						bPY = round(nPY + PhysicsEngine::EPSILON);
					} else {
						bPY = round(nPY - PhysicsEngine::EPSILON);
					}

					// Pr�fe, ob Block vorhanden ist (Werte ausserhalb der Welt werden automatisch abgefangen)
					if(this -> world -> GetBlock((int32_t)bPX, (int32_t)bPY) != 0) {
						// Berechne Weg, der zu viel zur�ckgelegt worden ist mithilfe der Kollisionsgleichungen
						// Berechne deltaX und deltaY
						double deltaX;
						double deltaY;
						if(vX > 0.0) {
							deltaX = nPX - bPX + 0.5;
						} else if(vX < 0.0) {
							deltaX = bPX + 0.5 - nPX;
						}
						if(vY > 0.0) {
							deltaY = nPY - bPY + 0.5;
						} else if(vY < 0.0) {
							deltaY = bPY + 0.5 - nPY;
						}
						// Verhindere bei den Trivialf�llen vAngle = 90� und vAngle = 0� eine Nulldivision bzw. eine Multiplikation mit Unendlich
						if(vX < std::numeric_limits<double>::epsilon() && vX > -std::numeric_limits<double>::epsilon()) { // v.GetX() == 0
							collisionDistance = deltaY;
							if(vY < 0.0) {
								collisionSide = Character::Side::BOTTOM;
							} else {
								collisionSide = Character::Side::TOP;
							}
							continue;
						} else if(vY < std::numeric_limits<double>::epsilon() && vY > -std::numeric_limits<double>::epsilon()) { // v.GetY() == 0
							collisionDistance = deltaX;
							if(vX < 0.0) {
								collisionSide = Character::Side::LEFT;
							} else {
								collisionSide = Character::Side::RIGHT;
							}
							continue;
						}

						double tanVAngle = tan(vAngle);
						double l1 = sqrt(deltaY * deltaY + deltaY * deltaY * tanVAngle * tanVAngle);
						double l2 = sqrt(deltaX * deltaX + (deltaX * deltaX) / (tanVAngle * tanVAngle));

						// Verwende l1 oder l2 je nachdem, ob ein weiterer Block involviert ist
						if(i == leadingVertex) {
							if(collisionDistance < min(l1, l2)) {
								collisionDistance = min(l1, l2);
								if(l1 > l2) {
									if(vX < 0.0) {
										collisionSide = Character::Side::LEFT;
									} else {
										collisionSide = Character::Side::RIGHT;
									}
								} else {
									if(vY < 0.0) {
										collisionSide = Character::Side::BOTTOM;
									} else {
										collisionSide = Character::Side::TOP;
									}
								}
							}
						} else if(i == verticalTrailingVertex) {
							if(collisionDistance < l2) {
								collisionDistance = l2;
								if(vX < 0.0) {
									collisionSide = Character::Side::LEFT;
								} else {
									collisionSide = Character::Side::RIGHT;
								}
							}
						} else if(i == horizontalTrailingVertex) {
							if(collisionDistance < l1) {
								collisionDistance = l1;
								if(vY < 0.0) {
									collisionSide = Character::Side::BOTTOM;
								} else {
									collisionSide = Character::Side::TOP;
								}
							}
						}
					}
				}

				if(collisionDistance > 0.0) {
					// Setze neue Position falls Kollision vorhanden ist
					double tDiff = collisionDistance / velocity.Length(); // Zeit, die man zur�ckspulen muss, um die Kollision aufzul�sen
					double tFinal = t - tDiff;
					if(tFinal < 0.0) {
						// Bisher ungel�ster Bug
						// Verwende letzte Position und runde
						newPosition = position;
						if(collisionSide == Character::Side::TOP || collisionSide == Character::Side::BOTTOM) {
							newPosition.SetY(round(newPosition.GetY()));
						} else {
							newPosition.SetX(round(newPosition.GetX()));
						}
					} else {
						newPosition = position + v * tFinal;
						// Runde Koordinate Senkrecht zur Kollision, so dass der Spieler den Block perfekt ber�hrt
						if(collisionSide == Character::Side::TOP || collisionSide == Character::Side::BOTTOM) {
							newPosition.SetY(round(newPosition.GetY()));
						} else {
							newPosition.SetX(round(newPosition.GetX()));
						}
					}
					collided = true;
					break;
				} else {
					// Setze neue Position normal
					newPosition = position + v * t;
					this -> character -> SetPosition(newPosition);
					if(t == maxT) {
						break;
					}
					t += PhysicsEngine::COLLISION_RAY_TRACE_T;
				}
			} // while(true)

			// Setze neue Position
			this -> character -> SetPosition(newPosition);
			if(collided) {
				// Setze Geschwindigkeitskomponente vertikal zur Kollision je nach Impulserhaltung
				Vector2 newVelocity = this -> character -> GetVelocity();
				Vector2 finalVelocity;
				BlockData b1;
				BlockData b2;
				// Diese Bl�cke blockieren m�glicherweise
				uint32_t _b1;
				uint32_t _b2;
				switch(collisionSide) {
				case Character::Side::TOP:
				{
					// Oben
					b1 = BlockRegistry::GetBlock(this -> world -> GetBlock((int32_t)floor(newPosition.GetX()), (int32_t)round(newPosition.GetY() + 1.0)));
					b2 = BlockRegistry::GetBlock(this -> world -> GetBlock((int32_t)ceil(newPosition.GetX()), (int32_t)round(newPosition.GetY() + 1.0)));
					_b1 = this -> world -> GetBlock((int32_t)floor(newPosition.GetX()), (int32_t)round(newPosition.GetY() - 1.0));
					_b2 = this -> world -> GetBlock((int32_t)ceil(newPosition.GetX()), (int32_t)round(newPosition.GetY() - 1.0));
					double yComponent = -max(b1.conservedImpulse, b2.conservedImpulse) * newVelocity.GetY();
					if(abs(yComponent) < PhysicsEngine::MIN_BOUNCE_VELOCITY || _b1 != 0 || _b2 != 0) {
						yComponent = 0.0;
					}
					finalVelocity = Vector2(newVelocity.GetX(), yComponent);
					this -> character -> SetImpulseChange(Character::Axis::Y_AXIS, true);
					break;
				}
				case Character::Side::BOTTOM:
				{
					// Unten
					b1 = BlockRegistry::GetBlock(this -> world -> GetBlock((int32_t)floor(newPosition.GetX()), (int32_t)round(newPosition.GetY() - 1.0)));
					b2 = BlockRegistry::GetBlock(this -> world -> GetBlock((int32_t)ceil(newPosition.GetX()), (int32_t)round(newPosition.GetY() - 1.0)));
					_b1 = this -> world -> GetBlock((int32_t)floor(newPosition.GetX()), (int32_t)round(newPosition.GetY() + 1.0));
					_b2 = this -> world -> GetBlock((int32_t)ceil(newPosition.GetX()), (int32_t)round(newPosition.GetY() + 1.0));
					double yComponent = -max(b1.conservedImpulse, b2.conservedImpulse) * newVelocity.GetY();
					if(abs(yComponent) < PhysicsEngine::MIN_BOUNCE_VELOCITY || _b1 != 0 || _b2 != 0) {
						yComponent = 0.0;
					}
					finalVelocity = Vector2(newVelocity.GetX(), yComponent);
					this -> character -> SetImpulseChange(Character::Axis::Y_AXIS, true);
					break;
				}
				case Character::Side::RIGHT:
				{
					// Rechts
					b1 = BlockRegistry::GetBlock(this -> world -> GetBlock((int32_t)round(newPosition.GetX() + 1.0), (int32_t)floor(newPosition.GetY())));
					b2 = BlockRegistry::GetBlock(this -> world -> GetBlock((int32_t)round(newPosition.GetX() + 1.0), (int32_t)ceil(newPosition.GetY())));
					_b1 = this -> world -> GetBlock((int32_t)round(newPosition.GetX() - 1.0), (int32_t)floor(newPosition.GetY()));
					_b2 = this -> world -> GetBlock((int32_t)round(newPosition.GetX() - 1.0), (int32_t)ceil(newPosition.GetY()));
					double xComponent = -max(b1.conservedImpulse, b2.conservedImpulse) * newVelocity.GetX();
					if(abs(xComponent) < PhysicsEngine::MIN_BOUNCE_VELOCITY || _b1 != 0 || _b2 != 0) {
						xComponent = 0.0;
					}
					finalVelocity = Vector2(xComponent, newVelocity.GetY());
					this -> character -> SetImpulseChange(Character::Axis::X_AXIS, true);
					break;
				}
				case Character::Side::LEFT:
				{
					// Links
					b1 = BlockRegistry::GetBlock(this -> world -> GetBlock((int32_t)round(newPosition.GetX() - 1.0), (int32_t)floor(newPosition.GetY())));
					b2 = BlockRegistry::GetBlock(this -> world -> GetBlock((int32_t)round(newPosition.GetX() - 1.0), (int32_t)ceil(newPosition.GetY())));
					_b1 = this -> world -> GetBlock((int32_t)round(newPosition.GetX() + 1.0), (int32_t)floor(newPosition.GetY()));
					_b2 = this -> world -> GetBlock((int32_t)round(newPosition.GetX() + 1.0), (int32_t)ceil(newPosition.GetY()));
					double xComponent = -max(b1.conservedImpulse, b2.conservedImpulse) * newVelocity.GetX();
					if(abs(xComponent) < PhysicsEngine::MIN_BOUNCE_VELOCITY || _b1 != 0 || _b2 != 0) {
						xComponent = 0.0;
					}
					finalVelocity = Vector2(xComponent, newVelocity.GetY());
					this -> character -> SetImpulseChange(Character::Axis::X_AXIS, true);
					break;
				}
				}
				this -> character -> SetVelocity(finalVelocity);
			}
		}
	} // Integriere Geschwindigkeit und l�se Kollisionen

	if(deltaTime > 0.0) {
		this -> character -> SetAcceleration((this -> character -> GetVelocity() - oldVelocity) / deltaTime);
	} else {
		this -> character -> SetAcceleration(Vector2::Zero());
	}

	this -> debugPosition = this -> character -> GetPosition();
}

void PhysicsEngine::RecognizeCollisions() {
	double pX = this -> character -> GetPosition().GetX();
	double pY = this -> character -> GetPosition().GetY();
	this -> character -> SetCollision(Character::Side::TOP, false);
	this -> character -> SetCollision(Character::Side::BOTTOM, false);
	this -> character -> SetCollision(Character::Side::RIGHT, false);
	this -> character -> SetCollision(Character::Side::LEFT, false);
	if(pX == floor(pX)) { // Kollisionen links oder rechts m�glich
		if(this -> world -> GetBlock((int32_t)(pX - 1.0), (int32_t)floor(pY + PhysicsEngine::EPSILON)) != 0 || this -> world -> GetBlock((int32_t)(pX - 1.0), (int32_t)floor(pY + 1.0 - PhysicsEngine::EPSILON)) != 0) {
			this -> character -> SetCollision(Character::Side::LEFT, true);
		}
		if(this -> world -> GetBlock((int32_t)(pX + 1.0), (int32_t)floor(pY + PhysicsEngine::EPSILON)) != 0 || this -> world -> GetBlock((int32_t)(pX + 1.0), (int32_t)floor(pY + 1.0 - PhysicsEngine::EPSILON)) != 0) {
			this -> character -> SetCollision(Character::Side::RIGHT, true);
		}
	}
	if(pY == floor(pY)) { // Kollision oben oder unten m�glich
		if(this -> world -> GetBlock((int32_t)floor(pX + PhysicsEngine::EPSILON), (int32_t)(pY - 1.0)) != 0 || this -> world -> GetBlock((int32_t)floor(pX + 1.0 - PhysicsEngine::EPSILON), (int32_t)(pY - 1.0)) != 0) {
			this -> character -> SetCollision(Character::Side::BOTTOM, true);
		}
		if(this -> world -> GetBlock((int32_t)floor(pX + PhysicsEngine::EPSILON), (int32_t)(pY + 1.0)) != 0 || this -> world -> GetBlock((int32_t)floor(pX + 1.0 - PhysicsEngine::EPSILON), (int32_t)(pY + 1.0)) != 0) {
			this -> character -> SetCollision(Character::Side::TOP, true);
		}
	}
}

void PhysicsEngine::DrawDebugInformation() {
	// Zeichne Vektoren

	Graphics::Line vectorLines[5];
	Graphics::Circle vectorHeads[5];
	uint32_t centerX = Window::GetWidth() / 2;
	uint32_t centerY = Window::GetHeight() / 2;

	this -> DrawVector(&vectorLines[0], &vectorHeads[0], centerX, centerY, this -> debugVelocity, 1.0f, 1.0f, 1.0f);

	this -> DrawVector(&vectorLines[1], &vectorHeads[1], centerX, centerY, this -> debugGravityAcceleration, 1.0f, 1.0f, 0.0f);
	this -> DrawVector(&vectorLines[2], &vectorHeads[2], centerX, centerY, this -> debugInternalAcceleration, 1.0f, 0.0f, 0.0f);
	this -> DrawVector(&vectorLines[3], &vectorHeads[3], centerX, centerY, this -> debugPrimaryAcceleration, 0.0f, 0.8f, 0.0f);
	this -> DrawVector(&vectorLines[4], &vectorHeads[4], centerX, centerY, this -> debugFrictionAcceleration, 0.0, 1.0, 1.0);

	Graphics::TexturedSquare inputs[3];
	if(this -> character -> GetInput(Character::Input::JUMP)) {
		inputs[0] = { 58, 300, 48, 1 };
	} else {
		inputs[0] = { 58, 300, 48, 4 };
	}
	if(this -> character -> GetInput(Character::Input::WALK_LEFT)) {
		inputs[1] = { 10, 252, 48, 2 };
	} else {
		inputs[1] = { 10, 252, 48, 5 };
	}
	if(this -> character -> GetInput(Character::Input::WALK_RIGHT)) {
		inputs[2] = { 106, 252, 48, 3 };
	} else {
		inputs[2] = { 106, 252, 48, 6 };
	}
	Graphics::DrawTexturedSquares(inputs, 3);
	Graphics::FlushTexturedSquares();

	wstringstream ss;

	ss.str(L"");
	ss << L"X-Position    : " << this -> debugPosition.GetX() << flush;
	Graphics::AddTextToQueue(ss.str(), 10, 140, 1.0f, 1.0f, 1.0f);

	ss.str(L"");
	ss << L"Y-Position    : " << this -> debugPosition.GetY() << flush;
	Graphics::AddTextToQueue(ss.str(), 10, 120, 1.0f, 1.0f, 1.0f);

	ss.str(L"");
	ss << L"a Gravitation : " << this -> debugGravityAcceleration.Length() << L" m/s�" << flush;
	Graphics::AddTextToQueue(ss.str(), 10, 100, 1.0f, 1.0f, 0.0f);

	ss.str(L"");
	ss << L"a Eigen       : " << this -> debugInternalAcceleration.Length() << L" m/s�" << flush;
	Graphics::AddTextToQueue(ss.str(), 10, 80, 1.0f, 0.0f, 0.0f);

	ss.str(L"");
	ss << L"a Prim�r      : " << this -> debugPrimaryAcceleration.Length() << L" m/s�" << flush;
	Graphics::AddTextToQueue(ss.str(), 10, 60, 0.0f, 0.8f, 0.0f);

	ss.str(L"");
	ss << L"a Reibung     : " << this -> debugFrictionAcceleration.Length() << L" m/s�" << flush;
	Graphics::AddTextToQueue(ss.str(), 10, 40, 0.0f, 1.0f, 1.0f);

	ss.str(L"");
	ss << L"v             : " << this -> debugVelocity.Length() << L" m/s" << flush;
	Graphics::AddTextToQueue(ss.str(), 10, 20, 1.0f, 1.0f, 1.0f);

	Graphics::DrawLines(vectorLines, 5);
	Graphics::FillCircles(vectorHeads, 5);
	Graphics::FlushLines();
	Graphics::FlushFilledCircles();
}

void PhysicsEngine::DrawVector(Graphics::Line* line, Graphics::Circle* circle, int32_t offsetX, int32_t offsetY, Vector2 const& vector, float red, float green, float blue) {
	int32_t x = (int32_t)(vector.GetX() * PhysicsEngine::DEBUG_VECTOR_UNIT_LENGTH);
	int32_t y = (int32_t)(vector.GetY() * PhysicsEngine::DEBUG_VECTOR_UNIT_LENGTH);
	*line = { offsetX, offsetY, offsetX + x, offsetY + y, red, green, blue };
	*circle = { offsetX + x, offsetY + y, 4, 0, 360, red, green, blue };
}

const double PhysicsEngine::COLLISION_RAY_TRACE_T = 1.0;
const double PhysicsEngine::MIN_BOUNCE_VELOCITY = 0.1;
const double PhysicsEngine::EPSILON = 0.000000001;

}
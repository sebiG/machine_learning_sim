#pragma once

#include <stdafx.h>

#include <system/errors.h>

namespace Sim {

// Datenstruktur f�r Eigenschaften von Bl�cken
struct BlockData {
	wstring name;
	double frictionCoefficent;
	double conservedImpulse; // 0.0 - 1.0
};

// Klasse, um Eigenschaften von verchiedenen Bl�cken zu verwalten
// Singleton
class BlockRegistry {
public:
	BlockRegistry() = delete; // Verhindere Konstruktion
	~BlockRegistry() = default;

	// Gibt Status der Programmklasse zur�ck
	inline static bool IsCreated() {
		return BlockRegistry::isCreated;
	}
	// Erstellt Blockregister und lade Bl�cke von Konfigurationsdatei
	static void Create();
	// Zerst�rt Blockregister
	static void Destroy();

	const static string BLOCK_FILE;
	const static string CURRENT_BLOCK_FILE_VERSION;

	// Gibt Anzahl registrierte Bl�cke zur�ck
	static inline uint32_t GetBlockCount() {
		return (uint32_t)BlockRegistry::blockData.size();
	}
	// Gibt Block zur�ck
	static inline BlockData const& GetBlock(uint32_t id) {
		if(id < BlockRegistry::blockData.size()) {
			return BlockRegistry::blockData[id];
		} else {
			throw System::Error("Error occured in BlockRegistry::GetBlock, couldn't fetch BlockData for nonexistent block");
		}
	}

private:
	static bool isCreated;

	// Den Bl�cken werden Identifikationsnummern zugeordnet, in der Reihenfolge, in der sie hinzugef�gt werden
	static vector<BlockData> blockData;
};

}
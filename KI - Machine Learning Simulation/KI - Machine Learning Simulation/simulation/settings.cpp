#include <simulation/settings.h>

#include <system/log.h>

using namespace System;

namespace Sim {

Settings::Settings() {
	// Standardwerte setzen
	this -> characterMass = 1.0;
	this -> gravityAcceleration = 9.81;
	this -> maxWalkingSpeed = 8.0;
	this -> horizontalForce = 30.0;
	this -> airHorizontalForce = 4.0;
	this -> jumpImpulse = 7.0;
}

void Settings::Load(string filename) {
	Log::Write("begin loading settings from file " + filename);

	ifstream is(filename, ios::binary);
	if(!is.is_open()) {
		throw Error("Error occured in Settings::Load, file not found\nFilename: " + filename);
	}

	string version;
	while(is.peek() != '\n') {
		version.push_back(is.get());
	}
	is.get();

	Log::Write("settings file version checked");

	if(version != Settings::CURRENT_SAVE_FILE_VERSION) {
		throw Error("Error occured in Settings::Load, file version is incompatible\nFile Version: " + version + " Required Version: " + Settings::CURRENT_SAVE_FILE_VERSION);
	}

	Log::Write("reading settings data...");

	is.read((char*)&this -> characterMass, sizeof(double));
	is.read((char*)&this -> gravityAcceleration, sizeof(double));
	is.read((char*)&this -> maxWalkingSpeed, sizeof(double));
	is.read((char*)&this -> horizontalForce, sizeof(double));
	is.read((char*)&this -> airHorizontalForce, sizeof(double));
	is.read((char*)&this -> jumpImpulse, sizeof(double));

	is.close();

	Log::Write("done loading settings");
}

void Settings::Save(string filename) {
	Log::Write("begin saving settings to " + filename);

	ofstream os(filename, ios::binary | ios::trunc);
	if(!os.is_open()) {
		throw Error("Error occured in Swttings::Save, unable to open file\nFilename: " + filename);
	}

	os << Settings::CURRENT_SAVE_FILE_VERSION << "\n" << flush;

	Log::Write("settings file version written");

	Log::Write("writing settings data...");

	os.write((char*)&this -> characterMass, sizeof(double));
	os.write((char*)&this -> gravityAcceleration, sizeof(double));
	os.write((char*)&this -> maxWalkingSpeed, sizeof(double));
	os.write((char*)&this -> horizontalForce, sizeof(double));
	os.write((char*)&this -> airHorizontalForce, sizeof(double));
	os.write((char*)&this -> jumpImpulse, sizeof(double));

	os.close();

	Log::Write("done saving settings");
}

const string Settings::CURRENT_SAVE_FILE_VERSION = "v1.4";

}
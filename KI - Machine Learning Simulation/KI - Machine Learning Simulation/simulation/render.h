#pragma once

#include <stdafx.h>

namespace Sim {

class World;
class Character;

// Klasse, die die Elemente der Simulation zeichnen
class Render {
public:
	Render(World* world, Character* character) : world(world), character(character) {};
	~Render() = default;

	const static uint32_t BLOCK_SIZE = 48;

	// Zeichnet
	void Draw();

private:
	World* world;
	Character* character;
};

}
#include <system/log.h>

#include <system/errors.h>

namespace System {

void Log::Create() {
	if(Log::isCreated) {
		// Log bereits erstellt
		throw string("Attempting to create already created log singleton");
	}
	Log::isCreated = true;

	// Erzeuge Protokolldateipfad aus aktuellem Datum und Zeit
	string fileName = "log/";
	
	time_t now = time(0);
	tm tm_;
	char buf[80];
	localtime_s(&tm_, &now);
	strftime(buf, sizeof(buf), "%F %H-%M-%S", &tm_);

	fileName += buf;

	fileName += ".log";

	// �ffne Protokolldatei
	Log::logFile.open(fileName);

	clock_t c = clock();
	Log::logFile << "Logfile created: " << buf << " clock: " << c << endl;
	Log::logFile << "===================================================================" << endl << endl;

	// Dupliziere Ausgabe in Konsole
	cout << "Logfile created: " << buf << " clock: " << c << endl;
	cout << "===================================================================" << endl << endl;
}

void Log::Destroy() {
	if(!Log::isCreated) {
		// Log bereits zerst�rt
		throw string("Attempting to destroy already destroyed log singleton");
	}
	Log::isCreated = false;

	// Schliesse Protokolldatei
	Log::logFile.close();
}

void Log::Write(string const& s) {
	Log::AssertCreated();
	if(Log::Verbose) {
		// nur wenn erweiterte Ausgabe angeschaltet ist
		time_t now = time(0);
		tm tm_;
		char buf[50];
		localtime_s(&tm_, &now);
		strftime(buf, sizeof(buf), "%F %H-%M-%S", &tm_);

		clock_t c = clock();
		Log::logFile << "[ " << buf << " " << c << " ] ";
		Log::logFile << s << endl;

		// Dupliziere Ausgabe in Konsole
		cout << "[ " << buf << " " << c << " ] ";
		cout << s << endl;
	}
}

void Log::WriteImportant(string const& s) {
	Log::AssertCreated();
	time_t now = time(0);
	tm tm_;
	char buf[50];
	localtime_s(&tm_, &now);
	strftime(buf, sizeof(buf), "%F %H-%M-%S", &tm_);

	clock_t c = clock();
	Log::logFile << endl << "** [ " << buf << " " << c << " ] ";
	Log::logFile << s << endl << endl;

	// Dupliziere Ausgabe in Konsole
	cout << endl << "** [ " << buf << " " << c << " ] ";
	cout << s << endl << endl;
}

void Log::AssertCreated() {
	if(!Log::isCreated) {
		// Werfe nicht fataler Fehler
		throw Error("Cannot access member functions of Log while not created");
	}
}	

bool Log::Verbose = false;
bool Log::isCreated = false;
ofstream Log::logFile = ofstream();

}
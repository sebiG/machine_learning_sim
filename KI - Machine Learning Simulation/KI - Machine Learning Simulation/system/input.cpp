#include <system/input.h>

#include <system/errors.h>
#include <system/com.h>
#include <system/log.h>

namespace System {

void Input::Create(HINSTANCE instance) {
	if(Input::isCreated) {
		// Benutzereingabe bereits erstellt
		throw string("Attempting to create already created program singleton");
	}
	Input::isCreated = true;

	Log::Write("begin Input creation");

	// Variablen f�r Fehlerbehandlung
	HRESULT ret;
	stringstream errorString;

	Input::instance = instance;

	memset(Input::keyboardState_, (int)KeyState::RELEASED, sizeof(KeyState) * 256);
	memset(Input::mouseState_, (int)KeyState::RELEASED, sizeof(KeyState) * 4);

	ret = DirectInput8Create(Input::instance, DIRECTINPUT_VERSION, IID_IDirectInput8, (void**)&Input::directInput, NULL);
	if(FAILED(ret)) {
		// Werfe fataler Fehler
		errorString.str("");
		errorString << "0x" << hex << ret << flush;

		throw FatalError("Error occured in DirectInput8Create, DirectInput initialization failed\nHRESULT code: " + errorString.str());
	}
	ret = Input::directInput -> CreateDevice(GUID_SysKeyboard, &Input::keyboard, NULL);
	if(FAILED(ret)) {
		// Werfe fataler Fehler
		errorString.str("");
		errorString << "0x" << hex << ret << flush;

		throw FatalError("Error occured in IDirectInput8::CreateDevice, Keyboard device creation failed\nHRESULT code: " + errorString.str());
	}
	ret = Input::directInput -> CreateDevice(GUID_SysMouse, &Input::mouse, NULL);
	if(FAILED(ret)) {
		// Werfe fataler Fehler
		errorString.str("");
		errorString << "0x" << hex << ret << flush;

		throw FatalError("Error occured in IDirectInput8::CreateDevice, Mouse device creation failed\nHRESULT code: " + errorString.str());
	}

	Log::Write("DirectInput devices created");

	ret = Input::keyboard -> SetDataFormat(&c_dfDIKeyboard);
	if(FAILED(ret)) {
		// Werfe fataler Fehler
		errorString.str("");
		errorString << "0x" << hex << ret << flush;

		throw FatalError("Error occured in IDirectInputDevice8::SetDataFormat, Keyboard data format set failed\nHRESULT code: " + errorString.str());
	}
	// Gib Tastatur automatisch frei, wenn das Fenster in den Hintergrund ger�t
	// Erm�gliche, dass andere Anwendungen gleichzeitig Tastatureingaben empfangen k�nnen
	ret = Input::keyboard -> SetCooperativeLevel(Window::GetHwnd(), DISCL_FOREGROUND | DISCL_NONEXCLUSIVE);
	if(FAILED(ret)) {
		// Werfe fataler Fehler
		errorString.str("");
		errorString << "0x" << hex << ret << flush;

		throw FatalError("Error occured in IDirectInputDevice8::SetCooperativeLevel, Keyboard cooperative level set failed\nHRESULT code: " + errorString.str());
	}

	Log::Write("Keyboard configured");

	ret = Input::mouse -> SetDataFormat(&c_dfDIMouse);
	if(FAILED(ret)) {
		// Werfe fataler Fehler
		errorString.str("");
		errorString << "0x" << hex << ret << flush;

		throw FatalError("Error occured in IDirectInputDevice8::SetDataFormat, Mouse data format set failed\nHRESULT code: " + errorString.str());
	}
	// Gib Maus automatisch frei, wenn das Fenster in den Hintergrund ger�t
	// Erm�gliche, dass andere Anwendungen gleichzeitig Mauseingaben empfangen k�nnen
	ret = Input::mouse -> SetCooperativeLevel(Window::GetHwnd(), DISCL_FOREGROUND | DISCL_NONEXCLUSIVE);
	if(FAILED(ret)) {
		// Werfe fataler Fehler
		errorString.str("");
		errorString << "0x" << hex << ret << flush;

		throw FatalError("Error occured in IDirectInputDevice8::SetCooperativeLevel, Mouse cooperative level set failed\nHRESULT code: " + errorString.str());
	}

	Log::Write("Mouse configured");

	Log::Write("Input creation terminated");
}

void Input::Destroy() {
	if(!Input::isCreated) {
		// Benutzereingabe bereits zerst�rt
		throw string("Attempting to destroy already destroyed program singleton");
	}
	Input::isCreated = false;

	Log::Write("begin Input destruction");

	COM::SafeRelease<IDirectInput8>(&Input::directInput);
	COM::SafeRelease<IDirectInputDevice8>(&Input::keyboard);
	COM::SafeRelease<IDirectInputDevice8>(&Input::mouse);

	Log::Write("Input destruction terminated");
}

void Input::Update() {
	Input::AssertCreated();

	// Variablen f�r Fehlerbehandlung
	HRESULT ret;
	stringstream errorString;

	ret = Input::keyboard -> GetDeviceState(sizeof(char) * 256, (LPVOID)&Input::keyboardState);
	if(FAILED(ret)) {
		if(ret == DIERR_INPUTLOST || ret == DIERR_NOTACQUIRED) {
			// Anwendung war vorher im Hintergrund
			Input::keyboard -> Acquire();
		} else {
			// Werfe Fehler
			errorString.str("");
			errorString << "0x" << hex << ret << flush;
			throw Error("Error occured in IDirectInputDevice8::GetDeviceState, Keyboard device state query failed for unecpected reason\nHRESULT code: " + errorString.str());
		}
	} else {
		// Lese Tastaturdaten
		for(uint32_t i = 0; i < 256; i++) {
			if(Input::keyboardState_[i] == KeyState::DOWN) {
				Input::keyboardState_[i] = KeyState::PRESSED;
			}
			if(Input::keyboardState_[i] == KeyState::UP) {
				Input::keyboardState_[i] = KeyState::RELEASED;
			}
			if(Input::keyboardState[i] & 0x80) {
				if(Input::keyboardState_[i] == KeyState::RELEASED) {
					Input::keyboardState_[i] = KeyState::DOWN;
				}
			} else {
				if(Input::keyboardState_[i] == KeyState::PRESSED) {
					Input::keyboardState_[i] = KeyState::UP;
				}
			}
		}
	}

	ret = Input::mouse -> GetDeviceState(sizeof(DIMOUSESTATE), (LPVOID)&Input::mouseState);
	if(FAILED(ret)) {
		if(ret == DIERR_INPUTLOST || ret == DIERR_NOTACQUIRED) {
			// Anwendung war vorher im Hintergrund
			Input::mouse -> Acquire();
		} else {
			// Werfe Fehler
			errorString.str("");
			errorString << "0x" << hex << ret << flush;
			throw Error("Error occured in IDirectInputDevice8::GetDeviceState, Mouse device state query failed for unecpected reason\nHRESULT code: " + errorString.str());
		}
	} else {
		// Lese Mausdaten
		for(uint32_t i = 0; i < 4; i++) {
			if(Input::mouseState_[i] == KeyState::DOWN) {
				Input::mouseState_[i] = KeyState::PRESSED;
			}
			if(Input::mouseState_[i] == KeyState::UP) {
				Input::mouseState_[i] = KeyState::RELEASED;
			}
			if(Input::mouseState.rgbButtons[i] & 0x80) {
				if(Input::mouseState_[i] == KeyState::RELEASED) {
					Input::mouseState_[i] = KeyState::DOWN;
				}
			} else {
				if(Input::mouseState_[i] == KeyState::PRESSED) {
					Input::mouseState_[i] = KeyState::UP;
				}
			}
		}
	}

	// Beziehe Mausposition mittels Win32
	POINT mousePos;
	GetCursorPos(&mousePos);
	Input::absoluteMousePosition.x = (int32_t)mousePos.x;
	Input::absoluteMousePosition.y = (int32_t)mousePos.y;
	ScreenToClient(Window::GetHwnd(), &mousePos);
	Input::relativeMousePosition.x = (int32_t)mousePos.x;
	Input::relativeMousePosition.y = (int32_t)mousePos.y;
}

Input::KeyState Input::GetKeyboardState(uint8_t button) {
	Input::AssertCreated();

	return Input::keyboardState_[button];
}

Input::KeyState Input::GetMouseButtonState(Input::MouseButton button) {
	Input::AssertCreated();

	return Input::mouseState_[(int32_t)button];
}

Input::MouseMovement Input::GetMouseMovement() {
	Input::AssertCreated();

	MouseMovement mouseMovement;
	mouseMovement.dX = (int32_t)Input::mouseState.lX;
	mouseMovement.dY = (int32_t)Input::mouseState.lY;
	mouseMovement.scroll = (int32_t)Input::mouseState.lZ;

	return mouseMovement;
}

Input::MousePosition Input::GetAbsoluteMousePosition() {
	Input::AssertCreated();

	return Input::absoluteMousePosition;
}

Input::MousePosition Input::GetRelativeMousePosition() {
	Input::AssertCreated();

	return Input::relativeMousePosition;
}

void Input::AssertCreated() {
	if(!Input::isCreated) {
		// Werfe nicht fataler Fehler
		throw Error("Cannot access member functions of Input while not created");
	}
}

bool Input::isCreated = false;

HINSTANCE Input::instance = NULL;

IDirectInput8* Input::directInput = nullptr;
IDirectInputDevice8* Input::keyboard = nullptr;
IDirectInputDevice8* Input::mouse = nullptr;

BYTE Input::keyboardState[256];
DIMOUSESTATE Input::mouseState;

Input::KeyState Input::keyboardState_[256];
Input::KeyState Input::mouseState_[4];

Input::MousePosition Input::absoluteMousePosition;
Input::MousePosition Input::relativeMousePosition;

}
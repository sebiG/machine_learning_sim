#pragma once

#include <stdafx.h>

namespace System {

class ProgramMode;

// Hauptklasse der Simulation
// Singleton
// Erstellt alle Systemkomponenten und startet die Programmschleife
class Program {
public:
	// Enumeration f�r Programmmodi
	enum class ProgramModeEnum {
		PROGRAM_MODE_MAIN_MENU = 0,
		PROGRAM_MODE_SIMULATION = 1,
		PROGRAM_MODE_EDITOR = 2
	};

	Program() = delete; // Verhindere Konstruktion
	~Program() = default;

	// Gibt Status der Programmklasse zur�ck
	inline static bool IsCreated() {
		return Program::isCreated;
	}
	// Erstellt Programm und Systemkomponenten
	static void Create(HINSTANCE instance);
	// Zerst�rt Program
	static void Destroy();
	// Startet Programmschleife
	static void Run();

	// Wechsle Programmmodus
	static inline void SwitchProgramMode(ProgramModeEnum mode) {
		Program::nextProgramMode = mode;
	}
	// Stoppe Programm
	static inline void Stop() {
		Program::stop = true;
	}

private:
	// Hilfefunktion, um sicherzugehen, dass das Fenster bereits erstellt wurde
	// (Create und Destroy verwenden eigene Implementationen)
	void AssertCreated();
	static bool isCreated;

	// Zwischenspeicher f�r standardm�ssiger COUNOUT$ stream
	static FILE* oldConoutStream;
	static HINSTANCE instance;
	static bool running;

	// Z�hler
	static double counterFrequency;
	static int64_t lastFrame;
	static uint32_t frameCount;
	static double frameTimeCount;

	// Programmmodi
	static ProgramModeEnum currentProgramMode;
	static ProgramModeEnum nextProgramMode;
	static vector<ProgramMode*> programModes;

	static bool stop;
};

}
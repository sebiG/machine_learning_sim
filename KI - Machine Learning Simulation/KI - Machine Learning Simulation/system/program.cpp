#include <system/program.h>

#include <system/errors.h>
#include <system/event.h>
#include <system/log.h>
#include <system/graphics.h>
#include <system/input.h>
#include <menu/mainMenu.h>
#include <simulation/simulation.h>
#include <simulation/blockRegistry.h>
#include <simulation/physicsBehaviour.h>
#include <editor/editor.h>

using namespace Menu;
using namespace Sim;
using namespace Edit;

namespace System {

void Program::Create(HINSTANCE instance) {
	if(Program::isCreated) {
		// Programm bereits erstellt
		throw FatalError("Attempting to create already created program singleton");
	}
	Program::isCreated = true;

	Program::instance = instance;

	// Erstelle Konsole
	AllocConsole();
	freopen_s(&Program::oldConoutStream, "CONOUT$", "w", stdout);

	// Erstelle Protokoll
	Log::Create();

	// Erstelle Fenster
	Window::Create(Program::instance, 1000, 700, 1000, 700);

	// Erstelle Grafiksystem
	Graphics::Create();
	Graphics::VSYNC = false;

	// Erstelle Benutzerinteraktionssystem
	Input::Create(Program::instance);

	// Erstelle Blockregister
	BlockRegistry::Create();

	// Erstelle Datenbank mit Relationen von physikalischen Konstanten und Variabeln
	PhysicsBehaviour::Create();

	// FPS-Z�hler
	Program::frameCount = 0;
	Program::frameTimeCount = 0.0;

	// Erstelle Programmodi und starte erster Modus
	Program::programModes.push_back(new MainMenu());
	Program::programModes.push_back(new Simulation());
	Program::programModes.push_back(new Editor());

	Program::currentProgramMode = ProgramModeEnum::PROGRAM_MODE_MAIN_MENU;
	Program::nextProgramMode = ProgramModeEnum::PROGRAM_MODE_MAIN_MENU;
	Program::programModes[(uint32_t)ProgramModeEnum::PROGRAM_MODE_MAIN_MENU] -> Init();

	Log::WriteImportant("Program sucessfully started");
}

void Program::Destroy() {
	if(!Program::isCreated) {
		// Programm bereits zerst�rt
		throw FatalError("Attempting to destroy already destroyed program singleton");
	}
	Program::isCreated = false;

	Log::WriteImportant("Stopping program");

	// L�sche Datenbank mit Relationen von physikalischen Konstanten und Variabeln
	PhysicsBehaviour::Destroy();

	// Zerst�re Blockregister
	BlockRegistry::Destroy();

	// Zerst�re Benutzerinteraktionssystem
	Input::Destroy();

	// Zerst�re Grafiksystem
	// (Bereits geschlossenes Fenster hat darauf keinen Einfluss)
	Graphics::Destroy();

	// Zerst�re Fenster, wenn nicht bereits geschehen
	// (Ein geschlossenens Fenster ist oft der Grund f�r das Beenden des Programms)
	if(Window::IsCreated()) {
		Window::Destroy();
	}

	// Zerst�re Protokoll
	Log::Destroy();

	// Zerst�re Konsole
	FILE* stream;
	freopen_s(&stream, "CONOUT$", "w", Program::oldConoutStream);
	FreeConsole();
}

void Program::Run() {
	MSG lastMessage;

	// Zeige Fenster
	Window::Show();

	// Starte Z�hler f�r Framezeit
	LARGE_INTEGER li;
	if(!QueryPerformanceFrequency(&li)) {
		throw FatalError("Error occured in QueryPerformanceCounterFrequency, counter frequency query failed");
	}
	Program::counterFrequency = double(li.QuadPart);

	QueryPerformanceCounter(&li);
	Program::lastFrame = li.QuadPart;

	// Windows Ereignisschlaufe
	while(Window::IsCreated()) {
		Input::Update();

		Graphics::Clear();

		// Aktualisiere Z�hler
		LARGE_INTEGER li;
		QueryPerformanceCounter(&li);
		double deltaTime = double(li.QuadPart - Program::lastFrame) / Program::counterFrequency;
		Program::lastFrame = li.QuadPart;
		Program::frameCount++;
		Program::frameTimeCount += deltaTime;
		if(Program::frameTimeCount > 1.0) {
			Program::frameTimeCount -= 1.0;
			stringstream ss;
			ss << "Bildfrequenz: " << Program::frameCount << " Hz, Bildzeit: " << Program::frameTimeCount << " s" << flush;
			SetWindowText(Window::GetHwnd(), ss.str().c_str());
			Program::frameCount = 0;
		}

		// Aktualisiere Programmodus
		Program::programModes[(uint32_t)Program::currentProgramMode] -> Update(deltaTime);

		Graphics::SwapBuffers();

		// Behandle Window-Ereignisse
		if(PeekMessage(&lastMessage, 0, 0, 0, PM_REMOVE)) {
			TranslateMessage(&lastMessage);
			DispatchMessage(&lastMessage);
		}

		// Wechsle Programmodi wenn n�tig
		if(Program::nextProgramMode != Program::currentProgramMode) {
			stringstream ss;
			ss << (uint32_t)Program::currentProgramMode << " new: " << (uint32_t)Program::nextProgramMode << flush;
			Log::WriteImportant("changing program mode, old: " + ss.str());
			Program::programModes[(uint32_t)Program::currentProgramMode] -> DeInit();
			Program::currentProgramMode = Program::nextProgramMode;
			Program::programModes[(uint32_t)Program::currentProgramMode] -> Init();
		}

		// Stoppe Programm wenn n�tig
		if(Program::stop) {
			Log::WriteImportant("recieved stop request");
			Program::programModes[(uint32_t)Program::currentProgramMode] -> DeInit();
			break;
		}
	}
}

void Program::AssertCreated() {
	if(!Program::isCreated) {
		// Werfe nicht fataler Fehler
		throw Error("Cannot access member functions of Program while not created");
	}
}

bool Program::isCreated = false;
FILE* Program::oldConoutStream = NULL;
HINSTANCE Program::instance = NULL;
bool Program::running = false;

double Program::counterFrequency = 0.0;
int64_t Program::lastFrame = 0;
uint32_t Program::frameCount = 0;
double Program::frameTimeCount = 0.0;

Program::ProgramModeEnum Program::currentProgramMode;
Program::ProgramModeEnum Program::nextProgramMode;
vector<ProgramMode*> Program::programModes;

bool Program::stop = false;

}
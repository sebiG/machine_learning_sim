#pragma once

#include <stdafx.h>

namespace System {

// Klasse f�r Runtimeprotokoll
// Singleton
// Erm�glicht das Protokollieren von Runtime-Ereignissen
class Log {
public:
	Log() = delete; // Verhindere Konstruktion
	~Log() = default;

	// Gibt Status der Protokollklasse zur�ck
	inline static bool IsCreated() {
		return Log::isCreated;
	}
	// Startet Protokoll (pseudo-Konstruktor)
	static void Create();
	// Stoppt Protokoll (pseudo-Destruktor)
	static void Destroy();

	// Schreibt vernachl�ssigbarer Protokolleintrag (kann durch verbose-Flag aktiviert werden)
	static void Write(string const& s);
	// Schreibt wichtiger Protokolleintrag (wird durch verbose-Flag nicht beeintr�chtigt)
	static void WriteImportant(string const& s);

	// Kontrolliert, ob vernachl�ssigbare Protokolleintr�ge gespeichert werden (true) oder ignoriert werden (false)
	static bool Verbose;

private:
	// Hilfefunktion, um sicherzugehen, dass das Fenster bereits erstellt wurde
	// (Create und Destroy verwenden eigene Implementationen)
	static void AssertCreated();
	static bool isCreated;

	static ofstream logFile;
};

}
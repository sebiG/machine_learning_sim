#pragma once

#include <stdafx.h>

#include <system/window.h>
#include <system/event.h>

namespace System {

// Klasse f�r Benutzereingaben �ber Tastatur und Maus
// Singleton
// Stellt Schnittstelle zwischen DirectInput und Projektsoftware her
class Input {
public:
	Input() = delete; // Verhindere Konstruktion
	~Input() = default;

	// Enumeration f�r Tastenstaus
	enum class KeyState {
		RELEASED,
		PRESSED,
		UP,
		DOWN
	};

	// Enumeration f�r Maustasten
	enum class MouseButton {
		LEFT,
		RIGHT,
		MIDDLE,
		UNUSED
	};

	// Struktur f�r Mausbewegung
	struct MouseMovement {
		int32_t dX;
		int32_t dY;
		int32_t scroll;
	};

	// Struktur f�r Mausposition
	struct MousePosition {
		int32_t x;
		int32_t y;
	};

	// Gibt Status der Benutzereingabeklasse zur�ck
	inline static bool IsCreated() {
		return Input::isCreated;
	}
	// Erstellt Benutzereingabesystem (pseudo-Konstruktor)
	static void Create(HINSTANCE instance);
	// Zerst�rt Benutzereingabesystem (pseudo-Destruktor)
	static void Destroy();
	// Aktualisiert Benutzereingaben, wird jede Frame aufgerufen
	static void Update();

	// Gibt Tastenstatus auf Tastatur zur�ck
	static KeyState GetKeyboardState(uint8_t button);
	// Gibt Tastenstatus an Maus zur�ck
	static KeyState GetMouseButtonState(MouseButton button);
	// Gibt Mausbewegung seit letztem Update zur�ck
	static MouseMovement GetMouseMovement();
	// Gibt absolute Mausposition zur�ck (relativ zum Bildschirm)
	static MousePosition GetAbsoluteMousePosition();
	// Gibt relative Mausposition zur�ck (relativ zum Fenster)
	static MousePosition GetRelativeMousePosition();

private:
	// Hilfefunktion, um sicherzugehen, dass das Fenster bereits erstellt wurde
	// (Create und Destroy verwenden eigene Implementationen)
	static void AssertCreated();

	static bool isCreated;

	static HINSTANCE instance;
	static IDirectInput8* directInput;
	static IDirectInputDevice8* keyboard;
	static IDirectInputDevice8* mouse;

	static BYTE keyboardState[256];
	static DIMOUSESTATE mouseState;

	static KeyState keyboardState_[256];
	static KeyState mouseState_[4];

	static MousePosition absoluteMousePosition;
	static MousePosition relativeMousePosition;
};

}
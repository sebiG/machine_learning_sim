#pragma once

#include <stdafx.h>

namespace System {

// Basisklasse f�r Programmmodi
// Erm�glicht das Programm f�r mehrere Zwecke unabh�ngig voneinander zu verwenden.
class ProgramMode {
public:
	ProgramMode() = default; // Standardkonstruktor
	virtual ~ProgramMode() = default; // Standarddestruktor

	// Funktion, die jede Frame aufgerufen wird
	virtual void Update(double deltaTime) = 0;
	// Funktion, die bei Aktivierung des Modus aufgerufen wird
	// Erm�glicht zusammen mit DeInit effiziente Resourcenverwaltung
	virtual void Init() = 0;
	// Funktion, die bei Deaktivierung des Modus aufgerufen wird
	virtual void DeInit() = 0;
};

}
#pragma once

#include <stdafx.h>

namespace System {

// Beide Fehlerklassen sind identisch, m�ssen aber von catch-Bl�cken unterschieden werden

struct FatalError {
	FatalError(string const& errorMessage) : errorMessage(errorMessage) {}
	inline string const& What() {
		return this -> errorMessage;
	}
private:
	string errorMessage;
};

struct Error {
	Error(string const& errorMessage) : errorMessage(errorMessage) {}
	inline string const& What() {
		return this -> errorMessage;
	}
private:
	string errorMessage;
};

}
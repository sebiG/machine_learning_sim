#pragma once

#include <stdafx.h>

#include <system/event.h>

namespace System {

// Klasse f�r Anwendungsfenster
// Singleton
// Stellt Schnittstelle zwischen Win32 und Projektsoftware her
class Window {
public:
	// Eindeutige Konstanten f�r Ereignistypen
	const static uint32_t WINDOW_MAXIMIZE = 0x0000;
	const static uint32_t WINDOW_MINIMIZE = 0x0001;
	const static uint32_t WINDOW_RESTORE = 0x0002;
	const static uint32_t WINDOW_SIZE = 0x0003;
	const static uint32_t WINDOW_POSITION = 0x0004;
	const static uint32_t WINDOW_CLOSE = 0x0005;
	const static uint32_t WINDOW_DESTROY = 0x0006;
	const static uint32_t WINDOW_SETFOCUS = 0x0007;
	const static uint32_t WINDOW_KILLFOCUS = 0x0008;
	const static uint32_t WINDOW_MOUSEWHEEL = 0x0009;
	const static uint32_t WINDOW_DRAW = 0x000a;
	const static uint32_t WINDOW_HOVER = 0x000b;
	const static uint32_t WINDOW_LEAVE = 0x000c;
	const static uint32_t WINDOW_CHAR = 0x000d;
	const static uint32_t WINDOW_SIZING = 0x000e;

	Window() = delete; // Verhindere Konstruktion
	~Window() = default;

	// Gibt status de Fensterklasse zur�ck
	inline static bool IsCreated() {
		return Window::isCreated;
	}
	// Erstellt Fenster (pseudo-Konstruktor)
	static void Create(HINSTANCE instance, uint32_t width, uint32_t height, uint32_t minWidth, uint32_t minHeight);
	// Zerst�rt Fenster (pseudo-Destruktor)
	// Wird automatisch nach WM_DESTROY aufgerufen
	static void Destroy();

	// Gibt Fenster-Handle zur�ck
	inline static HWND GetHwnd() {
		Window::AssertCreated();
		return Window::hwnd;
	}
	// Gibt Breite des Benutzerbereichs zur�ck
	inline static uint32_t GetWidth() {
		Window::AssertCreated();
		return Window::width;
	}
	// Gibt H�he des Benutzerbereichs zur�ck
	inline static uint32_t GetHeight() {
		Window::AssertCreated();
		return Window::height;
	}
	// Gibt true zur�ck, falls das Fenster im Fokus ist; ansonsten false
	inline static bool HasFocus() {
		Window::AssertCreated();
		return Window::hasFocus;
	}

	// Zeigt Fenster an
	inline static void Show() {
		Window::AssertCreated();
		ShowWindow(Window::hwnd, SW_SHOW);
	}
	// Versteckt Fenster
	inline static void Hide() {
		Window::AssertCreated();
		ShowWindow(Window::hwnd, SW_HIDE);
	}

	// Workaround f�r Ereignissystem
	inline static void Register(EventReciever* reciever, uint32_t eventID) {
		Window::AssertCreated();
		Window::eventSender.Register(reciever, eventID);
	}
	inline static void Unregister(EventReciever* reciever, uint32_t eventID) {
		// Keine �berpr�fung
		Window::eventSender.Unregister(reciever, eventID);
	}

	// Windows-Prozedur f�r Fensterereignisse
	static LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

private:
	// Hifeklasse, die die Verwendung von Ereignissen erm�glicht
	class WindowEventSender : public EventSender {
	public:
		friend Window;
	};

	// Hilfefunktion, um sicherzugehen, dass das Fenster bereits erstellt wurde
	// (Create und Destroy verwenden eigene Implementationen)
	static void AssertCreated();

	// Eigene Implementation f�r Fensterereignisse
	static bool OnWindowsEvent(UINT msg, WPARAM wParam, LPARAM lParam);

	static bool isCreated;

	static HINSTANCE instance;
	static uint32_t width;
	static uint32_t height;
	static uint32_t minWidth;
	static uint32_t minHeight;
	static HWND hwnd;
	static bool hasFocus;

	static WindowEventSender eventSender;
};

}
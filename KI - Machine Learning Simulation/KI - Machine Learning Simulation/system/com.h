#pragma once

#include <stdafx.h>

namespace System {

// Namesapce f�r die Verwaltung von Windows Component-Object-Model Objekten
// (Keine statische Klasse, da keine statischen Daten vorhanden sind)
namespace COM {

// L�scht Objekt sicher
template <typename T> inline static void SafeRelease(T **object) {
	if(*object != nullptr) {
		(*object) -> Release();
		*object = 0;
	}
}
// Dupliziert Objekt sicher
template <typename T> inline static T* SafeAcquire(T* object) {
	if(object != nullptr) {
		object -> AddRef();
	}

	return object;
}

}

}
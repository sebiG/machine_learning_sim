#include <system/graphics.h>

#include <system/errors.h>
#include <system/com.h>
#include <system/log.h>

namespace System {

void Graphics::Create() {
	if(Graphics::isCreated) {
		// Grafiksystem bereits erstellt
		throw string("Attempting to create already created graphics singleton");
	}
	Graphics::isCreated = true;

	Log::Write("begin Graphics creation");

	// Variablen f�r Fehlerbehandlung
	HRESULT ret;
	stringstream errorString;

	// Initialisiere COM
	ret = CoInitialize(NULL);
	if(FAILED(ret)) {
		// Werfe fataler Fehler
		errorString.str("");
		errorString << "0x" << hex << ret << flush;

		throw FatalError("Error occured in CoInitialize, COM initialization failed\nHRESULT code: " + errorString.str());
	}

	// Ereignisempf�nger registrieren
	Window::Register(&Graphics::eventReciever, Window::WINDOW_SIZE);

	// Erstelle Direct3D-Device
	
	ret = D3D11CreateDevice(NULL,
		D3D_DRIVER_TYPE_HARDWARE,
		NULL,
		D3D11_CREATE_DEVICE_SINGLETHREADED | D3D11_CREATE_DEVICE_BGRA_SUPPORT,
		0, 0,
		D3D11_SDK_VERSION,
		&Graphics::d3dDevice,
		&Graphics::d3dFeatureLevel,
		&Graphics::d3dDeviceContext);
	if(FAILED(ret)) {
		// Werfe fataler Fehler
		errorString.str("");
		errorString << "0x" << hex << ret << flush;

		throw FatalError("Error occured in D3D11CreateDevice, d3d device creation failed\nHRESULT code: " + errorString.str());
	}
	Log::Write("D3D Device created");
	Log::Write("D3D DeviceContext created");

	// �berpr�fe Feature-Stufe
	if(Graphics::d3dFeatureLevel != D3D_FEATURE_LEVEL_11_0) {
		//Werfe fataler Fehler
		errorString.str("");
		errorString << "Found: " << hex << Graphics::d3dFeatureLevel << " Required: " << D3D_FEATURE_LEVEL_11_0 << flush;

		throw FatalError("D3D feature level is not matching. " + errorString.str());
	}

	// Rufe Hierarchie von COM-Objekten auf, um ein IDXGIFactory-Interface zu bekommen
	IDXGIDevice* dxgiDevice = nullptr;
	IDXGIAdapter* dxgiAdapter = nullptr;
	IDXGIFactory* dxgiFactory = nullptr;

	ret = Graphics::d3dDevice -> QueryInterface(__uuidof(IDXGIDevice), (void**)&dxgiDevice);
	if(FAILED(ret)) {
		// Werfe fataler Fehler
		errorString.str("");
		errorString << "0x" << hex << ret << flush;

		throw FatalError("Error occured in ID3D11Device::QueryInterface, IDXGIDevice query failed\nHRESULT code: " + errorString.str());
	}

	ret = dxgiDevice -> GetParent(__uuidof(IDXGIAdapter), (void**)&dxgiAdapter);
	if(FAILED(ret)) {
		// Werfe fataler Fehler
		errorString.str("");
		errorString << "0x" << hex << ret << flush;

		throw FatalError("Error occured in IDXGIDevice::GetParent, IDXGIAdapter query failed\nHRESULT code: " + errorString.str());
	}

	ret = dxgiAdapter -> GetParent(__uuidof(IDXGIFactory), (void**)&dxgiFactory);
	if(FAILED(ret)) {
		// Werfe fataler Fehler
		errorString.str("");
		errorString << "0x" << hex << ret << flush;

		throw FatalError("Error occured in IDXGIAdapter::GetParent, IDXGIFactory query failed\nHRESULT code: " + errorString.str());
	}

	// Erstelle Bufferkette
	DXGI_SWAP_CHAIN_DESC swapChainDesc;
	memset(&swapChainDesc, 0, sizeof(DXGI_SWAP_CHAIN_DESC));
	swapChainDesc.BufferCount = 1;
	swapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	swapChainDesc.BufferDesc.Width = Window::GetWidth();
	swapChainDesc.BufferDesc.Height = Window::GetHeight();
	swapChainDesc.BufferDesc.RefreshRate.Numerator = 60;
	swapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	swapChainDesc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
	swapChainDesc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swapChainDesc.Flags = 0;
	swapChainDesc.OutputWindow = Window::GetHwnd();
	swapChainDesc.SampleDesc.Count = 1;
	swapChainDesc.SampleDesc.Quality = 0;
	swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
	swapChainDesc.Windowed = true;

	ret = dxgiFactory -> CreateSwapChain(Graphics::d3dDevice, &swapChainDesc, &Graphics::dxgiSwapChain);
	if(FAILED(ret)) {
		// Werfe fataler Fehler
		errorString.str("");
		errorString << "0x" << hex << ret << flush;

		throw FatalError("Error occured in IDXGIFactory::CreateSwapChain, IDXGISwapChain creation failed\nHRESULT code: " + errorString.str());
	}
	Log::Write("DXGI SwapChain created");

	// R�ume auf
	COM::SafeRelease<IDXGIDevice>(&dxgiDevice);
	COM::SafeRelease<IDXGIAdapter>(&dxgiAdapter);
	COM::SafeRelease<IDXGIFactory>(&dxgiFactory);

	// WIC initialisieren

	ret = CoCreateInstance(CLSID_WICImagingFactory, NULL, CLSCTX_INPROC_SERVER, IID_IWICImagingFactory, (void**)&Graphics::wicFactory);
	if(FAILED(ret)) {
		// Werfe fataler Fehler
		errorString.str("");
		errorString << "0x" << hex << ret << flush;

		throw FatalError("Error occured in CoCreateInstance, WICFactory creation failed\nHRESULT code: " + errorString.str());
	}
	Log::Write("WIC initialized");

	// Direct2D initialisieren

	ret = D2D1CreateFactory(D2D1_FACTORY_TYPE_SINGLE_THREADED, &Graphics::d2dFactory);
	if(FAILED(ret)) {
		// Werfe fataler Fehler
		errorString.str("");
		errorString << "0x" << hex << ret << flush;

		throw FatalError("Error occured in D2D1CreateFactory, D2D factory creation failed\nHRESULT code: " + errorString.str());
	}

	// DirectWrite initialisieren

	ret = DWriteCreateFactory(DWRITE_FACTORY_TYPE_SHARED, __uuidof(IDWriteFactory), (IUnknown**)&(Graphics::dwFactory));
	if(FAILED(ret)) {
		// Werfe fataler Fehler
		errorString.str("");
		errorString << "0x" << hex << ret << flush;

		throw FatalError("Error occured in DWriteCreateFactory, DWrite factory creation failed\nHRESULT code: " + errorString.str());
	}

	// Erstelle Textformat

	ret = Graphics::dwFactory -> CreateTextFormat(Graphics::stringConverter.from_bytes(Graphics::FONT_NAME).c_str(), NULL, DWRITE_FONT_WEIGHT_REGULAR, DWRITE_FONT_STYLE_NORMAL, DWRITE_FONT_STRETCH_NORMAL, Graphics::FONT_SIZE, L"en-us", &Graphics::dwTextFormat);
	if(FAILED(ret)) {
		// Werfe fataler Fehler
		errorString.str("");
		errorString << "\"" << Graphics::FONT_NAME << " " << Graphics::FONT_SIZE << " pt" << "\" failed\nHRESULT code: " << hex << ret << flush;

		throw FatalError("Error occured in ID2D1Factory::CreateTextFormat, text format creation for font " + errorString.str());
	}

	// Erstelle Render-Ziel

	ID3D11Texture2D* backBuffer = nullptr;
	ret = Graphics::dxgiSwapChain -> GetBuffer(0, __uuidof(ID3D11Texture2D), (void**)&backBuffer);
	if(FAILED(ret)) {
		// Werfe fataler Fehler
		errorString.str();
		errorString << "0x" << hex << ret << flush;

		throw FatalError("Error occured in DXGISwapChain::GetBuffer, Backbuffer query failed after resize\nHRESULT code: " + errorString.str());
	}
	ret = Graphics::d3dDevice -> CreateRenderTargetView(backBuffer, 0, &Graphics::d3dRenderTargetView);
	if(FAILED(ret)) {
		// Werfe fataler Fehler
		errorString.str();
		errorString << "0x" << hex << ret << flush;

		throw FatalError("Error occured in D3D11Device::CreateRenderTargetView, RenderTargetView creation failed after resize\nHRESULT code: " + errorString.str());
	}
	COM::SafeRelease<ID3D11Texture2D>(&backBuffer);
	Log::Write("D3D RenderTargetView created");

	// Erstelle allgemeiner Transformationsbuffer

	D3D11_BUFFER_DESC transformBufferDesc;
	memset(&transformBufferDesc, 0, sizeof(D3D11_BUFFER_DESC));

	transformBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	transformBufferDesc.ByteWidth = sizeof(float) * 16;
	transformBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	transformBufferDesc.MiscFlags = 0;
	transformBufferDesc.StructureByteStride = 0;
	transformBufferDesc.Usage = D3D11_USAGE_DYNAMIC;

	ret = Graphics::d3dDevice -> CreateBuffer(&transformBufferDesc, NULL, &Graphics::transformBuffer);
	if(FAILED(ret)) {
		// Werfe fataler Fehler
		errorString.str("");
		errorString << "0x" << hex << ret << flush;

		throw FatalError("Error occured in ID3D11Device::CreateBuffer, transform buffer creation failed\nHRESULT code: " + errorString.str());
	}
	Log::Write("TransformBuffer initialized");

	// Erstelle verpixelter Sampler

	D3D11_SAMPLER_DESC pixelatedSamplerDesc;
	memset(&pixelatedSamplerDesc, 0, sizeof(D3D11_SAMPLER_DESC));

	pixelatedSamplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
	pixelatedSamplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
	pixelatedSamplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
	pixelatedSamplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_POINT;

	ret = Graphics::d3dDevice -> CreateSamplerState(&pixelatedSamplerDesc, &Graphics::pixelatedSampler);
	if(FAILED(ret)) {
		// Werfe fataler Fehler
		errorString.str("");
		errorString << "0x" << hex << ret << flush;

		throw FatalError("Error occured in ID3D11Device::CreateSamplerState, Pixelated sampler creation failed\nHRESULT code: " + errorString.str());
	}
	Log::Write("Pixelated sampler created");

	// Erstelle allgemeiner Buffer

	D3D11_BUFFER_DESC dataBufferDesc;
	memset(&dataBufferDesc, 0, sizeof(D3D11_BUFFER_DESC));

	dataBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	dataBufferDesc.ByteWidth = 2048;
	dataBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	dataBufferDesc.MiscFlags = 0;
	dataBufferDesc.StructureByteStride = 0;
	dataBufferDesc.Usage = D3D11_USAGE_DYNAMIC;

	ret = Graphics::d3dDevice -> CreateBuffer(&dataBufferDesc, NULL, &Graphics::dataBuffer);
	if(FAILED(ret)) {
		// Werfe fataler Fehler
		errorString.str("");
		errorString << "0x" << hex << ret << flush;

		throw FatalError("Error occured in ID3D11Device::CreateBuffer, data buffer creation failed\nHRESULT code: " + errorString.str());
	}
	Log::Write("Data buffer created");

	// Lade Textur f�r Szene

	Graphics::LoadTexture(Graphics::TEXTURE_FILE, &Graphics::squaresTexture, &Graphics::squaresTextureView);

	// lade shader

	char* texturedSquaresVertexShaderBytecode = nullptr;
	char* texturedSquaresGeometryShaderBytecode = nullptr;
	char* texturedSquaresPixelShaderBytecode = nullptr;

	char* shapesPixelShaderBytecode = nullptr;

	char* linesVertexShaderBytecode = nullptr;
	char* linesGeometryShaderBytecode = nullptr;

	char* circlesVertexShaderBytecode = nullptr;
	char* circlesLineGeometryShaderBytecode = nullptr;
	char* circlesFillGeometryShaderBytecode = nullptr;

	char* rectsVertexShaderBytecode = nullptr;
	char* rectsLineGeometryShaderBytecode = nullptr;
	char* rectsFillGeometryShaderBytecode = nullptr;

	uint32_t texturedSquaresVertexShaderSize;
	uint32_t texturedSquaresGeometryShaderSize;
	uint32_t texturedSquaresPixelShaderSize;

	uint32_t shapesPixelShaderSize;

	uint32_t linesVertexShaderSize;
	uint32_t linesGeometryShaderSize;

	uint32_t circlesVertexShaderSize;
	uint32_t circlesLineGeometryShaderSize;
	uint32_t circlesFillGeometryShaderSize;

	uint32_t rectsVertexShaderSize;
	uint32_t rectsLineGeometryShaderSize;
	uint32_t rectsFillGeometryShaderSize;

	Graphics::LoadShader(Graphics::TEXTURE_VERTEX_SHADER_FILE, &texturedSquaresVertexShaderBytecode, &texturedSquaresVertexShaderSize);
	Graphics::LoadShader(Graphics::TEXTURE_GEOMETRY_SHADER_FILE, &texturedSquaresGeometryShaderBytecode, &texturedSquaresGeometryShaderSize);
	Graphics::LoadShader(Graphics::TEXTURE_PIXEL_SHADER_FILE, &texturedSquaresPixelShaderBytecode, &texturedSquaresPixelShaderSize);

	Graphics::LoadShader(Graphics::SHAPE_PIXEL_SHADER_FILE, &shapesPixelShaderBytecode, &shapesPixelShaderSize);

	Graphics::LoadShader(Graphics::LINE_VERTEX_SHADER_FILE, &linesVertexShaderBytecode, &linesVertexShaderSize);
	Graphics::LoadShader(Graphics::LINE_GEOMETRY_SHADER_FILE, &linesGeometryShaderBytecode, &linesGeometryShaderSize);

	Graphics::LoadShader(Graphics::CIRCLE_VERTEX_SHADER_FILE, &circlesVertexShaderBytecode, &circlesVertexShaderSize);
	Graphics::LoadShader(Graphics::CIRCLE_LINE_GEOMETRY_SHADER_FILE, &circlesLineGeometryShaderBytecode, &circlesLineGeometryShaderSize);
	Graphics::LoadShader(Graphics::CIRCLE_FILL_GEOMETRY_SHADER_FILE, &circlesFillGeometryShaderBytecode, &circlesFillGeometryShaderSize);

	Graphics::LoadShader(Graphics::RECT_VERTEX_SHADER_FILE, &rectsVertexShaderBytecode, &rectsVertexShaderSize);
	Graphics::LoadShader(Graphics::RECT_LINE_GEOMETRY_SHADER_FILE, &rectsLineGeometryShaderBytecode, &rectsLineGeometryShaderSize);
	Graphics::LoadShader(Graphics::RECT_FILL_GEOMETRY_SHADER_FILE, &rectsFillGeometryShaderBytecode, &rectsFillGeometryShaderSize);

	ret = Graphics::d3dDevice -> CreateVertexShader(texturedSquaresVertexShaderBytecode, texturedSquaresVertexShaderSize, NULL, &Graphics::texturedSquaresVertexShader);
	if(FAILED(ret)) {
		// Werfe fataler Fehler
		errorString.str("");
		errorString << "0x" << hex << ret << flush;

		throw FatalError("Error occured in ID3D11Device::CreateVertexShader, " + Graphics::TEXTURE_VERTEX_SHADER_FILE + string(" vertex shader creation failed\nHRESULT code: ") + errorString.str());
	}
	ret = Graphics::d3dDevice -> CreateGeometryShader(texturedSquaresGeometryShaderBytecode, texturedSquaresGeometryShaderSize, NULL, &Graphics::texturedSquaresGeometryShader);
	if(FAILED(ret)) {
		// Werfe fataler Fehler
		errorString.str("");
		errorString << "0x" << hex << ret << flush;

		throw FatalError("Error occured in ID3D11Device::CreateVertexShader, " + Graphics::TEXTURE_GEOMETRY_SHADER_FILE + string(" geometry shader creation failed\nHRESULT code: ") + errorString.str());
	}
	ret = Graphics::d3dDevice -> CreatePixelShader(texturedSquaresPixelShaderBytecode, texturedSquaresPixelShaderSize, NULL, &Graphics::texturedSquaresPixelShader);
	if(FAILED(ret)) {
		// Werfe fataler Fehler
		errorString.str("");
		errorString << "0x" << hex << ret << flush;

		throw FatalError("Error occured in ID3D11Device::CreatePixelShader, " + Graphics::TEXTURE_PIXEL_SHADER_FILE + string(" pixel shader creation failed\nHRESULT code: ") + errorString.str());
	}

	ret = Graphics::d3dDevice -> CreatePixelShader(shapesPixelShaderBytecode, shapesPixelShaderSize, NULL, &Graphics::shapesPixelShader);
	if(FAILED(ret)) {
		// Werfe fataler Fehler
		errorString.str("");
		errorString << "0x" << hex << ret << flush;

		throw FatalError("Error occured in ID3D11Device::CreatePixelShader, " + Graphics::SHAPE_PIXEL_SHADER_FILE + string(" pixel shader creation failed\nHRESULT code: ") + errorString.str());
	}

	ret = Graphics::d3dDevice -> CreateVertexShader(linesVertexShaderBytecode, linesVertexShaderSize, NULL, &Graphics::linesVertexShader);
	if(FAILED(ret)) {
		// Werfe fataler Fehler
		errorString.str("");
		errorString << "0x" << hex << ret << flush;

		throw FatalError("Error occured in ID3D11Device::CreateVertexShader, " + Graphics::LINE_VERTEX_SHADER_FILE + string(" vertex shader creation failed\nHRESULT code: ") + errorString.str());
	}
	ret = Graphics::d3dDevice -> CreateGeometryShader(linesGeometryShaderBytecode, linesGeometryShaderSize, NULL, &Graphics::linesGeometryShader);
	if(FAILED(ret)) {
		// Werfe fataler Fehler
		errorString.str("");
		errorString << "0x" << hex << ret << flush;

		throw FatalError("Error occured in ID3D11Device::CreateGeometryShader, " + Graphics::LINE_GEOMETRY_SHADER_FILE + string(" geometry shader creation failed\nHRESULT code: ") + errorString.str());
	}

	ret = Graphics::d3dDevice -> CreateVertexShader(circlesVertexShaderBytecode, circlesVertexShaderSize, NULL, &Graphics::circlesVertexShader);
	if(FAILED(ret)) {
		// Werfe fataler Fehler
		errorString.str("");
		errorString << "0x" << hex << ret << flush;

		throw FatalError("Error occured in ID3D11Device::CreateVertexShader, " + Graphics::CIRCLE_VERTEX_SHADER_FILE + string(" vertex shader creation failed\nHRESULT code: ") + errorString.str());
	}
	ret = Graphics::d3dDevice -> CreateGeometryShader(circlesLineGeometryShaderBytecode, circlesLineGeometryShaderSize, NULL, &Graphics::circlesLineGeometryShader);
	if(FAILED(ret)) {
		// Werfe fataler Fehler
		errorString.str("");
		errorString << "0x" << hex << ret << flush;

		throw FatalError("Error occured in ID3D11Device::CreateGeometryShader, " + Graphics::CIRCLE_LINE_GEOMETRY_SHADER_FILE + string(" geometry shader creation failed\nHRESULT code: ") + errorString.str());
	}
	ret = Graphics::d3dDevice -> CreateGeometryShader(circlesFillGeometryShaderBytecode, circlesFillGeometryShaderSize, NULL, &Graphics::circlesFillGeometryShader);
	if(FAILED(ret)) {
		// Werfe fataler Fehler
		errorString.str("");
		errorString << "0x" << hex << ret << flush;

		throw FatalError("Error occured in ID3D11Device::CreateGeometryShader, " + Graphics::CIRCLE_FILL_GEOMETRY_SHADER_FILE + string(" geometry shader creation failed\nHRESULT code: ") + errorString.str());
	}

	ret = Graphics::d3dDevice -> CreateVertexShader(rectsVertexShaderBytecode, rectsVertexShaderSize, NULL, &Graphics::rectsVertexShader);
	if(FAILED(ret)) {
		// Werfe fataler Fehler
		errorString.str("");
		errorString << "0x" << hex << ret << flush;

		throw FatalError("Error occured in ID3D11Device::CreateVertexShader, " + Graphics::RECT_VERTEX_SHADER_FILE + string(" vertex shader creation failed\nHRESULT code: ") + errorString.str());
	}
	ret = Graphics::d3dDevice -> CreateGeometryShader(rectsLineGeometryShaderBytecode, rectsLineGeometryShaderSize, NULL, &Graphics::rectsLineGeometryShader);
	if(FAILED(ret)) {
		// Werfe fataler Fehler
		errorString.str("");
		errorString << "0x" << hex << ret << flush;

		throw FatalError("Error occured in ID3D11Device::CreateGeometryShader, " + Graphics::RECT_LINE_GEOMETRY_SHADER_FILE + string(" geometry shader creation failed\nHRESULT code: ") + errorString.str());
	}
	ret = Graphics::d3dDevice -> CreateGeometryShader(rectsFillGeometryShaderBytecode, rectsFillGeometryShaderSize, NULL, &Graphics::rectsFillGeometryShader);
	if(FAILED(ret)) {
		// Werfe fataler Fehler
		errorString.str("");
		errorString << "0x" << hex << ret << flush;

		throw FatalError("Error occured in ID3D11Device::CreateGeometryShader, " + Graphics::RECT_FILL_GEOMETRY_SHADER_FILE + string(" geometry shader creation failed\nHRESULT code: ") + errorString.str());
	}

	// Eingabeformate erstellen

	D3D11_INPUT_ELEMENT_DESC texturedSquaresInputElementDesc[3];
	memset(texturedSquaresInputElementDesc, 0, sizeof(D3D11_INPUT_ELEMENT_DESC) * 3);

	texturedSquaresInputElementDesc[0] = { "POSITION", 0, DXGI_FORMAT_R32G32_SINT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 };
	texturedSquaresInputElementDesc[1] = { "POSITION", 1, DXGI_FORMAT_R32_UINT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 };
	texturedSquaresInputElementDesc[2] = { "TEXCOORD", 0, DXGI_FORMAT_R32_UINT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 };

	ret = Graphics::d3dDevice -> CreateInputLayout(texturedSquaresInputElementDesc, 3, texturedSquaresVertexShaderBytecode, texturedSquaresVertexShaderSize, &Graphics::texturedSquaresInputLayout);
	if(FAILED(ret)) {
		// Werfe fataler Fehler
		errorString.str("");
		errorString << "0x" << hex << ret << flush;

		throw FatalError("Error occured in ID3D11Device::CreateInputLayout, textured squares input layout creation failed\nHRESULT code: " + errorString.str());
	}
	Log::Write("textured squares input layout created");

	D3D11_INPUT_ELEMENT_DESC linesInputElementDesc[3];
	memset(linesInputElementDesc, 0, sizeof(D3D11_INPUT_ELEMENT_DESC) * 3);

	linesInputElementDesc[0] = { "POSITION", 0, DXGI_FORMAT_R32G32_SINT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 };
	linesInputElementDesc[1] = { "POSITION", 1, DXGI_FORMAT_R32G32_SINT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 };
	linesInputElementDesc[2] = { "COLOR", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 };

	ret = Graphics::d3dDevice -> CreateInputLayout(linesInputElementDesc, 3, linesVertexShaderBytecode, linesVertexShaderSize, &Graphics::linesInputLayout);
	if(FAILED(ret)) {
		// Werfe fataler Fehler
		errorString.str("");
		errorString << "0x" << hex << ret << flush;

		throw FatalError("Error occured in ID3D11Device::CreateInputLayout, lines input layout creation failed\nHRESULT code: " + errorString.str());
	}
	Log::Write("lines input layout created");

	D3D11_INPUT_ELEMENT_DESC circlesInputElementDesc[4];
	memset(circlesInputElementDesc, 0, sizeof(D3D11_INPUT_ELEMENT_DESC) * 4);

	circlesInputElementDesc[0] = { "POSITION", 0, DXGI_FORMAT_R32G32_SINT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 };
	circlesInputElementDesc[1] = { "COLOR", 0, DXGI_FORMAT_R32_UINT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 };
	circlesInputElementDesc[2] = { "COLOR", 1, DXGI_FORMAT_R32G32_UINT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 };
	circlesInputElementDesc[3] = { "COLOR", 2, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 };

	ret = Graphics::d3dDevice -> CreateInputLayout(circlesInputElementDesc, 4, circlesVertexShaderBytecode, circlesVertexShaderSize, &Graphics::circlesInputLayout);
	if(FAILED(ret)) {
		// Werfe fataler Fehler
		errorString.str("");
		errorString << "0x" << hex << ret << flush;

		throw FatalError("Error occured in ID3D11Device::CreateInputLayout, circles input layout creation failed\nHRESULT code: " + errorString.str());
	}
	Log::Write("circles input layout created");

	D3D11_INPUT_ELEMENT_DESC rectsInputElementDesc[3];
	memset(rectsInputElementDesc, 0, sizeof(D3D11_INPUT_ELEMENT_DESC) * 3);

	rectsInputElementDesc[0] = { "POSITION", 0, DXGI_FORMAT_R32G32_SINT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 };
	rectsInputElementDesc[1] = { "POSITION", 1, DXGI_FORMAT_R32G32_UINT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 };
	rectsInputElementDesc[2] = { "COLOR", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 };

	ret = Graphics::d3dDevice -> CreateInputLayout(rectsInputElementDesc, 3, rectsVertexShaderBytecode, rectsVertexShaderSize, &Graphics::rectsInputLayout);
	if(FAILED(ret)) {
		// Werfe fataler Fehler
		errorString.str("");
		errorString << "0x" << hex << ret << flush;

		throw FatalError("Error occured in ID3D11Device::CreateInputLayout, rects input layout creation failed\nHRESULT code: " + errorString.str());
	}
	Log::Write("rects input layout created");

	// Shadercodes l�schen

	delete texturedSquaresVertexShaderBytecode;
	delete texturedSquaresGeometryShaderBytecode;
	delete texturedSquaresPixelShaderBytecode;

	delete shapesPixelShaderBytecode;

	delete linesVertexShaderBytecode;
	delete linesGeometryShaderBytecode;

	delete circlesVertexShaderBytecode;
	delete circlesLineGeometryShaderBytecode;
	delete circlesFillGeometryShaderBytecode;

	delete rectsVertexShaderBytecode;
	delete rectsLineGeometryShaderBytecode;
	delete rectsFillGeometryShaderBytecode;

	// Render-Ziel, Tiefenbuffer, Schablonenbuffer, Sichtfeld sowie Werte f�r den Transformationsbuffer werden in der Resize-Methode gesetzt

	Graphics::Resize(Window::GetWidth(), Window::GetHeight());

	Log::Write("Graphics creation terminated");
}

void Graphics::Destroy() {
	if(!Graphics::isCreated) {
		// Grafiksystem bereits zerst�rt
		throw string("Attempting to destroy already destroyed graphics singleton");
	}
	Graphics::isCreated = false;

	Log::Write("begin Graphics destruction");

	// Ereignisempf�nger abmelden
	if(Window::IsCreated()) {
		Window::Unregister(&Graphics::eventReciever, Window::WINDOW_SIZE);
	}

	// Aufr�umen
	COM::SafeRelease<ID3D11InputLayout>(&Graphics::rectsInputLayout);
	COM::SafeRelease<ID3D11GeometryShader>(&Graphics::rectsFillGeometryShader);
	COM::SafeRelease<ID3D11GeometryShader>(&Graphics::rectsLineGeometryShader);
	COM::SafeRelease<ID3D11VertexShader>(&Graphics::rectsVertexShader);

	COM::SafeRelease<ID3D11InputLayout>(&Graphics::circlesInputLayout);
	COM::SafeRelease<ID3D11GeometryShader>(&Graphics::circlesFillGeometryShader);
	COM::SafeRelease<ID3D11GeometryShader>(&Graphics::circlesLineGeometryShader);
	COM::SafeRelease<ID3D11VertexShader>(&Graphics::circlesVertexShader);

	COM::SafeRelease<ID3D11InputLayout>(&Graphics::linesInputLayout);
	COM::SafeRelease<ID3D11GeometryShader>(&Graphics::linesGeometryShader);
	COM::SafeRelease<ID3D11VertexShader>(&Graphics::linesVertexShader);
	
	COM::SafeRelease<ID3D11PixelShader>(&Graphics::shapesPixelShader);

	COM::SafeRelease<ID3D11ShaderResourceView>(&Graphics::squaresTextureView);
	COM::SafeRelease<ID3D11Texture2D>(&Graphics::squaresTexture);
	COM::SafeRelease<ID3D11InputLayout>(&Graphics::texturedSquaresInputLayout);
	COM::SafeRelease<ID3D11PixelShader>(&Graphics::texturedSquaresPixelShader);
	COM::SafeRelease<ID3D11GeometryShader>(&Graphics::texturedSquaresGeometryShader);
	COM::SafeRelease<ID3D11VertexShader>(&Graphics::texturedSquaresVertexShader);

	COM::SafeRelease<IDWriteTextFormat>(&Graphics::dwTextFormat);

	COM::SafeRelease<ID3D11Buffer>(&Graphics::dataBuffer);
	COM::SafeRelease<ID3D11SamplerState>(&Graphics::pixelatedSampler);
	COM::SafeRelease<ID3D11Buffer>(&Graphics::transformBuffer);

	COM::SafeRelease<ID2D1SolidColorBrush>(&Graphics::d2dFontBrush);
	COM::SafeRelease<ID2D1RenderTarget>(&Graphics::d2dRenderTarget);
	COM::SafeRelease<ID3D11RenderTargetView>(&Graphics::d3dRenderTargetView);

	COM::SafeRelease<IDWriteFactory>(&Graphics::dwFactory);
	COM::SafeRelease<ID2D1Factory>(&Graphics::d2dFactory);
	COM::SafeRelease<IWICImagingFactory>(&Graphics::wicFactory);

	COM::SafeRelease<IDXGISwapChain>(&Graphics::dxgiSwapChain);
	COM::SafeRelease<ID3D11DeviceContext>(&Graphics::d3dDeviceContext);
	COM::SafeRelease<ID3D11Device>(&Graphics::d3dDevice);

	Log::Write("Graphics destruction terminated");
}

void Graphics::DrawTexturedSquares(TexturedSquare* texturedSquares, uint32_t squareListSize) {
	Graphics::AssertCreated();

	if(Graphics::texturedSquaresCacheSize + squareListSize < MAX_SQUARE_LIST_SIZE) {
		memcpy(&Graphics::texturedSquaresCache[Graphics::texturedSquaresCacheSize], texturedSquares, squareListSize * sizeof(TexturedSquare));
		Graphics::texturedSquaresCacheSize += squareListSize;
	} else {
		stringstream ss;
		ss << MAX_SQUARE_LIST_SIZE - Graphics::texturedSquaresCacheSize << " slots left, " << squareListSize << " supplied" << flush;
		throw Error("Error occured in Graphics::DrawTexturedSquares, squares buffer overflow. There are " + ss.str());
	}
}

void Graphics::DrawLines(Line* lines, uint32_t lineListSize) {
	Graphics::AssertCreated();

	if(Graphics::linesCacheSize + lineListSize < MAX_LINE_LIST_SIZE) {
		memcpy(&Graphics::linesCache[Graphics::linesCacheSize], lines, lineListSize * sizeof(Line));
		Graphics::linesCacheSize += lineListSize;
	} else {
		stringstream ss;
		ss << MAX_LINE_LIST_SIZE - Graphics::linesCacheSize << " slots left, " << lineListSize << " supplied" << flush;
		throw Error("Error occured in Graphics::DrawLines, lines buffer overflow. There are " + ss.str());
	}
}

void Graphics::DrawCircles(Circle* circles, uint32_t circleListSize) {
	Graphics::AssertCreated();

	if(Graphics::circlesCacheSize + circleListSize < MAX_CIRCLE_LIST_SIZE) {
		memcpy(&Graphics::circlesCache[Graphics::circlesCacheSize], circles, circleListSize * sizeof(Circle));
		Graphics::circlesCacheSize += circleListSize;
	} else {
		stringstream ss;
		ss << MAX_CIRCLE_LIST_SIZE - Graphics::circlesCacheSize << " slots left, " << circleListSize << " supplied" << flush;
		throw Error("Error occured in Graphics::DrawCircles, circles buffer overflow. There are " + ss.str());
	}
}

void Graphics::FillCircles(Circle* circles, uint32_t circleListSize) {
	Graphics::AssertCreated();

	if(Graphics::filledCirclesCacheSize + circleListSize < MAX_CIRCLE_LIST_SIZE) {
		memcpy(&Graphics::filledCirclesCache[Graphics::filledCirclesCacheSize], circles, circleListSize * sizeof(Circle));
		Graphics::filledCirclesCacheSize += circleListSize;
	} else {
		stringstream ss;
		ss << MAX_CIRCLE_LIST_SIZE - Graphics::filledCirclesCacheSize << " slots left, " << circleListSize << " supplied" << flush;
		throw Error("Error occured in Graphics::FillCircles, circles buffer overflow. There are " + ss.str());
	}
}

void Graphics::DrawRects(Rect* rects, uint32_t rectListSize) {
	Graphics::AssertCreated();

	if(Graphics::rectsCacheSize + rectListSize < MAX_RECT_LIST_SIZE) {
		memcpy(&Graphics::rectsCache[Graphics::rectsCacheSize], rects, rectListSize * sizeof(Rect));
		Graphics::rectsCacheSize += rectListSize;
	} else {
		stringstream ss;
		ss << MAX_RECT_LIST_SIZE - Graphics::rectsCacheSize << " slots left, " << rectListSize << " supplied" << flush;
		throw Error("Error occured in Graphics::DrawRects, rects buffer overflow. There are " + ss.str());
	}
}

void Graphics::FillRects(Rect* rects, uint32_t rectListSize) {
	Graphics::AssertCreated();

	if(Graphics::filledRectsCacheSize + rectListSize < MAX_RECT_LIST_SIZE) {
		memcpy(&Graphics::filledRectsCache[Graphics::filledRectsCacheSize], rects, rectListSize * sizeof(Rect));
		Graphics::filledRectsCacheSize += rectListSize;
	} else {
		stringstream ss;
		ss << MAX_RECT_LIST_SIZE - Graphics::filledRectsCacheSize << " slots left, " << rectListSize << " supplied" << flush;
		throw Error("Error occured in Graphics::FillRects, rects buffer overflow. There are " + ss.str());
	}
}

void Graphics::FlushTexturedSquares() {
	Graphics::AssertCreated();

	if(Graphics::texturedSquaresCacheSize == 0) {
		return;
	}

	// Variablen f�r Fehlerbehandlung
	HRESULT ret;
	stringstream errorString;

	// Daten in VRAM laden

	D3D11_MAPPED_SUBRESOURCE dataSubresource;
	ret = Graphics::d3dDeviceContext -> Map(Graphics::dataBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &dataSubresource);
	if(FAILED(ret)) {
		// Werfe Fehler
		errorString.str("");
		errorString << "0x" << hex << ret << flush;

		throw Error("Error occured in ID3D11DeviceContext::Map, textured squares buffer mapping failed\nHRESULT code: " + errorString.str());
	}
	memcpy(dataSubresource.pData, Graphics::texturedSquaresCache, Graphics::texturedSquaresCacheSize * sizeof(TexturedSquare));
	Graphics::d3dDeviceContext -> Unmap(Graphics::dataBuffer, 0);

	// D3D-Objekte setzen

	Graphics::d3dDeviceContext -> VSSetShader(Graphics::texturedSquaresVertexShader, NULL, 0);
	Graphics::d3dDeviceContext -> GSSetShader(Graphics::texturedSquaresGeometryShader, NULL, 0);
	Graphics::d3dDeviceContext -> PSSetShader(Graphics::texturedSquaresPixelShader, NULL, 0);

	Graphics::d3dDeviceContext -> IASetInputLayout(Graphics::texturedSquaresInputLayout);
	uint32_t stride = sizeof(TexturedSquare);
	uint32_t offset = 0;
	Graphics::d3dDeviceContext -> IASetVertexBuffers(0, 1, &Graphics::dataBuffer, &stride, &offset);
	Graphics::d3dDeviceContext -> IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_POINTLIST);

	Graphics::d3dDeviceContext -> PSSetShaderResources(0, 1, &Graphics::squaresTextureView);
	Graphics::d3dDeviceContext -> PSSetSamplers(0, 1, &Graphics::pixelatedSampler);
	Graphics::d3dDeviceContext -> GSSetConstantBuffers(0, 1, &Graphics::transformBuffer);

	// Zeichen

	Graphics::d3dDeviceContext -> Draw(Graphics::texturedSquaresCacheSize, 0);

	// Reset-Variabeln (f�r **)

	ID3D11ShaderResourceView* resetShaderResource = NULL;
	ID3D11Buffer* resetBuffer = NULL;
	ID3D11SamplerState* resetSampler = NULL;

	// D3D-Objekte zur�cksetzen

	Graphics::d3dDeviceContext -> VSSetShader(NULL, NULL, 0);
	Graphics::d3dDeviceContext -> GSSetShader(NULL, NULL, 0);
	Graphics::d3dDeviceContext -> PSSetShader(NULL, NULL, 0);

	Graphics::d3dDeviceContext -> IASetInputLayout(NULL);
	stride = 0;
	offset = 0;
	Graphics::d3dDeviceContext -> IASetVertexBuffers(0, 1, &resetBuffer, &stride, &offset);

	Graphics::d3dDeviceContext -> PSSetShaderResources(0, 1, &resetShaderResource);
	Graphics::d3dDeviceContext -> PSSetSamplers(0, 1, &resetSampler);
	Graphics::d3dDeviceContext -> GSSetConstantBuffers(0, 1, &resetBuffer);

	// cache zur�cksetzen
	
	Graphics::texturedSquaresCacheSize = 0;
}

void Graphics::FlushLines() {
	Graphics::AssertCreated();

	// Variablen f�r Fehlerbehandlung
	HRESULT ret;
	stringstream errorString;

	// Daten in VRAM lden

	D3D11_MAPPED_SUBRESOURCE dataSubresource;
	ret = Graphics::d3dDeviceContext -> Map(Graphics::dataBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &dataSubresource);
	if(FAILED(ret)) {
		// Werfe Fehler
		errorString.str("");
		errorString << "0x" << hex << ret << flush;

		throw Error("Error occured in ID3D11DeviceContext::Map, lines buffer mapping failed\nHRESULT code: " + errorString.str());
	}
	memcpy(dataSubresource.pData, Graphics::linesCache, Graphics::linesCacheSize * sizeof(Line));
	Graphics::d3dDeviceContext -> Unmap(Graphics::dataBuffer, 0);

	// D3D-Objekte setzen

	Graphics::d3dDeviceContext -> VSSetShader(Graphics::linesVertexShader, NULL, 0);
	Graphics::d3dDeviceContext -> GSSetShader(Graphics::linesGeometryShader, NULL, 0);
	Graphics::d3dDeviceContext -> PSSetShader(Graphics::shapesPixelShader, NULL, 0);

	Graphics::d3dDeviceContext -> IASetInputLayout(Graphics::linesInputLayout);
	uint32_t stride = sizeof(Line);
	uint32_t offset = 0;
	Graphics::d3dDeviceContext -> IASetVertexBuffers(0, 1, &Graphics::dataBuffer, &stride, &offset);
	Graphics::d3dDeviceContext -> IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_POINTLIST);

	Graphics::d3dDeviceContext -> GSSetConstantBuffers(0, 1, &Graphics::transformBuffer);

	// Zeichnen

	Graphics::d3dDeviceContext -> Draw(Graphics::linesCacheSize, 0);

	// Reset-Variabeln (f�r **)

	ID3D11Buffer* resetBuffer = NULL;

	// D3D-Objekte zur�cksetzen

	Graphics::d3dDeviceContext -> VSSetShader(NULL, NULL, 0);
	Graphics::d3dDeviceContext -> GSSetShader(NULL, NULL, 0);
	Graphics::d3dDeviceContext -> PSSetShader(NULL, NULL, 0);

	Graphics::d3dDeviceContext -> IASetInputLayout(NULL);
	stride = 0;
	offset = 0;
	Graphics::d3dDeviceContext -> IASetVertexBuffers(0, 1, &resetBuffer, &stride, &offset);

	Graphics::d3dDeviceContext -> GSSetConstantBuffers(0, 1, &resetBuffer);

	// Cache zur�cksetzen

	Graphics::linesCacheSize = 0;
}

void Graphics::FlushCircles() {
	Graphics::AssertCreated();

	// Variablen f�r Fehlerbehandlung
	HRESULT ret;
	stringstream errorString;

	// Daten in VRAM lden

	D3D11_MAPPED_SUBRESOURCE dataSubresource;
	ret = Graphics::d3dDeviceContext -> Map(Graphics::dataBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &dataSubresource);
	if(FAILED(ret)) {
		// Werfe Fehler
		errorString.str("");
		errorString << "0x" << hex << ret << flush;

		throw Error("Error occured in ID3D11DeviceContext::Map, circles buffer mapping failed\nHRESULT code: " + errorString.str());
	}
	memcpy(dataSubresource.pData, Graphics::circlesCache, Graphics::circlesCacheSize * sizeof(Circle));
	Graphics::d3dDeviceContext -> Unmap(Graphics::dataBuffer, 0);

	// D3D-Objekte setzen

	Graphics::d3dDeviceContext -> VSSetShader(Graphics::circlesVertexShader, NULL, 0);
	Graphics::d3dDeviceContext -> GSSetShader(Graphics::circlesLineGeometryShader, NULL, 0);
	Graphics::d3dDeviceContext -> PSSetShader(Graphics::shapesPixelShader, NULL, 0);

	Graphics::d3dDeviceContext -> IASetInputLayout(Graphics::circlesInputLayout);
	uint32_t stride = sizeof(Circle);
	uint32_t offset = 0;
	Graphics::d3dDeviceContext -> IASetVertexBuffers(0, 1, &Graphics::dataBuffer, &stride, &offset);
	Graphics::d3dDeviceContext -> IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_POINTLIST);

	Graphics::d3dDeviceContext -> GSSetConstantBuffers(0, 1, &Graphics::transformBuffer);

	// Zeichnen

	Graphics::d3dDeviceContext -> Draw(Graphics::circlesCacheSize, 0);

	// Reset-Variabeln (f�r **)

	ID3D11Buffer* resetBuffer = NULL;

	// D3D-Objekte zur�cksetzen

	Graphics::d3dDeviceContext -> VSSetShader(NULL, NULL, 0);
	Graphics::d3dDeviceContext -> GSSetShader(NULL, NULL, 0);
	Graphics::d3dDeviceContext -> PSSetShader(NULL, NULL, 0);

	Graphics::d3dDeviceContext -> IASetInputLayout(NULL);
	stride = 0;
	offset = 0;
	Graphics::d3dDeviceContext -> IASetVertexBuffers(0, 1, &resetBuffer, &stride, &offset);

	Graphics::d3dDeviceContext -> GSSetConstantBuffers(0, 1, &resetBuffer);

	// Cache zur�cksetzen

	Graphics::circlesCacheSize = 0;
}

void Graphics::FlushFilledCircles() {
	Graphics::AssertCreated();

	// Variablen f�r Fehlerbehandlung
	HRESULT ret;
	stringstream errorString;

	// Daten in VRAM lden

	D3D11_MAPPED_SUBRESOURCE dataSubresource;
	ret = Graphics::d3dDeviceContext -> Map(Graphics::dataBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &dataSubresource);
	if(FAILED(ret)) {
		// Werfe Fehler
		errorString.str("");
		errorString << "0x" << hex << ret << flush;

		throw Error("Error occured in ID3D11DeviceContext::Map, circles buffer mapping failed\nHRESULT code: " + errorString.str());
	}
	memcpy(dataSubresource.pData, Graphics::filledCirclesCache, Graphics::filledCirclesCacheSize * sizeof(Circle));
	Graphics::d3dDeviceContext -> Unmap(Graphics::dataBuffer, 0);

	// D3D-Objekte setzen

	Graphics::d3dDeviceContext -> VSSetShader(Graphics::circlesVertexShader, NULL, 0);
	Graphics::d3dDeviceContext -> GSSetShader(Graphics::circlesFillGeometryShader, NULL, 0);
	Graphics::d3dDeviceContext -> PSSetShader(Graphics::shapesPixelShader, NULL, 0);

	Graphics::d3dDeviceContext -> IASetInputLayout(Graphics::circlesInputLayout);
	uint32_t stride = sizeof(Circle);
	uint32_t offset = 0;
	Graphics::d3dDeviceContext -> IASetVertexBuffers(0, 1, &Graphics::dataBuffer, &stride, &offset);
	Graphics::d3dDeviceContext -> IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_POINTLIST);

	Graphics::d3dDeviceContext -> GSSetConstantBuffers(0, 1, &Graphics::transformBuffer);

	// Zeichnen

	Graphics::d3dDeviceContext -> Draw(Graphics::filledCirclesCacheSize, 0);

	// Reset-Variabeln (f�r **)

	ID3D11Buffer* resetBuffer = NULL;

	// D3D-Objekte zur�cksetzen

	Graphics::d3dDeviceContext -> VSSetShader(NULL, NULL, 0);
	Graphics::d3dDeviceContext -> GSSetShader(NULL, NULL, 0);
	Graphics::d3dDeviceContext -> PSSetShader(NULL, NULL, 0);

	Graphics::d3dDeviceContext -> IASetInputLayout(NULL);
	stride = 0;
	offset = 0;
	Graphics::d3dDeviceContext -> IASetVertexBuffers(0, 1, &resetBuffer, &stride, &offset);

	Graphics::d3dDeviceContext -> GSSetConstantBuffers(0, 1, &resetBuffer);

	// Cache zur�cksetzen

	Graphics::filledCirclesCacheSize = 0;
}

void Graphics::FlushRects() {
	Graphics::AssertCreated();

	// Variablen f�r Fehlerbehandlung
	HRESULT ret;
	stringstream errorString;

	// Daten in VRAM lden

	D3D11_MAPPED_SUBRESOURCE dataSubresource;
	ret = Graphics::d3dDeviceContext -> Map(Graphics::dataBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &dataSubresource);
	if(FAILED(ret)) {
		// Werfe Fehler
		errorString.str("");
		errorString << "0x" << hex << ret << flush;

		throw Error("Error occured in ID3D11DeviceContext::Map, rects buffer mapping failed\nHRESULT code: " + errorString.str());
	}
	memcpy(dataSubresource.pData, Graphics::rectsCache, Graphics::rectsCacheSize * sizeof(Rect));
	Graphics::d3dDeviceContext -> Unmap(Graphics::dataBuffer, 0);

	// D3D-Objekte setzen

	Graphics::d3dDeviceContext -> VSSetShader(Graphics::rectsVertexShader, NULL, 0);
	Graphics::d3dDeviceContext -> GSSetShader(Graphics::rectsLineGeometryShader, NULL, 0);
	Graphics::d3dDeviceContext -> PSSetShader(Graphics::shapesPixelShader, NULL, 0);

	Graphics::d3dDeviceContext -> IASetInputLayout(Graphics::rectsInputLayout);
	uint32_t stride = sizeof(Rect);
	uint32_t offset = 0;
	Graphics::d3dDeviceContext -> IASetVertexBuffers(0, 1, &Graphics::dataBuffer, &stride, &offset);
	Graphics::d3dDeviceContext -> IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_POINTLIST);

	Graphics::d3dDeviceContext -> GSSetConstantBuffers(0, 1, &Graphics::transformBuffer);

	// Zeichnen

	Graphics::d3dDeviceContext -> Draw(Graphics::rectsCacheSize, 0);

	// Reset-Variabeln (f�r **)

	ID3D11Buffer* resetBuffer = NULL;

	// D3D-Objekte zur�cksetzen

	Graphics::d3dDeviceContext -> VSSetShader(NULL, NULL, 0);
	Graphics::d3dDeviceContext -> GSSetShader(NULL, NULL, 0);
	Graphics::d3dDeviceContext -> PSSetShader(NULL, NULL, 0);

	Graphics::d3dDeviceContext -> IASetInputLayout(NULL);
	stride = 0;
	offset = 0;
	Graphics::d3dDeviceContext -> IASetVertexBuffers(0, 1, &resetBuffer, &stride, &offset);

	Graphics::d3dDeviceContext -> GSSetConstantBuffers(0, 1, &resetBuffer);

	// Cache zur�cksetzen

	Graphics::rectsCacheSize = 0;
}

void Graphics::FlushFilledRects() {
	Graphics::AssertCreated();

	// Variablen f�r Fehlerbehandlung
	HRESULT ret;
	stringstream errorString;

	// Daten in VRAM lden

	D3D11_MAPPED_SUBRESOURCE dataSubresource;
	ret = Graphics::d3dDeviceContext -> Map(Graphics::dataBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &dataSubresource);
	if(FAILED(ret)) {
		// Werfe Fehler
		errorString.str("");
		errorString << "0x" << hex << ret << flush;

		throw Error("Error occured in ID3D11DeviceContext::Map, rects buffer mapping failed\nHRESULT code: " + errorString.str());
	}
	memcpy(dataSubresource.pData, Graphics::filledRectsCache, Graphics::filledRectsCacheSize * sizeof(Rect));
	Graphics::d3dDeviceContext -> Unmap(Graphics::dataBuffer, 0);

	// D3D-Objekte setzen

	Graphics::d3dDeviceContext -> VSSetShader(Graphics::rectsVertexShader, NULL, 0);
	Graphics::d3dDeviceContext -> GSSetShader(Graphics::rectsFillGeometryShader, NULL, 0);
	Graphics::d3dDeviceContext -> PSSetShader(Graphics::shapesPixelShader, NULL, 0);

	Graphics::d3dDeviceContext -> IASetInputLayout(Graphics::rectsInputLayout);
	uint32_t stride = sizeof(Rect);
	uint32_t offset = 0;
	Graphics::d3dDeviceContext -> IASetVertexBuffers(0, 1, &Graphics::dataBuffer, &stride, &offset);
	Graphics::d3dDeviceContext -> IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_POINTLIST);

	Graphics::d3dDeviceContext -> GSSetConstantBuffers(0, 1, &Graphics::transformBuffer);

	// Zeichnen

	Graphics::d3dDeviceContext -> Draw(Graphics::filledRectsCacheSize, 0);

	// Reset-Variabeln (f�r **)

	ID3D11Buffer* resetBuffer = NULL;

	// D3D-Objekte zur�cksetzen

	Graphics::d3dDeviceContext -> VSSetShader(NULL, NULL, 0);
	Graphics::d3dDeviceContext -> GSSetShader(NULL, NULL, 0);
	Graphics::d3dDeviceContext -> PSSetShader(NULL, NULL, 0);

	Graphics::d3dDeviceContext -> IASetInputLayout(NULL);
	stride = 0;
	offset = 0;
	Graphics::d3dDeviceContext -> IASetVertexBuffers(0, 1, &resetBuffer, &stride, &offset);

	Graphics::d3dDeviceContext -> GSSetConstantBuffers(0, 1, &resetBuffer);

	// Cache zur�cksetzen

	Graphics::filledRectsCacheSize = 0;
}

void Graphics::AddTextToQueue(wstring text, uint32_t x, uint32_t y, float red, float green, float blue) {
	Graphics::AssertCreated();

	Graphics::textToDraw.push_back(TextBox());
	Graphics::textToDraw.back().text = text;
	Graphics::textToDraw.back().x = x;
	Graphics::textToDraw.back().y = y;
	Graphics::textToDraw.back().color.red = red;
	Graphics::textToDraw.back().color.green = green;
	Graphics::textToDraw.back().color.blue = blue;
}

void Graphics::DrawTextQueue() {
	Graphics::AssertCreated();

	// Zeichne mit D2D

	Graphics::d2dRenderTarget -> BeginDraw();

	Graphics::d2dRenderTarget -> SetTransform(D2D1::IdentityMatrix());

	// Zeichne jeder Text (ignoriere Breite und H�he der Textbox)

	for(uint32_t i = 0; i < Graphics::textToDraw.size(); i++) {
		D2D1_RECT_F textRect = D2D1::RectF(
			(float)Graphics::textToDraw[i].x,
			(float)(Window::GetHeight() - Graphics::textToDraw[i].y - 1),
			1000000.0f,
			1000000.0f);

		Graphics::d2dFontBrush -> SetColor(D2D1::ColorF(Graphics::textToDraw[i].color.red, Graphics::textToDraw[i].color.green, Graphics::textToDraw[i].color.blue, 1.0f));

		Graphics::d2dRenderTarget -> DrawTextA(Graphics::textToDraw[i].text.c_str(), (uint32_t)Graphics::textToDraw[i].text.length(), Graphics::dwTextFormat, textRect, Graphics::d2dFontBrush, D2D1_DRAW_TEXT_OPTIONS_NONE, DWRITE_MEASURING_MODE_NATURAL);
	}

	// Schreibe Daten in Buffer

	Graphics::d2dRenderTarget -> EndDraw();

	// L�sche Textboxliste

	Graphics::textToDraw.clear();
}

void Graphics::AssertCreated() {
	if(!Graphics::isCreated) {
		// Werfe nicht fataler Fehler
		throw Error("Cannot access member functions of Graphics while not created");
	}
}

void Graphics::Resize(uint32_t width, uint32_t height) {
	Graphics::AssertCreated();

	Log::Write("begin resize");

	// Variablen f�r Fehlerbehandlung
	HRESULT ret;
	stringstream errorString;

	float widthF = (float)width;
	float heightF = (float)height;

	// Alte Objekte l�schen
	COM::SafeRelease<ID3D11RenderTargetView>(&Graphics::d3dRenderTargetView);
	COM::SafeRelease<ID2D1RenderTarget>(&Graphics::d2dRenderTarget);
	COM::SafeRelease<ID2D1SolidColorBrush>(&Graphics::d2dFontBrush);

	// Bufferkette anpassen
	ret = Graphics::dxgiSwapChain -> ResizeBuffers(2, width, height, DXGI_FORMAT_UNKNOWN, 0);
	if(FAILED(ret)) {
		// Werfe fataler Fehler
		errorString.str("");
		errorString << "0x" << hex << ret << flush;

		throw FatalError("Error occured in IDXGISwapChain::Resize, SwapChain resize failed\nHRESULT code: " + errorString.str());
	}
	Log::Write("DXGI SwapChain resized");

	// Erstelle Render-Ziel

	ID3D11Texture2D* backBuffer = nullptr;
	ret = Graphics::dxgiSwapChain -> GetBuffer(0, __uuidof(ID3D11Texture2D), (void**)&backBuffer);
	if(FAILED(ret)) {
		// Werfe fataler Fehler
		errorString.str();
		errorString << "0x" << hex << ret << flush;

		throw FatalError("Error occured in DXGISwapChain::GetBuffer, Backbuffer query failed after resize\nHRESULT code: " + errorString.str());
	}
	ret = Graphics::d3dDevice -> CreateRenderTargetView(backBuffer, 0, &Graphics::d3dRenderTargetView);
	if(FAILED(ret)) {
		// Werfe fataler Fehler
		errorString.str();
		errorString << "0x" << hex << ret << flush;

		throw FatalError("Error occured in D3D11Device::CreateRenderTargetView, RenderTargetView creation failed after resize\nHRESULT code: " + errorString.str());
	}
	COM::SafeRelease<ID3D11Texture2D>(&backBuffer);
	Log::Write("D3D RenderTargetView resized");

	// Binde Ausgabeelemente

	Graphics::d3dDeviceContext -> OMSetRenderTargets(1, &Graphics::d3dRenderTargetView, NULL);

	// Passe Sichtfeld an

	D3D11_VIEWPORT viewport;
	memset(&viewport, 0, sizeof(D3D11_VIEWPORT));
	viewport.TopLeftX = 0.0f;
	viewport.TopLeftY = 0.0f;
	viewport.Width = widthF;
	viewport.Height = heightF;
	viewport.MinDepth = 0.0f;
	viewport.MaxDepth = 1.0f;

	Graphics::d3dDeviceContext -> RSSetViewports(1, &viewport);
	Log::Write("Viewport adjusted");

	// Erstelle D2D-Renderziel, verwende dazu den gleichen Backbuffer

	IDXGISurface* dxgiBackBuffer = nullptr;
	ret = Graphics::dxgiSwapChain -> GetBuffer(0, __uuidof(IDXGISurface), (void**)&dxgiBackBuffer);
	if(FAILED(ret)) {
		// Werfe fataler Fehler
		errorString.str("");
		errorString << "0x" << hex << ret << flush;

		throw FatalError("Error occured in IDXGISwapChain::GetBuffer, Backbuffer query failed\nHRESULT code: " + errorString.str());
	}
	DXGI_SURFACE_DESC backBufferDesc;
	ret = dxgiBackBuffer -> GetDesc(&backBufferDesc);
	if(FAILED(ret)) {
		// Werfe fataler Fehler
		errorString.str("");
		errorString << "0x" << hex << ret << flush;

		throw FatalError("Error occured in IDXGISurface::GetDesc, Surface description query failed\nHRESULT code: " + errorString.str());
	}
	D2D1_RENDER_TARGET_PROPERTIES d2dRenderTargetProperties = D2D1::RenderTargetProperties(D2D1_RENDER_TARGET_TYPE_HARDWARE, D2D1::PixelFormat(DXGI_FORMAT_R8G8B8A8_UNORM, D2D1_ALPHA_MODE_PREMULTIPLIED));
	ret = Graphics::d2dFactory -> CreateDxgiSurfaceRenderTarget(dxgiBackBuffer, d2dRenderTargetProperties, &Graphics::d2dRenderTarget);
	if(FAILED(ret)) {
		// Werfe fataler Fehler
		errorString.str("");
		errorString << "0x" << hex << ret << flush;

		throw FatalError("Error occured in ID2D1Factory::CreateDxgiSurfaceRenderTarget, D2D render target creation failed\nHRESULT code: " + errorString.str());
	}
	COM::SafeRelease<IDXGISurface>(&dxgiBackBuffer);
	Log::Write("D2D render target created");

	// Erstelle D2D Pinsel

	ret = Graphics::d2dRenderTarget -> CreateSolidColorBrush(D2D1::ColorF(0.8f, 0.8f, 0.8f, 1.0f), &Graphics::d2dFontBrush);
	if(FAILED(ret)) {
		// Werfe fataler Fehler
		errorString.str("");
		errorString << "0x" << hex << ret << flush;

		throw FatalError("Error occured in ID2D1RenderTarget::CreateSolidColorBrush, D2D font brush creation failed\nHRESULT code: " + errorString.str());
	}

	// Setze allgemeiner Transformationsbuffer

	D3D11_MAPPED_SUBRESOURCE transformSubresource;
	ret = Graphics::d3dDeviceContext -> Map(Graphics::transformBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &transformSubresource);
	if(FAILED(ret)) {
		// Werfe fataler Fehler
		errorString.str("");
		errorString << "0x" << hex << ret << flush;

		throw FatalError("Error occured in ID3D11DeviceContext::Map, transform buffer mapping failed\nHRESULT code: " + errorString.str());
	}
	float zn = 0.0f;
	float zf = 1.0f;

	float transformMatrix[16] = {
		2.0f / widthF, 0.0f, 0.0f, 0.0f,
		0.0f, 2.0f / heightF, 0.0f, 0.0f,
		0.0f, 0.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, 0.0f, 1.0f
	};
	memcpy(transformSubresource.pData, transformMatrix, sizeof(float) * 16);
	Graphics::d3dDeviceContext -> Unmap(Graphics::transformBuffer, 0);
	Log::Write("Transform buffer set");

	Log::Write("graphics resize terminated");
}

void Graphics::LoadTexture(string fileName, ID3D11Texture2D** texture, ID3D11ShaderResourceView** shaderResourceView) {
	Graphics::AssertCreated();

	Log::Write("begin texture loading for " + fileName);

	// Variablen f�r Fehlerbehandlung
	HRESULT ret;
	stringstream errorString;

	// Erstelle PNG-Entschl�ssler

	IWICBitmapDecoder* decoder;
	ret = Graphics::wicFactory -> CreateDecoderFromFilename(Graphics::stringConverter.from_bytes(fileName).c_str(), NULL, GENERIC_READ, WICDecodeMetadataCacheOnDemand, &decoder);
	if(FAILED(ret)) {
		// Werfe fataler Fehler
		errorString.str("");
		errorString << "0x" << hex << ret << flush;

		throw FatalError("Error occured in IWICImagingFactory::CreateDecoderFromFilename, image file decoder creation failed\nHRESULT code: " + errorString.str());
	}

	// Bilddaten befinden sich in der ersten Frame (PNG ohne multisampling / layers)

	IWICBitmapFrameDecode* frameDecode;
	ret = decoder -> GetFrame(0, &frameDecode);
	if(FAILED(ret)) {
		// Werfe fataler Fehler
		errorString.str("");
		errorString << "0x" << hex << ret << flush;

		throw FatalError("Error occured in IWICBitmapDecoder::GetFrame, unable to fetch image frame\nHRESULT code: " + errorString.str());
	}

	// �berpr�fe Pixelformat

	WICPixelFormatGUID pixelFormat;
	ret = frameDecode -> GetPixelFormat(&pixelFormat);
	if(FAILED(ret)) {
		// Werfe fataler Fehler
		errorString.str("");
		errorString << "0x" << hex << ret << flush;

		throw FatalError("Error occured in IWICBitmapFrameDecode::GetPixelFormat, unable to fetch pixel format\nHRESULT code: " + errorString.str());
	}
	if(pixelFormat != GUID_WICPixelFormat32bppBGRA) {
		// Falsches Pixelformat
		errorString.str("");
		errorString << "Found: " << hex << pixelFormat.Data1 << "," << pixelFormat.Data2 << "," << pixelFormat.Data3 << "," << pixelFormat.Data4 << " Required: " << hex << GUID_WICPixelFormat32bppBGRA.Data1 << "," << GUID_WICPixelFormat32bppBGRA.Data2 << "," << GUID_WICPixelFormat32bppBGRA.Data3 << "," << GUID_WICPixelFormat32bppBGRA.Data4 << flush;

		throw FatalError("Texture file has wrong pixel format. " + errorString.str());
	}

	// Kopiere Pixel

	char* buffer;
	uint32_t width;
	uint32_t height;
	uint32_t rowPitch;
	uint32_t size;
	ret = frameDecode -> GetSize(&width, &height);
	if(FAILED(ret)) {
		// Werfe fataler Fehler
		errorString.str("");
		errorString << "0x" << hex << ret << flush;

		throw FatalError("Error occured in IWICBitmapFrameDecode::GetSize, unable to fetch size\nHRESULT code: " + errorString.str());
	}
	rowPitch = (width * 32 + 7) / 8;
	size = rowPitch * height;
	buffer = new char[size];
	ret = frameDecode -> CopyPixels(NULL, rowPitch, size, (BYTE*)buffer);
	if(FAILED(ret)) {
		// Werfe fataler Fehler
		errorString.str("");
		errorString << "0x" << hex << ret << flush;

		throw FatalError("Error occured in IWICBitmapFrameDecode::CopyPixels, unable to load bitmap data into memory\nHRESULT code: " + errorString.str());
	}

	// Erstelle D3D Textur

	D3D11_TEXTURE2D_DESC textureDesc;
	D3D11_SUBRESOURCE_DATA textureData;
	memset(&textureDesc, 0, sizeof(D3D11_TEXTURE2D_DESC));
	memset(&textureData, 0, sizeof(D3D11_SUBRESOURCE_DATA));

	textureDesc.ArraySize = 1;
	textureDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
	textureDesc.CPUAccessFlags = 0;
	textureDesc.Format = DXGI_FORMAT_B8G8R8A8_UNORM; // Format der PNG-Datei, hlsl wandelt automatisch um
	textureDesc.Width = width;
	textureDesc.Height = height;
	textureDesc.MipLevels = 1;
	textureDesc.MiscFlags = 0;
	textureDesc.SampleDesc.Count = 1;
	textureDesc.SampleDesc.Quality = 0;
	textureDesc.Usage = D3D11_USAGE_IMMUTABLE;

	textureData.pSysMem = buffer;
	textureData.SysMemPitch = rowPitch;
	textureData.SysMemSlicePitch = size;

	ret = Graphics::d3dDevice -> CreateTexture2D(&textureDesc, &textureData, texture);
	if(FAILED(ret)) {
		// Werfe fataler Fehler
		errorString.str("");
		errorString << "0x" << hex << ret << flush;

		throw FatalError("Error occured in ID3D11Device::CreateTexture2D, texture creation from file failed\nHRESULT code: " + errorString.str());
	}

	// Erstelle Shaderresourcenansicht

	D3D11_SHADER_RESOURCE_VIEW_DESC textureViewDesc;
	memset(&textureViewDesc, 0, sizeof(D3D11_SHADER_RESOURCE_VIEW_DESC));

	textureViewDesc.Texture2D.MipLevels = 1;
	textureViewDesc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
	textureViewDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;

	ret = Graphics::d3dDevice -> CreateShaderResourceView(*texture, &textureViewDesc, shaderResourceView);
	if(FAILED(ret)) {
		// Werfe fataler Fehler
		errorString.str("");
		errorString << "0x" << hex << ret << flush;

		throw FatalError("Error occured in ID3D11Device::CreateShaderResourceView, texture shader resource view creation failed\nHRESULT code: " + errorString.str());
	}

	// Aufr�umen

	COM::SafeRelease<IWICBitmapFrameDecode>(&frameDecode);
	COM::SafeRelease<IWICBitmapDecoder>(&decoder);
	delete buffer;

	Log::Write("texture loading terminated");
}

void Graphics::LoadShader(string fileName, char** buffer, uint32_t* size) {
	Graphics::AssertCreated();

	Log::Write("begin shader loading for " + fileName);

	// �ffne Datei

	ifstream is(fileName, ios::binary);
	if(!is.good()) {
		// Werfe fataler Fehler
		throw FatalError("Cannot open shader file " + fileName);
	}
	streampos begin = is.tellg();
	is.seekg(0, ios::end);
	streampos end = is.tellg();
	is.seekg(0, ios::beg);
	*size = (uint32_t)(end - begin);

	// Lese Inhalt

	*buffer = new char[*size];
	is.read(*buffer, *size);
	is.close();

	Log::Write("shader loading terminated");
}

const string Graphics::TEXTURE_FILE = "resources/textures.png";
const string Graphics::TEXTURE_VERTEX_SHADER_FILE = "resources/texture_vs.cso";
const string Graphics::TEXTURE_PIXEL_SHADER_FILE = "resources/texture_ps.cso";
const string Graphics::TEXTURE_GEOMETRY_SHADER_FILE = "resources/texture_gs.cso";
const string Graphics::SHAPE_PIXEL_SHADER_FILE = "resources/shape_PS.cso";
const string Graphics::LINE_VERTEX_SHADER_FILE = "resources/line_vs.cso";
const string Graphics::LINE_GEOMETRY_SHADER_FILE = "resources/line_gs.cso";
const string Graphics::CIRCLE_VERTEX_SHADER_FILE = "resources/circle_vs.cso";
const string Graphics::CIRCLE_LINE_GEOMETRY_SHADER_FILE = "resources/circleLine_gs.cso";
const string Graphics::CIRCLE_FILL_GEOMETRY_SHADER_FILE = "resources/circleFill_gs.cso";
const string Graphics::RECT_VERTEX_SHADER_FILE = "resources/rect_vs.cso";
const string Graphics::RECT_LINE_GEOMETRY_SHADER_FILE = "resources/rectLine_gs.cso";
const string Graphics::RECT_FILL_GEOMETRY_SHADER_FILE = "resources/rectFill_gs.cso";
const string Graphics::FONT_NAME = "Consolas";
const float Graphics::FONT_SIZE = 15.0f;

bool Graphics::VSYNC = true;

bool Graphics::isCreated = false;

Graphics::GraphicsEventReciever Graphics::eventReciever = Graphics::GraphicsEventReciever();

ID3D11Device* Graphics::d3dDevice = nullptr;
ID3D11DeviceContext* Graphics::d3dDeviceContext = nullptr;
D3D_FEATURE_LEVEL Graphics::d3dFeatureLevel = (D3D_FEATURE_LEVEL)0;

ID3D11RenderTargetView* Graphics::d3dRenderTargetView = nullptr;

ID3D11Buffer* Graphics::transformBuffer = nullptr;
ID3D11SamplerState* Graphics::pixelatedSampler = nullptr;
ID3D11Buffer* Graphics::dataBuffer = nullptr;

ID3D11VertexShader* Graphics::texturedSquaresVertexShader = nullptr;
ID3D11GeometryShader* Graphics::texturedSquaresGeometryShader = nullptr;
ID3D11PixelShader* Graphics::texturedSquaresPixelShader = nullptr;
ID3D11InputLayout* Graphics::texturedSquaresInputLayout = nullptr;
ID3D11Texture2D* Graphics::squaresTexture = nullptr;
ID3D11ShaderResourceView* Graphics::squaresTextureView = nullptr;

ID3D11PixelShader* Graphics::shapesPixelShader = nullptr;

ID3D11VertexShader* Graphics::linesVertexShader = nullptr;
ID3D11GeometryShader* Graphics::linesGeometryShader = nullptr;
ID3D11InputLayout* Graphics::linesInputLayout = nullptr;

ID3D11VertexShader* Graphics::circlesVertexShader = nullptr;
ID3D11GeometryShader* Graphics::circlesLineGeometryShader = nullptr;
ID3D11GeometryShader* Graphics::circlesFillGeometryShader = nullptr;
ID3D11InputLayout* Graphics::circlesInputLayout = nullptr;

ID3D11VertexShader* Graphics::rectsVertexShader = nullptr;
ID3D11GeometryShader* Graphics::rectsLineGeometryShader = nullptr;
ID3D11GeometryShader* Graphics::rectsFillGeometryShader = nullptr;
ID3D11InputLayout* Graphics::rectsInputLayout = nullptr;

Graphics::TexturedSquare Graphics::texturedSquaresCache[];
Graphics::Line Graphics::linesCache[];
Graphics::Circle Graphics::circlesCache[];
Graphics::Circle Graphics::filledCirclesCache[];
Graphics::Rect Graphics::rectsCache[];
Graphics::Rect Graphics::filledRectsCache[];

uint32_t Graphics::texturedSquaresCacheSize = 0;
uint32_t Graphics::linesCacheSize = 0;
uint32_t Graphics::circlesCacheSize = 0;
uint32_t Graphics::filledCirclesCacheSize = 0;
uint32_t Graphics::rectsCacheSize = 0;
uint32_t Graphics::filledRectsCacheSize = 0;

IDXGISwapChain* Graphics::dxgiSwapChain = nullptr;

IWICImagingFactory* Graphics::wicFactory = nullptr;

ID2D1Factory* Graphics::d2dFactory = nullptr;
ID2D1RenderTarget* Graphics::d2dRenderTarget = nullptr;
ID2D1SolidColorBrush* Graphics::d2dFontBrush = nullptr;

IDWriteFactory* Graphics::dwFactory = nullptr;
IDWriteTextFormat* Graphics::dwTextFormat = nullptr;
vector<Graphics::TextBox> Graphics::textToDraw;

StringConverter Graphics::stringConverter;

}

#pragma once

#include <stdafx.h>

namespace System {

// Ereignisse werden direkt vom Erzeuger zum Empf�nger gesendet, sie werden nicht hierarchisch weitergeleitet

// Event-ID, wenn keine spezifische ID angegeben wird
// Event-ID's m�ssen eindeutig definiert werden
// Daher sind die ersten beiden stellen in hexadezimal eindeutig f�r jede Header-Datei, die letzen beiden eindeutig f�r jede ID.
const uint32_t ALL_EVENTS = 0xffffffff;

// Struktur f�r Ereignisparamter
class EventParam {
public:
	// Konstruktorersatz
	template<typename T> inline static EventParam Encode(T data) {
		// Umwandlung mittels Zeigern
		EventParam e;
		e.data = *((int64_t*)(&data));
		return e;
	}
	// Funktion f�r Datenzugriff
	template<typename T> inline T Decode() const {
		// Umwandlung mittels Zeigern
		return *((T*)(&this -> data));
	}
private:
	// 64 bit Speicher, kann in alle primitiven Typen und Zeiger umgewandelt werden
	int64_t data;
};

// Struktur f�r Ereignisse
struct Event {
	Event(uint32_t eventID, EventParam param1, EventParam param2) : eventID(eventID), param1(param1), param2(param2) {}

	// Art des Ereginis
	uint32_t eventID;
	// Datenspeicher f�r zus�tzliche Parameter
	EventParam param1;
	EventParam param2;
};

// Klasse, die Ereignisbenachrichtigungen empfangen kann
// Um die Funktion der Klasse verwenden zu k�nnen, muss die Klasse vererbt werden
class EventReciever {
public:
	// Damit EventSender zugriff auf OnEvent hat
	friend class EventSender;

	EventReciever() = default;
	~EventReciever() = default;

protected:
	virtual void OnEvent(Event const& e) = 0;
};

// Klasse, die Ereignisbenachrichtigungen an Abonnenten (EventReciever) versenden kann
// Um die Funktionen der Klasse verwenden zu k�nnen, muss du Klasse vererbt werden.
class EventSender {
public:
	EventSender() = default;
	virtual ~EventSender();

	// Registriert neuer Abonnent f�r spezifischer Eventtyp
	void Register(EventReciever* reciever, uint32_t eventID);
	// L�st Registrierung von Abonnent f�r spezifiescher Eventtyp auf
	void Unregister(EventReciever* reciever, uint32_t eventID);

protected:
	// Sendet Ereignis an alle Abonnenten
	void SendEvent(Event const& e);

private:
	map<uint32_t, vector<EventReciever*> > eventRecievers;
};

}
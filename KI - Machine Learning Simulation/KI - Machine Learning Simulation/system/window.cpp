#include <system/window.h>

#include <system/log.h>
#include <system/errors.h>

#include <resource.h>

namespace System {

void Window::Create(HINSTANCE instance, uint32_t width, uint32_t height, uint32_t minWidth, uint32_t minHeight) {
	if(Window::isCreated) {
		// Fenster bereits erstellt
		throw string("Attempting to create already created window singleton");
	}
	Window::isCreated = true;

	Log::Write("begin Window creation");

	Window::instance = instance;
	Window::width = width;
	Window::height = height;
	Window::minWidth = minWidth;
	Window::minHeight = minHeight;

	WNDCLASSEX windowClass;
	memset(&windowClass, 0, sizeof(WNDCLASSEX));

	HBRUSH backgroundBrush = CreateSolidBrush(RGB(4, 26, 43));

	windowClass.cbClsExtra = 0;
	windowClass.cbSize = sizeof(WNDCLASSEX);
	windowClass.cbWndExtra = 0;
	windowClass.hbrBackground = backgroundBrush;
	windowClass.hCursor = LoadCursor(NULL, IDC_APPSTARTING);
	windowClass.hIcon = LoadIcon(Window::instance, MAKEINTRESOURCE(IDI_ICON1));
	windowClass.hIconSm = NULL;
	windowClass.hInstance = Window::instance;
	windowClass.lpfnWndProc = Window::WndProc;
	windowClass.lpszClassName = "KI_MachineLearning_Main";
	windowClass.lpszMenuName = NULL;
	windowClass.style = CS_VREDRAW | CS_HREDRAW;

	if(RegisterClassEx(&windowClass) == 0) {
		throw string("Failed to register window class");
	}

	Window::hwnd = CreateWindow("KI_MachineLearning_Main", "KI - Machine Learning Simulation", WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT, width, height, NULL, NULL, Window::instance, NULL);

	if(Window::hwnd == 0) {
		throw string("Failed to create window");
	}

	// Passe Fenster an, so dass die Benutzerfläche den Anforderungen entspricht
	RECT size;
	GetWindowRect(Window::hwnd, &size);
	size.right = width + size.left;
	size.bottom = height + size.top;
	AdjustWindowRect(&size, WS_OVERLAPPEDWINDOW, false);
	MoveWindow(Window::hwnd, size.left, size.top, size.right - size.left, size.bottom - size.top, false);

	Log::Write("Window creation terminated");
}

void Window::Destroy() {
	if(!Window::isCreated) {
		// Fenster bereits zerstört
		throw string("Attempting to destroy already destroyed window singleton");
	}
	Window::isCreated = false;

	Log::Write("begin Window destruction");

	if(Window::hwnd != NULL) {
		DestroyWindow(Window::hwnd);
		Window::hwnd = NULL;
	}

	Log::Write("Window destruction terminated");
}

LRESULT CALLBACK Window::WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) {
	// Starte eigene Ereginisverarbeitung oder Standardereignisverarbeitung
	if(Window::hwnd == hWnd) {
		if(Window::OnWindowsEvent(msg, wParam, lParam)) {
			return 0;
		}
	}
	return DefWindowProc(hWnd, msg, wParam, lParam);
}

bool Window::OnWindowsEvent(UINT msg, WPARAM wParam, LPARAM lParam) {
	// Unterscheide verschiedene Windows-Ereignisse und melde Ereignis an potenzielle Abonnenten
	if(msg == WM_SIZE) {
		if(wParam == SIZE_MAXIMIZED) {
			Window::eventSender.SendEvent(Event(WINDOW_MAXIMIZE, EventParam(), EventParam()));
		} else if(wParam == SIZE_MINIMIZED) {
			Window::eventSender.SendEvent(Event(WINDOW_MINIMIZE, EventParam(), EventParam()));
		} else if(wParam == SIZE_RESTORED) {
			Window::eventSender.SendEvent(Event(WINDOW_RESTORE, EventParam(), EventParam()));
		}
		RECT size;
		GetClientRect(Window::hwnd, &size);

		Window::eventSender.SendEvent(Event(WINDOW_SIZE, EventParam::Encode<uint32_t>((uint32_t)(size.right - size.left)), EventParam::Encode<uint32_t>((uint32_t)size.bottom - size.top)));
		Window::width = size.right - size.left;
		Window::height = size.bottom - size.top;
		return true;
	} else if(msg == WM_SIZING) {
		PRECT scaleRect = (PRECT)lParam;
		if(scaleRect -> right - scaleRect -> left < (int32_t)Window::minWidth) {
			scaleRect -> right = scaleRect -> left + (int32_t)Window::minWidth;
		}
		if(scaleRect -> bottom - scaleRect -> top < (int32_t)Window::minHeight) {
			scaleRect -> bottom = scaleRect -> top + (int32_t)Window::minHeight;
		}
		return true;
	} else if(msg == WM_MOVING) {
		return true;
	} else if(msg == WM_MOVE) {
		Window::eventSender.SendEvent(Event(WINDOW_POSITION, EventParam::Encode<int32_t>(LOWORD(wParam)), EventParam::Encode<int32_t>(HIWORD(wParam))));
		return true;
	} else if(msg == WM_CLOSE) {
		Window::eventSender.SendEvent(Event(WINDOW_CLOSE, EventParam(), EventParam()));
	} else if(msg == WM_DESTROY) {
		Window::eventSender.SendEvent(Event(WINDOW_DESTROY, EventParam(), EventParam()));

		Log::Write("Window recieved WM_DESTROY message. Closing window.");
		// zerstöre automatisch das Fenster wenn nicht bereits geschehen
		if(Window::isCreated) {
			Window::Destroy();
		}
		return true;
	} else if(msg == WM_SETFOCUS) {
		Window::eventSender.SendEvent(Event(WINDOW_SETFOCUS, EventParam(), EventParam()));
		Window::hasFocus = true;
		return true;
	} else if(msg == WM_KILLFOCUS) {
		Window::eventSender.SendEvent(Event(WINDOW_KILLFOCUS, EventParam(), EventParam()));
		Window::hasFocus = false;
		return true;
	} else if(msg == WM_MOUSEWHEEL) {
		Window::eventSender.SendEvent(Event(WINDOW_MOUSEWHEEL, EventParam::Encode<int16_t>(GET_WHEEL_DELTA_WPARAM(wParam)), EventParam()));
		return true;
	} else if(msg == WM_PAINT) {
		//Window::eventSender.SendEvent(Event(WINDOW_DRAW, EventParam(), EventParam()));
		return true;
	} else if(msg == WM_MOUSEHOVER) {
		Window::eventSender.SendEvent(Event(WINDOW_HOVER, EventParam(), EventParam()));
		return true;
	} else if(msg == WM_MOUSELEAVE) {
		Window::eventSender.SendEvent(Event(WINDOW_LEAVE, EventParam(), EventParam()));
		return true;
	} else if(msg == WM_CHAR) {
		Window::eventSender.SendEvent(Event(WINDOW_CHAR, EventParam::Encode<char>((char)wParam), EventParam()));
	}
	return false;
}

void Window::AssertCreated() {
	if(!Window::isCreated) {
		// Werfe nicht fataler Fehler
		throw Error("Cannot access member functions of Window while not created");
	}
}

bool Window::isCreated = false;

HINSTANCE Window::instance = NULL;
uint32_t Window::width = 0;
uint32_t Window::height = 0;
uint32_t Window::minWidth = 0;
uint32_t Window::minHeight = 0;
HWND Window::hwnd = NULL;
bool Window::hasFocus = false;

Window::WindowEventSender Window::eventSender = Window::WindowEventSender();

}
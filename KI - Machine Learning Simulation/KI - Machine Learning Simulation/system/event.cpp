#include <system/event.h>

namespace System {

EventSender::~EventSender() {
	this -> eventRecievers.clear();
}

void EventSender::Register(EventReciever* reciever, uint32_t eventID) {
	if(this -> eventRecievers.find(eventID) != this -> eventRecievers.end()) {
		if(find(this -> eventRecievers[eventID].begin(), this -> eventRecievers[eventID].end(), reciever) != this -> eventRecievers[eventID].end()) {
			// Bereits hinzugef�gt
			return;
		}
	} else {
		// Vector f�r eventID muss erstellt werden
		this -> eventRecievers.insert(pair<uint32_t, vector<EventReciever*> >(eventID, vector<EventReciever*>()));
	}

	// Empf�nger hinzuf�gen
	this -> eventRecievers[eventID].push_back(reciever);
}

void EventSender::Unregister(EventReciever* reciever, uint32_t eventID) {
	if(this -> eventRecievers.find(eventID) != this -> eventRecievers.end()) {
		vector<EventReciever*>::iterator iterator = find(this -> eventRecievers[eventID].begin(), this -> eventRecievers[eventID].end(), reciever);
		if(iterator != this -> eventRecievers[eventID].end()) {
			// Empf�nger ist registriert
			// Empf�nger entfernen
			this -> eventRecievers[eventID].erase(iterator);
		}
	}
}

void EventSender::SendEvent(Event const& e) {
	if(this -> eventRecievers.find(e.eventID) != this -> eventRecievers.end()) {
		for(uint32_t i = 0; i < this -> eventRecievers[e.eventID].size(); i++) {
			this -> eventRecievers[e.eventID][i] -> OnEvent(e);
		}
	}

	if(this -> eventRecievers.find(ALL_EVENTS) != this -> eventRecievers.end()) {
		for(uint32_t i = 0; i < this -> eventRecievers[ALL_EVENTS].size(); i++) {
			this -> eventRecievers[ALL_EVENTS][i] -> OnEvent(e);
		}
	}
}

}
#pragma once

#include <stdafx.h>

#include <system/window.h>
#include <system/event.h>

namespace System {

// Klasse f�r Grafiksystem
// Singleton
// Stellt Schnittstelle zwischen Direct3D, Direct2D, DirectWrite und Projektsoftware her
class Graphics {
public:
	Graphics() = delete; // Verhindere Konstruktion
	~Graphics() = default;

	// Gibt Status der Grafikklasse zur�ck
	inline static bool IsCreated() {
		return Graphics::isCreated;
	}
	// Erstellt Grafiksystem (pseudo-Konstruktor)
	static void Create();
	// Zerst�rt Grafiksystem (pseudo-Destruktor)
	static void Destroy();

	// Leert Fensterinhalt
	inline static void Clear() {
		Graphics::AssertCreated();
		float clearColor[4] = { 0.016f, 0.102f, 0.168f };
		Graphics::d3dDeviceContext -> ClearRenderTargetView(Graphics::d3dRenderTargetView, clearColor);
	}
	// Wechselt zwischen Vorder- und Hinterbuffer
	inline static void SwapBuffers() {
		Graphics::AssertCreated();
		Graphics::dxgiSwapChain -> Present((Graphics::VSYNC) ? 1 : 0, 0);
	}

	const static uint32_t MAX_SQUARE_LIST_SIZE = 128;
	const static uint32_t MAX_LINE_LIST_SIZE = 72;
	const static uint32_t MAX_CIRCLE_LIST_SIZE = 64;
	const static uint32_t MAX_RECT_LIST_SIZE = 72;
	const static string TEXTURE_FILE;
	const static string TEXTURE_VERTEX_SHADER_FILE;
	const static string TEXTURE_PIXEL_SHADER_FILE;
	const static string TEXTURE_GEOMETRY_SHADER_FILE;
	const static string SHAPE_PIXEL_SHADER_FILE;
	const static string LINE_VERTEX_SHADER_FILE;
	const static string LINE_GEOMETRY_SHADER_FILE;
	const static string CIRCLE_VERTEX_SHADER_FILE;
	const static string CIRCLE_LINE_GEOMETRY_SHADER_FILE;
	const static string CIRCLE_FILL_GEOMETRY_SHADER_FILE;
	const static string RECT_VERTEX_SHADER_FILE;
	const static string RECT_LINE_GEOMETRY_SHADER_FILE;
	const static string RECT_FILL_GEOMETRY_SHADER_FILE;
	const static string FONT_NAME;
	const static float FONT_SIZE; // in pt

	// Datenstruktur f�r texturierte Quadrate
	// Entspricht Eingabeformat f�r Vertex-Shader
	struct TexturedSquare {
		int32_t xOffset;
		int32_t yOffset;
		uint32_t scale;
		uint32_t textureIndex;
	};

	// Datenstruktur f�r Linien
	// Entspricht Eingabeformat f�r Vertex-Shader
	struct Line {
		int32_t x1;
		int32_t y1;
		int32_t x2;
		int32_t y2;
		struct {
			float red;
			float green;
			float blue;
		} color;
	};

	// Datenstruktur f�r Kreise
	// Entspricht Eingabeformat f�r Vertex-Shader
	struct Circle {
		int32_t x;
		int32_t y;
		uint32_t radius;
		uint32_t startAngle;
		uint32_t sectorAngle;
		struct {
			float red;
			float green;
			float blue;
		} color;
	};

	// Datenstruktur f�r Rechtecke
	// Entspricht Eingabeformat f�r Vertex-Shader
	struct Rect {
		int32_t x;
		int32_t y;
		uint32_t sizeX;
		uint32_t sizeY;
		struct {
			float red;
			float green;
			float blue;
		} color;
	};

	// F�gt jeweilige Objekte der Warteschlange hinzu
	// Ist die Warteschlange voll, wird sie automatisch gezeichnet
	static void DrawTexturedSquares(TexturedSquare* texturedSquares, uint32_t squareListSize);
	static void DrawLines(Line* lines, uint32_t lineListSize);
	static void DrawCircles(Circle* circles, uint32_t circleListSize);
	static void FillCircles(Circle* circles, uint32_t circleListSize);
	static void DrawRects(Rect* rects, uint32_t rectListSize);
	static void FillRects(Rect* rects, uint32_t rectListSize);
	// Erzwingt das Zeichnen der Warteschlangen
	static void FlushTexturedSquares();
	static void FlushLines();
	static void FlushCircles();
	static void FlushFilledCircles();
	static void FlushRects();
	static void FlushFilledRects();

	// F�ge Text der Warteschlange hinzu (Zeichnen mit DrawTextQueue())
	static void AddTextToQueue(wstring text, uint32_t x, uint32_t y, float red, float green, float blue);
	// Zeichne ganze Textwarteschlange mit Direct2D
	static void DrawTextQueue();

	static bool VSYNC; // Erm�glicht das Ein- und Ausschalten von Vertikaler Synchronisierung

private:
	// Ereignisempf�ngerklasse
	class GraphicsEventReciever : public EventReciever {
	public:
		inline void OnEvent(Event const& e) {
			if(e.eventID == Window::WINDOW_SIZE) {
				// Grafiksystem der neuen gr�sse anpassen
				Graphics::Resize(e.param1.Decode<uint32_t>(), e.param2.Decode<uint32_t>());
			}
		}
	};

	// Struktur um f�r Textboxencache
	struct TextBox {
		wstring text;
		uint32_t x;
		uint32_t y;
		struct {
			float red;
			float green;
			float blue;
		} color;
	};

	// Hilfefunktion, um sicherzugehen, dass das Fenster bereits erstellt wurde
	// (Create und Destroy verwenden eigene Implementationen)
	static void AssertCreated();

	static bool isCreated;

	// Passt Grafiksystem der Fenstergr�sse an
	static void Resize(uint32_t width, uint32_t height);

	// Hilfefunktion, um Texturen als Shaderresourcen zu laden (To-Texture-Rendering wird nicht ben�tigt)
	static void LoadTexture(string fileName, ID3D11Texture2D** texture, ID3D11ShaderResourceView** shaderResourceView);

	// Hilfefunktion, um Shaderdateien zu laden
	static void LoadShader(string fileName, char** buffer, uint32_t* size);

	// Ereignisempf�nger
	static GraphicsEventReciever eventReciever;

	// Direct3D-Objekte (global)
	static ID3D11Device* d3dDevice;
	static ID3D11DeviceContext* d3dDeviceContext;
	static D3D_FEATURE_LEVEL d3dFeatureLevel;

	static ID3D11RenderTargetView* d3dRenderTargetView;

	// Direct3D-Objekte (Szene)
	static ID3D11Buffer* transformBuffer;
	static ID3D11SamplerState* pixelatedSampler;
	static ID3D11Buffer* dataBuffer;

	static ID3D11VertexShader* texturedSquaresVertexShader;
	static ID3D11GeometryShader* texturedSquaresGeometryShader;
	static ID3D11PixelShader* texturedSquaresPixelShader;
	static ID3D11InputLayout* texturedSquaresInputLayout;
	static ID3D11Texture2D* squaresTexture;
	static ID3D11ShaderResourceView* squaresTextureView;

	static ID3D11PixelShader* shapesPixelShader;

	static ID3D11VertexShader* linesVertexShader;
	static ID3D11GeometryShader* linesGeometryShader;
	static ID3D11InputLayout* linesInputLayout;

	static ID3D11VertexShader* circlesVertexShader;
	static ID3D11GeometryShader* circlesLineGeometryShader;
	static ID3D11GeometryShader* circlesFillGeometryShader;
	static ID3D11InputLayout* circlesInputLayout;

	static ID3D11VertexShader* rectsVertexShader;
	static ID3D11GeometryShader* rectsLineGeometryShader;
	static ID3D11GeometryShader* rectsFillGeometryShader;
	static ID3D11InputLayout* rectsInputLayout;

	static TexturedSquare texturedSquaresCache[MAX_SQUARE_LIST_SIZE];
	static Line linesCache[MAX_LINE_LIST_SIZE];
	static Circle circlesCache[MAX_CIRCLE_LIST_SIZE];
	static Circle filledCirclesCache[MAX_CIRCLE_LIST_SIZE];
	static Rect rectsCache[MAX_RECT_LIST_SIZE];
	static Rect filledRectsCache[MAX_RECT_LIST_SIZE];

	static uint32_t texturedSquaresCacheSize;
	static uint32_t linesCacheSize;
	static uint32_t circlesCacheSize;
	static uint32_t filledCirclesCacheSize;
	static uint32_t rectsCacheSize;
	static uint32_t filledRectsCacheSize;

	// DXGI-Objekte
	static IDXGISwapChain* dxgiSwapChain;

	// WIC-Objekte
	static IWICImagingFactory* wicFactory;

	// Direct2D-Objekte
	static ID2D1Factory* d2dFactory;
	static ID2D1RenderTarget* d2dRenderTarget;
	static ID2D1SolidColorBrush* d2dFontBrush;

	// DirectWrite-Objekte
	static IDWriteFactory* dwFactory;
	static IDWriteTextFormat* dwTextFormat;
	static vector<TextBox> textToDraw;

	// Stringumwandler
	static StringConverter stringConverter;

public:
	friend GraphicsEventReciever;
};

}
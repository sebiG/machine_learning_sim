#include <stdafx.h>

#include <system/errors.h>
#include <system/program.h>
#include <system/log.h>

using namespace System;

// Haupteinstiegspunkt
// prevInstance und cmdShow werden nicht verwendet
int WINAPI WinMain(HINSTANCE instance, HINSTANCE prevInstance, LPSTR cmdLine, int cmdShow) {
	// Handler f�r fatale Fehler
	// Wird ein string geworfen, kann dieser nicht im Programm abgefangen werden und wird hier als fataler Fehler abgefangen.
	try {
		Log::Verbose = true;

		Program::Create(instance);

		Program::Run();

		Program::Destroy();
	} catch(Error e) {
		string errorMessage = "Error developed into Fatal Error: " + e.What();
		if(Log::IsCreated()) {
			// Schreibe in Protokolldatei falls ge�ffnet
			Log::WriteImportant(errorMessage);
		} else {
			// Sonst schreibe in Konsole
			cout << errorMessage << endl;
		}
		// Schreibe auch in ein Nachrichtenfenster
		MessageBox(NULL, errorMessage.c_str(), "Error", MB_ICONERROR);
	}  catch(FatalError e) {
		string errorMessage = "Fatal Error caught: " + e.What();
		if(Log::IsCreated()) {
			// Schreibe in Protokolldatei falls ge�ffnet
			Log::WriteImportant(errorMessage);
		} else {
			// Sonst schreibe in Konsole
			cout << errorMessage << endl;
		}
		// Schreibe auch in ein Nachrichtenfenster
		MessageBox(NULL, errorMessage.c_str(), "Error", MB_ICONERROR);
	}
	return 0;
}